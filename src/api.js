const url = "https://allabay.com/api";

async function apiCall(method, params) {
  params["auth"] = window.location.search;
  return fetch(`${url}?method=${method}`, {
    method: "POST",
    body: JSON.stringify(params),
  })
    .then((response) => {
      if (response.ok) {
        const contentType = response.headers.get("Content-Type") || "";

        if (contentType.includes("application/json")) {
          return response.json().catch((error) => {
            return Promise.reject("500");
          });
        }
      }
    })
    .catch((error) => {
      return Promise.reject("404");
    });
}

export default apiCall;
