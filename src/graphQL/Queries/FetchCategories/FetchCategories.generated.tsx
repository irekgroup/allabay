import * as Types from '../../../graphql-types';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
const defaultOptions = {} as const;
export type Fetch_CategoriesQueryVariables = Types.Exact<{ [key: string]: never; }>;


export type Fetch_CategoriesQuery = { __typename?: 'Query', categories?: Array<{ __typename?: 'CategoryType', id: string, name: string, subcategorySet: Array<{ __typename?: 'SubcategoryType', id: string, name: string }> } | null> | null };


export const Fetch_CategoriesDocument = gql`
    query fetch_categories {
  categories {
    id
    name
    subcategorySet {
      id
      name
    }
  }
}
    `;

/**
 * __useFetch_CategoriesQuery__
 *
 * To run a query within a React component, call `useFetch_CategoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetch_CategoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetch_CategoriesQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetch_CategoriesQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<Fetch_CategoriesQuery, Fetch_CategoriesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<Fetch_CategoriesQuery, Fetch_CategoriesQueryVariables>(Fetch_CategoriesDocument, options);
      }
export function useFetch_CategoriesLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<Fetch_CategoriesQuery, Fetch_CategoriesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<Fetch_CategoriesQuery, Fetch_CategoriesQueryVariables>(Fetch_CategoriesDocument, options);
        }
export type Fetch_CategoriesQueryHookResult = ReturnType<typeof useFetch_CategoriesQuery>;
export type Fetch_CategoriesLazyQueryHookResult = ReturnType<typeof useFetch_CategoriesLazyQuery>;
export type Fetch_CategoriesQueryResult = ApolloReactCommon.QueryResult<Fetch_CategoriesQuery, Fetch_CategoriesQueryVariables>;