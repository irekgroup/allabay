import * as Types from '../../../graphql-types';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
const defaultOptions = {} as const;
export type Fetch_KindsQueryVariables = Types.Exact<{
  parent: Types.Scalars['String'];
}>;


export type Fetch_KindsQuery = { __typename?: 'Query', kinds?: Array<{ __typename?: 'KindType', id: string, name: string, typeSet: Array<{ __typename?: 'TypeType', id: string, name: string }> } | null> | null };


export const Fetch_KindsDocument = gql`
    query fetch_kinds($parent: String!) {
  kinds(parent: $parent) {
    id
    name
    typeSet {
      id
      name
    }
  }
}
    `;

/**
 * __useFetch_KindsQuery__
 *
 * To run a query within a React component, call `useFetch_KindsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetch_KindsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetch_KindsQuery({
 *   variables: {
 *      parent: // value for 'parent'
 *   },
 * });
 */
export function useFetch_KindsQuery(baseOptions: ApolloReactHooks.QueryHookOptions<Fetch_KindsQuery, Fetch_KindsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<Fetch_KindsQuery, Fetch_KindsQueryVariables>(Fetch_KindsDocument, options);
      }
export function useFetch_KindsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<Fetch_KindsQuery, Fetch_KindsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<Fetch_KindsQuery, Fetch_KindsQueryVariables>(Fetch_KindsDocument, options);
        }
export type Fetch_KindsQueryHookResult = ReturnType<typeof useFetch_KindsQuery>;
export type Fetch_KindsLazyQueryHookResult = ReturnType<typeof useFetch_KindsLazyQuery>;
export type Fetch_KindsQueryResult = ApolloReactCommon.QueryResult<Fetch_KindsQuery, Fetch_KindsQueryVariables>;