import * as Types from '../../../graphql-types';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
const defaultOptions = {} as const;
export type ListTooltipQueryVariables = Types.Exact<{
  input: Types.Scalars['Int'];
}>;


export type ListTooltipQuery = { __typename?: 'Query', listTooltip?: Array<{ __typename?: 'TooltipType', id: string, idTooltip: number, textTooltip: string, isDelete: boolean }> | null };


export const ListTooltipDocument = gql`
    query listTooltip($input: Int!) {
  listTooltip(idMessage: $input) {
    id
    idTooltip
    textTooltip
    isDelete
  }
}
    `;

/**
 * __useListTooltipQuery__
 *
 * To run a query within a React component, call `useListTooltipQuery` and pass it any options that fit your needs.
 * When your component renders, `useListTooltipQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useListTooltipQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useListTooltipQuery(baseOptions: ApolloReactHooks.QueryHookOptions<ListTooltipQuery, ListTooltipQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<ListTooltipQuery, ListTooltipQueryVariables>(ListTooltipDocument, options);
      }
export function useListTooltipLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ListTooltipQuery, ListTooltipQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<ListTooltipQuery, ListTooltipQueryVariables>(ListTooltipDocument, options);
        }
export type ListTooltipQueryHookResult = ReturnType<typeof useListTooltipQuery>;
export type ListTooltipLazyQueryHookResult = ReturnType<typeof useListTooltipLazyQuery>;
export type ListTooltipQueryResult = ApolloReactCommon.QueryResult<ListTooltipQuery, ListTooltipQueryVariables>;