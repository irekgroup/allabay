import * as Types from '../../../graphql-types';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
const defaultOptions = {} as const;
export type FetchChatsQueryVariables = Types.Exact<{ [key: string]: never; }>;


export type FetchChatsQuery = { __typename?: 'Query', listChats?: Array<{ __typename?: 'ChatType', id: string, nameChat: string, isPinned: boolean, createAt: any, statusChat: Types.ChatMessageStatusChat, user1: { __typename?: 'UserType', id: string, firstName: string, lastName: string }, user2?: { __typename?: 'UserType', id: string, firstName: string, lastName: string } | null, lastMessages?: { __typename?: 'MessageQueryType', textMessage: string, idForTooltip: number, createAt: any, isRead: boolean, chatMessages: { __typename?: 'ChatType', id: string }, senderUser: { __typename?: 'UserType', id: string } } | null }> | null };


export const FetchChatsDocument = gql`
    query fetchChats {
  listChats {
    id
    nameChat
    user1 {
      id
      firstName
      lastName
    }
    user2 {
      id
      firstName
      lastName
    }
    isPinned
    createAt
    statusChat
    lastMessages {
      textMessage
      idForTooltip
      chatMessages {
        id
      }
      senderUser {
        id
      }
      createAt
      isRead
    }
  }
}
    `;

/**
 * __useFetchChatsQuery__
 *
 * To run a query within a React component, call `useFetchChatsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchChatsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchChatsQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchChatsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FetchChatsQuery, FetchChatsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<FetchChatsQuery, FetchChatsQueryVariables>(FetchChatsDocument, options);
      }
export function useFetchChatsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FetchChatsQuery, FetchChatsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<FetchChatsQuery, FetchChatsQueryVariables>(FetchChatsDocument, options);
        }
export type FetchChatsQueryHookResult = ReturnType<typeof useFetchChatsQuery>;
export type FetchChatsLazyQueryHookResult = ReturnType<typeof useFetchChatsLazyQuery>;
export type FetchChatsQueryResult = ApolloReactCommon.QueryResult<FetchChatsQuery, FetchChatsQueryVariables>;