import * as Types from '../../../graphql-types';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
const defaultOptions = {} as const;
export type GetChatQueryVariables = Types.Exact<{
  id: Types.Scalars['String'];
  limit: Types.Scalars['String'];
}>;


export type GetChatQuery = { __typename?: 'Query', chat?: Array<{ __typename?: 'MessageQueryType', id: string, textMessage: string, isRead: boolean, isDelete: boolean, idForTooltip: number, createAt: any, senderUser: { __typename?: 'UserType', id: string }, chatMessages: { __typename?: 'ChatType', id: string, nameChat: string, user1: { __typename?: 'UserType', id: string }, user2?: { __typename?: 'UserType', id: string } | null } }> | null };


export const GetChatDocument = gql`
    query getChat($id: String!, $limit: String!) {
  chat(idChat: $id, limit: $limit) {
    id
    senderUser {
      id
    }
    textMessage
    isRead
    isDelete
    idForTooltip
    chatMessages {
      id
      nameChat
      user1 {
        id
      }
      user2 {
        id
      }
    }
    createAt
  }
}
    `;

/**
 * __useGetChatQuery__
 *
 * To run a query within a React component, call `useGetChatQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetChatQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetChatQuery({
 *   variables: {
 *      id: // value for 'id'
 *      limit: // value for 'limit'
 *   },
 * });
 */
export function useGetChatQuery(baseOptions: ApolloReactHooks.QueryHookOptions<GetChatQuery, GetChatQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<GetChatQuery, GetChatQueryVariables>(GetChatDocument, options);
      }
export function useGetChatLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetChatQuery, GetChatQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<GetChatQuery, GetChatQueryVariables>(GetChatDocument, options);
        }
export type GetChatQueryHookResult = ReturnType<typeof useGetChatQuery>;
export type GetChatLazyQueryHookResult = ReturnType<typeof useGetChatLazyQuery>;
export type GetChatQueryResult = ApolloReactCommon.QueryResult<GetChatQuery, GetChatQueryVariables>;