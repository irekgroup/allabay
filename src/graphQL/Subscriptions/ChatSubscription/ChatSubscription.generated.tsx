import * as Types from '../../../graphql-types';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
const defaultOptions = {} as const;
export type ChatSubSubscriptionVariables = Types.Exact<{ [key: string]: never; }>;


export type ChatSubSubscription = { __typename?: 'Subscription', notifyMessage?: { __typename?: 'NotifyType', message?: { __typename?: 'MessageQueryType', id: string, textMessage: string, imageMassage: string, createAt: any, isRead: boolean, senderUser: { __typename?: 'UserType', id: string }, chatMessages: { __typename?: 'ChatType', id: string, nameChat: string, user1: { __typename?: 'UserType', id: string, lastName: string, firstName: string }, user2?: { __typename?: 'UserType', id: string, lastName: string, firstName: string } | null } } | null, chat?: { __typename?: 'ChatType', id: string } | null, bools?: { __typename?: 'BoolType', isMessage?: boolean | null, isDelete?: boolean | null, isRead?: boolean | null } | null } | null };


export const ChatSubDocument = gql`
    subscription chatSub {
  notifyMessage {
    message {
      id
      textMessage
      imageMassage
      createAt
      isRead
      senderUser {
        id
      }
      chatMessages {
        id
        nameChat
        user1 {
          id
          lastName
          firstName
        }
        user2 {
          id
          lastName
          firstName
        }
      }
    }
    chat {
      id
    }
    bools {
      isMessage
      isDelete
      isRead
    }
  }
}
    `;

/**
 * __useChatSubSubscription__
 *
 * To run a query within a React component, call `useChatSubSubscription` and pass it any options that fit your needs.
 * When your component renders, `useChatSubSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useChatSubSubscription({
 *   variables: {
 *   },
 * });
 */
export function useChatSubSubscription(baseOptions?: ApolloReactHooks.SubscriptionHookOptions<ChatSubSubscription, ChatSubSubscriptionVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useSubscription<ChatSubSubscription, ChatSubSubscriptionVariables>(ChatSubDocument, options);
      }
export type ChatSubSubscriptionHookResult = ReturnType<typeof useChatSubSubscription>;
export type ChatSubSubscriptionResult = ApolloReactCommon.SubscriptionResult<ChatSubSubscription>;