import * as Types from '../../../graphql-types';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
const defaultOptions = {} as const;
export type VerificationUserMutationVariables = Types.Exact<{
  phone?: Types.InputMaybe<Types.Scalars['String']>;
}>;


export type VerificationUserMutation = { __typename?: 'Mutation', verificationUser?: { __typename?: 'VerificationUser', ok?: boolean | null } | null };


export const VerificationUserDocument = gql`
    mutation verificationUser($phone: String) {
  verificationUser(input: {phone: $phone}) {
    ok
  }
}
    `;
export type VerificationUserMutationFn = ApolloReactCommon.MutationFunction<VerificationUserMutation, VerificationUserMutationVariables>;

/**
 * __useVerificationUserMutation__
 *
 * To run a mutation, you first call `useVerificationUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useVerificationUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [verificationUserMutation, { data, loading, error }] = useVerificationUserMutation({
 *   variables: {
 *      phone: // value for 'phone'
 *   },
 * });
 */
export function useVerificationUserMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<VerificationUserMutation, VerificationUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<VerificationUserMutation, VerificationUserMutationVariables>(VerificationUserDocument, options);
      }
export type VerificationUserMutationHookResult = ReturnType<typeof useVerificationUserMutation>;
export type VerificationUserMutationResult = ApolloReactCommon.MutationResult<VerificationUserMutation>;
export type VerificationUserMutationOptions = ApolloReactCommon.BaseMutationOptions<VerificationUserMutation, VerificationUserMutationVariables>;