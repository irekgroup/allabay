import * as Types from '../../../graphql-types';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
const defaultOptions = {} as const;
export type Read_ChatMutationVariables = Types.Exact<{
  idChat: Types.Scalars['String'];
}>;


export type Read_ChatMutation = { __typename?: 'Mutation', readChat?: { __typename?: 'ReadChat', ok?: boolean | null } | null };


export const Read_ChatDocument = gql`
    mutation read_chat($idChat: String!) {
  readChat(input: {idChat: $idChat}) {
    ok
  }
}
    `;
export type Read_ChatMutationFn = ApolloReactCommon.MutationFunction<Read_ChatMutation, Read_ChatMutationVariables>;

/**
 * __useRead_ChatMutation__
 *
 * To run a mutation, you first call `useRead_ChatMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRead_ChatMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [readChatMutation, { data, loading, error }] = useRead_ChatMutation({
 *   variables: {
 *      idChat: // value for 'idChat'
 *   },
 * });
 */
export function useRead_ChatMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<Read_ChatMutation, Read_ChatMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<Read_ChatMutation, Read_ChatMutationVariables>(Read_ChatDocument, options);
      }
export type Read_ChatMutationHookResult = ReturnType<typeof useRead_ChatMutation>;
export type Read_ChatMutationResult = ApolloReactCommon.MutationResult<Read_ChatMutation>;
export type Read_ChatMutationOptions = ApolloReactCommon.BaseMutationOptions<Read_ChatMutation, Read_ChatMutationVariables>;