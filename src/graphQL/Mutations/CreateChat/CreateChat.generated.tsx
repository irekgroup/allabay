import * as Types from '../../../graphql-types';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
const defaultOptions = {} as const;
export type CreateChatMutationVariables = Types.Exact<{
  input: Types.ChatCreateInput;
}>;


export type CreateChatMutation = { __typename?: 'Mutation', createChat?: { __typename?: 'CreateChat', ok?: boolean | null, chat?: { __typename?: 'ChatType', id: string, nameChat: string, user1: { __typename?: 'UserType', id: string, firstName: string, lastName: string }, user2?: { __typename?: 'UserType', id: string, firstName: string, lastName: string } | null } | null } | null };


export const CreateChatDocument = gql`
    mutation createChat($input: ChatCreateInput!) {
  createChat(input: $input) {
    chat {
      id
      nameChat
      user1 {
        id
        firstName
        lastName
      }
      user2 {
        id
        firstName
        lastName
      }
    }
    ok
  }
}
    `;
export type CreateChatMutationFn = ApolloReactCommon.MutationFunction<CreateChatMutation, CreateChatMutationVariables>;

/**
 * __useCreateChatMutation__
 *
 * To run a mutation, you first call `useCreateChatMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateChatMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createChatMutation, { data, loading, error }] = useCreateChatMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateChatMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreateChatMutation, CreateChatMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<CreateChatMutation, CreateChatMutationVariables>(CreateChatDocument, options);
      }
export type CreateChatMutationHookResult = ReturnType<typeof useCreateChatMutation>;
export type CreateChatMutationResult = ApolloReactCommon.MutationResult<CreateChatMutation>;
export type CreateChatMutationOptions = ApolloReactCommon.BaseMutationOptions<CreateChatMutation, CreateChatMutationVariables>;