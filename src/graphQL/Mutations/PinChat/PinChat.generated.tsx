import * as Types from '../../../graphql-types';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
const defaultOptions = {} as const;
export type PinChatMutationVariables = Types.Exact<{
  input: Types.ChatPinnedInput;
}>;


export type PinChatMutation = { __typename?: 'Mutation', pinChat?: { __typename?: 'PinnedChat', ok?: boolean | null } | null };


export const PinChatDocument = gql`
    mutation pinChat($input: ChatPinnedInput!) {
  pinChat(input: $input) {
    ok
  }
}
    `;
export type PinChatMutationFn = ApolloReactCommon.MutationFunction<PinChatMutation, PinChatMutationVariables>;

/**
 * __usePinChatMutation__
 *
 * To run a mutation, you first call `usePinChatMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `usePinChatMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [pinChatMutation, { data, loading, error }] = usePinChatMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function usePinChatMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<PinChatMutation, PinChatMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<PinChatMutation, PinChatMutationVariables>(PinChatDocument, options);
      }
export type PinChatMutationHookResult = ReturnType<typeof usePinChatMutation>;
export type PinChatMutationResult = ApolloReactCommon.MutationResult<PinChatMutation>;
export type PinChatMutationOptions = ApolloReactCommon.BaseMutationOptions<PinChatMutation, PinChatMutationVariables>;