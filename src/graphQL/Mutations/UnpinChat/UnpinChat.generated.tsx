import * as Types from '../../../graphql-types';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
const defaultOptions = {} as const;
export type UnpinChatMutationVariables = Types.Exact<{
  input: Types.ChatPinnedInput;
}>;


export type UnpinChatMutation = { __typename?: 'Mutation', unPinChat?: { __typename?: 'UnPinnedChat', ok?: boolean | null } | null };


export const UnpinChatDocument = gql`
    mutation unpinChat($input: ChatPinnedInput!) {
  unPinChat(input: $input) {
    ok
  }
}
    `;
export type UnpinChatMutationFn = ApolloReactCommon.MutationFunction<UnpinChatMutation, UnpinChatMutationVariables>;

/**
 * __useUnpinChatMutation__
 *
 * To run a mutation, you first call `useUnpinChatMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUnpinChatMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [unpinChatMutation, { data, loading, error }] = useUnpinChatMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUnpinChatMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UnpinChatMutation, UnpinChatMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<UnpinChatMutation, UnpinChatMutationVariables>(UnpinChatDocument, options);
      }
export type UnpinChatMutationHookResult = ReturnType<typeof useUnpinChatMutation>;
export type UnpinChatMutationResult = ApolloReactCommon.MutationResult<UnpinChatMutation>;
export type UnpinChatMutationOptions = ApolloReactCommon.BaseMutationOptions<UnpinChatMutation, UnpinChatMutationVariables>;