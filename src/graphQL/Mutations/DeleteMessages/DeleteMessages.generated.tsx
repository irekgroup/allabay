import * as Types from '../../../graphql-types';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
const defaultOptions = {} as const;
export type DeleteMessagesMutationVariables = Types.Exact<{
  input: Types.MessageDeleteInput;
}>;


export type DeleteMessagesMutation = { __typename?: 'Mutation', deleteMessages?: { __typename?: 'DeleteMessages', ok?: boolean | null } | null };


export const DeleteMessagesDocument = gql`
    mutation deleteMessages($input: MessageDeleteInput!) {
  deleteMessages(input: $input) {
    ok
  }
}
    `;
export type DeleteMessagesMutationFn = ApolloReactCommon.MutationFunction<DeleteMessagesMutation, DeleteMessagesMutationVariables>;

/**
 * __useDeleteMessagesMutation__
 *
 * To run a mutation, you first call `useDeleteMessagesMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteMessagesMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteMessagesMutation, { data, loading, error }] = useDeleteMessagesMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteMessagesMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteMessagesMutation, DeleteMessagesMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<DeleteMessagesMutation, DeleteMessagesMutationVariables>(DeleteMessagesDocument, options);
      }
export type DeleteMessagesMutationHookResult = ReturnType<typeof useDeleteMessagesMutation>;
export type DeleteMessagesMutationResult = ApolloReactCommon.MutationResult<DeleteMessagesMutation>;
export type DeleteMessagesMutationOptions = ApolloReactCommon.BaseMutationOptions<DeleteMessagesMutation, DeleteMessagesMutationVariables>;