import * as Types from '../../../graphql-types';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
const defaultOptions = {} as const;
export type CreateUserMutationVariables = Types.Exact<{
  firstName: Types.Scalars['String'];
  lastName: Types.Scalars['String'];
  phone: Types.Scalars['String'];
  birthday: Types.Scalars['Date'];
  gender: Types.Scalars['String'];
  password: Types.Scalars['String'];
}>;


export type CreateUserMutation = { __typename?: 'Mutation', updateInformationUser?: { __typename?: 'CreateUser', user?: { __typename?: 'UserType', phone: string, birthday?: any | null } | null } | null };


export const CreateUserDocument = gql`
    mutation CreateUser($firstName: String!, $lastName: String!, $phone: String!, $birthday: Date!, $gender: String!, $password: String!) {
  updateInformationUser(
    input: {firstName: $firstName, lastName: $lastName, phone: $phone, birthday: $birthday, gender: $gender, password: $password}
  ) {
    user {
      phone
      birthday
    }
  }
}
    `;
export type CreateUserMutationFn = ApolloReactCommon.MutationFunction<CreateUserMutation, CreateUserMutationVariables>;

/**
 * __useCreateUserMutation__
 *
 * To run a mutation, you first call `useCreateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createUserMutation, { data, loading, error }] = useCreateUserMutation({
 *   variables: {
 *      firstName: // value for 'firstName'
 *      lastName: // value for 'lastName'
 *      phone: // value for 'phone'
 *      birthday: // value for 'birthday'
 *      gender: // value for 'gender'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useCreateUserMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreateUserMutation, CreateUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<CreateUserMutation, CreateUserMutationVariables>(CreateUserDocument, options);
      }
export type CreateUserMutationHookResult = ReturnType<typeof useCreateUserMutation>;
export type CreateUserMutationResult = ApolloReactCommon.MutationResult<CreateUserMutation>;
export type CreateUserMutationOptions = ApolloReactCommon.BaseMutationOptions<CreateUserMutation, CreateUserMutationVariables>;