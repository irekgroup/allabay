import * as Types from '../../../graphql-types';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
const defaultOptions = {} as const;
export type SendTooltipMutationVariables = Types.Exact<{
  input: Types.SendTooltipInput;
}>;


export type SendTooltipMutation = { __typename?: 'Mutation', sendTooltip?: { __typename?: 'SendTooltip', ok?: boolean | null } | null };


export const SendTooltipDocument = gql`
    mutation sendTooltip($input: SendTooltipInput!) {
  sendTooltip(input: $input) {
    ok
  }
}
    `;
export type SendTooltipMutationFn = ApolloReactCommon.MutationFunction<SendTooltipMutation, SendTooltipMutationVariables>;

/**
 * __useSendTooltipMutation__
 *
 * To run a mutation, you first call `useSendTooltipMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSendTooltipMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [sendTooltipMutation, { data, loading, error }] = useSendTooltipMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useSendTooltipMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<SendTooltipMutation, SendTooltipMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<SendTooltipMutation, SendTooltipMutationVariables>(SendTooltipDocument, options);
      }
export type SendTooltipMutationHookResult = ReturnType<typeof useSendTooltipMutation>;
export type SendTooltipMutationResult = ApolloReactCommon.MutationResult<SendTooltipMutation>;
export type SendTooltipMutationOptions = ApolloReactCommon.BaseMutationOptions<SendTooltipMutation, SendTooltipMutationVariables>;