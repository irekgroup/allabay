import Icon28SettingsOutline from "@vkontakte/icons/dist/28/settings_outline";
import Icon24FavoriteOutline from "@vkontakte/icons/dist/24/favorite_outline";
import Icon24UserAddOutline from "@vkontakte/icons/dist/24/user_add_outline";

import Cell from "../MenuCell/MenuCell.js";
import "./DesktopMenu.css";
import React from "react";

export default class DesktopMenu extends React.Component {
  render() {
    return (
      <div className="desktopMenu">
        <div className="desktopMenu__Buttons">
          <Cell icon={<Icon24FavoriteOutline />}>Избранное</Cell>
          <Cell icon={<Icon28SettingsOutline width={24} height={24} />}>
            Настройки
          </Cell>
          <Cell icon={<Icon24UserAddOutline />}>Пригласить друзей</Cell>
        </div>
      </div>
    );
  }
}
