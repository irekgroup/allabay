import { Div, Card, SimpleCell, Link, Button, Title, Gallery, InfoRow, PanelHeaderButton, IconButton } from "@vkontakte/vkui";
import React, { useState } from "react";
import "./ProductCard.css"
import { Icon24ShareOutline, Icon24FavoriteOutline, Icon24MessageReplyOutline, Icon24SartOutline } from '@vkontakte/icons';

class ProductCard extends React.Component {

    state = {
        is_more: false
    }

    more = () => {
        this.setState({ is_more: !this.state.is_more })
    }

    render() {
        const { card } = this.props;
        return(
            <Card size="l">
                <div className="cardItem">
                <Gallery
                    slideWidth="100%"
                    style={{ height: 120 }}
                    bullets="light"
                >
                    {card.imgs &&
                        card.imgs.map((img) => (<div style={{ width: "100%", height: "100%" }}><img className="imgItem" src={img[img.type]} /></div>))
                    }
                </Gallery>
                <SimpleCell multiline disabled description="Доставка 5 дней" after={<Title className="valueItem" weight="heavy">{card.cost && card.cost}Р</Title>}>
                    <Title weight="heavy">
                        {card.name && card.name}
                    </Title>
                </SimpleCell>
                {
                    this.state.is_more && <div>
                        <SimpleCell multiline description="самовывоз" disabled after={<Title className="valueItem" weight="heavy">Москва</Title>}>Доставка</SimpleCell>
                        <SimpleCell multiline disabled description={card.state && card.state} after={<Title className="valueItem" weight="heavy">{card.brand && card.brand}</Title>}>Состояние</SimpleCell>
                        <SimpleCell multiline disabled description={card.description}>
                            Описание
                        </SimpleCell>

                        <Div style={{ display: "flex", justifyContent: "center", padding: "px 0px" }}>
                            <IconButton icon={<Icon24ShareOutline />} />
                            <IconButton icon={<Icon24SartOutline />} />
                            <IconButton icon={<Icon24FavoriteOutline />} style={{ marginLeft: 8 }} />
                            <IconButton icon={<Icon24MessageReplyOutline />} style={{ marginLeft: 8 }} />
                        </Div>
                    </div>
                }

                <Div style={{ display: "flex" }}>
                    <Button mode="commerce" style={{ backgroundColor: "#f8961d" }} stretched>Купить</Button>
                    <Link mode="secondary" style={{ marginLeft: 8 }} onClick={this.more} stretched>{this.state.is_more ? "Скрыть" : "Подробнее"}</Link>
                </Div>
                </div>
            </Card>
        )
    }
}

export default ProductCard;