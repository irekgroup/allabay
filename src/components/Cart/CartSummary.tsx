import React, {Dispatch, FC, SetStateAction} from "react";
import Button from "../Button/Button";
import tick from "../../assets/done.png";
import "./Cart.scss"

interface CartSummaryProps {
    setShowIndicator: Dispatch<SetStateAction<boolean>>;
}

const CartSummary: FC<CartSummaryProps> = ({setShowIndicator}) => {
    return (
        <div className="summary">
            <h2>Итого: N</h2>
            <Button width={"full"} mode={"primary"} onClick={()=> setShowIndicator(true)}>Получить реквизиты</Button>
            <p><img src={tick} alt="Значек"/><b>Согласен</b> с <a className={"agreement-link"} href="">Условиями, правилами возврата</a> и <a className={"agreement-link"} href="">Политикой в отношении организации обработки и обеспечения безопасности персональных данных</a></p>
        </div>
    )
}

export default CartSummary;