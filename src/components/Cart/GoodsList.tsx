import React, {Dispatch, FC, SetStateAction, useContext} from "react";
import {RootStoreContext} from "../../index";
import {observer} from "mobx-react";
import {Link} from "@vkontakte/vkui";
import {MODAL_UNAUTHORIZED, router} from "../../router";
import searchIcon from "../../img/main/search.png";
import Indicator from "../Notifications/Indicator/Indicator";
import "./Cart.scss"

interface GoodsListProps {
    goodsList: void[];
    isShowIndicator: boolean;
    setShowIndicator: Dispatch<SetStateAction<boolean>>;
}

const GoodsList: FC<GoodsListProps> = ({goodsList, isShowIndicator, setShowIndicator}) => {
    const {user} = useContext(RootStoreContext);

    const unauthorizedMessage = <>
        <div className={"unauth-container"}>
            <span className={"ind-link2"}>чтобы найти всё что нужно или <Link
                onClick={() => router.pushModal(MODAL_UNAUTHORIZED)}> авторизуйтесь </Link> чтобы увидеть уже добавленные
                товары.</span>
            <Indicator className={"indicator-container2"} setShowIndicator={setShowIndicator} isShowIndicator={isShowIndicator}/>
        </div>
    </>

    const authorizedMessage = <>
        чтобы добавить все что нужно в корзину.
    </>

    const emptyGoodList = <div className="cart-empty">
        <h1>Ваша корзина пуста</h1>
        <div className={"high-order-container"}>
            <p>Воспользуйтесь поиском </p>
            <div className={"ind-container"}>
                <Link className={"ind-link"} href={'#'}><img src={searchIcon}/></Link>
                <Indicator className={"indicator-container1"} setShowIndicator={setShowIndicator} isShowIndicator={isShowIndicator}/>
            </div>
            <p> {user.userData.id ? authorizedMessage : unauthorizedMessage}</p>
        </div>
    </div>


    return (
        <>
            {goodsList.length > 0
                ? goodsList
                : emptyGoodList
            }
        </>
    );
}

export default observer(GoodsList);
