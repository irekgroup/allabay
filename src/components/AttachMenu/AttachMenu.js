import { ActionSheet, ActionSheetItem } from "@vkontakte/vkui";
import "./AttachMenu.css";
import Icon28ArticleOutline from '@vkontakte/icons/dist/28/article_outline';
import Icon28CopyOutline from '@vkontakte/icons/dist/28/copy_outline';

const AttachMenu = ({ setActiveModal, onClose, user }) => (
    <ActionSheet onClose={onClose}>
        <ActionSheetItem 
            autoclose 
            before={<Icon28CopyOutline />}
            >
        Файл
        </ActionSheetItem>

        <ActionSheetItem 
            autoclose 
            before={<Icon28ArticleOutline />}
            onClick={() => setActiveModal("card")}
            >
        Карточка
        </ActionSheetItem>
    </ActionSheet>
);
export default AttachMenu;
