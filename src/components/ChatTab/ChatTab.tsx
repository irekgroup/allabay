import { Cell } from '@vkontakte/vkui';
import { FC, MouseEvent } from 'react';
import './ChatTab.scss';
import { Icon24Pin } from "@vkontakte/icons";

interface ChatTabProps {
    message: string;
    name: string;
    date: string;
    img: string;
    onClick: () => void;
    isRead?: boolean;
    isPin?: boolean;
    isSelected?: any;
    onContextMenu?: (e: any) => void;
}

const ChatTab: FC<ChatTabProps> = ({
                                       message,
                                       name,
                                       date,
                                       img,
                                       isRead = false,
                                       onClick,
                                       isPin,
                                       isSelected,
                                       onContextMenu
                                   }) => {
    return (
        <div className={'wrap'} onContextMenu={onContextMenu}>
            <Cell className={`chat-tab ${isSelected ? 'selected' : ''}`} onClick={onClick}>
                <div className="chat-tab__img">
                    <img src={img} alt=""/>
                </div>
                <div className="chat-tab__content">
                    <div>
                        <p>{name}</p>
                    </div>
                    <div>
                        <p>{message}</p>
                        <p>{date}</p>
                    </div>
                </div>
                <div className={'status'}>
                    {(!isRead && name !== 'Избранное') && <div className={'isnRead'}></div>}
                    {isPin && <Icon24Pin width={16} fill={'#c4c4c4'}/>}
                </div>
            </Cell>
        </div>
    );
};

export default ChatTab;
