import React, { FC, useState } from 'react';
import {
    Button,
    Card,
    CardGrid,
    FormItem,
    FormLayout,
    FormLayoutGroup,
    IconButton,
    Input,
    Title,
} from "@vkontakte/vkui";
import { Icon24CancelOutline } from "@vkontakte/icons";
import { Checkbox } from "@vkontakte/vkui/dist/components/Checkbox/Checkbox";
import Switcher from "../Switcher/Switcher";
import './AddressChange.scss';

interface AddressChangeProps{
    self?: boolean,
}

const AddressChange:FC<AddressChangeProps> = ({ self }) => {
    const [isAddress] = useState( self ? 2 : 1)
    return (
        <div  className={'addressChange wauto'}><CardGrid size={"l"}>
            <Card mode="shadow">
                <div className="header_card">
                    <Title weight="medium" level="2">Ваша страна</Title>
                    <FormLayout>
                        <FormItem style={{padding: '12px 0'}}>
                            <Input type={'text'} placeholder={'Российская Федерация'} after={
                                <IconButton hoverMode={'opacity'}>
                                    <Icon24CancelOutline/>
                                </IconButton>
                            }/>
                        </FormItem>
                    </FormLayout>
                    <p className="warning">Поля, помеченные звездочкой (*), обязательны к
                        заполнению. Укажите данные на русском или
                        английском языке.</p>
                </div>
            </Card>
            <Card mode="shadow">
                <div className="header_card">
                    <Title weight="medium" level="2">Контактная информация</Title>
                </div>
                <FormLayout>
                    <FormItem>
                        <Input type={'text'} placeholder={'Фамилия Имя Отчество'} after={
                            <IconButton hoverMode={'opacity'}>
                                <Icon24CancelOutline/>
                            </IconButton>
                        }/>
                    </FormItem>
                    <FormLayoutGroup  mode={'horizontal'}>
                        <FormItem style={{textAlign: 'center', width: 'min-content'}}>
                            <Input placeholder={'+7'}/>
                        </FormItem>
                        <FormItem style={{flexGrow: 4}}>
                            <Input placeholder={'917 - 237 - 37 - 09'}/>
                        </FormItem>
                    </FormLayoutGroup>
                </FormLayout>
            </Card>
            <Card mode="shadow">
                <div className="header_card">
                    <Title weight="medium" level="2">{isAddress === 1 ? 'Адреса доставки' : 'Адрес пункта выдачи'}</Title>
                </div>
                <FormLayout>
                    <FormItem>
                        <Input type={'text'} placeholder={'Найти на карте'} after={
                            <IconButton hoverMode={'opacity'}>
                                <Icon24CancelOutline/>
                            </IconButton>
                        }/>
                    </FormItem>

                    <FormItem>
                        <Input type={'text'} placeholder={'Регион'} after={
                            <IconButton hoverMode={'opacity'}>
                                <Icon24CancelOutline/>
                            </IconButton>
                        }/>
                    </FormItem>
                    <FormItem>
                        <Input type={'text'} placeholder={'Город'} after={
                            <IconButton hoverMode={'opacity'}>
                                <Icon24CancelOutline/>
                            </IconButton>
                        }/>
                    </FormItem>
                    <FormItem>
                        <Input type={'text'} placeholder={'Улица, дом, номер квартиры'} after={
                            <IconButton hoverMode={'opacity'}>
                                <Icon24CancelOutline/>
                            </IconButton>
                        }/>
                    </FormItem>
                    <FormItem>
                        <Input type={'text'} placeholder={'Индекс'} after={
                            <IconButton hoverMode={'opacity'}>
                                <Icon24CancelOutline/>
                            </IconButton>
                        }/>
                    </FormItem>
                </FormLayout>
            </Card>
            <Card mode="shadow">
                <div className="header_card">
                    <Title weight="medium" level="2">Для транспортных служб</Title>
                </div>
                <FormLayout>
                    <FormItem>
                        <Input type={'text'} placeholder={'Серия и номер паспорта'} after={
                            <IconButton hoverMode={'opacity'}>
                                <Icon24CancelOutline/>
                            </IconButton>
                        }/>
                    </FormItem>

                    <FormLayoutGroup mode={'horizontal'}>
                        <FormItem>
                            <Input type={'text'} placeholder={'Дата рождения'} after={
                                <IconButton hoverMode={'opacity'}>
                                    <Icon24CancelOutline/>
                                </IconButton>
                            }/>
                        </FormItem>
                        <FormItem>
                            <Input type={'text'} placeholder={'Дата выдачи'} after={
                                <IconButton hoverMode={'opacity'}>
                                    <Icon24CancelOutline/>
                                </IconButton>
                            }/>
                        </FormItem>
                    </FormLayoutGroup>
                    <FormItem>
                        <Input type={'text'} placeholder={'Кем выдан'} after={
                            <IconButton hoverMode={'opacity'}>
                                <Icon24CancelOutline/>
                            </IconButton>
                        }/>
                    </FormItem>
                    <FormItem>
                        <Input type={'text'} placeholder={'ИНН'} after={
                            <IconButton hoverMode={'opacity'}>
                                <Icon24CancelOutline/>
                            </IconButton>
                        }/>
                    </FormItem>
                    <FormItem style={{margin:'0', paddingBottom: 0, paddingTop: 0}}>
                        <Checkbox><span style={{fontSize: 12, fontWeight: 500, lineHeight: 0}}>Я согласен на использование данных для транспортных служб.
                                    Инфрмация будет нажедно защищена.</span></Checkbox>
                    </FormItem>
                    <FormItem style={{marginTop:5,paddingTop:'0'}}>
                        <p style={{ color: '#8BB5EB',marginTop:'0',paddingTop:'0' }}>Сохраняя данные, я соглашаюсь с общими условиями,
                            Политикой конфедициальности, а также на обработку
                            персональных данных в соотвесвии с ними</p>
                    </FormItem>
                </FormLayout>
            </Card>
        </CardGrid>
            <div style={{padding: '0 16px'}}>
                <div className={'row'}>
                    <p>Задать как адрес по умолчанию</p>
                    <Switcher />
                </div>
                <Button className={'saveBtn'} mode={'outline'}>Сохранить</Button>
            </div>
        </div>
    );
};

export default AddressChange;