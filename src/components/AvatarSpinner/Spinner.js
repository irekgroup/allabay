import React from "react";
import "./spinner.css";

const spinner = (props) => (
  <div
    className="loader"
    style={{ width: props.size || 64, height: props.size || 64 }}
  >
    <div className="inner one"></div>
    <div className="inner two"></div>
    <div className="inner three"></div>
  </div>
);

export default spinner;
