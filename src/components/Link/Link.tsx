import React, { FC } from "react";
import "./Link.scss";


interface LinkProps {
    children: React.ReactNode
}

const Link: FC<LinkProps> = ({ children }) => {
    return(
        <span className="link">
            {children}
        </span>
    );
}

export default Link;