import React, { FC } from 'react';
import galka from "../../img/chat/galka.svg";
import './Checkbox.scss';

interface CheckboxProps{
    children?: React.ReactNode,
    name?: string,
}

const Checkbox:FC<CheckboxProps> = ({children, name}) => {
    return (
        <label className={'ui-checkbox'}>
            <input type={"checkbox"} name={name} onChange={e => {
                if(e.currentTarget.checked){
                    e.currentTarget.style.background = `url(${galka}) var(--blue) center no-repeat`
                    e.currentTarget.style.backgroundSize = '60%'
                } else {
                    e.currentTarget.style.background = `#fff`
                }
            }}/>
            {children}
        </label>
    );
};

export default Checkbox;