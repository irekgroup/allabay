import { Tappable } from "@vkontakte/vkui";

import React from "react";
import "./MenuCell.css";

export default class MenuCell extends React.Component {
  render() {
    return (
      <Tappable
        className="menuCell"
        onClick={this.props.onClick ? this.props.onClick : () => {}}
        active={this.props.active ? "true" : "false"}
      >
        <div className="menuCell__Icon">{this.props.icon}</div>
        <div className="buttonText">{this.props.children}</div>
      </Tappable>
    );
  }
}
