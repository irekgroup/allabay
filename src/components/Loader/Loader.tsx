import React, { FC } from 'react';
import logoGrey from '../../img/main/logo-grey.svg';
import bigLogo from '../../img/main/biglogo.png';
import logo from '../../img/main/logo.svg';
import './Loader.scss';

interface LoaderProps{
    firstScreen?: boolean
}

const Loader:FC<LoaderProps> = ({firstScreen}) => {
    return (
        <div className={"loader"}>
            {!firstScreen
                ? <img src={logoGrey} alt="logo"/>
                :
                <div className={'fs'}>
                    <img width={'50px'} className={'small'} src={logo} alt=""/>
                    <img width={"150px"} src={bigLogo} alt="biglogo"/>
                </div>}
        </div>
    );
};

export default Loader;