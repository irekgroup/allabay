import React, { FC, useContext, useEffect, useRef, useState } from 'react';
import {
    Icon16MoreVertical,
    Icon24Favorite,
    Icon24FavoriteOutline,
} from "@vkontakte/icons";
import cartImg from "../../img/cart/cart-img-example.png";
import back from "../../img/feed/back.svg";
import front from "../../img/feed/front.svg";
import cartOutline from "../../img/feed/cartOutline.svg";
import '../CartItem/CartItem.scss';
import './CardItem.scss';
import Button from "../Button/Button";
import { Gallery, Link } from "@vkontakte/vkui";
import { MODAL_PRODUCT_CARD, PAGE_PRODUCT } from "../../router";
import { useRouter } from "@happysanta/router";
import { IProductInfo } from "../../mobX/CreateCard";
import { RootStoreContext } from "../../index";

interface CardItemProps {
    product?: IProductInfo,
}

const CardItem: FC<CardItemProps> = ({product}) => {
    const [more, setMore] = useState(false)
    const [fav, setFav] = useState(false)
    const [move, setMove] = useState(true)
    const [timer, setTimer] = useState(0)
    const [slideIndex, setSlideIndex] = useState(0);
    const [slideWidth, setSlideWidth] = useState(0);

    const {config} = useContext(RootStoreContext)

    const router = useRouter()

    const galleryRef = useRef<HTMLDivElement>(null)
    useEffect(() => {
        if(galleryRef.current){
            setSlideWidth(galleryRef.current.offsetWidth / (product?.photo?.length || 1))
        }
    }, [galleryRef])

    useEffect(() => {
        return () => {clearTimeout(timer)}
    }, [])

    const guarantee = (product && product.timeFrom && product.guaranteePeriod) ?
        Math.ceil(Math.abs(Date.parse(product.guaranteePeriod) - Date.parse(product.timeFrom)) / (1000 * 60 * 60 * 24)) : 0;
    return (
        <div className={`cart-item card-item ${more ? 'more' : ''}`}>
            <Gallery
                getRef={galleryRef}
                onTouchEndCapture={e => {
                    e.stopPropagation()
                }}
                onMouseMove={!('ontouchstart' in window) ?  e => {
                    if(move && e.nativeEvent.offsetX > 0){
                        const newIndex = Math.floor(e.nativeEvent.offsetX / slideWidth)
                        if(slideIndex !== newIndex){
                            setSlideIndex(newIndex)
                            setMove(false)
                            clearTimeout(timer)
                            setTimer(+setTimeout(() => {
                                setMove(true)
                            },150))
                        }
                    }
                } : () => {}}
                slideWidth="100%"
                style={{ height: 150 }}
                bullets="light"
                slideIndex={slideIndex}
                onChange={setSlideIndex}
                showArrows={false}>
                {product?.photo ?
                    product.photo.map((item, i) => {
                        return <img key={i} src={item.url} alt={item.title}/>
                    }) :
                        <img src={cartImg} alt=""/>
                }
            </Gallery>
            <div className="cart-item-content">
                <div className={'flex'}>
                    <div className={'flex icons'}>
                        <img src={cartOutline} alt=""/>
                        <img src={back} alt=""/>
                        <img src={front} alt=""/>
                    </div>
                    {fav ? <Icon24Favorite fill={'#F8961D'} onClick={() => {
                        setFav(false)
                        config.pushNotification('Товар удален из избранного')
                    }}/>
                        : <Icon24FavoriteOutline onClick={() => {
                            setFav(true)
                            config.pushNotification('Товар добавлен в избранное')
                        }} fill={'#000'}/>}
                </div>
                <div className={'flex-col'}>
                    <div className={'flex'}>
                        <h3>{product?.title || '' /*Капот WF 2012-2017*/}</h3>
                        <div className="costBlock">
                            {(product?.prevCost && product.newCost) && (
                                <>
                                    <p className={'prevCost cost'}>{product?.prevCost || '' /*5206 ₽*/}₽</p>
                                    <p className="discount">{-Math.round(100 - (+product.newCost.replace(',','.') / (+product?.prevCost.replace(',','.') / 100)))}%</p>
                                </>)}
                            <p className={`${product?.prevCost ? 'newCost' : ''} cost`}>{product?.newCost || '' }₽</p>
                        </div>
                    </div>
                    <div className={'flex desc'}>
                        {more &&
                        <div className={'types'} onClick={() => setMore(!more)}>
                            <p>Артикул: <span>{product?.number}</span></p> <p>Бренд: <span>{product?.brand || ''}</span></p>
                            <p>Гарантия: <span>{guarantee === 1 ? guarantee + ' день' : guarantee > 4 ? guarantee + ' дня' : guarantee + ' дней'}</span></p> <p>Состояние: <span>{product?.condition || ''}</span></p>
                            <p>Доставка: <span>{product?.sendType}</span></p>
                        </div>}
                        <p onClick={() => setMore(!more)}>
                            {product?.description || ''}
                        </p>
                        {more ? (
                            <div className={'flex btns'}>
                                    <Link onClick={() => router.pushPage(PAGE_PRODUCT, {})}>Подробнее &gt;</Link>
                                <div className={'flex'}>
                                    <Button mode={"primary"}>Получить реквизиты</Button>
                                    <Icon16MoreVertical onClick={() => router.pushModal(MODAL_PRODUCT_CARD)}/>
                                </div>
                            </div>
                        ) : <Icon16MoreVertical onClick={() => router.pushModal(MODAL_PRODUCT_CARD)}/>}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CardItem;