import React, { useState, useEffect } from "react";

import "swiper/swiper-bundle.css";
import Swiper, { Navigation, Pagination } from "swiper";
import logo from "../../img/biglogo.png";

const SliderS = (props) => {
  let slides = [
    props.firstCardI,
    "https://thedreambag.ru/wp-content/uploads/2018/04/zapchasti-reno-renault-v-sankt-peterburge_5_1.jpg",
    logo,
  ];

  Swiper.use([Navigation, Pagination]);
  // const swiper = new Swiper('.swiper-container', {
  //   pagination: {
  //     el: '.swiper-pagination',
  //     type: 'bullets',
  //     clickable: true,
  //   },
  //     observer: true,
  //     observeParents: true,
  //     observeSlideChildren: true,
  //     updateOnImagesReady: true,
  //     updateOnWindowResize: true,
  //     watchSlidesVisibility: true,
  //     watchSlidesProgress: true,
  //     runCallbacksOnInit: true,
  //     slideToClickedSlide: true,
  //     preloadImages: true,
  //     nested: true,
  //     navigation: {
  //     nextEl: '.swiper-button-next',
  //     prevEl: '.swiper-button-prev',
  //   }
  // })

  // swiper.on('slideChange', function () {
  //   console.log('slide changed');
  // })

  //setTimeout(()=> swiper.update(),2000)
  // useEffect(()=> {

  // },[])

  let swiper = null;

  function initSwiper() {
    swiper = new Swiper(".swiper-container", {
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      pagination: {
        el: ".swiper-pagination",
        type: "bullets",
        clickable: true,
      },
      observer: true,
      observeParents: true,
      bserveSlideChildren: true,
    });
  }
  useEffect(() => {
    initSwiper();
  }, []);

  return (
    <>
      <div class="swiper-container">
        <div class="swiper-wrapper">
          {slides.map((s) => (
            <div class="swiper-slide">
              <img src={s} />
            </div>
          ))}
        </div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
      </div>
    </>
  );
};

export default SliderS;
