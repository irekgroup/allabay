import React from 'react';
import trend from '../../img/feed/trend.svg';
import './PopularSearch.scss';

const PopularSearch = () => {
    return (
        <div className={'popularSearch'}>
            <div className="title">ПОПУЛЯРНЫЕ ЗАПРОСЫ</div>
            <div className="item"><img src={trend} alt="trend"/></div>
        </div>
    );
};

export default PopularSearch;