import "./CartItem.scss";
import cartCheckbox from "../../img/cart/cart-checkbox.svg";
import { FC, useContext, useState } from "react";
import { RootStoreContext } from "../..";
import {
    Icon16MoreVertical,
    Icon24Favorite,
    Icon28ShoppingCartOutline,
    Icon24ArrowUturnRightOutline,
    Icon24ArrowUturnLeftOutline, Icon24FavoriteOutline
} from '@vkontakte/icons';
import cartImg from "../../img/cart/cart-img-example.png";
import {IProductInfo} from "../../mobX/CreateCard";

interface CartItemProps {
    good: IProductInfo
    isSelected: Boolean;
    isScroll: (isScroll: boolean) => void;
    canSelect?: boolean;
}

const CartItem: FC<CartItemProps> = ({ isSelected, canSelect = true, good }) => {
    const rootStore = useContext(RootStoreContext);
    const { chat, config } = rootStore;
    const [currentImg, setCurrentImg] = useState<string>(cartImg);
    const [fav, setFav] = useState(false)

    // Если в корзине > 3 элементов (нужно прописать новую сущность в mobx) ((а нужно ли??))

    // cart.items > 3 ? isScroll(true) : isScroll(false)
    // isScroll(false);

    return (
        <div className="cart-item">
            {canSelect && <div
                className={
                    isSelected ? "cart-select-box selected" : "cart-select-box"
                }
            >
                <img src={cartCheckbox} alt="Чекбокс"/>
            </div>}
            <div className="cart-item-img">
                <img src={currentImg} alt="" />
            </div>
            <div className="cart-item-content">
                <div className={'flex'}>
                    <div className={'flex'}>
                        <Icon28ShoppingCartOutline />
                        <Icon24ArrowUturnLeftOutline/>
                        <Icon24ArrowUturnRightOutline/>
                    </div>
                    {!fav ? <Icon24FavoriteOutline fill={'#F8961D'} onClick={() => {
                        setFav(true)
                        config.pushNotification('Товар добавлен в избранное')
                    }}/> : <Icon24Favorite fill={'#F8961D'} onClick={() => {
                        setFav(false)
                        config.pushNotification('Товар удален из избранного')
                    }}/>}
                </div>
                <div className={'flex-col'}>
                    <div className={'flex'}>
                        <h3>Капот WF 2012-2017</h3>
                        <p>5206 ₽</p>
                    </div>
                    <div className={'flex'}>
                        <p>Бесплатная доставка</p>
                        <Icon16MoreVertical/>
                    </div>
                </div>

            </div>
        </div>
    );
};

export default CartItem;
