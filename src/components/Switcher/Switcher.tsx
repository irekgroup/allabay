import React, { useState } from 'react';
import './Switcher.scss';

export const Switcher = () => {
    const [pos, setPos] = useState(false)
    return (
        <div className={`switcher ${pos ? 'on' : ''}`} onClick={() => setPos(!pos)}>
            <div className="circle"/>
        </div>
    )
}

export default Switcher;