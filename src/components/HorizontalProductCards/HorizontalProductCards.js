import { Card, CardScroll, HorizontalCell, HorizontalScroll } from "@vkontakte/vkui"
import ProductCard from "../ProductCard/ProductCard"
import "./HorizontalProductCards.css"

const HorizontalProductCards = ({ cards, mode }) => {
    return(
        <CardScroll size="m">
                {cards.map((card, i) => (
                    <ProductCard key={i} card={card} />         
                ))}
        </CardScroll>
    )
}
export default HorizontalProductCards;