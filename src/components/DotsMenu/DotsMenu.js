import { Avatar, Tappable } from "@vkontakte/vkui";

import { ReactComponent as VKLogo } from "../../img/vk.svg";

import React from "react";
import "./DotsMenu.css";

const DotsMenuButton = (props) => {
  let vkUrl = `https://oauth.vk.com/authorize?client_id=7638764&display=page&response_type=code&v=5.124&state=123&redirect_uri=${window.location.origin}/vkauth`;

  return <a href={vkUrl}>{props.icon}</a>;
};

export default class DotsMenu extends React.Component {
  render() {
    return (
      <div className="hiddenDotsMenu">
        <div className="row">
          <DotsMenuButton icon={<VKLogo />} />
        </div>
      </div>
    );
  }
}
