import React, { FC, useContext } from 'react';
import { observer } from "mobx-react";
import { RootStoreContext } from "../../index";
import './LetterAvatar.scss'
import { isNumber } from "@vkontakte/vkjs";
import { UserData } from "../../mobX/User";

interface LetterAvatarProps {
    width?: string | number,
    height?: string | number,
    onClick?: () => void,
    userRef?: UserData,
    className?: string,
}

const LetterAvatar: FC<LetterAvatarProps> = observer(({ width, height, onClick, userRef, className }) => {
    const { user } = useContext(RootStoreContext)
    return (
        <div className={`letterAvatar ${className}`}
             onClick={onClick}
             style={{
                 height: isNumber(height) ? height + 'px' : height,
                 width: isNumber(width) ? width + 'px' : width,
                 lineHeight: isNumber(height) ? height + 'px' : height,
             }}>
            {userRef ?
                <>{userRef.firstName ? userRef.firstName[0] : ''}{userRef.lastName ? userRef.lastName[0] : ''}</>
                : <>{user.userData.firstName ? user.userData.firstName[0] : ''}
                    {user.userData.lastName ? user.userData.lastName[0] : ''}</>
            }
        </div>
    );
});

export default LetterAvatar;