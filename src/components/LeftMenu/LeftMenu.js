import Icon28SettingsOutline from "@vkontakte/icons/dist/28/settings_outline";
import Icon24FavoriteOutline from "@vkontakte/icons/dist/24/favorite_outline";
import Icon24UserAddOutline from "@vkontakte/icons/dist/24/user_add_outline";
import Icon20HomeOutline from "@vkontakte/icons/dist/20/home_outline";
import Icon28MoonOutline from "@vkontakte/icons/dist/28/moon_outline";

import { ReactComponent as Gps } from "../../img/gps.svg";
import { Tappable } from "@vkontakte/vkui";

import BigLogo from "../../img/biglogo.png";

import Cell from "../MenuCell/MenuCell.js";
import React from "react";
import "./LeftMenu.css";

export default class Footer extends React.Component {
  render() {
    let { toggleScheme, leftMenuOpened, toggleLeftMenu } = this.props;

    return (
      <div className="hiddenMenu" opened={leftMenuOpened ? "true" : "false"}>
        <Tappable
          className="hideMenuButton flex"
          onClick={toggleLeftMenu}
          style={{ alignItems: "flex-start" }}
        >
          <img src={BigLogo} />
        </Tappable>

        <div className="hiddenMenuButtons">
          <Cell icon={<Icon20HomeOutline width={24} height={24} />} active>
            Главная страница
          </Cell>
          <Cell icon={<Gps style={{ height: 24, fill: "gray" }} />}>
            Москва
          </Cell>
          <Cell icon={<Icon24FavoriteOutline />}>Избранное</Cell>
          <Cell icon={<Icon28SettingsOutline width={24} height={24} />}>
            Настройки
          </Cell>
          <Cell icon={<Icon24UserAddOutline />}>Пригласить друзей</Cell>
          <div className="schemeButton">
            <Tappable onClick={toggleScheme}>
              <Icon28MoonOutline />
            </Tappable>
          </div>
        </div>
      </div>
    );
  }
}
