import React, { FC } from 'react';
import { Icon28ChevronLeftOutline } from "@vkontakte/icons";
import { Radio } from "@vkontakte/vkui";
import Button from "../Button/Button";
import { SelectInput } from "../../panels/MobileFeed/MobileFeed";
import './Filters.scss'

interface IFiltersProps{
    onClose: () => void;
}

const Filters:FC<IFiltersProps> = ({onClose}) => {
    return (
        <div className={'filters'}>
            <div className="filter_main">
                <div className={'filter_header'}>
                    <Icon28ChevronLeftOutline style={{ cursor: 'pointer' }} fill={'#A6A6A6'} onClick={onClose}/>
                    <p>Уточнить</p>
                </div>
                <SelectInput title={'Город или регион'} values={['Москва', 'Санкт-Петербург', 'Казань']}/>
                <SelectInput title={'Категория'} values={['Транспортные средства', 'b', 'c']}/>
                <SelectInput title={'Подкатегория'} values={['a', 'b', 'c']}/>
                <SelectInput title={'Состояние'} values={['Новое', 'б/у']}/>
                <SelectInput title={'Цена, от'} values={['123', '132', '222']}/>
                <SelectInput title={'Цена, до'} values={['222', '333', '444']}/>
                <div className={'radioList'}>
                    <p>Желаемый способ получения</p>
                    <Radio name={'send'} value={1}>
                        Самовывоз в ПВЗ в день заказа до 12:00
                    </Radio>
                    <Radio name={'send'} value={2}>
                        Turbo доставка до ПВЗ за 1 час
                    </Radio>
                    <Radio name={'send'} value={3}>
                        Turbo доставка до адреса за 1 час
                    </Radio>
                    <Radio name={'send'} value={4}>
                        Курьерская доставка до ПВЗ за 1-2 дня
                    </Radio>
                    <Radio name={'send'} value={5}>
                        Курьерская доставка до адреса за 1-2 дня
                    </Radio>
                    <Radio name={'send'} value={6}>
                        Самовывоз из ПВЗ доставка 5-7 дней
                    </Radio>
                    <Radio name={'send'} value={7}>
                        Самовывоз из Почта Росии 5-7 дней
                    </Radio>
                    <Radio name={'send'} value={8}>
                        Курьерская, до адреса, Почта Росии 5-7 дней
                    </Radio>
                    <Radio name={'send'} value={9}>
                        Самовывоз в ТК (ПЭК, СДЕК, ДЕЛОВЫЕ ЛИНИИ и т.д.)
                    </Radio>
                    <Radio name={'send'} value={10}>
                        Курьерская до адреса ТК (ПЭК, СДЕК, ДЕЛОВЫЕ ЛИНИИ и т.д.)
                    </Radio>
                    <Radio name={'send'} value={11}>
                        Лот
                    </Radio>
                </div>
            </div>
            <Button mode={"secondary"}>Вперед</Button>
        </div>
    );
};

export default Filters;