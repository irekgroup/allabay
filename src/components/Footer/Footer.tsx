import { useRouter } from '@happysanta/router';
import { Icon28Send } from '@vkontakte/icons';
import { FixedLayout, IconButton, Link, Search } from '@vkontakte/vkui';

import React, { FC, useContext } from 'react';
import { RootStoreContext } from '../..';
import './Footer.scss';

interface FooterProps {
    messages?: any;
    toggleDesktopMenu?: any;
    showMessages?: any;
    showChat?: any;
}

const sendMessage = () => {
    console.log('workin`');
    // if (chat.message.replace(/(?:\r\n|\r|\n)/g, "").length > 0) {
    //   if (!socket.wss) {
    //   } else {
    //     let uuid = uuidv4();
    //     let message: Message = {
    //       text: chat.message,
    //       type: "outgoing",
    //       send: false,
    //       read: false,
    //       uuid: uuid,
    //       date: ""
    //     };
    //     let date = new Date();
    //     let hours = date.getHours();
    //     let minutes = date.getMinutes();
    //     message.date = `${(hours < 10 ? "0" : "") + hours}:${(minutes < 10 ? "0" : "") + minutes
    //       }`;
    //     let messages = chat.messages;
    //     messages.push(message);
    //     chat.updateMessages(messages);

    //     if (socket.wss.readyState === 1) {
    //       socket.wss.send(
    //         JSON.stringify({
    //           type: "message",
    //           text: chat.message,
    //           send: false,
    //           read: false,
    //           uuid: uuid,
    //         })
    //       );
    //     } else {
    //       return config.socket.reconnect
    //     }

    //     // let elem = document.getElementById("messages");
    //     // setTimeout(
    //     //   () => elem.scrollTo({ top: elem.scrollHeight, behavior: "smooth" }),
    //     //   50
    //     // // );
    //     // document.getElementById("input").value = "";
    //     setShowChat(true)
    //     chat.updateMessage("");
    //     // this.resizeTextarea();
    //   }
    // }
};

const Footer: FC<FooterProps> = ({ messages, toggleDesktopMenu, showMessages, showChat }) => {
    const router = useRouter();

    const rootStore = useContext(RootStoreContext);

    const { chat, user, config } = rootStore;

    const isMobile = config.isMobile;

    return (
        <FixedLayout id="footerLayout" filled vertical="bottom">
            <div className="flex footer">
                {showChat ? (
                    <div className="search footer-search">
                        <div className="search__logo">
                            <Search className="searchIcon" />
                        </div>
                        <input
                            onChange={(e) => chat.updateMessage(e.target.value)}
                            placeholder="Ваш вопрос"
                            className="search__input"
                        />
                        <div className="search__logo">
                            {/* <Search className="searchIcon" /> */}
                            <IconButton onClick={() => sendMessage()}>
                                <Icon28Send fill="#1E7CF2" />
                            </IconButton>
                        </div>
                    </div>
                ) : (
                    ''
                )}
                <div></div>
                <div
                    className="bar"
                    id="bar2"
                    // active={!isMobile || messages.length === 0 ? "true" : "false"}
                >
                    {showMessages && !isMobile && (
                        <div className="textBlock" style={{ userSelect: 'text' }}>
                            Москва
                        </div>
                    )}
                    <Link className="textBlock">Москва</Link>
                    <Link className="textBlock">Политика конфедициальности</Link>
                    <Link className="textBlock">Условия использования</Link>
                    <Link className="textBlock">Всё о allabay</Link>
                    {/* {!isMobile && <div style={{ flexGrow: 1 }} />} */}
                    {!isMobile && (
                        <Link className="bottomMenu" onClick={toggleDesktopMenu}>
                            Введение
                        </Link>
                    )}
                </div>
            </div>
        </FixedLayout>
    );
};

export default Footer;
