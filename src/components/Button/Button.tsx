import { Tappable, useAdaptivity, ViewWidth } from '@vkontakte/vkui';
import React, { useState, FC, useContext } from 'react';
import { RootStoreContext } from '../..';
import './Button.scss';

interface ButtonProps {
    children?: React.ReactNode;
    style?: React.CSSProperties;
    mode?: ButtonMode;
    color?: ButtonColor;
    width?: ButtonWidth;
    stretched?: boolean;
    disabled?: boolean;
    error?: boolean;
    onClick?: () => void;
}

type ButtonMode = 'outline' | 'primary' | 'secondary' | 'tetrairy' | 'commerce';
type ButtonColor = 'blue' | 'black';
type ButtonWidth = 'full' | 'normal';

const Button: FC<ButtonProps> = ({
    children,
    onClick,
    mode = 'outline',
    disabled = false,
    error = false,
    color = 'blue',
    width = 'normal',
    stretched = false,
    style,
}) => {
    const adaptivity = useAdaptivity();
    const screenWidth: any = adaptivity.viewWidth; // 320 <= 768 - 2; 768 <= 1024 - 3; 1024 > - 4

    return (
        <div className={`button-box ${width} ${mode}`} style={style}>
            <button
                onClick={!disabled ? onClick : () => {}}
                className={`${color} ${
                    disabled ? 'disabled button-main ' : 'button-main '
                }${mode} ${screenWidth == 2 ? 'button-main-mobile' : ''} ${
                    stretched ? 'stretched ' : ''
                } ${error ? 'error' : ''}`}>
                {children}
            </button>
        </div>
    );
};

export default Button;
