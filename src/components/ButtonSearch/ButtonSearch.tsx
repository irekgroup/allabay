import { Tappable } from "@vkontakte/vkui";
import React, { useState, FC } from "react";
import "./ButtonSearch.scss";

interface ButtonSearchProps {
    children: React.ReactNode,
    onClick: () => void
}

const ButtonSearch: FC<ButtonSearchProps> = ({ children, onClick }) => {
    return (
        <Tappable className="button-wrapper">
            <div className="search-btn" onClick={onClick}>{children}</div>
        </Tappable>
    )
}

export default ButtonSearch