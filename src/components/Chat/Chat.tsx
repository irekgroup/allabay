import React, {FC, useContext, useEffect, useRef, useState} from "react";
import "./Chat.css";
import {Card, Link, Spinner, Text} from "@vkontakte/vkui";
import {
    Icon12Verified,
    Icon20Dropdown,
    Icon28ChevronDownOutline,
    Icon28ChevronLeftOutline
} from "@vkontakte/icons";
import {observer} from "mobx-react";
import {RootStoreContext} from "../..";
import {ChatFilters, helperModal, Message} from "../../mobX/Chat";
import {gql, useApolloClient, useLazyQuery, useMutation} from "@apollo/client";
import {CREATE_MESSAGE} from "../CreateChat/CreateChat";
import {
    MODAL_CHAT_COMMANDS,
    MODAL_CREATE_CARD,
    PAGE_CHAT_DESKTOP,
    PAGE_CHAT_MOBILE,
    POPOUT_CHAT_OPTIONS,
    POPOUT_CHAT_TYPES,
    POPOUT_DELETE_ALERT
} from "../../router";
import dots from "../../img/chat/dots.svg";
import {useRouter} from "@happysanta/router";
import slash from '../../img/chat/slash.svg';
import boxes from '../../img/chat/boxes.svg';
import shablon from '../../img/chat/shablon.svg'
import galka from '../../img/chat/galka.svg';
import front from '../../img/feed/front.svg';
import back from '../../img/feed/back.svg';
import del from '../../img/chat/delete.svg';
import layers from '../../img/chat/layers.svg';
import cross from '../../img/chat/cross.svg';
import skrepka from '../../img/chat/skrepka.svg';
import search from '../../img/main/search.png';
import camera from '../../img/main/camera.png';
import Loader from "../Loader/Loader";
import {read_chat, read_chatVariables} from "./__generated__/read_chat";
import {createMessage, createMessageVariables} from "../CreateChat/__generated__/createMessage";
import {deleteMessages, deleteMessagesVariables} from "../../popouts/ChatOptions/__generated__/deleteMessages";
import {DELETE_MESSAGES} from "../../popouts/ChatOptions/ChatOptions";
import {listTooltip, listTooltip_listTooltip, listTooltipVariables} from "./__generated__/listTooltip";
import {sendTooltip, sendTooltipVariables} from "./__generated__/sendTooltip";

export const READ_CHAT = gql`
    mutation read_chat($idChat: String!){
        readChat(input: {idChat: $idChat}){
            ok
        }
    }
`

export const LIST_TOOLTIP = gql`
    query listTooltip($input: Int!){
        listTooltip(idMessage: $input){
            id
            idTooltip
            textTooltip
            isDelete
        }
    }
`

export const SEND_TOOLTIP = gql`
    mutation sendTooltip($input: SendTooltipInput!){
        sendTooltip(input: $input) {
            ok
        }
    }
`

export const UPDATE_CHAT = gql`
    mutation updateChat($input: ChatChangeInput!){
        updateChat(input: $input){
            ok
        }
    }
`

interface IconStatusProps {
    status: any;
    time: string;
}

interface MessageProps {
    message: Message;
    partner?: string;
}

interface MessageWrapperProps {
    children: React.ReactNode,
    onSelect: (n: 1 | -1) => void,
    selectType: 'click' | 'press',
}

const IconStatusMessage: FC<IconStatusProps> = ({status, time}) => (
    <div className="IconStatusMessage">
        <div className="IconStatusMessage__time">{time}</div>
        {!(status.is_send || status.is_read) && <Spinner size={'small'}/>}
        {status.is_send && <Icon12Verified className="IconStatusMessage__one"/>}
        {status.is_read && <Icon12Verified className="IconStatusMessage__two"/>}
    </div>
);

const MessageText: FC<MessageProps> = ({message, partner}) => {
    const {user} = useContext(RootStoreContext);
    return (
        <Card className="MessageText">
            <div className="MessageText__container">
                <div
                    className="MessageText__sender">{message.type === 'outgoing' ? user.userData.firstName : partner}</div>
                <Text weight="medium" className="MessageText__text" style={{wordBreak: 'break-all'}}>
                    {message.text && message.text}
                </Text>
                <IconStatusMessage
                    time={new Date(message.date).toLocaleDateString('ru-RU', {hour: '2-digit', minute: '2-digit'})}
                    status={{is_read: message.read, is_send: message.send}}/>
            </div>
        </Card>
    );
}

const MessageWrapper: FC<MessageWrapperProps> = ({children, onSelect, selectType}) => {
    const [timer, setTimer] = useState<NodeJS.Timeout | null>(null)
    const [select, setSelect] = useState(false)
    const [nowSelected, setNowSelected] = useState(false)
    useEffect(() => {
        if (selectType === 'press') {
            setSelect(false)
        }
    }, [selectType])
    return (
        <div
            className={`messageWrapper ${selectType === 'click' ? 'doted' : ''} ${(select && selectType === 'click') ? 'selected' : ''}`}
            onContextMenu={e => e.preventDefault()}
            onMouseDown={(!('ontouchstart' in window) && selectType === 'press') ? () => {
                setTimer(setTimeout(() => {
                    timer && clearTimeout(timer)
                    setTimer(null)
                }, 300))
            } : () => {
            }}
            onTouchStart={(('ontouchstart' in window) && selectType === 'press') ? (e) => {
                setTimer(setTimeout(() => {
                    timer && clearTimeout(timer)
                    setTimer(null)
                    onSelect(1)
                    setSelect(true)
                    setNowSelected(true)
                }, 300))
            } : () => {
            }}
            onTouchMove={(('ontouchstart' in window) && selectType === 'press') ? (e => {
                e.stopPropagation()
                if (timer) {
                    clearTimeout(timer)
                    setTimer(null)
                }
            }) : () => {
            }}
            onTouchCancel={(('ontouchstart' in window) && selectType === 'press') ? (e => {
                e.stopPropagation()
                if (timer) {
                    clearTimeout(timer)
                    setTimer(null)
                }
            }) : () => {
            }}
            onClick={selectType === 'press' ? () => {
                if (!nowSelected && select) {
                    setSelect(false)
                    onSelect(-1)
                }
                if (timer) {
                    clearTimeout(timer)
                    setTimer(null)
                } else if (!('ontouchstart' in window)) {
                    onSelect(1)
                    setSelect(true)
                    setNowSelected(true)
                }
                setNowSelected(false)
            } : () => {
                if (select) {
                    onSelect(-1)
                } else {
                    onSelect(1)
                }
                setSelect(!select)
            }}>
            <div className={`circle ${selectType === 'click' ? '' : 'hidden'}`}
                 style={select ? {background: `rgba(30, 124, 242, 0.6) url(${galka}) no-repeat center`} : {}}></div>
            {children}
        </div>
    )
}

const OutgoingDiv: FC = ({children}) => {

    return (
        <div className={`message outgoing`}>
            {children}
        </div>
    )
}

const IncomingDiv: FC = ({children}) => (
    <div className="message incoming" onContextMenu={e => e.preventDefault()}>
        {children}
    </div>
);

interface ChatProps {
    children?: React.ReactNode,
    loading?: boolean,
}

const Chat: FC<ChatProps> = observer(({loading = true}) => {
    const rootStore = useContext(RootStoreContext);
    const {config, chat, user} = rootStore;
    const {isMobile} = config;

    const router = useRouter()

    const [expanded, setExpanded] = useState(false);
    const [msg, setMsg] = useState('')
    const [image, setImage] = useState<null | string>(null)
    const [pressTime, setPressTime] = useState<number | null>(null)
    const [timer, setTimer] = useState<number | null>(null)
    const [helpersBtn, setHelpersBtn] = useState(false)
    const [selectedCount, setSelectedCount] = useState(0)
    // const [selectedIds, setSelectedIds] = useState<Record<string, boolean>>({})
    const [showScrollBtn, setShowScrollBtn] = useState(false)
    const [tooltipList, setTooltipList] = useState<(listTooltip_listTooltip | null)[]>([])

    const fileInput = useRef<HTMLInputElement>(null)

    const apollo = useApolloClient();
    const [createMessage, createdMessage] = useMutation<createMessage, createMessageVariables>(CREATE_MESSAGE);
    const [readChat, _] = useMutation<read_chat, read_chatVariables>(READ_CHAT)
    const [deleteMessages, deleteMessagesResponse] = useMutation<deleteMessages, deleteMessagesVariables>(DELETE_MESSAGES)
    const [listTooltip] = useLazyQuery<listTooltip, listTooltipVariables>(LIST_TOOLTIP)
    const [sendTooltip] = useMutation<sendTooltip, sendTooltipVariables>(SEND_TOOLTIP)

    const messagesRef = useRef<HTMLDivElement>(null)
    const isInitialMount = useRef(true);

    const scroll = () => {
        if (config.isMobile && messagesRef.current) {
            window.scroll({behavior: "smooth", top: messagesRef.current.scrollHeight});
        }
        messagesRef.current && messagesRef.current.scrollTo(0, messagesRef.current.scrollHeight);
    }

    const readChatFn = () => {
        if (!loading && (chat.dialogId && chat.messages.length && chat.messages[chat.messages.length - 1].type === 'incoming' && !chat.messages[chat.messages.length - 1].read)) {
            readChat({
                variables: {
                    idChat: chat.dialogId,
                },
            })
                .then((options) => {
                    (options.data?.readChat && options.data.readChat.ok && chat.dialogIdx !== null && +chat.dialogIdx >= 0) && chat.readChat('' + chat.dialogId);
                    setSelectedCount(0)
                })
                .catch(console.error)
        }
    }

    const fetchTooltipList = () => {
        if (typeof chat.dialogIdx === 'number' && chat.chatList[chat.dialogIdx]?.lastMessages?.idForTooltip) {
            listTooltip({
                variables: {
                    input: '' + chat.chatList[chat.dialogIdx].lastMessages?.idForTooltip,
                },
            }).then(data => {
                if (data.error) {
                    console.error(data.error)
                    return
                }
                data.data?.listTooltip ? setTooltipList(data.data?.listTooltip) : setTooltipList([])
            }).catch(e => console.error(e))
        } else {
            setTooltipList([])
        }
    }

    useEffect(() => {
        if (typeof chat.dialogIdx === 'number' && chat.dialogId && !loading) {
            scroll()
            fetchTooltipList()
        }
    }, [chat.dialogId, loading])

    useEffect(() => {
        if (chat.messages.length) {
            readChatFn()
        }
    }, [chat.messages.length])

    useEffect(() => {
        if (Object.values(chat.selectedIds).filter(b => b).length === 0) {
            setSelectedCount(0)
        }
    }, [chat.selectedIds])

    function scrollWatch() {
        if ((document.body.scrollHeight - window.scrollY) > 1100) {
            setShowScrollBtn(true)
        } else {
            setShowScrollBtn(false)
        }
    }

    useEffect(() => {
        readChatFn()
        if (!loading && config.isMobile) {
            window.addEventListener('scroll', scrollWatch)
            return () => {
                window.removeEventListener('scroll', scrollWatch)
            }
        }
    }, [loading])

    useEffect(() => {
        setMsg(chat.message)
    }, [chat.message])


    useEffect(() => {
        if (isInitialMount.current) {
            isInitialMount.current = false
        } else {
            chat.updateDialog(null, null, null)
            router.replacePage(config.isMobile ? PAGE_CHAT_MOBILE : PAGE_CHAT_DESKTOP)
        }
    }, [user.userData.id])

    const sendMessage = (text = msg, image: (string | null) = null) => {
        chat.pushNewMessageServerless({
            type: 'outgoing',
            date: new Date().toString(),
            uuid: '',
            text: text.trim(),
            read: false,
            send: false,
        } as Message, chat.dialogId ? chat.dialogId : '')
        if (!image) {
            createMessage({
                variables: {
                    input: {
                        textMessage: text.trim(),
                        chatMessage: chat.dialogId,
                    }
                },
            })
        } else {
            createMessage({
                variables: {
                    input: {
                        textMessage: text.trim(),
                        chatMessage: chat.dialogId,
                        imageMessage: image,
                    }
                },
            })
        }
        setMsg('')
        scroll()
    }

    if (loading) {
        return <Loader/>
    }

    return (
        <>
            <div className="chat-nav">
                {config.isMobile && <Link onClick={() => {
                    // chat.updateCurrentState('start')
                    router.replacePage(config.isMobile ? PAGE_CHAT_MOBILE : PAGE_CHAT_DESKTOP)
                }}><Icon28ChevronLeftOutline width={40} height={40} fill={'#a6a6a6'}
                                             onClick={() => chat.updateCurrentState('start')}/></Link>}
                <div className="col">
                    <h1>{!user.userData.isStaff ? chat.title : chat.partner}</h1>
                    <p>{!user.userData.isStaff
                        ? 'Москва'
                        : <span className={'staff'} onClick={() => {
                            router.pushPopup(POPOUT_CHAT_TYPES)
                        }}>
                      {typeof chat.dialogIdx === 'number' && ChatFilters[chat.chatList[chat.dialogIdx]?.statusChat]?.toLowerCase()}
                            <Icon20Dropdown/></span>}
                    </p>
                </div>
                <Link className={'dotsLink'} onClick={() => {
                    router.pushPopup(POPOUT_CHAT_OPTIONS)
                }}><img src={dots} alt="menu"/></Link>
            </div>
            <div
                onScroll={(e) => {
                    if (e.currentTarget.scrollHeight - e.currentTarget.scrollTop > 1000) {
                        setShowScrollBtn(true)
                    } else {
                        setShowScrollBtn(false)
                    }
                }}
                className={"messages " + (isMobile ? "mobile" : "")} id="messages" ref={messagesRef}>
                {chat.messages.map((item: Message, i: number) => {
                    if (item.type === "outgoing") {
                        return (
                            <div key={i}>
                                {item.text &&
                                    <MessageWrapper
                                        key={item.uuid}
                                        onSelect={(n: 1 | -1) => {
                                            chat.setSelectedIds({...chat.selectedIds, [item.uuid]: n === 1})
                                            setSelectedCount(selectedCount + n)
                                        }}
                                        selectType={selectedCount === 0 ? 'press' : 'click'}>
                                        <OutgoingDiv>
                                            <MessageText message={item} partner={chat.partner}/>
                                        </OutgoingDiv>
                                    </MessageWrapper>
                                }
                                { /*{item.cards &&
                    <HorizontalProductCards cards={item.cards} />
                } */}
                            </div>
                        );
                    } else if (
                        item.type === "incoming" &&
                        item.text !== "Сообщение получено"
                    ) {
                        return (
                            <div key={i}>
                                {item.text && (
                                    <MessageWrapper
                                        key={item.uuid}
                                        onSelect={(n: 1 | -1) => {
                                            chat.setSelectedIds({...chat.selectedIds, [item.uuid]: n === 1})
                                            setSelectedCount(selectedCount + n)
                                        }}
                                        selectType={selectedCount === 0 ? 'press' : 'click'}>
                                        <IncomingDiv>
                                            <MessageText message={item} partner={chat.partner}/>
                                        </IncomingDiv>
                                    </MessageWrapper>
                                )}
                                { /*{item.cards &&
                  <HorizontalProductCards cards={[item.cards]} />
                } */}

                            </div>
                        );
                    }
                })}
                {selectedCount > 0 && (
                    <div className={`selected_controls ${config.isMobile ? 'mobile' : ''} ${expanded ? 'higher' : ''}`}>
                        <div style={{cursor: 'pointer'}} onClick={() => setSelectedCount(0)}><img src={cross}
                                                                                                  alt="close"/></div>
                        <div style={{cursor: 'pointer'}}>
                            {selectedCount} {selectedCount !== 1 ? selectedCount > 4 ? 'Сообщений' : 'Сообщения' : 'Сообщенние'}</div>
                        <div style={{cursor: 'pointer'}}><img src={layers} alt="layers"/></div>
                        <div style={{cursor: 'pointer'}}><img src={back} alt=""/></div>
                        <div style={{cursor: 'pointer'}}><img src={front} alt=""/></div>
                        <div style={{cursor: 'pointer'}}
                             onClick={() => {
                                 router.pushPopup(POPOUT_DELETE_ALERT)
                             }}>
                            <img src={del} alt="delete"/>
                        </div>
                    </div>
                )}
                {showScrollBtn &&
                    <div className={`scrollDown ${selectedCount ? 'higher' : ''}`} onClick={scroll}>
                        <Icon28ChevronDownOutline fill={'#000'}/></div>}
            </div>
            <div className={config.isKeyboardActive ? "chat_inputs bottom" : "chat_inputs"}>
                <div className={`helpers ${!expanded && 'hidden'}`} style={expanded ? {
                    transform: "translateY(0)",
                    opacity: 1,
                    zIndex: 1
                } : {}}>
                    {tooltipList.map(item => (
                        <div
                            className={`helpers-item`}
                            key={item?.id}
                            onClick={() => {
                                if (expanded) {
                                    sendTooltip({
                                        variables: {
                                            input: {
                                                idChat: chat.dialogId,
                                                idTooltip: item?.id,
                                            }
                                        }
                                    }).then((data) => {
                                        if (data.errors) {
                                            console.error(data.errors)
                                            return
                                        }
                                        fetchTooltipList()
                                    }).catch(console.error)
                                    sendMessage(item?.textTooltip)
                                }
                            }}
                        >{item?.textTooltip}</div>
                    ))}
                </div>
                <img src={search} className={'search'} height={25} alt="search"/>
                <textarea className={"txtarea"}
                          placeholder={'Что вы ищите?'}
                          onBlur={() => {
                              config.updateIsKeyboardActive(false)
                              scroll()
                          }}
                          onClick={() => {
                              scroll()
                              config.isMobile && config.updateIsKeyboardActive(true)
                              // setExpanded(true)
                          }}
                          onKeyDown={(e) => {
                              if (!e.shiftKey && e.key === 'Enter') {
                                  sendMessage()
                              }
                          }}
                          onChange={(e) => {
                              e.currentTarget.style.height = e.currentTarget.scrollHeight + 'px'
                              if (!e.target.value.trim()) {
                                  e.currentTarget.style.height = '1px'
                              }
                              setMsg(e.target.value)
                          }}
                          value={msg}
                />
                <div className="icons" onContextMenu={e => e.preventDefault()}>
                    <input type="file" accept={"image/*"} hidden={true} ref={fileInput} onChange={(e) => {
                        if (e.target.files) {
                            const reader = new FileReader();
                            reader.onloadend = () => {
                                setImage(reader.result + '')
                            }
                            reader.readAsDataURL(e.target.files[0]);
                        }
                    }}/>
                    {(helpersBtn && tooltipList.length)
                        ? <img src={boxes} alt="" width={25} height={25}
                               onMouseDown={!('ontouchstart' in window || navigator.maxTouchPoints) ? () => {
                                   setPressTime(Date.now())
                                   setTimer(window.setTimeout(() => {
                                       setExpanded(!expanded)
                                   }, 150))
                               } : () => {
                               }}
                               onTouchStart={'ontouchstart' in window || navigator.maxTouchPoints ? () => {
                                   setPressTime(Date.now())
                                   setTimer(window.setTimeout(() => {
                                       setExpanded(!expanded)
                                   }, 150))
                               } : () => {
                               }}
                               onClick={() => {
                                   if (Date.now() - (pressTime || 0) < 150) {
                                       clearTimeout(timer || undefined)
                                       setTimer(null)
                                       setHelpersBtn(false)
                                   }
                                   setPressTime(null)
                               }}
                        />
                        : <img src={shablon} alt="" width={25} height={25}
                               onMouseDown={!('ontouchstart' in window || navigator.maxTouchPoints) ? () => {
                                   setPressTime(Date.now())
                                   setTimer(window.setTimeout(() => {
                                       chat.updateHelperModal(helperModal.TEMPLATES)
                                       router.pushModal(MODAL_CHAT_COMMANDS)
                                   }, 150))
                               } : () => {
                               }}
                               onTouchStart={'ontouchstart' in window || navigator.maxTouchPoints ? () => {
                                   setPressTime(Date.now())
                                   setTimer(window.setTimeout(() => {
                                       chat.updateHelperModal(helperModal.TEMPLATES)
                                       router.pushModal(MODAL_CHAT_COMMANDS)
                                   }, 150))
                               } : () => {
                               }}
                               onClick={() => {
                                   if (Date.now() - (pressTime || 0) < 150) {
                                       clearTimeout(timer || undefined)
                                       setTimer(null)
                                       setHelpersBtn(true)
                                   }
                                   setPressTime(null)
                               }}
                        />
                    }
                    <img src={slash} alt="cmd" width={25} height={25} onClick={() => {
                        chat.updateHelperModal(helperModal.COMMANDS)
                        router.pushModal(MODAL_CHAT_COMMANDS)
                    }}/>
                    {user.userData.isStaff || user.userData.isOperator && <div
                        onClick={() => {
                            if (config.isMobile) {
                                router.pushModal(MODAL_CREATE_CARD)
                            } else {
                                router.pushPage(PAGE_CHAT_DESKTOP)
                                chat.updateCurrentState('createCard')
                            }
                        }}
                        style={{
                            boxSizing: 'border-box',
                            width: 25,
                            height: 25,
                            border: '1px solid #A6A6A6',
                            borderRadius: 5,
                            display: "flex",
                            justifyContent: 'center',
                            alignItems: 'center',
                            cursor: 'pointer'
                        }}>
                        <img src={skrepka} alt="add" height={15}/>
                    </div>}
                    <div className="vl"/>
                    <img src={camera} alt="add" height={25} style={{paddingRight: 5}}/>
                </div>
            </div>
        </>
    );
})

export default Chat
