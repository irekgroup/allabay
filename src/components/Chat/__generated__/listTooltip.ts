/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: listTooltip
// ====================================================

export interface listTooltip_listTooltip {
  __typename: "TooltipType";
  id: string;
  idTooltip: number;
  textTooltip: string;
  isDelete: boolean;
}

export interface listTooltip {
  listTooltip: (listTooltip_listTooltip | null)[] | null;
}

export interface listTooltipVariables {
  input: string;
}
