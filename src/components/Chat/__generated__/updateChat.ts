/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ChatChangeInput } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL mutation operation: updateChat
// ====================================================

export interface updateChat_updateChat {
  __typename: "UpdateChat";
  ok: boolean | null;
}

export interface updateChat {
  updateChat: updateChat_updateChat | null;
}

export interface updateChatVariables {
  input: ChatChangeInput;
}
