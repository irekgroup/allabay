/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: read_chat
// ====================================================

export interface read_chat_readChat {
  __typename: "ReadChat";
  ok: boolean | null;
}

export interface read_chat {
  readChat: read_chat_readChat | null;
}

export interface read_chatVariables {
  idChat: string;
}
