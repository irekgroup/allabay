/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { SendTooltipInput } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL mutation operation: sendTooltip
// ====================================================

export interface sendTooltip_sendTooltip {
  __typename: "SendTooltip";
  ok: boolean | null;
}

export interface sendTooltip {
  sendTooltip: sendTooltip_sendTooltip | null;
}

export interface sendTooltipVariables {
  input: SendTooltipInput;
}
