/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: fetch_subcategories
// ====================================================

export interface fetch_subcategories_subcategories {
  __typename: "SubcategoryType";
  id: string;
  name: string;
}

export interface fetch_subcategories {
  subcategories: (fetch_subcategories_subcategories | null)[] | null;
}

export interface fetch_subcategoriesVariables {
  parent: string;
}
