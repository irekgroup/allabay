/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: fetch_types
// ====================================================

export interface fetch_types_types {
  __typename: "TypeType";
  id: string;
  name: string;
}

export interface fetch_types {
  types: (fetch_types_types | null)[] | null;
}

export interface fetch_typesVariables {
  parent: string;
}
