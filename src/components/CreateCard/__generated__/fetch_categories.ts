/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: fetch_categories
// ====================================================

export interface fetch_categories_categories_subcategorySet {
  __typename: "SubcategoryType";
  id: string;
  name: string;
}

export interface fetch_categories_categories {
  __typename: "CategoryType";
  id: string;
  name: string;
  subcategorySet: fetch_categories_categories_subcategorySet[];
}

export interface fetch_categories {
  categories: (fetch_categories_categories | null)[] | null;
}
