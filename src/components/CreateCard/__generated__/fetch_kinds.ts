/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: fetch_kinds
// ====================================================

export interface fetch_kinds_kinds_typeSet {
  __typename: "TypeType";
  id: string;
  name: string;
}

export interface fetch_kinds_kinds {
  __typename: "KindType";
  id: string;
  name: string;
  typeSet: fetch_kinds_kinds_typeSet[];
}

export interface fetch_kinds {
  kinds: (fetch_kinds_kinds | null)[] | null;
}

export interface fetch_kindsVariables {
  parent: string;
}
