import React, { ChangeEvent, FC, useContext, useEffect, useRef, useState } from 'react';
import './CreateCard.scss';
import { FormItem, FormLayout, FormLayoutGroup, Link, NativeSelect } from "@vkontakte/vkui";
import Input from "../Input/Input";
import { Icon12Add, Icon12View, Icon28ChevronLeftOutline, Icon28ChevronRightOutline } from "@vkontakte/icons";
import Button from "../Button/Button";
import calender from '../../img/chat/calendar.svg';
import skrepka from '../../img/chat/skrepka.svg';
import addPhoto from '../../img/chat/addPhoto.svg';
import closeIcon from '../../img/main/closer-icon.svg';
import { MODAL_CREATE_CARD_CONFIRM, POPOUT_CREATE_CARD_ALERT } from "../../router";
import { useRouter } from "@happysanta/router";
import { RootStoreContext } from "../../index";
import CardItem from "../CardItem/CardItem";
import { CreateCardTabs, IPhoto, IProductInfo } from "../../mobX/CreateCard";
import { observer } from "mobx-react";
import { gql, useLazyQuery } from "@apollo/client";
import {
    fetch_categories,
    fetch_categories_categories_subcategorySet
} from "./__generated__/fetch_categories";
import { fetch_kinds, fetch_kinds_kinds_typeSet, fetch_kindsVariables } from "./__generated__/fetch_kinds";

export const FETCH_CATEGORIES = gql`
    query fetch_categories {
        categories{
            id
            name
            subcategorySet{
                id
                name
            }
        }
    }
`

export const FETCH_KINDS = gql`
    query fetch_kinds($parent: String!){
        kinds(parent: $parent){
            id
            name
            typeSet{
                id
                name
            }
        }
    }
`

// export const FETCH_TYPES = gql`
//     query fetch_types($parent: String!){
//         types(parent: $parent){
//             id
//             name
//         }
//     }
// `
//
// export const FETCH_SUBCATEGORIES = gql`
//     query fetch_subcategories($parent: String!) {
//         subcategories(parent: $parent){
//             id
//             name
//         }
//     }
// `

const CreateCard: FC = observer(() => {
    const [productInfo, setProductInfo] = useState<IProductInfo>({})
    const [photoUrl, setPhotoUrl] = useState('')
    const [subcategories, setSubcategories] = useState<fetch_categories_categories_subcategorySet[]>([])
    const [types, setTypes] = useState<fetch_kinds_kinds_typeSet[]>([])

    const fileInputRef = useRef<HTMLInputElement>(null)
    const modalFS = useRef<HTMLDivElement>(null)
    const modalSS = useRef<HTMLDivElement>(null)

    const { config, createCard } = useContext(RootStoreContext)
    const router = useRouter()

    const [fetchCategories, categories] = useLazyQuery<fetch_categories>(FETCH_CATEGORIES)
    const [fetchKinds, kinds] = useLazyQuery<fetch_kinds, fetch_kindsVariables>(FETCH_KINDS)
    // const [fetchTypes, types] = useLazyQuery<fetch_types, fetch_typesVariables>(FETCH_TYPES)

    function onMouseDown(e: TouchEvent) {
        e.stopPropagation()
    }

    useEffect(() => {
        if(categories.data?.categories && productInfo.category){
            const cat = categories.data?.categories.find(i => i && i.name === productInfo.category)
            setSubcategories(cat ? cat.subcategorySet : [])
        }
    }, [productInfo.category])

    useEffect(() => {
        fetchKinds({
            variables: {
                parent: productInfo.subcategory || '',
            },
        })
            .catch(console.error)
    }, [productInfo.subcategory])

    useEffect(() => {
        if(kinds.data?.kinds){
            const cat = kinds.data?.kinds.find(i => i && i.name === productInfo.kind)
            setTypes(cat ? cat.typeSet : [])
        }
        // fetchTypes({
        //     variables: {
        //         parent: productInfo.kind || '',
        // },
        // }).catch(console.error)
    }, [productInfo.kind])

    useEffect(() => {
        fetchCategories().catch(console.error)
        createCard.getFromLocalStorage()
        createCard.setTab(CreateCardTabs.FIRST_SCREEN)
        // createCard.clear()
        if (modalFS.current && modalSS.current) {
            modalFS.current.addEventListener("touchend", onMouseDown, { capture: true })
            modalSS.current.addEventListener("touchend", onMouseDown, { capture: true })
            return () => {
                modalFS.current && modalFS.current.removeEventListener('touchend', onMouseDown)
                modalSS.current && modalSS.current.removeEventListener('touchend', onMouseDown)
            }
        }
    }, [])

    useEffect(() => {
        setProductInfo(createCard.cardIndex < createCard.cardList.length
            ? createCard.cardList[createCard.cardIndex]
            : {number: ''+Math.floor(Math.random() * (9999999999 - 1000 + 1) + 1000),})
    }, [createCard.cardList, createCard.cardIndex])

    const offsetIndex = (n: 1 | -1) => {
        createCard.setCard(productInfo)
        createCard.offsetIndex(n)
    }

    const setPhoto = (data: IPhoto) => {
        if (!productInfo.photo || productInfo.photo?.length < 6) {
            setProductInfo(prev => {
                return {
                    ...prev,
                    photo: prev.photo?.length ? [...prev.photo, data,] : [data,],
                }
            })
        }
    }

    return (
        <div className={'createCard'}>
            <div className="chat-header-text" style={{
                width: 'calc(100% + 30px)',
                marginBottom: 0,
                marginTop: '-10px',
                marginLeft: '-15px',
                marginRight: '-15px',
            }}>
                <h3>Создание карточки</h3>
            </div>
            <div>
                {createCard.tab === CreateCardTabs.FIRST_SCREEN ?
                    <div className={'firstScreen'} ref={config.isMobile ? modalFS : null}>
                        <p>Первичная информация о поставщике</p>
                        <FormLayout>
                            <FormItem>
                                <Input onChange={e => setProductInfo({
                                    ...productInfo,
                                    seller: {
                                        ...productInfo.seller,
                                        link: e.target.value,
                                    },
                                })} value={productInfo.seller?.link || ''} placeholder={'*Ссылка на предложение'}/>
                            </FormItem>
                            <FormItem>
                                <Input onChange={e => setProductInfo({
                                    ...productInfo,
                                    seller: {
                                        ...productInfo.seller,
                                        city: e.target.value,
                                    },
                                })} value={productInfo.seller?.city || ''} placeholder={'*Город поставщика'}/>
                            </FormItem>
                            <FormItem>
                                <Input onChange={e => setProductInfo({
                                    ...productInfo,
                                    seller: {
                                        ...productInfo.seller,
                                        tel: e.target.value,
                                    },
                                })} value={productInfo.seller?.tel || ''} placeholder={'*Номер телефона поставщика'}/>
                            </FormItem>
                            <FormItem>
                                <Input onChange={e => setProductInfo({
                                    ...productInfo,
                                    seller: {
                                        ...productInfo.seller,
                                        article: e.target.value,
                                    },
                                })} value={productInfo.seller?.article || ''} placeholder={'*Артикул'}/>
                            </FormItem>
                            <FormItem>
                                <Input onChange={e => setProductInfo({
                                    ...productInfo,
                                    seller: {
                                        ...productInfo.seller,
                                        cost: e.target.value,
                                    },
                                })} value={productInfo.seller?.cost || ''} placeholder={'*Стоимость покупки'}/>
                            </FormItem>
                        </FormLayout>
                        <p>Информация о доставке</p>
                        <FormLayout>
                            <FormItem>
                                <Input onChange={e => setProductInfo({
                                    ...productInfo,
                                    sendInf: {
                                        ...productInfo.sendInf,
                                        address: e.target.value,
                                    },
                                })} value={productInfo.sendInf?.address || ''} placeholder={'Адрес местонахождения товара'}/>
                            </FormItem>
                            <FormItem>
                                <Input onChange={e => setProductInfo({
                                    ...productInfo,
                                    sendInf: {
                                        ...productInfo.sendInf,
                                        tel: e.target.value,
                                    },
                                })} value={productInfo.sendInf?.tel || ''} placeholder={'Номер телефона курьера'}/>
                            </FormItem>
                            <FormItem>
                                <Input onChange={e => setProductInfo({
                                    ...productInfo,
                                    sendInf: {
                                        ...productInfo.sendInf,
                                        cost: e.target.value,
                                    },
                                })} value={productInfo.sendInf?.cost || ''} placeholder={'Стоимость доставки'}/>
                            </FormItem>
                            <FormItem>
                                <Input onChange={e => setProductInfo({
                                    ...productInfo,
                                    sendInf: {
                                        ...productInfo.sendInf,
                                        operatorAddress: e.target.value,
                                    },
                                })} value={productInfo.sendInf?.operatorAddress || ''} placeholder={'Адрес оператора перевозки'}/>
                                <p style={{ color: "#707070" }}>Первичная информация о поставщике не отображается в
                                    карточке
                                    товара</p>
                                <p>Пункты отмеченные (*), обязательны к заполнению</p>
                            </FormItem>
                        </FormLayout>
                        <div className="controlBtns">
                            {createCard.cardIndex >= 1 ?
                                <Link
                                    className={'nextBtn'}
                                    onClick={() => {
                                        offsetIndex(-1)
                                    }}>
                                    <Icon28ChevronLeftOutline/>Карточка {createCard.cardIndex}
                                </Link> : <div/>}
                            <Link
                                className={'nextBtn'}
                                onClick={() => createCard.setTab(CreateCardTabs.SECOND_SCREEN)}>
                                Далее<Icon28ChevronRightOutline/>
                            </Link>
                        </div>

                    </div>
                    : createCard.tab === CreateCardTabs.SECOND_SCREEN ?
                        <div className={'cardInfo secondScreen'} ref={config.isMobile ? modalSS : null}>
                            <FormLayout>
                                <FormItem>
                                    <p>Сведения о товаре</p>
                                    <Input placeholder={'Наименование'} value={productInfo.title || ''} onChange={e => {
                                        setProductInfo({
                                            ...productInfo,
                                            title: e.target.value
                                        })
                                    }}/>
                                </FormItem>
                                <FormItem>
                                    <Input placeholder={'Бренд'} value={productInfo.brand || ''} onChange={e => {
                                        setProductInfo({
                                            ...productInfo,
                                            brand: e.target.value
                                        })
                                    }}/>
                                </FormItem>
                                <FormItem>
                                    <input type="file" ref={fileInputRef} multiple accept={"image/*"} hidden  onChange={(e) => {
                                        if(e.target.files){
                                            const count = e.target.files.length <= 6 ? e.target.files.length : 6
                                            for (let i = 0; i < count; i++) {
                                                const reader = new FileReader();
                                                const title = e.target.files[i].name
                                                reader.onloadend = () => {
                                                    setPhoto({title, url: '' + reader.result})
                                                }
                                                reader.readAsDataURL(e.target.files[i]);
                                            }
                                        }
                                    }}/>
                                    <div
                                        onKeyDown={e => {
                                            if (e.key === 'Enter') {
                                                setPhoto({
                                                    url: photoUrl,
                                                    title: 'photo from url',
                                                })
                                                setPhotoUrl('')
                                            }
                                        }}
                                        onPaste={e => {
                                            if(e.clipboardData.files[0]) {
                                                const reader = new FileReader();
                                                const title = e.clipboardData.files[0].name
                                                reader.onloadend = () => {
                                                    setPhoto({ title, url: '' + reader.result })
                                                }
                                                reader.readAsDataURL(e.clipboardData.files[0]);
                                            }
                                        }}>
                                        <Input
                                            before={
                                                <img
                                                    src={skrepka}
                                                    style={{
                                                        marginLeft: '10px',
                                                        height: '15px',
                                                    }}
                                                />
                                            }
                                            value={photoUrl || ''}
                                            placeholder={'Прикрепите фото вставив ссылку'}
                                            onChange={(e) => {
                                                setPhotoUrl(e.target.value)
                                            }}
                                        />
                                        <div className="photos">
                                            {productInfo.photo?.map((item, i) => {
                                                return <div className={'photo'} key={i}>
                                                    <img src={closeIcon} alt="close" className="close" onClick={() => {
                                                        setProductInfo({
                                                            ...productInfo,
                                                            photo: [...(productInfo.photo || []).slice(0, i),...(productInfo.photo || []).slice(i + 1)]
                                                        })
                                                    }}/>
                                                    <img src={item.url} alt={item.title}/>
                                                </div>
                                            })}
                                            {(!productInfo.photo || productInfo.photo?.length < 6) &&
                                                <div className="photo addPhoto"
                                                     onClick={() => {
                                                         fileInputRef.current && fileInputRef.current.click()
                                                     }}>
                                                    <img src={addPhoto} alt="add"/>
                                                </div>}
                                        </div>
                                    </div>
                                </FormItem>
                                <FormItem>
                                    <Input placeholder={'Старая цена'} value={productInfo.prevCost || ''} onChange={e => {
                                        const replaced = e.target.value.replace(' ','').replace(',', '.')
                                        if(!isNaN(+replaced)) {
                                            setProductInfo({
                                                ...productInfo,
                                                prevCost: e.target.value.replace(' ','').trim()
                                            })
                                        }
                                    }}/>
                                </FormItem>
                                <FormItem className={'mb6'}>
                                    <Input placeholder={'Новая цена'} value={productInfo.newCost || ''} onChange={e => {
                                        const replaced = e.target.value.replace(' ','').replace(',', '.')
                                        if(!isNaN(+replaced)) {
                                            setProductInfo({
                                                ...productInfo,
                                                newCost: e.target.value.replace(' ','').trim()
                                            })
                                        }
                                    }}/>
                                </FormItem>
                                <FormLayoutGroup mode={'horizontal'}>
                                    <FormItem>
                                        <DateInput value={productInfo.timeFrom || '' } placeholder={'Срок доставки с'} onChange={(e:any) => {
                                            setProductInfo({
                                                ...productInfo,
                                                timeFrom: e?.target.value
                                            })
                                        }}/>
                                    </FormItem>
                                    <FormItem>
                                        <DateInput value={productInfo.timeTo || ''} placeholder={'Срок доставки по'} onChange={(e:any) => {
                                            setProductInfo({
                                                ...productInfo,
                                                timeTo: e?.target.value
                                            })
                                        }}/>
                                    </FormItem>
                                </FormLayoutGroup>
                                <FormLayoutGroup mode={'horizontal'}>
                                    <FormItem>
                                        <DateInput value={productInfo.guaranteePeriod || ''} placeholder={'Гарантийный срок'} onChange={(e:any) => {
                                            setProductInfo({
                                                ...productInfo,
                                                guaranteePeriod: e?.target.value
                                            })
                                        }}/>
                                    </FormItem>
                                    <FormItem>
                                        <NativeSelect placeholder={'Состояние'} value={productInfo.condition || ''} onChange={e => {
                                            setProductInfo({
                                                ...productInfo,
                                                condition: e.target.value
                                            })
                                        }}>
                                            <option value="Новое">Новое</option>
                                            <option value="Бу">Бу</option>
                                        </NativeSelect>
                                    </FormItem>
                                </FormLayoutGroup>
                                <FormItem>
                                    <NativeSelect style={{margin: '6px 0'}} placeholder={'Способ получения'} value={productInfo.sendType || ''} onChange={e => {
                                        setProductInfo({
                                            ...productInfo,
                                            sendType: e.target.value,
                                        })
                                    }}>
                                        <option value='Самовывоз в ПВЗ в день заказа до 12:00'>Самовывоз в ПВЗ в день заказа до 12:00</option>
                                        <option value="Курьерская доставка до ПВЗ за 1 час">Курьерская доставка до ПВЗ за 1 час</option>
                                        <option value="Курьерская доставка до адреса за 1 час ">Курьерская доставка до адреса за 1 час </option>
                                        <option value="Курьерская доставка до ПВЗ за 1-2 дня">Курьерская доставка до ПВЗ за 1-2 дня</option>
                                        <option value="Самовывоз из ПВЗ доставка 5-7 дней">Самовывоз из ПВЗ доставка</option>
                                        <option value="Самовывоз из Почта Росии 5-7 дней">Самовывоз из Почта Росии</option>
                                        <option value="Курьерская, до адреса, Почта Росии 5-7 дней">Курьерская, до адреса, Почта Росии</option>
                                        <option value="Самовывоз в ТК (ПЭК, СДЕК, ДЕЛОВЫЕ ЛИНИИ и т.д.)">Самовывоз в ТК (ПЭК, СДЕК, ДЕЛОВЫЕ ЛИНИИ и т.д.)</option>
                                        <option value="Курьерская до адреса с ТК (ПЭК, СДЕК, ДЕЛОВЫЕ ЛИНИИ и т.д.)">Курьерская до адреса с ТК (ПЭК, СДЕК, ДЕЛОВЫЕ ЛИНИИ и т.д.)</option>
                                    </NativeSelect>
                                </FormItem>
                                <FormLayoutGroup mode={'horizontal'}>
                                    <FormItem>
                                        <NativeSelect placeholder={'Категория'} value={productInfo.category || ''} onChange={e => {
                                            setProductInfo({
                                                ...productInfo,
                                                category: e.target.value,
                                            })
                                        }}>
                                            {categories.data?.categories && categories.data?.categories.map(cat => (
                                                <option value={cat?.name} key={cat?.id}>{cat?.name}</option>
                                            ))}
                                        </NativeSelect>
                                    </FormItem>
                                    <FormItem>
                                        <NativeSelect
                                            placeholder={'Подкатегория'} value={productInfo.subcategory || ''}
                                            onChange={e => {
                                                setProductInfo({
                                                    ...productInfo,
                                                    subcategory: e.target.value,
                                                })
                                            }}>
                                            {subcategories.map((item) => (
                                                <option value={item?.name} key={item?.id}>{item?.name}</option>
                                            ))}
                                        </NativeSelect>
                                    </FormItem>
                                </FormLayoutGroup>
                                <FormLayoutGroup mode={'horizontal'}>
                                    <FormItem>
                                        <NativeSelect placeholder={'Вид'} value={productInfo.kind || ''} onChange={e => {
                                            setProductInfo({
                                                ...productInfo,
                                                kind: e.target.value,
                                            })
                                        }}>
                                            {kinds.data?.kinds && kinds.data.kinds.map(item => (
                                                <option value={item?.name} key={item?.id}>{item?.name}</option>
                                            ))}
                                        </NativeSelect>
                                    </FormItem>
                                    <FormItem>
                                        <NativeSelect placeholder={'Тип'} value={productInfo.type || ''} onChange={e => {
                                            setProductInfo({
                                                ...productInfo,
                                                type: e.target.value,
                                            })
                                        }}>
                                            {types.map(item => (
                                                <option value={item?.name} key={item?.id}>{item?.name}</option>
                                            ))}
                                        </NativeSelect>
                                    </FormItem>
                                </FormLayoutGroup>
                                <FormItem>
                                    <Input placeholder={'Номер карточки'} value={productInfo.number ||  ''+Math.floor(Math.random() * (9999999999 - 1000 + 1) + 1000)} disabled/>
                                </FormItem>
                                <FormItem>
                                    <textarea placeholder={'Описание'} value={productInfo.description || ''} onChange={e => {
                                        e.currentTarget.style.height = e.currentTarget.scrollHeight + 'px'
                                        if(!e.target.value.trim()){
                                            e.currentTarget.style.height = '1px'
                                        }
                                        setProductInfo({
                                            ...productInfo,
                                            description: e.currentTarget.value
                                        })
                                    }}/>
                                </FormItem>
                            </FormLayout>
                            <div className="row">
                                <Link onClick={() => {
                                    createCard.pushCard(productInfo)
                                    offsetIndex(1)
                                    createCard.setTab(CreateCardTabs.FIRST_SCREEN)
                                }}><Icon12Add/>Добавить карточку</Link>
                                <Link onClick={() => {
                                    createCard.setTab(CreateCardTabs.PREVIEW)
                                    createCard.setCard(productInfo)
                                    // createCard.cardList[createCard.cardIndex] = productInfo
                                }}><Icon12View/>Предпросмотр</Link>
                                <Button mode={'secondary'} onClick={() => {
                                    console.log('click send')
                                    !config.isMobile ? router.pushPopup(POPOUT_CREATE_CARD_ALERT) : router.pushModal(MODAL_CREATE_CARD_CONFIRM)
                                }}>
                                    Отправить
                                </Button>
                            </div>
                            <div className="row" style={{marginBottom: 10}}>
                                <Link style={{zIndex: 1000}} onClick={() => {
                                    // createCard.pushCard(productInfo)
                                    createCard.pushCard({ ...productInfo, number: ''+Math.floor(Math.random() * (9999999999 - 1000 + 1) + 1000), })
                                    offsetIndex(1)
                                    createCard.setTab(CreateCardTabs.FIRST_SCREEN)
                                }}>
                                    Скопировать карточку
                                </Link>
                            </div>
                            <div className="controlBtns">
                                {createCard.cardIndex >= 1 ?
                                    <Link
                                        className={'nextBtn'}
                                        onClick={() => {
                                            offsetIndex(-1)
                                        }}>
                                        <Icon28ChevronLeftOutline/>Карточка {createCard.cardIndex}
                                    </Link> : <div/>}
                                <div className={'centerControlBtns'}>
                                    <Link onClick={() => {
                                        createCard.setTab(CreateCardTabs.FIRST_SCREEN)
                                    }}>
                                        Редактировать
                                    </Link>
                                    {createCard.cardList.length > 0 &&
                                        <Link
                                            className={'nextBtn'}
                                            style={{ color: '#E21B1B' }}
                                            onClick={() => {
                                                createCard.removeCurrCard()
                                                setProductInfo({})
                                                if(createCard.cardIndex === createCard.cardList.length){
                                                    createCard.offsetIndex(-1)
                                                }
                                            }}>
                                            Удалить
                                        </Link>}
                                </div>
                                {createCard.cardIndex < createCard.cardList.length - 1 ?
                                    <Link
                                        className={'nextBtn'}
                                        onClick={() => {
                                            offsetIndex(1)
                                        }}>
                                        Карточка {createCard.cardIndex + 2}<Icon28ChevronRightOutline/>
                                    </Link> : <Link className={'nextBtn'}/>}
                            </div>
                        </div>
                        :
                        <div className={'cardPreview'}>
                            <p style={{ textAlign: 'center', marginTop: 20}}>Предпросмотр</p>
                            <div className="controlBtns">
                                {createCard.cardIndex >= 1 &&
                                    <Link
                                        className={'nextBtn'}
                                        onClick={() => {
                                            offsetIndex(-1)
                                        }}>
                                        <Icon28ChevronLeftOutline/>Карточка {createCard.cardIndex}
                                    </Link>}
                                {createCard.cardIndex < createCard.cardList.length - 1 ?
                                    <Link
                                        className={'nextBtn'}
                                        onClick={() => {
                                            offsetIndex(1)
                                        }}>
                                        Карточка {createCard.cardIndex + 2}<Icon28ChevronRightOutline/>
                                    </Link> : <Link className={'nextBtn'}/>}
                            </div>
                            <CardItem product={productInfo}/>
                            <div className="row">
                                <Link
                                    onClick={() => createCard.setTab(CreateCardTabs.FIRST_SCREEN)}>Редактировать</Link>
                                <Button mode={'secondary'} onClick={() => {
                                    !config.isMobile ? router.pushPopup(POPOUT_CREATE_CARD_ALERT) : router.pushModal(MODAL_CREATE_CARD_CONFIRM)
                                }}>
                                    Отправить
                                </Button>
                            </div>
                        </div>
                }
            </div>
        </div>
    );
});

const DateInput = ({ placeholder = '', onChange, value }: { placeholder?: string, onChange?: (e?: ChangeEvent) => void, value?: string}) => (
    <div className="input-container" style={{ margin: 0, }}>
        <img src={calender} alt="calendar" style={{ marginLeft: 10 }}/>
        <input
            className={'input-main'}
            type={!value ? "text" : 'date'}
            placeholder={placeholder}
            onFocus={e => e.target.type = 'date'}
            value={value}
            onChange={e => {
                if (!e.currentTarget.value) {
                    e.target.type = 'text'
                }
                onChange && onChange(e)
            }
            }/>
    </div>
)

export default CreateCard;
