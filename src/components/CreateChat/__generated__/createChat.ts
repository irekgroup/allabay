/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ChatCreateInput } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL mutation operation: createChat
// ====================================================

export interface createChat_createChat_chat_user1 {
  __typename: "UserType";
  id: string;
  firstName: string;
  lastName: string;
}

export interface createChat_createChat_chat_user2 {
  __typename: "UserType";
  id: string;
  firstName: string;
  lastName: string;
}

export interface createChat_createChat_chat {
  __typename: "ChatType";
  id: string;
  nameChat: string;
  user1: createChat_createChat_chat_user1;
  user2: createChat_createChat_chat_user2 | null;
}

export interface createChat_createChat {
  __typename: "CreateChat";
  chat: createChat_createChat_chat | null;
  ok: boolean | null;
}

export interface createChat {
  createChat: createChat_createChat | null;
}

export interface createChatVariables {
  input: ChatCreateInput;
}
