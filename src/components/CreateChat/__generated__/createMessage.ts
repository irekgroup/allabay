/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MessageCreateInput } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL mutation operation: createMessage
// ====================================================

export interface createMessage_createMessage {
  __typename: "CreateMessage";
  ok: boolean | null;
}

export interface createMessage {
  createMessage: createMessage_createMessage | null;
}

export interface createMessageVariables {
  input: MessageCreateInput;
}
