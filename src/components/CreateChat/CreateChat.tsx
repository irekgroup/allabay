import Input from "../Input/Input";
import "./CreateChat.scss";
import chatIcon from "../../img/chat/chat-icon.svg";
import { Link, Spinner } from "@vkontakte/vkui";
import Button from "../Button/Button";
import { gql, useApolloClient, useMutation } from "@apollo/client";
import { createChat, createChatVariables } from "./__generated__/createChat";
import React, { useContext, useEffect, useState } from "react";
import { RootStoreContext } from "../../index";
import { useRouter } from "@happysanta/router";
import { PAGE_CHAT_DESKTOP, PAGE_CHAT_MOBILE } from "../../router";

export const CREATE_CHAT = gql`
mutation createChat($input: ChatCreateInput!){
    createChat(input: $input){
        chat {
            id
            nameChat
            user1{
                id
                firstName
                lastName
            }
            user2{
                id
                firstName
                lastName
            }
        }
        ok
    }
}`

export const CREATE_MESSAGE = gql`
mutation createMessage($input: MessageCreateInput!){
    createMessage(input: $input){
        ok
    }
}
`

const CreateChat = () => {
    const {user, chat, config} = useContext(RootStoreContext);

    const [name, setName] = useState('')
    const [loading, setLoading] = useState(false)
    const [showErrors, setShowErrors] = useState(false)

    const apollo = useApolloClient();
    const [createChat, createdChat] = useMutation<createChat, createChatVariables>(CREATE_CHAT);

    const router = useRouter();

    useEffect(()=>{
        if(createdChat.error){
            console.error(createdChat.error)
            return
        }
        if(!createdChat.loading && createdChat.data?.createChat?.ok && createdChat.data?.createChat?.chat){
            const partner = createdChat.data.createChat?.chat.user1.id === user.userData.id
                ? `${createdChat.data.createChat?.chat.user2?.firstName ?? 'Оператор'} ${createdChat.data.createChat.chat.user2?.lastName ?? ''}`
                : `${createdChat.data.createChat?.chat.user1.firstName} ${createdChat.data.createChat.chat.user1.lastName}`;
            chat.updateDialog(createdChat.data.createChat?.chat.id, createdChat.data.createChat.chat.nameChat, 0, partner)
            router.replacePage(!config.isMobile ? PAGE_CHAT_DESKTOP : PAGE_CHAT_MOBILE, {id: createdChat.data?.createChat?.chat.id})
        }
    }, [createdChat.loading])

    return (
        <>
            <div className="chat-header-text">
                <h3>Создание чата</h3>
            </div>
            <div className="create-chat">
                <div className="create-chat-icon">
                    <img src={chatIcon} alt="Иконка" />
                </div>
                <div className="create-chat-content">
                    <Input descr={'Название слишком длинное'} type={'text'} status={'error'} showErrors={showErrors}  value={name} onChange={(e) => {
                        if(e.target.value.trim().length >= 300){
                            setShowErrors(true)
                        } else {
                            setShowErrors(false)
                        }
                        setName(e.target.value)
                    }} placeholder="Название чата" />
                    <p>
                        Введите название и при желании загрузите фотографию.
                        Название беседы отразится в списке чатов, это ваш
                        поисковый запрос, например, вы ищите маршрут
                        Мосвка-Владивосток, напишите в названии
                        Мосвка-Владивосток или вы ищите Смартфон Apple iPhone 12
                        Pro 128 ГБ RU, тихоокеанский синий. Напишите в названии
                        Смартфон Apple iPhone 12 Pro 128 ГБ RU, тихоокеанский
                        синий
                    </p>
                    <Input placeholder="Москва" />
                    <p>
                        Выберите город доставки или установите город по
                        умолчанию в{" "}
                        <Link hasHover={true} hasActive={true}>
                            {" "}
                            личном кабинете
                        </Link>
                        .
                    </p>
                    <Button
                        disabled={loading || showErrors}
                        mode="secondary"
                        onClick={() => {
                            setLoading(true)
                            createChat({
                                variables:{
                                    input: {
                                        messageAllabay: true,
                                        userRecipient: false,
                                        nameChat: name,
                                    },
                                },
                                context: {
                                    headers: {
                                        Authorization: user.getToken(),
                                    }
                                },
                            })
                                .then(() => {
                                    setLoading(false)
                                    chat.updateCurrentState('start')
                                })
                                .catch(console.error)
                        }}
                    > {loading ? <Spinner size="small"/> : 'Создать' }</Button>
                </div>
            </div>
        </>
    );
};

export default CreateChat;
