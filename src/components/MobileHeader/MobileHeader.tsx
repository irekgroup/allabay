import React, { FC, useContext, useState } from 'react';
import './MobileHeader.scss';
import { IconButton, Link } from "@vkontakte/vkui";
import searchIcon from "../../img/main/search.png";
import BigLogo from "../../img/main/biglogo.png";
import Input from "../Input/Input";
import logo from "../../img/main/logo.svg";
import cameraIcon from "../../img/main/camera.png";
import newIcon from "../../img/main/new-icon.svg";
import closerIcon from "../../img/main/closer-icon.svg";
import filter from "../../img/main/filter.svg";
import compas from '../../img/feed/compas.svg';
import { useRouter } from "@happysanta/router";
import { RootStoreContext } from "../../index";
import { PAGE_FEED_DESKTOP, PAGE_FEED_MOBILE } from "../../router";

interface MobileHeaderProps {
    showSearchDefault?: boolean;
    isSearch?: boolean;
    isRightSearch?: boolean;
    onChange?: (e: any) => void;
    onClickFilter?: () => void;
    onInteresting?: () => void;
    onBack?: () => void;
}

const MobileHeader: FC<MobileHeaderProps> = ({
                                                 showSearchDefault = false,
                                                 onClickFilter,
                                                 onChange,
                                                 isSearch = true,
                                                 onInteresting,
                                                 isRightSearch,
                                                 onBack,
                                             }) => {
    const [showSearch, setShowSearch] = useState(showSearchDefault)
    const [showTip, setShowTip]: any = useState(false);
    const [showCloser, setShowCloser]: any = useState(false);
    const [showNewIcon, setShowNewIcon]: any = useState(false);
    const router = useRouter()
    const { config } = useContext(RootStoreContext)
    return (
        <div className={'mobileHeader'}>
            {!showSearch ?
                <div className={`header-logo ${!isSearch ? 'interesting' : ''}`}>
                    <Link className={`searchBtn ${!isSearch ? 'intIcon' : ''}`} onClick={() => {
                        if(onInteresting){
                            onInteresting()
                            setShowSearch(true)
                        } else {
                            setShowSearch(true)
                        }
                    }}>
                        <img src={isSearch ? searchIcon : compas} alt="search" className={'searchIcon'}/>
                        {!isSearch && <p>Интересное</p>}
                    </Link>
                    <Link onClick={() => router.pushPage(config.isMobile ? PAGE_FEED_MOBILE : PAGE_FEED_DESKTOP)}>
                        <img src={BigLogo} alt="logo" className={'logo'}/>
                    </Link>
                    {isRightSearch && <Link className={'searchBtn right'} onClick={() => setShowSearch(true)}>
                        <img src={searchIcon} alt="search" className={'searchIcon'}/>
                    </Link>}
                </div> :
                <div className={'header-logo space'}>
                    <Input placeholder={'Что вы ищете?'} before={<img src={logo} alt={'logo'}/>} onChange={onChange}/>
                    <IconButton title="Поиск по фото" className={'iconBtn'}>
                        <img
                            src={cameraIcon}
                            className="cameraIconImg"
                            alt="cameraIcon"
                        />
                        <img
                            src={newIcon}
                            onMouseEnter={() => {
                                setShowTip(true);
                            }}
                            onMouseLeave={() => {
                                setShowTip(false);
                            }}
                            onClick={() => {
                                setShowCloser(true);
                            }}
                            className={'newImg ' + (showNewIcon ? 'hide' : '')}
                        />
                        <img
                            src={closerIcon}
                            className={
                                'closerTip ' +
                                (showCloser ? 'active ' : '') +
                                (showNewIcon ? 'hide' : '')
                            }
                            alt="closerIcon"
                            onClick={() => {
                                setShowNewIcon(true);
                            }}
                        />
                        <div className={'showTip ' + (showTip ? 'active' : '')}>
                            <p>Поиск по фото</p>
                        </div>
                    </IconButton>
                    <img onClick={onClickFilter} src={filter} alt="filter" className={'filter'}/>
                </div>}
        </div>
    );
};

export default MobileHeader;