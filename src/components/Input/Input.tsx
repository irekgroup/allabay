import { Icon28RefreshOutline } from '@vkontakte/icons';
import React, { useState, FC, useEffect, useContext } from 'react';
import ReactInputMask from 'react-input-mask';
import { RootStoreContext } from '../..';
import './Input.scss';

import iconTipCloser from '../../img/main/icon-tip-closer.svg';

interface InputProps {
    value?: string;
    name?: string;
    placeholder?: string;
    type?: InputType;
    status?: any;
    descr?: string;
    disabled?: boolean;
    showErrors?: boolean;
    before?: React.ReactNode;
    onClick?: () => void;
    onChange?: (e: any) => void;
}

type InputType = 'text' | 'tel' | 'email' | 'password' | 'date' | 'last-numbers';

export type inpStatus = 'default' | 'error' | 'right';

const Input: FC<InputProps> = ({
    value,
    placeholder,
    type = 'text',
    onChange,
    disabled = false,
    showErrors = false,
    before,
    descr,
    status,
    name,

}) => {
    const root = useContext(RootStoreContext);
    const [hideTip, setHideTip] = useState(false);
    const [seconds, setSeconds] = useState(59);

    useEffect(() => {
        // setHideTip(false);
        if (showErrors) {
            setTimeout(() => {
                setHideTip(true);
            }, 3000);
        }
    }, [showErrors]);


    if (type === 'tel') {
        return (
            <div className={`input-container ${showErrors && status ? status.status : ''}`}>
                {before}
                <span
                    style={{
                        fontSize: 13.3,
                        marginLeft: 5,
                        marginRight: -5,
                        whiteSpace: 'nowrap',
                    }}>
                    +7
                </span>
                <div style={{ width: '1px', background: 'black' }}></div>
                <ReactInputMask
                    mask="(999) 999 99 99"
                    onChange={!disabled ? onChange : () => {}}
                    value={value}
                    type={type}
                    name={name}>
                    {//@ts-ignore
                    (inputProps: any) => (
                        <input
                            {...inputProps}
                            placeholder={placeholder}
                            className={`input-main ${disabled ? 'disabled ' : ''}`}></input>
                    )}


                </ReactInputMask>
                {showErrors && status.status !== 'right' ? (
                    <img
                        onClick={() => {
                            setHideTip(false);
                        }}
                        className="iconCloser"
                        src={iconTipCloser}
                        alt="Подсказка"
                    />
                ) : (
                    ''
                )}

                {name !== 'changePassCheck' && name !== 'logInpPass' ? (
                    <div className={'passwordTip' + (hideTip ? ' hide' : '')}>
                        <p className="tel">{descr}</p>
                        {showErrors ? (
                            <img
                                onClick={() => {
                                    setHideTip(true);
                                }}
                                className="iconCloser"
                                src={iconTipCloser}
                                alt="Подсказка"
                            />
                        ) : (
                            ''
                        )}
                    </div>
                ) : (
                    ''
                )}
            </div>
        );
    } else if (type === 'last-numbers') {
        if (seconds > 0) {
            setTimeout(() => setSeconds(seconds - 1), 1000);
        }
        return (
            <div className={`input-container ${showErrors && status ? status.status : ''}`}>
                {before}
                <div style={{ width: '1px', background: 'black' }}></div>
                <ReactInputMask
                    mask="99 99"
                    onChange={!disabled ? onChange : () => {}}
                    value={value}
                    type={type}
                    name={name}>
                    {//@ts-ignore
                    (inputProps: any) => (
                        <input
                            {...inputProps}
                            placeholder={placeholder}
                            className={`input-main ${disabled ? 'disabled ' : ''}`}></input>
                    )}
                </ReactInputMask>
                <div className="refreshTimer">
                    {seconds > 0 ? (
                        <div>
                            0:{seconds < 10 ? '0' : ''}
                            {seconds}
                        </div>
                    ) : (
                        <Icon28RefreshOutline width={24} title="Запросить код заново" />
                    )}
                </div>
                {showErrors && status.status !== 'right' ? (
                    <img
                        onClick={() => {
                            setHideTip(false);
                        }}
                        className="iconCloser last-numbers"
                        src={iconTipCloser}
                        alt="Подсказка"
                    />
                ) : (
                    ''
                )}

                {name !== 'changePassCheck' && name !== 'logInpPass' ? (
                    <div className={'passwordTip' + (hideTip ? ' hide' : '')}>
                        <p>{descr}</p>
                        {showErrors ? (
                            <img
                                onClick={() => {
                                    setHideTip(true);
                                }}
                                className="iconCloser"
                                src={iconTipCloser}
                                alt="Подсказка"
                            />
                        ) : (
                            ''
                        )}
                    </div>
                ) : (
                    ''
                )}
            </div>
        );
    }

    return (
        <div
            className={`input-container ${type === 'password' ? 'password ' : ''} ${
                name === 'profileInpSurname' ? 'surname' : ''
            } ${showErrors && status ? status.status : ''}`}>
            {before}
            <input
                name={name}
                placeholder={placeholder}
                value={value}
                type={type}
                onChange={!disabled ? onChange : () => {}}
                className={`input-main ${disabled ? 'disabled ' : ''}`}
            />
            {showErrors && status.status !== 'right' && name !== 'profileInpSurname' ? (
                <img
                    onClick={() => {
                        setHideTip(false);
                    }}
                    className={`iconCloser ${type === 'date' ? 'date' : ''}`}
                    src={iconTipCloser}
                    alt="Подсказка"
                />
            ) : (
                ''
            )}

            {name !== 'profileInpSurname' ? (
                <div className={'passwordTip' + (hideTip ? ' hide' : '')}>
                    <p className={`${type === 'date' ? 'date' : ''}`}>{descr}</p>
                    {showErrors ? (
                        <img
                            onClick={() => {
                                setHideTip(true);
                            }}
                            className="iconCloser"
                            src={iconTipCloser}
                            alt="Подсказка"
                        />
                    ) : (
                        ''
                    )}
                </div>
            ) : (
                ''
            )}
        </div>
    );
};

export default Input;
