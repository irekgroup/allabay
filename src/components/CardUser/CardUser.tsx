import { Avatar, Button, CellButton, Div } from "@vkontakte/vkui";
import React, { FC, useContext, useEffect } from "react"
import "./CardUser.scss";
import { MODAL_SHARE, MODAL_UNAUTHORIZED, PAGE_LOGIN, PAGE_PROFILE } from "../../router";
import plane from "../../img/cardUser/share.svg";
import addUser from "../../img/cardUser/addUser.svg";
import { RootStoreContext } from "../../index";
import { useRouter } from "@happysanta/router";
import LetterAvatar from "../LetterAvatar/LetterAvatar";
import { observer } from "mobx-react";


const CardUser:FC = observer(({}) => {
    const rootStore = useContext(RootStoreContext);
    const { config, user } = rootStore;
    const router = useRouter()
    useEffect(() => {
        if(!user.userData.id){
            router.popPage()
        }
    }, [user.userData])
    return (
        <div className={!config.isMobile ? "card_user" : "card_user-modal"}>
            <Div>
                <div className="flex_center">
                    {user.userData.photo ? <Avatar src={user.userData.photo}/> : <LetterAvatar width={48} height={48}/>}
                    <b>{user.userData.firstName} {user.userData.lastName}</b>
                    <p>{user.userData.phone?.[0] == '7' && '+'}{user.userData.phone}</p>
                </div>
                <div className="nav_menu_card_user">
                    <Button size="m" className="button_control_account" onClick={() => {
                        // router.popPage();
                        router.pushPage(PAGE_PROFILE)
                    }} stretched>Управление аккаунтом</Button>
                    <div className={'accounts'}>
                        {user.authorizedUsers.map(({ i, userData}) => {
                            if (i === user.currUserIdx) return;
                            return (
                                <CellButton
                                    key={i}
                                    className={'accounts_item'}
                                    before={userData.photo ? <Avatar/> : <LetterAvatar userRef={userData} width={40} height={40}/>}
                                    style={{ color: '#000', width: '100%', paddingRight: '50px' }}
                                    onClick={() => {
                                        // user.updateUserData(userData)
                                        router.popPage()
                                        user.updateCurrUserIdx(i)
                                    }}>
                                    <div>
                                        <b>{userData.firstName} {userData.lastName}</b>
                                        <p>{userData.phone}</p>
                                    </div>
                                </CellButton>
                            )
                        })}
                    </div>
                    <div className="menu_cell_button_card_user">
                        <CellButton style={{ color: '#1E7CF2' }} before={<img src={plane} style={{
                            width: '35px'
                        }}/>} onClick={() => {
                            if (config.isMobile) {
                                if (navigator.share) {
                                    navigator.share({
                                        title: 'Allabay.com',
                                        text: 'Привет, я использую allabay для поиска и передвижения. Присоединяйся! Держи ссылку: allabay.com',
                                        url: 'https://allabay.com',
                                    }).catch(e => {
                                        console.error('Navigator.share error')
                                    })
                                }
                            } else {
                                router.pushModal(MODAL_SHARE)
                            }
                        }}>
                            Пригласить друга</CellButton>
                        <CellButton style={{ color: '#1E7CF2' }}
                                    onClick={() => {
                                        if (config.isMobile) {
                                            router.replaceModal(MODAL_UNAUTHORIZED)
                                        } else {
                                            router.pushPage(PAGE_LOGIN)
                                        }
                                    }}
                                    before={<img src={addUser} style={{
                                        width: '35px'
                                    }}/>}>Добавить аккаунт</CellButton>
                    </div>
                    <Button onClick={() => user.logout()} size="m" className="button_exit_account" mode="commerce" stretched>
                        Выйти из аккаунта
                    </Button>
                </div>
            </Div>
        </div>
    )
});

export default CardUser;