import React, {Dispatch, FC, SetStateAction, useEffect, useState} from "react";
import {ReactComponent as IndicatorLogo} from "../../../assets/indicator.svg"
import './Indicator.scss'

interface IndicatorProps {
    isShowIndicator: boolean;
    setShowIndicator: Dispatch<SetStateAction<boolean>>;
    className: string;
}

const Indicator: FC<IndicatorProps> = ({isShowIndicator, setShowIndicator, className}) => {
    useEffect(() => {
        if(isShowIndicator){
            animate();
        }

    }, [isShowIndicator])
    const [isAnimate, setAnimateStatus] = useState(false);
    const animate = () => {
        setAnimateStatus(true);
        setTimeout(() => setAnimateStatus(false), 1000);
    }

    const hideIndicator = () => {
        setShowIndicator(false);
    }


    return (
        <div className={className}>
            {isShowIndicator &&
                <IndicatorLogo onClick={hideIndicator} className={isAnimate ? `shake indicator-logo` : "indicator-logo"}/>
            }
        </div>
    );
}

export default Indicator;
