import React, { FC, useContext, useEffect, useState } from 'react';
import { RootStoreContext } from "../../index";
import './Notifications.scss';
import { observer } from "mobx-react";
import { Icon24Done } from "@vkontakte/icons";

const Notifications: FC = observer(() => {
    const [timer, setTimer] = useState<number | null>(null)
    const { config } = useContext(RootStoreContext)

    useEffect(() => {
        if (config.notifications.length) {
            if (timer) {
                window.clearTimeout(timer)
                setTimer(null)
            }
            setTimer(window.setTimeout(() => {
                config.clearNotifications()
            }, 1800))
        }
    }, [config.notifications.length])

    return (
        <div className={'notificationsAlert'}>
            {config.notifications.map((notification, i) => (
                <div className="notificationAlert-item" key={i}>
                    {notification.icon || <Icon24Done/>}
                    <p>{notification.text}</p>
                </div>
            ))}
        </div>
    );
});

export default Notifications;