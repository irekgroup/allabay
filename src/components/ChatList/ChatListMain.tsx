import React, {FC, useContext, useEffect, useState} from "react";
import "./ChatList.scss";
import {RootStoreContext} from "../..";
import {observer} from "mobx-react";
import Loader from "../Loader/Loader";
import {ChatFilters, IChat} from "../../mobX/Chat";
import {ChatListCreateButton} from "./ChatListCreateButton";
import {ChatListSearch} from "./ChatListSearch";
import {ChatList} from "./ChatList";
import {getChatsByUserFilter} from "./helpers/getChatsByUserFilter";

interface ChatListProps {
    isScroll: (isScroll: boolean) => void;
    chatId?: string | null;
}


const ChatListMain: FC<ChatListProps> = observer(({isScroll, chatId}) => {
    const rootStore = useContext(RootStoreContext);
    const {chat, user} = rootStore;
    const [filteredChats, setFilteredChats] = useState<(IChat | null)[]>([]);
    const [userInputFilter, setUserInputFilter] = useState('');
    const [loading, setLoading] = useState(false);

    /**
     * Управляет состоянием загрузки.
     */
    useEffect(() => {
        const isChatListNotNull = chat.chatList[0] !== null;

        if (user.userData.id && isChatListNotNull) {
            setLoading(false)
        } else {
            setLoading(true)
        }
    }, [chat.chatList])

    /**
     * При изменении пользовательского фильтра или списка чатов производит фильтрацию чатов.
     */
    useEffect(() => {
        userInputFilter ? setFilteredChats(getChatsByUserFilter(chat.chatList, userInputFilter)) : setFilteredChats(chat.chatList)
    }, [userInputFilter, chat.chatList])

    /**
     * Устанавливает состояние загрузки на false, если есть идентификатор пользователя.
     */
    useEffect(() => {
        if (!user.userData.id) {
            setLoading(false)
        }
    }, [])

    /**
     * Фильтрует список чатов, при изменении категории фильтра.
     */
    useEffect(() => {
        let ChatsFilteredBy: IChat[];
        if (chat.filterBy === ChatFilters.ALL) {
            ChatsFilteredBy = chat.chatList
        } else {
            const key = Object.keys(ChatFilters)[Object.values(ChatFilters).indexOf(chat.filterBy)]
            ChatsFilteredBy = chat.chatList.filter(item => item?.statusChat === key)
        }
        userInputFilter ? setFilteredChats(getChatsByUserFilter(ChatsFilteredBy, userInputFilter)) : setFilteredChats(ChatsFilteredBy)
    }, [chat.filterBy])

    if (loading) {
        return <Loader/>
    }

    return (
        <div className="chats-container">
            <ChatListCreateButton/>
            <ChatListSearch setFilter={setUserInputFilter}/>
            <ChatList fetchLoading={loading} filtered={filteredChats}/>
        </div>
    );
});

export default ChatListMain;
