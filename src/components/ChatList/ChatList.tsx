import ChatTab from "../ChatTab/ChatTab";
import React, {FC, SetStateAction, useContext, useEffect, useState} from "react";
import {PAGE_CHAT_DESKTOP, PAGE_CHAT_MOBILE, POPOUT_CHAT_CONTEXT_MENU} from "../../router";
import {useRouter} from "@happysanta/router";
import {RootStoreContext} from "../../index";
import chatImg from "../../img/chat/chat-avatar.png"
import Chat, {IChat} from "../../mobX/Chat";
import {FirstChatTab} from "./FirstChatTab";
import {ChatListFooter} from "./ChatListFooter";
import {EmptyChatList} from "./EmptyChatList";
import {useFetchChatsQuery} from "../../graphQL/Queries/FetchChats/FetchChats.generated";

interface ChatListProps {
    fetchLoading: boolean;
    filtered: (IChat | null)[]
}


export const ChatList: FC<ChatListProps> = ({filtered, fetchLoading}) => {
    const router = useRouter()
    const rootStore = useContext(RootStoreContext);
    const {chat, user, config} = rootStore;
    const userId = user.userData.id;

    function updateDialog(chat: Chat, chatItem: IChat, i: number) {
        const operatorFullName = `${chatItem?.user2?.firstName ?? 'Оператор'} ${chatItem?.user2?.lastName ?? ''}`;
        const userFullName = `${chatItem?.user1.firstName} ${chatItem?.user1.lastName}`;

        chat.updateDialog(
            chatItem.id,
            chatItem?.nameChat,
            i,
            chatItem.user1.id === userId
                ? operatorFullName
                : userFullName)
        router.replacePage(config.isMobile ? PAGE_CHAT_MOBILE : PAGE_CHAT_DESKTOP, {id: '' + chatItem.id})
    }

    function getNewDate(chatItem: IChat | null) {
        return new Date(chatItem?.lastMessages?.createAt
            || Date.now()).toLocaleDateString('ru-RU', {
            day: 'numeric',
            month: 'short'
        })

    }

    return (
        <div className="chats-list">
            <FirstChatTab setLoading={fetchLoading}/>
            {(chat.chatList[0] && userId)
                ?
                (filtered.map((chatItem, i) => {
                    if (!chatItem) {
                        return;
                    }
                    return (
                        <ChatTab
                            key={chatItem.id + i}
                            message={chatItem.lastMessages?.textMessage || ''}
                            name={user.userData.isStaff
                                ? (chatItem.user1.id === userId
                                    ? `${chatItem?.user2?.firstName ?? 'Оператор'} ${chatItem?.user2?.lastName ?? ''}`
                                    : `${chatItem?.user1.firstName} ${chatItem?.user1.lastName}`)
                                : chatItem.nameChat}
                            date={chatItem.lastMessages?.createAt
                                ? getNewDate(chatItem)
                                : ''}
                            onClick={() => {
                                updateDialog(chat, chatItem, i)
                            }}
                            isPin={chatItem.isPinned}
                            isRead={chatItem.lastMessages?.isRead}
                            img={chatImg}
                            isSelected={chat.currentState !== "create" && i === chat.dialogIdx && !config.isMobile}
                            onContextMenu={(e: MouseEvent) => {
                                e.preventDefault()
                                chat.setSelectedChat(i)
                                config.setMousePos({x: e.clientX, y: e.clientY,})
                                router.pushPopup(POPOUT_CHAT_CONTEXT_MENU)
                            }}
                        />
                    );
                })
            ) : (
                <EmptyChatList fetchLoading={fetchLoading} />
            )}
            <ChatListFooter/>

        </div>
    )
}
