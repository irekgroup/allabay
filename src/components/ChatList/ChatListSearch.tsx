import Input from "../Input/Input";
import chatSearch from "../../img/chat/chat-search.svg";
import {Icon20Dropdown} from "@vkontakte/icons";
import React, {SetStateAction, useContext} from "react";
import {RootStoreContext} from "../../index";
import {POPOUT_CHAT_TYPES, router} from "../../router";

function moveToChat() {
    router.pushPopup(POPOUT_CHAT_TYPES);
}

/**
 * setFilter: функция для установки состояния фильтра.
 */
interface ChatSearchProps {
    setFilter: React.Dispatch<SetStateAction<string>>;
}

export function ChatListSearch(props: ChatSearchProps) {
    const {setFilter} = props;
    const rootStore = useContext(RootStoreContext);
    const {chat, config} = rootStore;

    return <div className="chats-search">
        <Input
            before={<img src={chatSearch} alt="Поиск в мессенеджере"/>}
            placeholder="Поиск в мессенеджере"
            onChange={e => setFilter(e.target.value.trim().toLowerCase())}
        />
        {config.isMobile &&
            <p className={"selectChatType"} onClick={moveToChat}>
                {chat.filterBy[0].toUpperCase() + chat.filterBy.toLowerCase().slice(1)} <Icon20Dropdown/>
            </p>}
    </div>;
}
