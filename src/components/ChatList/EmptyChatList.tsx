import React, {FC, useContext} from "react";
import {RootStoreContext} from "../../index";

interface EmptyChatList{
    fetchLoading: boolean;
}

export const EmptyChatList: FC<EmptyChatList> = ({fetchLoading}) => {
    const {config} = useContext(RootStoreContext);
    return (
        <div className="no-chats" style={config.isMobile ? {width: '100%'} : {}}>
            {
                fetchLoading ? <p className={'medium'}>Загрузка...</p> : <p className={'medium'}>Чатов пока нет</p>
            }
        </div>
    )
}