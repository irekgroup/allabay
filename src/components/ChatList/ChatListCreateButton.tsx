import {Link} from "@vkontakte/vkui";
import newChatIcon from "../../img/main/create-new-white.svg";
import React from "react";
import {moveToCreate} from "./helpers/moveToCreate";

export const ChatListCreateButton = () => {
    return <div className="chats-head">
        <p>Чаты</p>
        <Link>
            <img
                src={newChatIcon}
                onClick={moveToCreate}
                alt="Создать новый чат"
            />
        </Link>
    </div>;
}

