import {Link} from "@vkontakte/vkui";
import {moveToCreate} from "./helpers/moveToCreate";
import newChatIcon from "../../img/main/create-new-white.svg";
import React, {useContext} from "react";
import {RootStoreContext} from "../../index";

export function ChatListFooter() {
    const {user, chat, config} = useContext(RootStoreContext);
    const userId = user.userData.id;

    return (
        <>
            {(chat.chatList.length <= 7 || !userId) && (
                <div className={`no-chats ${config.isMobile ? 'full' : ''}`}>
                    <p>Создайте новый</p>
                    <Link onClick={moveToCreate}>
                        <img src={newChatIcon} alt="New chat"/>
                    </Link>
                    <p className={'fsmall'}>Создать</p>
                    <p className={'small'}>
                        Узнайвайте наличие, стоимость и сроки доставки в ваш город,<br/> экономь на
                        поездках,<br/> экономь
                        на покупках,<br/> продавай по системе <Link>FBS</Link> или <Link>FBA</Link>.
                    </p>
                </div>
            )}

        </>
    )
}