import React, {useContext, useEffect} from "react";
import {PAGE_CHAT_DESKTOP, PAGE_CHAT_MOBILE} from "../../router";
import {useRouter} from "@happysanta/router";
import {RootStoreContext} from "../../index";
import chatLogo from "../../img/chat/fav.png";
import ChatTab from "../ChatTab/ChatTab";
import {useCreateChatMutation} from "../../graphQL/Mutations/CreateChat/CreateChat.generated";

interface FirstChatTabProps{
    setLoading: boolean;
}

export function FirstChatTab(props: FirstChatTabProps){
    const router = useRouter()
    const rootStore = useContext(RootStoreContext);
    const {chat, user, config} = rootStore;
    const [createDefaultChat, createDefaultChatData] = useCreateChatMutation();

    /**
     * Добавляет в чат оператора и перенаправляет в чат.
     */
    useEffect(() => {
        if (createDefaultChatData.error) {
            return console.error(`An error occurred while creating a chat: ${createDefaultChatData.error}`);
        }
        const isLoading = createDefaultChatData.loading;
        const isGetData = createDefaultChatData.data?.createChat?.ok;
        const createdChat = createDefaultChatData.data?.createChat?.chat;

        if (!isLoading && isGetData && createdChat) {
            const partner = createdChat.user1.id === user.userData.id
                ? `${createdChat.user2?.firstName ?? 'Оператор'} ${createdChat.user2?.lastName ?? ''}`
                : `${createdChat.user1.firstName} ${createdChat.user1.lastName}`;
            chat.updateDialog(createdChat.id, createdChat.nameChat, 0, partner)
            router.replacePage(!config.isMobile ? PAGE_CHAT_DESKTOP : PAGE_CHAT_MOBILE, {id: createdChat.id})
        }
    }, [createDefaultChatData.loading])

    return(
        <ChatTab name={'Избранное'} message={'Список пуст'} date={'21 авг.'} onClick={() => {
            createDefaultChat({
                variables: {
                    input: {
                        messageAllabay: true,
                        userRecipient: false,
                        nameChat: "default chat",
                    },
                },
                context: {
                    headers: {
                        Authorization: user.getToken(),
                    }
                },
            })
                .then(() => {
                    chat.updateCurrentState('start')
                })
                .catch((err) => console.log(err))
        }} img={chatLogo}/>
    );
}