import {MODAL_CREATE, router} from "../../../router";

export function moveToCreate() {
    router.pushModal(MODAL_CREATE)
}
