import {IChat} from "../../../mobX/Chat";

export function getChatsByUserFilter(chatList: IChat[], userInputFilter: string) {
    return chatList.filter(item => item?.nameChat.trim().toLowerCase().includes(userInputFilter));
}