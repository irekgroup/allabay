import "./StartChat.scss";
import { Link } from "@vkontakte/vkui";
import newChatIcon from "../../img/main/create-new-white.svg";
import { useContext } from "react";
import { RootStoreContext } from "../..";
import { MODAL_CREATE } from "../../router";
import { useRouter } from "@happysanta/router";

const StartChat = () => {
    const rootStore = useContext(RootStoreContext);
    const { chat } = rootStore;
    const router = useRouter()

    return (
        <>
            <div className="chat-header-text">
                <h3>Выбор чата</h3>
            </div>
            <div className="start-chat">
                <p>
                    Выберите какой-либо чат <br />
                    <span>или</span> <br />
                    создайте новый
                </p>
                <Link>
                    <img
                        src={newChatIcon}
                        onClick={() => {
                            router.pushModal(MODAL_CREATE)
                            // chat.updateCurrentState("create");
                        }}
                        alt="Создать новый чат"
                    />
                </Link>
            </div>
        </>
    );
};

export default StartChat;
