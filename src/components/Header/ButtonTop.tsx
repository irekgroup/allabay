import React, {FC, useContext} from "react";
import {RootStoreContext} from "../../index";
import {Tappable} from "@vkontakte/vkui";

interface ButtonTopProps {
    children?: React.ReactNode;
    className?: string;
    text?: string;
    onClick?: () => void;
}

const ButtonTop: FC<ButtonTopProps> = ({children, text, className, onClick}) => {
    const rootStore = useContext(RootStoreContext);

    const {config} = rootStore;

    const isMobile = config.isMobile;

    return (
        <div
            onClick={onClick}
            className={
                'button_top' + (isMobile ? ' mobile' : '') + (className ? ' ' + className : '')
            }>
            <Tappable className="button_top_tappable">
                <div className="content_button_top">
                    {children}
                    <span className="text_button_top">{text}</span>
                </div>
            </Tappable>
        </div>
    );
};

export default ButtonTop;