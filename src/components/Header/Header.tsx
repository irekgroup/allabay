import {
    FixedLayout,
    Tappable,
    Avatar,
    Button,
    IconButton,
    Link,
} from '@vkontakte/vkui';
import {v4 as uuidv4} from 'uuid';

import AvatarSpinner from '../AvatarSpinner/Spinner.js';

import cameraIcon from '../../img/main/camera.png';
import newIcon from '../../img/main/new-icon.svg';
import closerIcon from '../../img/main/closer-icon.svg';

import React, {FC, useContext, useState} from 'react';
import './Header.scss';
import {observer} from 'mobx-react';
import {RootStoreContext} from '../../index';
import {useLocation, useRouter} from '@happysanta/router';
import {
    MODAL_CREATE,
    PAGE_CHAT_DESKTOP,
    PAGE_CHAT_MOBILE, PAGE_FEED_DESKTOP, PAGE_FEED_MOBILE,
    PAGE_LOGIN,
    PANEL_DESKTOP_CHAT,
    PANEL_MOBILE_CHAT,
    POPOUT_CARDUSER,
} from '../../router';
import {Message} from '../../mobX/Chat.js';

import BigLogo from '../../img/main/biglogo.png';

import messenegerWithoutNotification from '../../img/main/messeneger-without-notification3.svg';
import cartWidthoutGoods from '../../img/main/cart5.svg';
import searchIcon from '../../img/main/search.png';
import createGrey from '../../img/main/create-new-white.svg';
import filter from '../../img/main/filter.svg';
import messengerBold from '../../img/main/messanger_bold.svg';
import cartBold from '../../img/main/cart_bold.svg';
import LetterAvatar from "../LetterAvatar/LetterAvatar";
import ButtonTop from "./ButtonTop";

interface HeaderProps {
    children?: React.ReactNode;
    isBack: boolean;
    showSearch?: boolean;
    showLogo?: boolean;
    newSearch?: boolean
}

const Header: FC<HeaderProps> = observer(({isBack, showSearch = false, showLogo = false, newSearch = true}) => {
    const router = useRouter();
    const rootStore = useContext(RootStoreContext);
    const [showChat, setShowChat] = useState(false);
    const [expanded, setExpanded] = useState(false);
    const [showTip, setShowTip]: any = useState(false);
    const [showCloser, setShowCloser]: any = useState(false);
    const [showNewIcon, setShowNewIcon]: any = useState(false);
    const {user, config, chat} = rootStore;
    const {socket} = config;
    const location = useLocation();
    const isMobile = config.isMobile;

    const sendMessage = () => {
        if (chat.message.replace(/(?:\r\n|\r|\n)/g, '').length > 0) {
            if (!socket.wss) {
            } else {
                let uuid = uuidv4();
                let message: Message = {
                    text: chat.message,
                    type: 'outgoing',
                    send: false,
                    read: false,
                    uuid: uuid,
                    date: '',
                } as Message;
                let date = new Date();
                let hours = date.getHours();
                let minutes = date.getMinutes();
                message.date = `${(hours < 10 ? '0' : '') + hours}:${
                    (minutes < 10 ? '0' : '') + minutes
                }`;
                let messages = chat.messages;
                messages.push(message);

                if (socket.wss.readyState === 1) {
                    socket.wss.send(
                        JSON.stringify({
                            type: 'message',
                            text: chat.message,
                            send: false,
                            read: false,
                            uuid: uuid,
                        }),
                    );
                } else {
                    return config.socket.reconnect;
                }

                setShowChat(true);
                chat.updateMessage('');
            }
        }
    };

    const moveToPage = () => {
        router.pushPage(config.isMobile ? PAGE_FEED_MOBILE : PAGE_FEED_DESKTOP)
    }

    return (
        <FixedLayout className={showLogo ? 'border-bottom' : ''} filled vertical="top">
            <div className={'header ' + (isMobile ? 'mobile ' : '') + (showLogo ? 'header-chat' : '')}>
                <div className="header-logo">
                    <Link onClick={moveToPage}>
                        <img src={BigLogo} className="searchIconImg" alt="searchIcon"/>
                    </Link>
                </div>

                {newSearch
                    ? <div className="header-search_new">
                        <div className="search-container">
                            <img src={searchIcon} alt=""/>
                            <input placeholder={'Найдется все'} type="text"/>
                            <img src={filter} alt=""/>
                            <img src={cameraIcon} alt=""/>
                        </div>
                    </div>
                    : <div className="header-search">
                        <Button className="header-search__category">
                            <p>Все</p>
                        </Button>
                        <div className="search">
                            <div className="search__logo">
                                <img src={searchIcon} className="searchIconImg" alt="searchIcon"/>
                            </div>
                            <input
                                onChange={(e) => chat.updateMessage(e.target.value)}
                                onFocus={() => {
                                    setExpanded((prev) => true);
                                }}
                                onBlur={() => {
                                    setExpanded((prev) => false);
                                }}
                                placeholder="Найдется все"
                                className="search__input"
                            />
                            <div className="search__logo">
                                <IconButton title="Поиск по фото" onClick={() => sendMessage()}>
                                    <img
                                        src={cameraIcon}
                                        className="cameraIconImg"
                                        alt="cameraIcon"
                                    />
                                    <img
                                        src={newIcon}
                                        onMouseEnter={() => {
                                            setShowTip(true);
                                        }}
                                        onMouseLeave={() => {
                                            setShowTip(false);
                                        }}
                                        onClick={() => {
                                            setShowCloser(true);
                                        }}
                                        className={'newImg ' + (showNewIcon ? 'hide' : '')}
                                    />
                                    <img
                                        src={closerIcon}
                                        className={
                                            'closerTip ' +
                                            (showCloser ? 'active ' : '') +
                                            (showNewIcon ? 'hide' : '')
                                        }
                                        alt="closerIcon"
                                        onClick={() => {
                                            setShowNewIcon(true);
                                        }}
                                    />
                                    <div className={'showTip ' + (showTip ? 'active' : '')}>
                                        <p>Поиск по фото</p>
                                    </div>
                                </IconButton>
                                <IconButton title={'Фильтрация'}>
                                    <img src={filter} alt={'filter'}/>
                                </IconButton>
                            </div>
                        </div>
                        <Button className="header-search__action">
                            <p>Поиск</p>
                        </Button>
                    </div>
                }

                <div className={showSearch ? 'with-search' : ''}>
                    {isMobile ? (
                        <ButtonTop className="header_search">
                            <img src={searchIcon} alt="messeneger" className="headerIcon"/>
                        </ButtonTop>
                    ) : (
                        <></>
                    )}
                    <ButtonTop text="Входящие"
                               onClick={() => {
                                   chat.setIsCart(false)
                                   config.isMobile
                                       ? config.updateActivePanel(PANEL_MOBILE_CHAT)
                                       : config.updateActivePanel(PANEL_DESKTOP_CHAT);
                                   router.pushPage(
                                       config.activePanel == PANEL_DESKTOP_CHAT
                                           ? PAGE_CHAT_DESKTOP
                                           : PAGE_CHAT_MOBILE,
                                   );
                               }}>
                        <img
                            src={(location.getPageId() === PAGE_CHAT_DESKTOP && !chat.isCart) ? messengerBold : messenegerWithoutNotification}
                            alt="messeneger"
                            className="headerIcon"
                        />
                        {chat.newMessagesCount > 0 && <div className="count-of-messages">{chat.newMessagesCount}</div>}
                    </ButtonTop>

                    <ButtonTop className={'headerNotice'} text="Корзина">
                        <img
                            src={(location.getPageId() === PAGE_CHAT_DESKTOP && chat.isCart) ? cartBold : cartWidthoutGoods}
                            alt="cart"
                            onClick={() => {
                                chat.setIsCart(true)
                                config.isMobile
                                    ? config.updateActivePanel(PANEL_MOBILE_CHAT)
                                    : config.updateActivePanel(PANEL_DESKTOP_CHAT);
                                router.pushPage(
                                    config.activePanel == PANEL_DESKTOP_CHAT
                                        ? PAGE_CHAT_DESKTOP
                                        : PAGE_CHAT_MOBILE,
                                );
                            }}
                            className="headerIcon"
                        />
                        <div className="notice">0</div>
                    </ButtonTop>
                    <ButtonTop onClick={() => router.pushModal(MODAL_CREATE)}>
                        <img
                            style={{height: 30, width: 'auto'}}
                            src={createGrey}
                            alt="create"
                            className="headerIcon"
                        />
                    </ButtonTop>

                    {user.userData.id ? (
                        <Tappable className="profile_user">
                            {user.userData.photo ?
                                <Avatar
                                    size={isMobile ? 36 : 50}
                                    src={user.userData.photo}
                                    onClick={() => {
                                        router.pushPopup(POPOUT_CARDUSER);
                                    }}>
                                    {!user.userData.id && <AvatarSpinner size={isMobile ? 36 : 50}/>}
                                </Avatar>
                                : <LetterAvatar
                                    height={50} width={50}
                                    onClick={() => {
                                        router.pushPopup(POPOUT_CARDUSER);
                                    }}/>}
                        </Tappable>
                    ) : (
                        <div className={'profile_user' + (isMobile ? ' mobile' : '')}>
                            <Button
                                stretched
                                size={isMobile ? 's' : 'm'}
                                style={{backgroundColor: '#1e7cf2'}}
                                className="user-login"
                                onClick={() => /*router.pushModal(MODAL_UNAUTHORIZED)*/ router.pushPage(PAGE_LOGIN)}>
                                Войти
                            </Button>
                        </div>
                    )}
                </div>
            </div>
        </FixedLayout>
    );
});


export default Header;
