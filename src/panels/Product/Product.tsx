import React, { FC, useContext, useEffect, useRef, useState } from 'react';
import {
    Avatar,
    Card,
    CardScroll,
    Gallery,
    IconButton,
    Link,
    NativeSelect,
    Panel, Radio, RadioGroup,
    SplitCol,
    SplitLayout
} from "@vkontakte/vkui";
import cartImg from "../../img/cart/cart-img-example.png";
import cartOutline from "../../img/feed/cartOutline.svg";
import cart from '../../img/main/cart5.svg';
import back from "../../img/feed/back.svg";
import front from "../../img/feed/front.svg";
import cry from '../../img/feed/cry.png';
import heart from '../../img/feed/heart.png';
import cool from '../../img/feed/cool.png';
import notcool from '../../img/feed/notcool.png';
import wrongCost from '../../img/feed/wrongCost.png';
import phh from '../../img/feed/phh.png';
import alertSvg from '../../img/cart/alert.svg';
import like from '../../img/product/like.svg';
import dislike from '../../img/product/dislike.svg';
import addPhoto from '../../img/chat/addPhoto.svg';
import galka from '../../img/chat/galka.svg';
import {
    Icon16MoreVertical,
    Icon24ArrowUpOutline,
    Icon20ChevronLeftOutline,
    Icon20ChevronRightOutline,
    Icon24Cancel, Icon24Done,
    Icon24Favorite,
    Icon24FavoriteOutline,
    Icon24MoreVertical,
    Icon24ArrowDownOutline,
    Icon28ChevronLeftOutline,
    Icon28Favorite,
    Icon28FavoriteOutline,
} from "@vkontakte/icons";
import Button from "../../components/Button/Button";
import './Product.scss';
import { RootStoreContext } from "../../index";
import MobileHeader from "../../components/MobileHeader/MobileHeader";
import lightning from '../../img/feed/lightning.svg';
import { TextTooltip } from "@vkontakte/vkui/unstable";
import "@vkontakte/vkui/dist/unstable.css";
import { useParams, useRouter } from "@happysanta/router";
import {
    MODAL_FAST_RESPONSE,
    MODAL_PRODUCT_CARD,
    MODAL_REVIEW_REPORT,
    MODAL_REVIEW_RULES,
    POPOUT_PRODUCT_ADDED
} from "../../router";
import Header from "../../components/Header/Header";
import { IProductInfo } from "../../mobX/CreateCard";
import Checkbox from "../../components/Checkbox/Checkbox";

interface ProductProps {
    id: string
}

const Product: FC<ProductProps> = ({ id }) => {
    const [fav, setFav] = useState(false)
    const [currImg, setCurrImg] = useState(0)
    const [guarantee, setGuarantee] = useState(0)
    const [productInfo, setProductInfo] = useState<IProductInfo>({})
    const [openPhoto, setOpenPhoto] = useState(false)
    const [showEnterReview, setShowEnterReview] = useState(false)
    const [reviewType, setReviewType] = useState(true)
    const [selectIsOpen, setSelectIsOpen] = useState(false)

    const router = useRouter()
    const { config, createCard } = useContext(RootStoreContext)

    const fromCreate = !!createCard.cardList.length

    const params = useParams()

    if (params.id) {
        createCard.clear()
    } else if (!createCard.cardList.length) {
        // router.popPage()
    }

    useEffect(() => {
        if (fromCreate) {
            const product = createCard.cardList[createCard.cardIndex]
            if (product) {
                setProductInfo(product)
                setGuarantee((product.guaranteePeriod && product.timeFrom) ? Math.ceil(Math.abs(Date.parse(product.guaranteePeriod) - Date.parse(product.timeFrom)) / (1000 * 60 * 60 * 24)) : 0)
            }

        }
    }, [])

    return (
        <Panel id={id} className={'product'}>
            <div className={`openPhoto ${!openPhoto ? 'hidden' : ''}`}>
                <div className="openPhoto-main">
                    <div className="col small">
                        {productInfo.photo && productInfo.photo.map((item, i) => {
                            return <img
                                src={item.url}
                                alt={item.title}
                                key={i}
                                className={currImg === i ? 'active' : ''}
                                onClick={() => setCurrImg(i)}/>
                        })}
                    </div>
                    <div className="col big">
                        <div className="images" style={{ transform: `translateX(${-100 * +currImg}%)`, }}>
                            {productInfo.photo && productInfo.photo.map((item, i) => {
                                return (
                                    <div className={'img-wrapper'}>
                                        <img
                                            src={item.url}
                                            alt={item.title}
                                            key={i}/>
                                    </div>)
                            })}
                        </div>
                        <div className="controls">
                            <div className="left">
                                <Icon20ChevronLeftOutline height={100} width={100} onClick={() => {
                                    if (currImg - 1 < 0) {
                                        if (productInfo.photo?.length) {
                                            setCurrImg(productInfo.photo?.length - 1)
                                        } else {
                                            setCurrImg(0)
                                        }
                                    } else {
                                        setCurrImg(currImg - 1)
                                    }
                                }}/>
                            </div>
                            <div className="right">
                                <Icon20ChevronRightOutline height={100} width={100} onClick={() => {
                                    if (productInfo.photo?.length) {
                                        setCurrImg((currImg + 1) >= productInfo.photo?.length ? 0 : currImg + 1)
                                    }
                                }}/>
                            </div>
                        </div>
                    </div>
                    <div className="close" onClick={() => setOpenPhoto(false)}>
                        <Icon24Cancel/>
                    </div>
                </div>

            </div>
            {config.isMobile ? <MobileHeader/> : <Header isBack={false} showSearch={true} showLogo={true}/>}
            {(!config.isMobile && !!createCard.cardList.length) &&
                <div className={'desktopPreviewTitle'}>
                    <Icon28ChevronLeftOutline height={32} className={'back backDesktop'} fill={'#a6a6a6'}
                                              onClick={() => {
                                                  router.popPage()
                                              }}/>
                    <p>Предпросмотр</p>
                </div>}
            <SplitLayout>
                {!config.isMobile &&
                    <SplitCol maxWidth={170}>
                        <div className="smallPhotos">
                            {productInfo.photo && productInfo.photo.map((item, i) => {
                                return <img
                                    src={item.url}
                                    alt={item.title}
                                    key={i}
                                    className={currImg === i ? 'active' : ''}
                                    onClick={() => setCurrImg(i)}/>
                            })}
                        </div>
                    </SplitCol>}
                <SplitCol spaced={!config.isMobile} width={!config.isMobile ? 450 : 'auto'}
                          maxWidth={!config.isMobile ? 450 : 'auto'}>
                    {config.isMobile &&
                        <Icon28ChevronLeftOutline height={32} className={'back'} fill={'#000'} onClick={() => {
                            router.popPage()
                        }}/>}
                    <Gallery
                        slideWidth={config.isMobile ? '100%' : "450px"}
                        style={config.isMobile ? { height: '100%' } : { maxHeight: '300px' }}
                        bullets="dark"
                        onChange={setCurrImg}
                        slideIndex={currImg}
                        showArrows={true}>
                        {productInfo.photo && productInfo.photo.map((item, i) => {
                            return <img
                                onClickCapture={() => !config.isMobile && setOpenPhoto(true)}
                                key={i}
                                src={item.url}
                                alt={item.title}/>
                        })}
                    </Gallery>
                </SplitCol>
                {!config.isMobile &&
                    <SplitCol>
                        <div className={'product-main-info'}>
                            <h1>{productInfo.title}</h1>
                            <div className={'favCount'}>
                                <Icon24Favorite fill={'#f8961d'}/>
                                <p>5000 тыс. в избранном</p>
                            </div>
                            <div className="price">
                                <p className="cost">{productInfo.newCost}₽
                                    <span className={'discount'}>
                                    {(productInfo.prevCost && productInfo.newCost)
                                        && -Math.round(100 - (+productInfo.prevCost / (+productInfo.newCost / 100)))}%
                                </span>
                                </p>
                                <p className="lastPrice"><s>{productInfo.prevCost} ₽</s></p>
                            </div>
                            <div className="btns">
                                <div>
                                    <Button mode={'primary'}>ПОЛУЧИТЬ РЕКВЕЗИТЫ</Button>
                                    <Button mode={'outline'} onClick={() => router.pushPopup(POPOUT_PRODUCT_ADDED)}><img
                                        src={cart} alt=""/>ДОБАВИТЬ В КОРЗИНУ</Button>
                                    <div className="fastResponseDesktop">
                                        <Link onClick={() => router.pushModal(MODAL_FAST_RESPONSE)}>
                                            Быстрая реакция </Link>
                                        <TextTooltip
                                            offsetSkidding={-150}
                                            text="Поможет выразить отношение к товару, Ваш ответ поможет нам стать лучше.">
                                            <img src={lightning} alt="lightning" style={{ height: 35 }}/>
                                        </TextTooltip>
                                        <div className={`reactions open`}>
                                            <div>
                                                <Icon24Favorite fill={'#f8961d'}/>
                                                <span>0</span>
                                            </div>
                                            <div>
                                                <img src={cool} alt="lightning"/>
                                                <span>0</span>
                                            </div>
                                            <div>
                                                <img src={heart} alt="lightning"/>
                                                <span>0</span>
                                            </div>
                                            <div>
                                                <img src={notcool} alt="lightning"/>
                                                <span>0</span>
                                            </div>
                                            <div>
                                                <img src={phh} alt="lightning"/>
                                                <span>0</span>
                                            </div>
                                            <div>
                                                <img src={wrongCost} alt="lightning"/>
                                                <span>0</span>
                                            </div>
                                            <div>
                                                <img src={cry} alt="lightning"/>
                                                <span>0</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <IconButton>
                                        <svg width="28" height="auto" viewBox="0 0 14 12" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M0.5 11C0.5 11.2761 0.723858 11.5 1 11.5C1.27614 11.5 1.5 11.2761 1.5 11H0.5ZM13.3536 4.35355C13.5488 4.15829 13.5488 3.84171 13.3536 3.64645L10.1716 0.464466C9.97631 0.269204 9.65973 0.269204 9.46447 0.464466C9.2692 0.659728 9.2692 0.976311 9.46447 1.17157L12.2929 4L9.46447 6.82843C9.2692 7.02369 9.2692 7.34027 9.46447 7.53553C9.65973 7.7308 9.97631 7.7308 10.1716 7.53553L13.3536 4.35355ZM1.5 11V7H0.5V11H1.5ZM4 4.5H13V3.5H4V4.5ZM1.5 7C1.5 5.61929 2.61929 4.5 4 4.5V3.5C2.067 3.5 0.5 5.067 0.5 7H1.5Z"
                                                fill="#1E7CF2"/>
                                        </svg>
                                        <span>Поделиться</span>
                                    </IconButton>
                                    <IconButton hasActive={false} onClick={() => {
                                        config.pushNotification(fav ? 'Товар удален из избранного' : 'Товар добавлен в избранное',
                                            <Icon24Done/>)
                                        setFav(!fav)
                                    }}>
                                        {!fav
                                            ? <Icon24FavoriteOutline width={28} height={28} fill={'#F8961D'}/>
                                            : <Icon24Favorite width={28} height={28} fill={'#F8961D'}/>}
                                        <span>В избранное</span>
                                    </IconButton>
                                </div>
                            </div>
                        </div>
                    </SplitCol>}
            </SplitLayout>
            {config.isMobile ? <div className="cart-item-content">
                    <div className={'flex icon_buttons'}>
                        <div className={'flex icons'}>
                            <img src={cartOutline} alt=""/>
                            <img src={back} alt=""/>
                            <img src={front} alt=""/>
                        </div>
                        {fav ? <Icon24Favorite fill={'#F8961D'} onClick={() => {
                                config.pushNotification('Товар удален из избранного', <Icon24Done/>)
                                setFav(false)
                            }}/>
                            : <Icon24FavoriteOutline onClick={() => {
                                config.pushNotification('Товар добавлен в избранное', <Icon24Done/>)
                                setFav(true)
                            }} fill={'#000'}/>}
                    </div>
                    <div className={'flex-col'}>
                        <div className={'flex space'}>
                            <h3>{productInfo.title}</h3>
                            <div className="costBlock">
                                {productInfo.prevCost && <p className={'prevCost cost'}>{productInfo.prevCost}₽</p>}
                                {(productInfo.prevCost && productInfo.newCost) &&
                                    <p className="discount">{-Math.round(100 - (+productInfo.prevCost / (+productInfo.newCost / 100)))}%</p>}
                                <p className={`${productInfo.prevCost ? 'newCost' : ''} cost`}>{productInfo.newCost}₽</p>
                            </div>
                        </div>
                        <div className={'flex desc'}>
                            <div className={'types'}>
                                <p><span>Номер карточки:</span> {productInfo.number}</p>
                                <p><span>Бренд:</span> {productInfo.brand}</p>
                                <p><span>Срок доставки:</span> с {productInfo.timeFrom} по {productInfo.timeTo}</p>
                                <p>
                                    <span>Гарантия: </span>{guarantee === 1 ? guarantee + ' день' : guarantee > 4 ? guarantee + ' дня' : guarantee + ' дней'}
                                </p>
                                <p><span>Способ получения: </span>{productInfo.sendType}</p>
                                <p><span>Категория: </span>{productInfo.category}</p>
                                <p><span>Подкатегория: </span>{productInfo.subcategory}</p>
                                <p><span>Вид: </span>{productInfo.kind}</p>
                                <p><span>Тип: </span>{productInfo.type}</p>
                                <p><span>Состояние: </span>{productInfo.condition}</p>
                            </div>
                            <p>{productInfo.description}</p>
                            <div className={'flex btns'}>
                                <p style={{ display: 'flex' }}>
                                    <Link onClick={() => router.pushModal(MODAL_FAST_RESPONSE)}>
                                        Быстрая реакция</Link>
                                    <TextTooltip hideDelay={2000}
                                                 text="Поможет выразить отношение к товару, Ваш ответ поможет нам стать лучше.">
                                        <img src={lightning} alt="lightning"/>
                                    </TextTooltip>
                                </p>

                                <div className={'flex'}>
                                    <Button mode={"primary"} onClick={() => router.pushPopup(POPOUT_PRODUCT_ADDED)}>Получить
                                        реквизиты</Button>
                                    <Icon16MoreVertical onClick={() => router.pushModal(MODAL_PRODUCT_CARD)}/>
                                </div>
                            </div>
                            <div className={`reactions open`}>
                                <div>
                                    <Icon24Favorite fill={'#f8961d'}/>
                                    <span>0</span>
                                </div>
                                <div>
                                    <img src={cool} alt="lightning"/>
                                    <span>0</span>
                                </div>
                                <div>
                                    <img src={heart} alt="lightning"/>
                                    <span>0</span>
                                </div>
                                <div>
                                    <img src={notcool} alt="lightning"/>
                                    <span>0</span>
                                </div>
                                <div>
                                    <img src={phh} alt="lightning"/>
                                    <span>0</span>
                                </div>
                                <div>
                                    <img src={wrongCost} alt="lightning"/>
                                    <span>0</span>
                                </div>
                                <div>
                                    <img src={cry} alt="lightning"/>
                                    <span>0</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> :
                <div className={'cart-item-content-desktop'}>
                    <h2>О товаре</h2>
                    <p><span>Номер карточки: </span>{productInfo.number}</p>
                    <p><span>Бренд: </span>{productInfo.brand}</p>
                    <p><span>Срок доставки:</span> с {productInfo.timeFrom} по {productInfo.timeTo}</p>
                    <p>
                        <span>Гарантия: </span>{guarantee === 1 ? guarantee + ' день' : guarantee <= 4 ? guarantee + ' дня' : guarantee + ' дней'}
                    </p>
                    <p><span>Способ получения: </span>{productInfo.sendType}</p>
                    <p><span>Категория: </span>{productInfo.category}</p>
                    <p><span>Подкатегория: </span>{productInfo.subcategory}</p>
                    <p><span>Вид: </span>{productInfo.kind}</p>
                    <p><span>Тип: </span>{productInfo.type}</p>
                    <p><span>Состояние: </span>{productInfo.condition}</p>
                    <p><span>Описание: </span>{productInfo.description}</p>
                </div>}
            <div className="reviews">
                <h2>Отзывы и вопросы {config.isMobile ? <Icon16MoreVertical onClick={() => router.pushModal(MODAL_REVIEW_RULES)}/> :
                    <small onClick={() => router.pushModal(MODAL_REVIEW_RULES)}>Правила оформления отзывов и вопросов</small>}</h2>
                <div className={`tabs`}>
                    <div className="tab" onClick={() => setReviewType(true)}>Отзывы(0)</div>
                    <div className="tab" onClick={() => setReviewType(false)}>Вопросы(0)</div>
                    <div className={`line ${reviewType ? 'left' : 'right'}`}/>
                </div>
                <div className="writeButtons">
                    {!config.isMobile &&
                        <Button mode={'secondary'} onClick={() => setShowEnterReview(!showEnterReview)}>НАПИСАТЬ
                            ОТЗЫВ</Button>}
                    <Button
                        style={config.isMobile ? { margin: 0 } : {}}
                        mode={'secondary'}
                        onClick={() => setShowEnterReview(!showEnterReview)}>
                        {(config.isMobile && reviewType) ? 'НАПИСАТЬ ОТЗЫВ' : 'НАПИСАТЬ ВОПРОС'}
                    </Button>
                </div>
                <div className={'noOrder'}><img src={alertSvg} alt=""/>Вы не можете оставить отзыв, так как не
                    заказывали данный товар или отказались от его доставки.
                </div>
                {showEnterReview && <EnterReview isQuestion={!reviewType} onClose={() => setShowEnterReview(false)}/>}
                <div className="reviewList">
                    <div className="sortWrapper">
                        <div className="sort">
                            <NativeSelect onMouseDown={() => setSelectIsOpen(true)} onChange={() => {
                                setSelectIsOpen(false)
                            }} onBlur={() => {
                                setSelectIsOpen(false)
                            }}>
                                <option value={1}>Дате ↑</option>
                                <option value={2}>Дате ↓</option>
                                <option value={3}>Оценке ↑</option>
                                <option value={4}>Оценке ↓</option>
                                <option value={5}>Полезности ↑</option>
                                <option value={6}>Полезности ↓</option>
                            </NativeSelect>
                            <input type="checkbox" id="stateInput" checked={selectIsOpen}/>
                            <label htmlFor="stateInput" className={`arrows`}/>
                        </div>
                        <div className="withPhoto">
                            <Checkbox>С фото(0)</Checkbox>
                        </div>
                    </div>
                    <div className="comments">
                        <Comment/>
                        <Comment/>
                    </div>
                </div>
            </div>
            {/*{config.isMobile && <EnterReview isQuestion={!reviewType}/>}*/}
            <div className="lastSearched">
                <h4 style={config.isMobile ? {} : { fontSize: '24px' }}>Возможно, вам понравится</h4>
                <div className="lastSearchedList">
                    {config.isMobile ?
                        <CardScroll size="s" withSpaces>
                            <LastSearchedItem/>
                            <LastSearchedItem/>
                            <LastSearchedItem/>
                            <LastSearchedItem/>
                        </CardScroll>
                        : <div className={'list-row'}>
                            <LastSearchedItem/>
                            <LastSearchedItem/>
                            <LastSearchedItem/>
                            <LastSearchedItem/>
                        </div>}
                </div>
            </div>
        </Panel>
    );
};

export const LastSearchedItem: FC = () => {
    return (
        <Card mode={"shadow"} style={{ marginTop: "5px", marginBottom: "5px" }} className={'lastSearchItem'}>
            <img src={cartImg} alt="img"/>
            <div className={'lastSearchItem-desc'}>
                <h4>54 000 ₽</h4>
                <p>Бренд / Название запчасти...</p>
            </div>
        </Card>
    )
}

const Comment: FC = () => {
    const [showAnswers, setShowAnswers] = useState(false)
    const [typeAnswer, setTypeAnswer] = useState(false)
    const router = useRouter()
    return (
        <div className="comment">
            <div className="col">
                <div className="row">
                    <Avatar size={50}/>
                    <div className="col">
                        <div className={'row'}>
                            <div style={{marginRight: 10}}>Анастасия</div>
                            <Icon24MoreVertical onClick={() => router.pushModal(MODAL_REVIEW_REPORT)} style={{
                                cursor: 'pointer',
                            }}/>
                        </div>
                        <div className="stars row">
                            <Icon28Favorite fill={'#f8961d'}/>
                            <Icon28Favorite fill={'#f8961d'}/>
                            <Icon28Favorite fill={'#f8961d'}/>
                            <Icon28Favorite fill={'#f8961d'}/>
                            <Icon28Favorite fill={'#f8961d'}/>
                        </div>
                    </div>
                    <div className={'date'}>
                        <div>
                            18 января, 23:15
                        </div>
                    </div>
                </div>
                <div className="row text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquam et quibusdam tenetur
                    totam! A eius eum in libero neque numquam optio, quam quasi vitae voluptatibus? Ipsum maiores optio
                    quos.
                </div>
                <div className="row gap">
                    <div className="row">
                        <Link onClick={() => setTypeAnswer(!typeAnswer)}>Ответить</Link>
                        {showAnswers
                            ? <Link
                                style={{ color: '#1E7CF2' }}
                                onClick={() => setShowAnswers(false)}>Скрыть ответы</Link>
                            : <Link onClick={() => setShowAnswers(true)}>Ответы (2)</Link>}
                    </div>
                    <div className="row likes">
                        <div className="like"><img src={like} alt=""/>0</div>
                        <div className="dislike"><img src={dislike} alt=""/>0</div>
                    </div>
                </div>
                {showAnswers &&
                    <div className={'answerList'}>
                        <div className="answer col">
                            <div className="row name">
                                <Avatar size={50}/>
                                <div className="row">
                                    <p>Имя</p>
                                    <p>20 января, 08:15</p>
                                </div>
                            </div>
                            <div className="row text">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, adipisci
                                    aliquidLorem</p>
                                <div className="likes row">
                                    <div className="like"><img src={like} alt=""/>0</div>
                                    <div className="dislike"><img src={dislike} alt=""/>0</div>
                                </div>
                            </div>
                        </div>
                    </div>}
                {typeAnswer && <EnterAnswer onClose={() => setTypeAnswer(false)}/>}
            </div>
        </div>
    )
}

const EnterAnswer: FC<{ onClose?: () => void }> = ({ onClose }) => {
    const [symbols, setSymbols] = useState(0)
    return (
        <div className="textEnter">
            <div className="row">
                <p>Apple</p><p>Введенно символов: {symbols}/300</p>
            </div>
            <textarea onChange={e => {
                setSymbols(e.target.value.length)
            }} maxLength={300}/>
            <div className="buttons row">
                <Button mode={'secondary'}>ОТПРАВИТЬ</Button>
                <Button mode={'outline'} onClick={onClose}>ОТМЕНИТЬ</Button>
            </div>
        </div>
    )
}

const EnterReview: FC<{ onClose?: () => void; isQuestion?: boolean; }> = ({ onClose, isQuestion }) => {
    const [symbols, setSymbols] = useState(0)
    const [stars, setStars] = useState(4)
    const [photos, setPhotos] = useState<string[]>([])
    const { config } = useContext(RootStoreContext)
    const fileRef = useRef<HTMLInputElement>(null)

    return (
        <div className="textEnter enterReview">
            <div className="title">
                <p>Новый {isQuestion ? 'вопрос' : 'отзыв'} к товару TITLE</p>
            </div>
            {!isQuestion && <div className="row head">
                <div className="col">
                    <p>Ваша оценка</p>
                    <div className="stars row">
                        {[0, 0, 0, 0, 0].map((_, i) => {
                            return i <= stars
                                ? <Icon28Favorite width={config.isMobile ? 28 : 32} height={config.isMobile ? 28 : 32}
                                                  onClick={() => setStars(i)} key={i}
                                                  fill={'#f8961d'}/>
                                : <Icon28FavoriteOutline width={config.isMobile ? 28 : 32}
                                                         height={config.isMobile ? 28 : 32} onClick={() => setStars(i)}
                                                         key={i}
                                                         fill={'#f8961d'}/>
                        })}
                    </div>
                </div>
                <div className="col">
                    <p>Соответствие</p>
                    <NativeSelect>
                        <option>Соответствует описанию</option>
                        <option>Не соответствует описанию</option>
                    </NativeSelect>
                </div>
            </div>}
            <div className="row">
                <p>{isQuestion ? 'Вопрос' : 'Отзыв'}</p><p>Введенно символов: {symbols}/1000</p>
            </div>
            <textarea onChange={e => {
                setSymbols(e.target.value.length)
            }} maxLength={1000}/>
            <div className="col photos">
                <p>Загрузите фото</p>
                <div className="row">
                    <input
                        type="file" hidden multiple ref={fileRef}
                        accept={'image/jpeg, image/gif, image/png, image/bmp'}
                        onChange={e => {
                            e.target.files && Object.values(e.target.files).forEach(file => {
                                const reader = new FileReader()
                                reader.onloadend = () => {
                                    setPhotos(prev => [...prev, '' + reader.result])
                                }
                                if (file.size < 10485760) {
                                    reader.readAsDataURL(file)
                                }
                            })
                        }}
                    />
                    <div className="addPhoto photo" onClick={() => fileRef.current && fileRef.current.click()}>
                        <img src={addPhoto}/>
                    </div>
                    {photos.map((url, i) => (
                        <div className="photo" key={i} onClick={() => {
                            setPhotos([...photos.slice(0, i), ...photos.slice(i + 1)])
                        }}>
                            <img src={url}/>
                        </div>
                    ))}
                </div>
                <p>Размер фото до 10 МБ. Поддерживаемые форматы: JPG, GIF, JPEG, PNG, BMP</p>
            </div>
            <div className="col profileVisibility">
                <b>Видимость в профиле</b>
                <RadioGroup>
                    <Radio name={'profileVisibility'}>Всем</Radio>
                    <Radio name={'profileVisibility'}>Не отображать в Странице</Radio>
                </RadioGroup>
            </div>
            <div className="buttons row">
                <Button mode={'secondary'}>ОТПРАВИТЬ</Button>
                <Button mode={'outline'} onClick={onClose}>ОТМЕНИТЬ</Button>
            </div>
            <p style={{ color: '#707070', marginTop: 10, fontSize: 12, textAlign: 'center' }}>Размещая отзыв на сайте,
                Вы даёте согласие на использование данных отзыва на сторонних ресурсах</p>
        </div>
    )
}

export default Product;