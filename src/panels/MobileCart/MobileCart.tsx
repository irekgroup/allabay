import React, {FC, useContext, useState} from 'react';
import { observer } from "mobx-react";
import { Panel } from '@vkontakte/vkui';
import './MobileCart.scss';
import { RootStoreContext } from "../../index";
import MobCardHeader from "./Cart/MobCardHeader";
import MobileHeader from "../../components/MobileHeader/MobileHeader";
import CartItem from "../../components/CartItem/CartItem";
import {Icon16Add, Icon16Minus} from "@vkontakte/icons";
import GoodsList from "../../components/Cart/GoodsList";
import CartSummary from "../../components/Cart/CartSummary";

interface MobileCardProps {
    id: string
}

const MobileCart: FC<MobileCardProps> = observer(({ id }) => {
    const [count, setCount] = useState(1);
    const [isShowIndicator, setIsShowIndicator] = useState(false);
    const {createCard} = useContext(RootStoreContext);

    const goodsList = createCard.cardList.map(value => {
        <div className={'cart-full'}>
            <div className="cart-full-cards">
                <CartItem key={value.number} isSelected={false} isScroll={isScroll => true} good={value}/>
                <div className="count">
                    <Icon16Add fill={'#1E7CF2'} onClick={() => setCount(count + 1)}/>
                    <p>{count}</p>
                    <Icon16Minus fill={'#1E7CF2'} onClick={() => setCount(count > 1 ? count - 1 : 1)}/>
                </div>
            </div>
        </div>
    });

    return (
        <Panel className={'mobileCart'} id={id}>
            <MobileHeader />
            <div className={'mob-cart-container'}>
                <MobCardHeader amountOfGoods={createCard.cardList.length}/>
                <GoodsList isShowIndicator={isShowIndicator} setShowIndicator={setIsShowIndicator} goodsList={goodsList}/>
                <CartSummary setShowIndicator={setIsShowIndicator}/>
            </div>
        </Panel>
    );
});

export default MobileCart;