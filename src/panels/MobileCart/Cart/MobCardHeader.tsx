import React, {FC, useState} from "react";
import {
    Icon16Add,
    Icon16Minus,
    Icon24CheckBoxOff,
    Icon24CheckBoxOn,
    Icon28DeleteOutlineAndroid
} from "@vkontakte/icons";
import CartItem from "../../../components/CartItem/CartItem";

interface MobCardHeader {
    amountOfGoods: number
}

const MobCardHeader: FC<MobCardHeader> = ({amountOfGoods}) =>{
    const [selected, setSelected] = useState(false);

    return(
        <>
            <div className="cart_nav">
                <h3>Корзина</h3>
                <p>Всего товаров: {amountOfGoods}</p>
            </div>
            {amountOfGoods > 0
                ?  <div className={'cart-full'}>
                    <div className="cart-full-nav">
                        <p>Выбрать всё (N)</p>
                        <div className={'cart-full-nav-icons'}>
                            <Icon28DeleteOutlineAndroid/>
                            {!selected ?
                                <Icon24CheckBoxOff width={28} height={28} onClick={() => setSelected(true)}/> :
                                <Icon24CheckBoxOn width={28} height={28} onClick={() => setSelected(false)}/>
                            }
                        </div>
                    </div>
                </div>
                : null
            }
        </>
    )
}

export default MobCardHeader;