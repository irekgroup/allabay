import React, { FC, useContext, useState } from 'react';
import { Avatar, Card, Link, Panel } from "@vkontakte/vkui";
import './DesktopFeed.scss';
import Header from "../../components/Header/Header";
import map from "../../img/feed/map.svg";
import aproved from '../../img/feed/aproved.svg';
import cross from '../../img/chat/cross.svg';
import ad from '../../img/feed/ad.svg';
import man from '../../img/createModal/man.svg';
import card from '../../img/createModal/card.svg';
import box from '../../img/createModal/box.svg';
import car from '../../img/createModal/car.svg';
import stars from '../../img/feed/stars.svg';
import smile from '../../img/feed/smile.svg';
import logo from '../../img/main/logo.svg';
import lock from '../../img/feed/lock.svg';
import logoTransparent from '../../img/feed/logo_transparent.svg';
import contactList from '../../img/feed/conact_list.svg';
import userPlus from '../../img/feed/user_plus.svg';
import { Icon24Add, Icon24Cancel, Icon28ArrowRightOutline, Icon28ChevronLeftOutline } from "@vkontakte/icons";
import Button from "../../components/Button/Button";
import { TextTooltip } from "@vkontakte/vkui/dist/components/TextTooltip/TextTooltip";
import CardItem from "../../components/CardItem/CardItem";
import { MODAL_CREATE, MODAL_UNAUTHORIZED, PAGE_LOGIN, PAGE_PROFILE, router } from "../../router";
import { RootStoreContext } from "../../index";
import Input from "../../components/Input/Input";
import MobileHeader from "../../components/MobileHeader/MobileHeader";
import Filters from "../../components/Filters/Filters";
import PopularSearch from "../../components/PopularSearch/PopularSearch";
import LetterAvatar from "../../components/LetterAvatar/LetterAvatar";

interface IDesktopFeedProps {
    id: string,
}

const DesktopFeed: FC<IDesktopFeedProps> = ({ id }) => {
    const [slide, setSlide] = useState(0)
    const [down, setDown] = useState<number | null>(null)
    const [selectRegion, setSelectRegion] = useState(true)
    const [tab, setTab] = useState(0)
    const [filter, setFilter] = useState(false)
    const [popularSearchOpen, setPopularSearchOpen] = useState(false)

    const { user, config } = useContext(RootStoreContext)
    const [showBanner, setShowBanner] = useState<boolean>(!user.userData.id)

    const cards = [
        {
            avatar: <img src={logo} className={'cardUserPhoto'} alt=""/>,
            title: 'allabay',
            slogan: 'Тратьте меньше, улыбайтесь больше.',
            approved: true,
            desk: (<div className="row">
                <div className="block"/>
                <div className="block"/>
                <div className="block"/>
            </div>),
            buttonText: 'Смотреть',
            isWatcher: true,
        }, {
            avatar: <img src={logo} className={'cardUserPhoto'} alt=""/>,
            slogan: 'Тратьте меньше, улыбайтесь больше.',
            title: 'allabay',
            approved: true,
            desk: (<div className="row closedAccount">
                <img src={lock} alt=""/>
                <p>Это закрытый аккаунт</p>
            </div>),
            buttonText: 'Смотреть',
        },
        {
            avatar: <img src={logo} className={'cardUserPhoto'} alt=""/>,
            title: 'Войдите в allabay',
            approved: false,
            desk: (<div className={'col desk'}>
                <p style={{ fontSize: 13 }}>Чтобы посмотреть новинки, популярные предложения и тренды от создателей,
                    найти новые страницы, которые
                    вам понравятся.</p>
                <Link
                    onClick={() => config.isMobile ? router.pushModal(MODAL_UNAUTHORIZED) : router.pushPage(PAGE_LOGIN)}>Зарегестрироваться</Link>
            </div>),
            buttonText: 'Войти',
            buttonAction: () => config.isMobile ? router.pushModal(MODAL_UNAUTHORIZED) : router.pushPage(PAGE_LOGIN),
        },
        {
            avatar: user.userData.photo ? <Avatar className={'cardUserPhoto'} src={user.userData.photo}/> :
                <LetterAvatar className={'cardUserPhoto'} width={48} height={48}/>,
            title: 'Заполните свой профиль',
            approved: false,
            desk: (<div className={'col desk'}>
                <p>Завершите настройку профиля, что бы оформлять в 1 клик</p>
                <p style={{ marginTop: 5 }}><span style={{ color: '#F8961D' }}>2 из 3</span> завершено</p>
            </div>),
            buttonText: 'Начать',
            buttonAction: () => {
                router.pushPage(PAGE_PROFILE)
            }
        }, {
            avatar: <img className={'cardUserPhoto'} src={contactList}/>,
            title: 'Подключите контакты',
            approved: false,
            desk: (<div className={'col desk'}>
                <p>Найдите людей, которых вы знаете</p>
            </div>),
            buttonText: 'Подключить',
        }, {
            avatar: <img className={'cardUserPhoto'} src={userPlus}/>,
            title: 'Найдите, на кого смотреть ',
            approved: false,
            desk: (<div className={'col desk'}>
                <p>Смотрите на страницы пользователей в allabay и следите за их предложениями</p>
            </div>),
            buttonText: 'Найти страницы',
        },
    ]

    const onSliderStart = (e: any) => {
        setDown(e.touches ? e.touches[0].clientX : e.clientX)
    }

    const onSliderMove = (e: any) => {
        if (down) {
            const delta = (e.touches ? e.touches[0].clientX : e.clientX) - down
            e.currentTarget.style.transform = `translateX(-${(slide * 250) + -delta * 3}px)`
            if (delta <= -50 && slide < 3) {
                setSlide(slide + 1)
                e.currentTarget.style.transform = `translateX(-${((slide + 1) * 250)}px)`
                setDown(null)
            } else if (delta >= 50 && slide > 0) {
                setSlide(slide - 1)
                e.currentTarget.style.transform = `translateX(-${((slide - 1) * 250)}px)`
                setDown(null)
            }
        }
    }

    const onSliderEnd = (e: any) => {
        setDown(null)
        e.currentTarget.style.transform = `translateX(-${(slide * 250)}px)`
    }

    return (
        <Panel id={id} className={'desktopFeed'}>
            {config.isMobile
                ? <MobileHeader
                    onChange={(e) => {
                        if (e.target.value.trim()) {
                            !popularSearchOpen && setPopularSearchOpen(true)
                        } else {
                            setPopularSearchOpen(false)
                        }
                    }}
                    isSearch={false}
                    onInteresting={() => setTab(1)}
                    isRightSearch={true}
                    onClickFilter={() => setFilter(true)}
                />
                : <Header isBack={false} showSearch={true} showLogo={true} newSearch={true}/>}
            {(config.isMobile && filter) && <Filters onClose={() => setFilter(false)}/>}
            {(config.isMobile && popularSearchOpen) && <PopularSearch/>}
            <div className="main row">
                {!config.isMobile && <div className="col col-main">
                    <div className="wrapper">
                        <div className="slides">
                            <div className="slide"></div>
                            <div className="slide"></div>
                            <div className="slide"></div>
                            <div className="slide"></div>
                        </div>
                        <div className="dots">
                            <div className="dot"/>
                            <div className="dot"/>
                            <div className="dot"/>
                            <div className="dot"/>
                            <div className="activeDot dot active"/>
                        </div>
                    </div>
                    <div className={'row'}>
                        <p>Войдите для получения наилучших впечатлений</p>
                        <Link onClick={() => router.pushPage(PAGE_LOGIN)}>Войти безопасно</Link>
                    </div>
                </div>}

                {(tab !== 1 || !config.isMobile) &&
                    <div className="col col-main">
                        {!config.isMobile && <h1>Лента</h1>}
                        <Card className="card region" mode={'shadow'}>
                            {selectRegion ? <div className="row">
                                    <div className="col">
                                        <img src={map} alt="map"/>
                                    </div>
                                    <div className={'col'}>
                                        <p>Хотите искать в Москве?</p>
                                        <div className={'row'}>
                                            <Link onClick={() => setSelectRegion(false)}>Да</Link>
                                            <Link>Нет, сменить регион</Link>
                                        </div>
                                    </div>
                                </div> :
                                <div className="row create">
                                    <img className={'ad'} src={ad} alt=""/>
                                    <div className="col">
                                        <div className="row">
                                            <input type="text" placeholder={'Что угодно?'}/>
                                            <TextTooltip hideDelay={1000} text={'Лучшие предложения'}>
                                                <img className={'stars'} src={stars} alt=""/>
                                            </TextTooltip>
                                        </div>
                                        <div className="row">
                                            <TextTooltip text={'Предложить / найти поездку'}>
                                                <img src={car} alt="" onClick={() => router.pushModal(MODAL_CREATE)}/>
                                            </TextTooltip>
                                            <TextTooltip text={'Предлжить / найти товарную карточку'}>
                                                <img src={card} alt="" onClick={() => router.pushModal(MODAL_CREATE)}/>
                                            </TextTooltip>
                                            <TextTooltip text={'Предложить / найти лот'}>
                                                <img src={box} alt="" onClick={() => router.pushModal(MODAL_CREATE)}/>
                                            </TextTooltip>
                                            <TextTooltip text={'Предложить / найти доставку'}>
                                                <img src={man} alt="" onClick={() => router.pushModal(MODAL_CREATE)}/>
                                            </TextTooltip>
                                        </div>
                                    </div>
                                </div>}
                        </Card>
                        <div className="col hello">
                            <h2>
                                Добро пожаловать в allabay!
                            </h2>
                            <h2>Рекомендации для вас</h2>
                            <p>Смотрите на интересные страницы, чтобы видеть предложения, которые они публикуют.</p>
                        </div>

                        <div
                            onTouchStart={onSliderStart}
                            onTouchEnd={onSliderEnd}
                            onTouchMove={down ? onSliderMove : undefined}
                            onMouseDown={onSliderStart}
                            onMouseMove={down ? onSliderMove : undefined}
                            onMouseUp={onSliderEnd}
                            onMouseLeave={down ? onSliderEnd : undefined}
                            className="cardSlider"
                            style={{ transform: `translateX(-${(slide * 245)}px)` }}>
                            {cards.map((item, idx) => (
                                <Card
                                    onClick={slide !== idx ? (e) => {
                                        const slideT = idx > slide ? slide + 1 : slide - 1
                                        setSlide(slideT)
                                        if (e.currentTarget.parentElement) {
                                            e.currentTarget.parentElement.style.transform = `translateX(-${(slideT * 245)}px)`
                                        }
                                    } : undefined}
                                    key={idx}
                                    mode={'shadow'}
                                    className={`${slide === idx ? 'active' : 'smaller'}`}
                                >
                                    <div className="col">
                                        {item.avatar}
                                        <div className="title">
                                            <h3>{item.title}{item.approved ?
                                                <img src={aproved} alt="approved"/> : ''}</h3>
                                            <p>{item.slogan}</p>
                                        </div>
                                        {item.desk}
                                        {!item.isWatcher
                                            ? <Button mode={'secondary'}
                                                      onClick={user.userData.id
                                                          ? item.buttonAction
                                                          : config.isMobile
                                                              ? () => router.pushModal(MODAL_UNAUTHORIZED)
                                                              : () => router.pushPage(PAGE_LOGIN)}>{item.buttonText}
                                            </Button>
                                            : <div className={'watcher'}><Button mode={'secondary'}>Смотрящий</Button>
                                            </div>}
                                        {/*<img src={logo} className={'cardUserPhoto'} alt=""/>*/}
                                        {/*<div className="title">*/}
                                        {/*    <h3>allabay <img src={aproved} alt=""/></h3>*/}
                                        {/*    <p>Тратьте меньше, улыбайтесь больше.</p>*/}
                                        {/*</div>*/}
                                        {/*<div className="row">*/}
                                        {/*    <div className="block"/>*/}
                                        {/*    <div className="block"/>*/}
                                        {/*    <div className="block"/>*/}
                                        {/*</div>*/}
                                        {/*{true*/}
                                        {/*    ? <Button mode={'secondary'}>Смотреть</Button>*/}
                                        {/*    : <div className={'watcher'}><Button mode={'secondary'}>Смотрящий</Button>*/}
                                        {/*    </div>*/}
                                        {/*}*/}
                                        <img src={cross} className={'close'} alt=""/>
                                    </div>
                                </Card>
                            ))}
                            {/*<div className="left">*/}
                            {/*    <div className="themesArrow" onClick={() => {*/}
                            {/*        setSlide(slide - 1)*/}
                            {/*    }}>*/}
                            {/*        <Icon28ArrowRightOutline/>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                            {/*<div className="right">*/}
                            {/*    <div className="themesArrow" onClick={() => {*/}
                            {/*        setSlide(slide + 1)*/}
                            {/*    }}>*/}
                            {/*        <Icon28ArrowRightOutline/>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                        </div>

                        <div className="hello hello-second">
                            <h2 style={{ margin: '18px 0', marginBottom: 0 }}>Сделайте свою ленту более насыщенной с
                                помощью
                                тем</h2>
                            <p>Вы будите видеть новинки, популярные предложения и тренды прямо в своей ленте</p>
                        </div>
                        <div className="themes">
                            <div className="themesArrow">
                                <Icon28ArrowRightOutline/>
                            </div>
                            <ThemeCard title={'Товары Для Транспортных Средств'}/>
                            <ThemeCard title={'Техника и Эллектроника'}/>
                            <ThemeCard title={'Книги и Учебники'}/>
                            <ThemeCard title={'Цифровые Товары'}/>
                            <ThemeCard title={'Товары Для Спорта'}/>
                            <Link style={{ marginLeft: 30 }}>Больше тем</Link>
                        </div>
                        <div className="cardList">
                            <div className="cardList-item">
                                <div className="row">
                                    <div>
                                        <img src={logo} alt=""/>
                                        <p> Addvisor</p>
                                    </div>
                                    <p>@addvisor · 11 янв. </p>
                                </div>
                                <CardItem product={{
                                    title: 'test',
                                    prevCost: '1000',
                                    newCost: '500',
                                }}/>
                                {config.isMobile && <div className="review">
                                    <Input before={<img src={smile} alt=""/>} placeholder={'Добавьте отзыв'}/>
                                    <Link>Опубликовать</Link>
                                </div>}
                            </div>
                            <div className="cardList-item">
                                <div className="row">
                                    <div>
                                        <img src={logo} alt=""/>
                                        <p> Addvisor</p>
                                    </div>
                                    <p>@addvisor · 11 янв. </p>
                                </div>
                                <CardItem product={{
                                    title: 'test',
                                    prevCost: '1000',
                                    newCost: '500',
                                }}/>
                                {config.isMobile && <div className="review">
                                    <Input before={<img src={smile} alt=""/>} placeholder={'Добавьте отзыв'}/>
                                    <Link>Опубликовать</Link>
                                </div>}
                            </div>
                            <div className="cardList-item">
                                <div className="row">
                                    <div>
                                        <img src={logo} alt=""/>
                                        <p> Addvisor</p>
                                    </div>
                                    <p>@addvisor · 11 янв. </p>
                                </div>
                                <CardItem product={{
                                    title: 'test',
                                    prevCost: '1000',
                                    newCost: '500',
                                }}/>
                                {config.isMobile && <div className="review">
                                    <Input before={<img src={smile} alt=""/>} placeholder={'Добавьте отзыв'}/>
                                    <Link>Опубликовать</Link>
                                </div>}
                            </div>
                        </div>
                    </div>}

                {(!config.isMobile || tab === 1) &&
                    <div className="col col-main col-right">
                        {config.isMobile &&
                            <div className="back">
                                <Icon28ChevronLeftOutline onClick={() => setTab(0)} style={{ cursor: 'pointer' }}
                                                          height={42} width={42} fill={'#a6a6a6'}/>
                                <p>Популярные товары</p>
                            </div>}
                        <div className={'rightCol'}>
                            {!config.isMobile && <div className="block">
                                <UserCard title={'marvel'} desk={'Marvel Entertainment'} buttonText={'Переключится'}/>
                            </div>}
                            <div className="block">
                                <div className="row titles">
                                    <p className={'recomentations_for_you'}>Рекомендации для вас</p>
                                    {!config.isMobile && <p><Link>Все</Link></p>}
                                </div>
                                <UserCard title={'marvel'} desk={'Marvel Entertainment'} buttonText={'Смотреть'}/>
                                <UserCard title={'marvel'} desk={'Marvel Entertainment'} buttonText={'Смотреть'}/>
                                <UserCard title={'marvel'} desk={'Marvel Entertainment'}
                                          buttonText={<span style={{ color: '#707070' }}>Вы смотрите</span>}/>
                                <UserCard title={'marvel'} desk={'Marvel Entertainment'} buttonText={'Смотреть'}/>
                                <UserCard title={'marvel'} desk={'Marvel Entertainment'} buttonText={'Смотреть'}/>
                                {config.isMobile && <Link className={'more'}>Показать еще</Link>}
                            </div>
                            <div className="block popular">
                                {!config.isMobile && <div className="row titles">
                                    <p>Популярные товары </p>
                                </div>}
                                <div className={'col'}>
                                    <Link><p>1 · Игры · Новинка </p></Link>
                                    <Link><p>Игра Wordle</p></Link>
                                </div>
                                <div className={'col'}>
                                    <Link><p>2 · Игры · Новинка </p></Link>
                                    <Link><p>Игра Wordle</p></Link>
                                    <p>Предложений: 280 тыс.</p>
                                </div>
                                <div className={'col'}>
                                    <Link><p>3 · Игры · Новинка </p></Link>
                                    <Link><p>Игра Wordle</p></Link>
                                    <p>Предложений: 280 тыс.</p>
                                </div>
                                <Link>Показать еще</Link>
                            </div>
                        </div>

                        {!config.isMobile && <div className="footer block">
                            <p>Москва</p>
                            <p style={{ color: '#707070' }}>Политика конфедициальности
                                Условия использования
                                Все о Allabay Введение</p>
                            <p style={{ color: '#707070' }}>© 2022 allabay from allabay</p>
                        </div>}
                    </div>}
            </div>

            {(showBanner && !config.isMobile) &&
                <div className="banner">
                    <img src={cross} className={'close'} alt="close" onClick={() => setShowBanner(false)}/>
                    <div className="wrap row">
                        <div className="col logo_transparent-wrap">
                            <img src={logoTransparent} className={'logo_transparent'} alt="logo"/>
                        </div>
                        <div className="col">
                            <p>Войдите в allabay</p>
                            <p>Войдите, чтобы посмотреть новинки, популярные предложения и тренды от создателей, </p>
                            <p>найти новые страницы, которые вам понравятся.</p>
                        </div>
                        <div className="col">
                            <Button mode={'secondary'}>Войти</Button>
                            <Link>Зарегестрироваться</Link>
                        </div>
                    </div>
                </div>
            }
        </Panel>
    )
};

const ThemeCard: FC<{ title: string }> = ({ title }) => {
    return (
        <div className="row themeCard">
            <div className="col">
                {title}
            </div>
            <div className="col">
                <Icon24Add fill={'#1E7CF2'}/>
            </div>
            <div className="col">
                <TextTooltip text={'Отклонить'}>
                    <Icon24Cancel fill={'#A6A6A6'}/>
                </TextTooltip>
            </div>
        </div>
    )
}

const UserCard: FC<{ title: string, desk: string, buttonText: string | React.ReactNode }> = ({
                                                                                                 title,
                                                                                                 desk,
                                                                                                 buttonText,
                                                                                             }) => {
    return (
        <div className="userBlock row">
            <div className="col">
                <img src={map} alt=""/>
            </div>
            <div className="col">
                <div className="title">{title}<img src={aproved} alt=""/></div>
                <p>{desk}</p>
                <Link>{buttonText}</Link>
            </div>
        </div>
    )
}

export default DesktopFeed;