import React, {FC, useContext, useState} from "react";
import {RootStoreContext} from "../../../index";
import {observer} from "mobx-react";
import CartHeader from "./CartHeader";
import GoodsList from "../../../components/Cart/GoodsList";
import CartItem from "../../../components/CartItem/CartItem";
import {Icon16Add, Icon16Minus} from "@vkontakte/icons";
import CartSummary from "../../../components/Cart/CartSummary";
interface CartProps {
}

const CartMain: FC<CartProps> = ({}) => {
    const { createCard } = useContext(RootStoreContext);
    const [scrollCart] = useState(false);
    const [isShowIndicator, setIsShowIndicator] = useState(false);

    const goodsList = createCard.cardList.map(value => {
        <>
            <CartItem key={value.number} isSelected={true} isScroll={isScroll => true} good={value}/>
            <div className="count">
                <Icon16Add fill={'#1E7CF2'}/>
                <p>N</p>
                <Icon16Minus fill={'#1E7CF2'}/>
            </div>
        </>
    })

    let indicatorProps = {isShowIndicator: isShowIndicator, setShowIndicator: setIsShowIndicator}
    return (
        <div className={scrollCart ? "right-cart scrolling" : "right-cart"}>
            <CartHeader amountOfGoods={createCard.cardList.length} />
            <GoodsList goodsList={goodsList} {...indicatorProps} />
            <CartSummary setShowIndicator={setIsShowIndicator}/>
        </div>
    )
}

export default observer(CartMain);