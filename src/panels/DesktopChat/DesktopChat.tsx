import { Link, Panel } from "@vkontakte/vkui";
import { observer } from "mobx-react";
import React, { FC, useContext, useEffect, useState } from "react";
import { RootStoreContext } from "../..";
import Footer from "../../components/Footer/Footer";
import Header from "../../components/Header/Header";
import Chat from "../../components/Chat/Chat";
import { PAGE_CHAT_DESKTOP, PAGE_CHAT_MOBILE, PANEL_DESKTOP_CHAT } from "../../router";
import CreateChat from "../../components/CreateChat/CreateChat";
import StartChat from "../../components/StartChat/StartChat";
import "./DesktopChat.scss";
import ChatListMain from "../../components/ChatList/ChatListMain";
import { useRouter } from "@happysanta/router";
import CreateCard from "../../components/CreateCard/CreateCard";
import {ChatFilters, Message} from "../../mobX/Chat";
import CartMain from "./Cart/CartMain";
import {useGetChatLazyQuery} from "../../graphQL/Queries/GetChat/GetChat.generated";

interface DesktopChatProps {
    id: string;
}

const DesktopChat: FC<DesktopChatProps> = observer(({ id }) => {
    const rootStore = useContext(RootStoreContext);
    const { config, chat, user } = rootStore;
    const [scrollChats, setScrollingChats] = useState(false);
    const [chatId, setChatId] = useState<null | string>(null);
    const [loadingMessages, setLoading] = useState(true)

    config.updateActivePanel(PANEL_DESKTOP_CHAT);

    const [getChat, getChatData] = useGetChatLazyQuery({fetchPolicy: "cache-and-network"});

    const isScrollingChats = (isScroll: boolean) => {
        setScrollingChats(isScroll);
    };

    const router = useRouter();

    useEffect(() => {
        config.isMobile && router.pushPage(PAGE_CHAT_MOBILE);
    }, [config.isMobile])

    /**
     * Получает идентификатор чата из строки браузера.
     */
    router.onEnterPage(PAGE_CHAT_DESKTOP, (route) => {
        const { id } = route.getParams()
        setChatId(id)
    })

    useEffect(() => {
        if (chatId) {
            setLoading(true)
            getChat({
                variables: {
                    id: chatId,
                    limit: "50",
                }
            })
        }
    }, [chatId])

    /**
     * Срабатывает при необходимости обновить список чатов, когда подписка уведомила нас об изменнеиях.
     */
    useEffect(() => {
        if (chat.refetchChat) {
            if (chat.refetchChat === chatId) {
                getChatData.refetch({
                    id: chatId,
                    limit: "50",
                })
            }
            chat.setUpdateChat(null)
        }
    }, [chat.refetchChat])


    useEffect(() => {
        if (getChatData.loading) return;
        if (getChatData.error) {
            console.error(`При получении чата возникла ошибка. ${getChatData.error}`)
            return;
        }
        if (getChatData.data && getChatData.data.chat && getChatData.data.chat[0]) {
            chat.updateMessages(getChatData.data.chat.map((msg: any) => {
                return {
                    uuid: msg.id,
                    date: msg.createAt,
                    read: msg.isRead,
                    type: msg.senderUser.id === user.userData.id ? 'outgoing' : 'incoming',
                    send: true,
                    text: msg.textMessage
                } as Message
            }))
            typeof chat.dialogIdx === 'number'
            && chat.updateLastMessage(chat.dialogIdx, {
                senderUser: '',
                textMessage: chat.messages[chat.messages.length - 1].text,
                createAt: chat.messages[chat.messages.length - 1].date
            })
        } else {
            chat.updateMessages([])
        }
        setLoading(false)
    }, [getChatData.loading])

    return (
        <Panel id={id} style={{ backgroundColor: "var(--background_content)" }}>
            <Header isBack={false} showLogo={true} showSearch={true}/>
            <div className="filterTypes">
                {Object.values(ChatFilters).map((text, i) =>
                    <Link
                        key={i}
                        onClick={() => chat.setChatFilter(text)}
                        className={chat.filterBy === text ? 'active' : ''}
                    >
                        {text}
                    </Link>)}
            </div>
            <div className={scrollChats ? "sidebar scrolling" : "sidebar"}>
                <ChatListMain chatId={chatId} isScroll={isScrollingChats}/>
            </div>
            <div className={`centered_flex ${chatId ? " pt" : ""}`}>
                {chatId ? (
                    <Chat loading={loadingMessages}/>
                ) : chat.currentState == "start" ? (
                    <StartChat/>
                ) : chat.currentState === "create" ? (
                    <CreateChat/>
                ) : (
                    <CreateCard/>
                )}
            </div>
            <CartMain ></CartMain>
            {!config.isKeyboardActive ? <Footer/> : <></>}
        </Panel>
    );
});

export default DesktopChat;
