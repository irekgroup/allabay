import { HorizontalScroll, Panel, SplitCol, SplitLayout, Tabs, TabsItem } from "@vkontakte/vkui";
import Footer from "../../components/Footer/Footer";
import Header from "../../components/Header/Header";

import React, { FC, useContext, useEffect, useRef, useState } from "react";
import "./Profile.css";
import Tab from "./Tab";
import { observer } from "mobx-react";
import { RootStoreContext } from "../../index";
import MobileHeader from "../../components/MobileHeader/MobileHeader";
import { PendingStatus } from "../../mobX/User";
import Loader from "../../components/Loader/Loader";
import { router } from "../../router";

interface IMyComponentProps {
    globState: any,
    setGlobalState: any,
    toggleScheme: any,
    isMobile: any,
    setPopout: any,
    setActiveModal: any,
    sendWithSocket: any,
    openUnathorizedAlert: any,
    go: any,
    id: any,
}

interface IMyComponentState {
    dotsMenuOpened: any,
    leftMenuOpened: any,
    CardUserOpened: any,
    desktopMenuOpened: any,
    activeTab: any,
}

interface ProfilePanelProps {
    id: string,
    children?: React.ReactNode,
}

export enum ProfileTabs {
    MAIN,
    INFO,
    ORDERS,
    CHECKS,
    FAVOURITE,
    NOTIFICATIONS,
}

const Profile: FC<ProfilePanelProps> = observer(({ id }) => {
    const [activeTab, setActiveTab] = useState<ProfileTabs>(ProfileTabs.MAIN)
    const [touchPos, setTouchPos] = useState<number | null>(null)

    const scrollRef = useRef<HTMLDivElement>(null)

    const scrollTabs = (n:number) => {
        if(scrollRef.current){
            scrollRef.current.scrollTo(scrollRef.current.scrollLeft + n, 0)
        }
    }

    const rootStore = useContext(RootStoreContext);
    const { config, user } = rootStore;
    const { isMobile } = config;

    useEffect(() => {
        if(!user.userData.id){
            router.popPage()
        }
    }, [user.userData.id])

    if (user.status === PendingStatus.LOADING) {
        return <Loader/>
    } else if (user.status === PendingStatus.REJECTED) {
        router.popPage()
        return <></>
    }

    return (
        <Panel id={id}>
            {!config.isMobile ? <Header
                isBack={true}
                showSearch={true}
                showLogo={true}
            /> : <MobileHeader/>}
            <div style={{ marginTop: isMobile ? 70 : 100, justifyContent: "center" }}>
                <Tabs>
                    <HorizontalScroll
                        getRef={scrollRef}
                        showArrows={true}
                        getScrollToLeft={i => i - 120}
                        getScrollToRight={i => i + 120}
                    >
                        <TabsItem selected={activeTab === ProfileTabs.MAIN}
                                  onClick={() => setActiveTab(ProfileTabs.MAIN)}>Главная</TabsItem>
                        <TabsItem selected={activeTab === ProfileTabs.INFO}
                                  onClick={() => setActiveTab(ProfileTabs.INFO)}>Личная информация</TabsItem>
                        <TabsItem selected={activeTab === ProfileTabs.ORDERS}
                                  onClick={() => setActiveTab(ProfileTabs.ORDERS)}>Заказы и возврат</TabsItem>
                        <TabsItem selected={activeTab === ProfileTabs.CHECKS}
                                  onClick={() => setActiveTab(ProfileTabs.CHECKS)}>Электронная чеки</TabsItem>
                        <TabsItem selected={activeTab === ProfileTabs.FAVOURITE}
                                  onClick={() => setActiveTab(ProfileTabs.FAVOURITE)}>Избранное</TabsItem>
                        <TabsItem selected={activeTab === ProfileTabs.NOTIFICATIONS}
                                  onClick={() => setActiveTab(ProfileTabs.NOTIFICATIONS)}>Настройки
                            уведомлений</TabsItem>
                    </HorizontalScroll>
                </Tabs>
            </div>

            <div>
                <SplitLayout
                    style={{ minHeight: 'calc(100vh - 300px)', marginTop: 20, justifyContent: "center" }}
                    onTouchStart={e => {
                        setTouchPos(e.touches[0].pageX)
                    }}
                    onTouchEnd={e => {
                        if(touchPos && e.changedTouches[0] && e.changedTouches[0].pageX - touchPos < -100){
                            if(activeTab < 5){
                                scrollTabs(120)
                                setActiveTab(activeTab + 1)
                            } else {
                                scrollTabs(-120 * 6)
                                setActiveTab(0)
                            }
                        }
                        if(touchPos && e.changedTouches[0] && e.changedTouches[0].pageX - touchPos > 100){
                            if(activeTab > 0){
                                scrollTabs(-120)
                                setActiveTab(activeTab - 1)
                            } else {
                                scrollTabs(120 * 6)
                                setActiveTab(5)
                            }
                        }
                        setTouchPos(null)
                    }}
                >
                    <SplitCol
                        width={!config.isMobile ? "560px" : "auto"}
                        maxWidth={!config.isMobile ? "800px" : "100%"}
                    >
                        <Tab
                            activeTab={activeTab}
                            setActiveModal={setActiveTab}
                        />
                    </SplitCol>
                </SplitLayout>
            </div>
            <Footer/>
        </Panel>
    )
})

export default Profile;