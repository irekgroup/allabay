import React, { FC } from "react";
import MainTab from "./Tabs/MainTab/MainTab";
import PersonalInformation from "./Tabs/PersonalInformation/PersonalInformation";
import Orders from "./Tabs/Orders/Orders";
import Checks from "./Tabs/Checks/Checks";
import Favorite from "./Tabs/Favorite/Favorite";
import Notifacations from "./Tabs/Notifacations/Notifacations";
import { ProfileTabs } from "./Profile";

interface IMyComponentProps {
    setActiveModal: (tab: ProfileTabs) => void,
    activeTab: any
}

const Tab: FC<IMyComponentProps> = ({ activeTab, setActiveModal }) => {
    switch(activeTab) {
        case ProfileTabs.MAIN:
            return(
                <MainTab setActiveModal={setActiveModal}/>
            )
        break;

        case ProfileTabs.INFO:
            return <PersonalInformation setActiveModal={setActiveModal} />
        break;

        case ProfileTabs.ORDERS:
            return <Orders setActiveModal={setActiveModal}/>
        break;

        case ProfileTabs.CHECKS:
            return <Checks />
        break;

        case ProfileTabs.FAVOURITE:
            return <Favorite setActiveModal={setActiveModal}/>
        break;

        case ProfileTabs.NOTIFICATIONS:
            return <Notifacations />
        break;

        default:
            return(
                <div />
            )
        break;
    }
}

export default Tab;