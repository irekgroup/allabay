import React, { FC, useContext, useState } from 'react';
import { Div, Card, CardGrid, Checkbox, FormItem, FormLayout, Title, Link, HorizontalScroll, Tabs, TabsItem } from "@vkontakte/vkui";
import Input from "../../../../components/Input/Input";
import Button from "../../../../components/Button/Button";
import './Orders.scss';
import { RootStoreContext } from "../../../../index";
import { ProfileTabs } from "../../Profile";
import { PAGE_FEED_DESKTOP, router } from '../../../../router';

interface OrdersProps {
    setActiveModal: (tab: ProfileTabs) => void;
}

enum OrdersTabs {
    SENDS,
    BUYS,
    RETURNS,
    RETURNS_PROCEED,
}

const Orders: FC<OrdersProps> = ({ setActiveModal }) => {
    const { config } = useContext(RootStoreContext)
    const [isOrder, setIsOrder] = useState(false)
    const [orderTab, setOrderTab] = useState<OrdersTabs>(OrdersTabs.SENDS);
    return (
        <div className={'orders'}>
             <Tabs>
                    <HorizontalScroll
                        showArrows={true}
                        getScrollToLeft={i => i - 120}
                        getScrollToRight={i => i + 120}>
                            <TabsItem selected={orderTab === OrdersTabs.SENDS} onClick={() => setOrderTab(OrdersTabs.SENDS)}>Доставки</TabsItem>
                            <TabsItem selected={orderTab === OrdersTabs.BUYS} onClick={() => setOrderTab(OrdersTabs.BUYS)}>Покупки</TabsItem>
                            <TabsItem selected={orderTab === OrdersTabs.RETURNS} onClick={() => setOrderTab(OrdersTabs.RETURNS)}>Возвраты</TabsItem>
                            <TabsItem selected={orderTab === OrdersTabs.RETURNS_PROCEED} onClick={() => setOrderTab(OrdersTabs.RETURNS_PROCEED)}>Оформление возврата</TabsItem>
                    </HorizontalScroll>
                </Tabs>
            <div className={`${isOrder ? 'orderPage' : ''} ${config.isMobile ? 'mob' : ''} container`}
                style={!config.isMobile ? { padding: "0 12px" } : {}}>
                {orderTab === OrdersTabs.SENDS &&
                    <div className={'order_tab'}>
                        <p>У Вас пока нет доставок.</p> 
                        <p>Воспользуйтесь поиском, чтобы найти что-то конкретное</p>
                        <Button onClick={() => router.pushPage(PAGE_FEED_DESKTOP)} mode={'secondary'}>Перейти на главную</Button>
                    </div>}
                    {orderTab === OrdersTabs.BUYS &&
                    <div className={'order_tab'}>
                        <p>У Вас пока нет покупок. </p> 
                        <p>Воспользуйтесь поиском, чтобы найти что-то конкретное</p>
                        <Button  onClick={() => router.pushPage(PAGE_FEED_DESKTOP)} mode={'secondary'}>Перейти на главную</Button>
                    </div>}
                    {orderTab === OrdersTabs.RETURNS &&
                    <div className={'order_tab'}>
                        <p>Эта страница пуста, потому что Вы пока не сделали ни одной заявки  на возврат.</p> 
                        <p>После того, как Вы сделаете возврат, здесь будет отображаться вся история Ваших заявок.</p>
                        <Button  onClick={() => router.pushPage(PAGE_FEED_DESKTOP)} mode={'secondary'}>Перейти на главную</Button>
                    </div>}
                    {orderTab === OrdersTabs.RETURNS_PROCEED &&
                    <div className={'order_tab'}>
                        <p>Для того, чтобы оформить возврат, зайдите в раздел <Link onClick={() => setOrderTab(OrdersTabs.BUYS)}>Покупки</Link> и выберите нужный товар.</p>
                        <Button  onClick={() => router.pushPage(PAGE_FEED_DESKTOP)} mode={'secondary'}>Перейти на главную</Button>
                    </div>}
               {false && <CardGrid size={"l"}>
                    <Card mode={'shadow'}>
                        <div className={`header_card`}>
                            <Title weight="medium"
                                   level="2">{!isOrder ? 'Фильтр по заказам' : 'Заказ № 0355229131'}</Title>
                            {!config.isMobile && isOrder &&
                            <div className={'btns'}>
                                <Button mode={'tetrairy'} onClick={() => setIsOrder(false)}>Отменить</Button>
                                <Button mode={'tetrairy'}>Оформить возврат</Button>
                            </div>
                            }
                        </div>
                        {!isOrder ? <FormLayout>
                            <FormItem>
                                <p>Тип заказа</p>
                                <div className={'orderType'}>
                                    <Checkbox>Покупка</Checkbox>
                                    <Checkbox>Возврат</Checkbox>
                                </div>
                            </FormItem>
                            <FormItem style={{ alignItems: 'flex-start' }}>
                                <p>Статусы заказа</p>
                                <div className="grid-2-2">
                                    <Checkbox>Создан</Checkbox>
                                    <Checkbox>В работе</Checkbox>
                                    <Checkbox>Закрыт</Checkbox>
                                    <Checkbox>Удален</Checkbox>
                                </div>

                            </FormItem>
                            <FormItem style={{ alignItems: 'flex-start' }}>
                                <p>Дата создания</p>
                                <div className="grid-date">
                                    <div className={'date left'}><span>с</span> <Input type={'date'}/></div>
                                    <div className={'date'}><span>по</span> <Input type={'date'}/></div>
                                </div>
                            </FormItem>
                            <FormItem style={{ alignItems: 'flex-start' }}>
                                <p>Поставка товара</p>
                                <div className="grid-date">
                                    <div className={'date left'}><span>с</span> <Input type={'date'}/></div>
                                    <div className={'date'}><span>по</span> <Input type={'date'}/></div>
                                </div>
                            </FormItem>
                            <FormItem className={'getBtn center'}>
                                <Button mode="secondary">Получить</Button>
                            </FormItem>
                        </FormLayout> : (
                            <Div>
                                <div className={'orderInfoItem'}>
                                    <p>Дата создания</p>
                                    <p> 2011212020 15:57:17</p>
                                </div>
                                <div className={'orderInfoItem'}>
                                    <p>Дата поставки </p>
                                    <p>C: 21.12.2020 По: 05.01.2021</p>
                                </div>
                                <div className={'orderInfoItem'}>
                                    <p>Покупатель </p>
                                    <p>Низамов Ирек Ильгамович</p>
                                </div>
                                <div className={'orderInfoItem'}>
                                    <p>Адрес доставки</p>
                                    <p>Низамов Ирек Ильгамович 423253, Республика Татарстан, Лениногорск, Переулок
                                        Стадионный, 3, тел. +789172373709</p>
                                </div>
                                <div className={'orderInfoItem'}>
                                    <p>Получатель </p>
                                    <p>Ирек +79172373709</p>
                                </div>
                                <div className={'orderInfoItem'}>
                                    <p>Сумма по заказу </p>
                                    <p>1 525.00 ₽</p>
                                </div>
                                <div className={'orderInfoItem'}>
                                    <p>Способ получения</p>
                                    <p>Доставка</p>
                                </div>
                                <div className={'orderInfoItem'}>
                                    <p>Пункт выдачи</p>
                                    <p>-----</p>
                                </div>
                                <div className={'orderInfoItem'}>
                                    <p>Статус</p>
                                    <p>Создан</p>
                                </div>
                            </Div>
                        )}
                    </Card>
                    <Card mode={'shadow'} className={'fullBlue'}>
                        <div className="header_card">
                            <Title weight="medium" level="2">{!isOrder ?
                                <div className={'sb'}><span>Результат поиска</span> <span>12 заказов на сумму: 24313.00 ₽</span>
                                </div> :
                                <span className={'center'}>Сумма 1552 ₽</span>}</Title>
                        </div>
                    </Card>
                    {!config.isMobile && <Card mode={'shadow'}>
                        <div className={`table ${isOrder ? 'order' : ''}`}>
                            {!isOrder ? <>
                                <div className="row title">
                                    <div>№ заказа</div>
                                    <div>Дата создания</div>
                                    <div>Дата поставки</div>
                                    <div>Тип доставки</div>
                                    <div>Сумма заказа</div>
                                </div>
                                <Order setIsOrder={setIsOrder}/>
                                <Order setIsOrder={setIsOrder}/>
                                <Order setIsOrder={setIsOrder}/>
                                <Order setIsOrder={setIsOrder}/>
                                <Order setIsOrder={setIsOrder}/>
                            </> : <>
                                <div className="row title">
                                    <div>Бренд</div>
                                    <div>Артикул</div>
                                    <div>Количество</div>
                                    <div>Наименование</div>
                                    <div>Сумма</div>
                                    <div>Ожидаемая дата</div>
                                </div>
                                <div className="row">
                                    <div>0355635047</div>
                                    <div>12.12.2020</div>
                                    <div>16.12.2020</div>
                                    <div>Доставка до пункта выдачи Почта России</div>
                                    <div>1525.00 ₽</div>
                                    <div>28.12.2020</div>
                                </div>
                                <div className="row">
                                    <div>0355635047</div>
                                    <div>12.12.2020</div>
                                    <div>16.12.2020</div>
                                    <div>Доставка до пункта выдачи Почта России</div>
                                    <div>1525.00 ₽</div>
                                    <div>28.12.2020</div>
                                </div>
                                <div className="row">
                                    <div>0355635047</div>
                                    <div>12.12.2020</div>
                                    <div>16.12.2020</div>
                                    <div>Доставка до пункта выдачи Почта России</div>
                                    <div>1525.00 ₽</div>
                                    <div>28.12.2020</div>
                                </div>
                            </>}
                        </div>
                    </Card>}
                    {config.isMobile && !isOrder && <div className={'ordersList'}>
                        <Order setIsOrder={setIsOrder}/>
                        <Order setIsOrder={setIsOrder}/>
                        <Order setIsOrder={setIsOrder}/>
                        <Order setIsOrder={setIsOrder}/>
                    </div>}
                    {config.isMobile && isOrder && <div className={'ordersList'}>
                        <OrderPart/>
                        <OrderPart/>
                        <OrderPart/>
                        <OrderPart/>
                        <Card mode={'shadow'} className={'fullBlue'}>
                            <div className="header_card">
                                <Title weight="medium" level="2">Итого</Title>
                            </div>
                        </Card>
                        <div className={'res'}>
                            <div className={'item'}>
                                <div>Количество</div>
                                <div>234</div>
                            </div>
                            <div  className={'item'}>
                                <div>Сумма</div>
                                <div>1 525.00 ₽ </div>
                            </div>
                        </div>
                    </div>}
                </CardGrid>}
                {
                    config.isMobile && <div className={'notification'}>Уведомления: {!config.isMobile &&
                    <span className={'blue'}>&nbsp;не подключены</span>} <Button mode={'secondary'}>Подключить</Button>
                    </div>
                }
            </div>
        </div>
    );
};

const Order = ({ setIsOrder }: { setIsOrder: (b: boolean) => void }) => {
    const { config } = useContext(RootStoreContext)
    return (
        <div className={'order'}>
            {!config.isMobile ?
                <div className="row">
                    <div onClick={() => setIsOrder(true)}>0355635047</div>
                    <div>12.12.2020</div>
                    <div>16.12.2020</div>
                    <div>Доставка до пункта выдачи Почта России</div>
                    <div>1525.00 ₽</div>
                </div> :
                <Card mode={'shadow'} className={'orderCard'}>
                    <div><span>№ заказа</span>0355635047</div>
                    <div><span>Дата создания</span>12.12.2020</div>
                    <div><span>Дата поставки</span>16.12.2020</div>
                    <div><span>Тип поставки</span>Доставка до пункта выдачи Почта России</div>
                    <div><span>Сумма заказа</span>1525.00 ₽</div>
                    <Button mode={'secondary'} onClick={() => setIsOrder(true)}>Подробнее</Button>
                </Card>}
        </div>


    )
}

const OrderPart = () => {
    return (
        <Card mode={'shadow'} className={'orderCard orderPart'}>
            <h1>№1</h1>
            <div  className={'orderInfoItem'}><span>Бренд</span><span>0355635047</span></div>
            <div  className={'orderInfoItem'}><span>Артикул</span><span>12.12.2020</span></div>
            <div  className={'orderInfoItem'}><span>Количество</span><span>16.12.2020</span></div>
            <div className={'orderInfoItem'}><span>Наименование</span><span>Доставка до пункта выдачи Почта России</span></div>
            <div className={'orderInfoItem'}><span>Сумма</span><span>1525.00 ₽</span></div>
            <div className={'orderInfoItem'}><span>Ожидаемая дата</span><span>22222</span></div>
        </Card>
    )
}

export default Orders;