import {
    Button,
    Card,
    CardGrid,
    Div,
    FormItem,
    FormLayout,
    FormLayoutGroup,
    Input, Link,
    List,
    Radio, RadioGroup,
    Select,
    Title,
    useAdaptivity,
    ViewWidth
} from "@vkontakte/vkui";
import { Icon28ChevronDownOutline } from '@vkontakte/icons';
import { Checkbox } from "@vkontakte/vkui/dist/components/Checkbox/Checkbox";
import React, { FC, useContext, useState } from "react";
import "./PersonalInformation.css"
import tip from '../../../../img/main/tip-open.svg';
import { TextTooltip } from "@vkontakte/vkui/unstable";
import { ProfileTabs } from "../../Profile";
import { RootStoreContext } from "../../../../index";
import { useRouter } from "@happysanta/router";
import {
    MODAL_PERSONAL_INFORMATION,
    MODAL_PROFILE_ADD_CAR,
    MODAL_PROFILE_ADDRESS,
    MODAL_PROFILE_ADDRESS_SELF
} from "../../../../router";
import AddressChange from "../../../../components/AddressChange/AddressChange";

interface Props {
    setActiveModal: (tab: ProfileTabs) => void
}

const PersonalInformation: FC<Props> = ({ setActiveModal }) => {

    const [isAddress, setIsAddress] = useState(0);
    const [sex, setSex] = useState(1);
    const { viewWidth }: any = useAdaptivity();
    const { config } = useContext(RootStoreContext);
    const adresses = [];

    const router = useRouter()

    return (
        <div className="main">
            <div className={`${config.isMobile ? 'mob' : ''} container personalInformation`}
                 style={!config.isMobile ? { padding: "0 12px" } : {}}>
                {!isAddress ? <CardGrid size={viewWidth > ViewWidth.MOBILE ? "m" : "l"}>
                    <Card mode="shadow">
                        <div className="header_card">
                            <Title weight="medium" level="2">Персональные данные</Title>
                        </div>
                        {!config.isMobile ?
                            <FormLayout className="contentCardForm">
                                <FormItem>
                                    <Input placeholder="ФИО"/>
                                </FormItem>

                                <FormItem>
                                    <Input placeholder="Номер телефона"/>
                                </FormItem>

                                <FormItem>
                                    <Input placeholder="Почта"/>
                                </FormItem>

                                <FormLayoutGroup mode={config.isMobile ? "vertical" : "horizontal"}>
                                    <FormItem top={!config.isMobile && "Дата рождения"}>
                                        <Input type="date"/>
                                    </FormItem>

                                    <FormItem>
                                        <RadioGroup mode={config.isMobile ? "horizontal" : "vertical"}>
                                            <Radio name={'sex'} value={'men'} onChange={() => setSex(1)}>Мужской</Radio>
                                            <Radio style={!config.isMobile ? { marginTop: 5 } : {}} name={'sex'}
                                                   value={'woman'} onChange={() => setSex(2)}>Женский</Radio>
                                        </RadioGroup>
                                    </FormItem>
                                </FormLayoutGroup>

                                <FormItem className={'center'}>
                                    <Button mode="primary">Добавить получателя</Button>
                                </FormItem>
                            </FormLayout> :
                            <FormLayout onClick={() => router.pushModal(MODAL_PERSONAL_INFORMATION)}>
                                <FormItem>
                                    <Input placeholder="ФИО"/>
                                </FormItem>
                                <FormItem className={'center'}>
                                    <Button mode="primary">Добавить получателя</Button>
                                </FormItem>
                            </FormLayout>}
                    </Card>

                    <div style={viewWidth > ViewWidth.MOBILE ? { width: '49.48%' } : {
                        width: '100%',
                        marginTop: '8px',
                        marginBottom: '8px'
                    }}>
                        <Card mode="shadow" style={{ width: '100%', marginBottom: "8px" }}>
                            <div className="header_card">
                                <Title weight="medium" level="2">Адреса доставки</Title>
                            </div>
                            {adresses.length === 0 ?
                                <FormLayout>
                                    <FormItem>
                                        <Input placeholder="Ваша страна"
                                               onClick={() => config.isMobile ? router.pushModal(MODAL_PROFILE_ADDRESS) : setIsAddress(1)}/>
                                    </FormItem>
                                    <FormItem className={'center'}>
                                        <Button className={'center'} mode="primary"
                                                onClick={() => config.isMobile ? router.pushModal(MODAL_PROFILE_ADDRESS) : setIsAddress(1)}>
                                            Добавить адрес
                                        </Button>
                                    </FormItem>
                                </FormLayout>
                                :
                                <List>

                                </List>
                            }
                        </Card>
                        <Card mode={'shadow'} style={{ width: '100%' }}>
                            <div className="header_card">
                                <Title style={{ display: "flex", alignItems: 'center' }} weight="medium" level="2">
                                    Пункты выдачи
                                    <TextTooltip
                                        hideDelay={2000}
                                        text={'Найдите ближайший пункт выдачи в вашем городе.\n ' +
                                            'Если пункта выдачи нет, создайте его сами.\n ' +
                                            'Это может быть отделение Почты Росии,\n ' +
                                            'Курьерских служб, транспорных компаний и даже\n ' +
                                            'Пятерочка в вашем доме.'}>
                                        <img style={{ cursor: 'pointer', marginLeft: '5px' }} height={16} src={tip}
                                             alt="tip"/>
                                    </TextTooltip>
                                </Title>
                            </div>
                            <FormLayout>
                                <FormItem>
                                    <Input placeholder="Ваша страна"
                                           onClick={() => config.isMobile ? router.pushModal(MODAL_PROFILE_ADDRESS_SELF) : setIsAddress(2)}/>
                                </FormItem>

                                <FormItem className={'center'}>
                                    <Button mode="primary"
                                            onClick={() => config.isMobile ? router.pushModal(MODAL_PROFILE_ADDRESS_SELF) : setIsAddress(2)}>
                                        Добавить адрес
                                    </Button>
                                </FormItem>
                            </FormLayout>
                        </Card>
                    </div>

                    <div style={viewWidth > ViewWidth.MOBILE ? {
                        marginRight: '8px',
                        marginTop: '8px',
                        width: '49.48%'
                    } : { width: '100%', marginTop: '8px', marginBottom: '8px' }}>
                        <Card mode="shadow" style={{ width: '100%', marginBottom: "8px" }}>
                            <div className="header_card" style={{ display: "flex", alignItems: 'center' }}>
                                <Title weight="medium" level="2">Гараж</Title>
                                <TextTooltip
                                    hideDelay={2000}
                                    text={'Найдите модель по VIN-номеру VIN автомобиля\n' +
                                        'является самым надежным идентификатором\n' +
                                        'В гараж можно добавить неограниченное количество ТС.\n' +
                                        'Задайте ТС по умолчанию для заказа запчастей в один клик.'}>
                                    <img style={{ cursor: 'pointer', marginLeft: '5px' }} height={16} src={tip} alt="tip"/>
                                </TextTooltip>
                            </div>
                            <List>
                                <FormLayout>
                                    <FormItem>
                                        <Input placeholder="Введите VIN код"
                                               onClick={() => router.pushModal(MODAL_PROFILE_ADD_CAR)}/>
                                    </FormItem>

                                    <FormItem className={'center'}>
                                        <Button mode="primary" onClick={() => router.pushModal(MODAL_PROFILE_ADD_CAR)}>
                                            Добавить TC
                                        </Button>
                                    </FormItem>
                                </FormLayout>
                            </List>
                        </Card>

                        {!config.isMobile && <Card mode={'shadow'} style={{ width: '100%' }}>
                            <FormLayoutGroup mode="vertical">
                                <FormItem>
                                    <Select placeholder={'Размер одежды'} options={[
                                        { value: '36', label: '36' },
                                        { value: '37', label: '37' },
                                        { value: '38', label: '38' },
                                        { value: '39', label: '39' },
                                        { value: '40', label: '40' },
                                    ]}/>
                                </FormItem>

                                <FormItem>
                                    <Select placeholder={'Размер обуви'} options={[
                                        { value: '36', label: '36' },
                                        { value: '37', label: '37' },
                                        { value: '38', label: '38' },
                                        { value: '39', label: '39' },
                                        { value: '40', label: '40' },
                                    ]}/>
                                </FormItem>
                            </FormLayoutGroup>
                        </Card>}
                        {
                            !config.isMobile &&
                            <Card mode={'shadow'} style={{ width: '100%' }}>
                                <div className="header_card">
                                    <Title weight="medium" level="2">Удаление личного кабинета</Title>
                                </div>
                                <Div style={{ paddingTop: 0, }}>
                                    <p style={{ marginTop: 0, color: "#707070" }}>Как только Ваш Личный Кабинет будет
                                        удален,
                                        Вы автоматически выйдете из системы и
                                        больше не сможете войти в этот аккаунт.</p>
                                    <Link style={{ color: '#1E7CF2' }}>Удалить личный кабинет</Link>
                                </Div>
                            </Card>
                        }
                    </div>
                    <Card mode={'shadow'}>
                        <div className="header_card">
                            <Title weight="medium" level="2" style={{ display: "flex", alignItems: 'center' }}>Способ
                                получения
                                <TextTooltip
                                    hideDelay={2000}
                                    text={'Укажите ваш предпочтительный способ получения заказа. \n' +
                                        'Мы будем учитывать эти данные при оформления заказа. \n' +
                                        'Иногда возможный способ доставки может отличиаться от \n' +
                                        'предпочительного, возможный способ доставки отображается\n' +
                                        'в карточке товара и зависит от вашего местоположения. \n'}>
                                    <img style={{ cursor: 'pointer', marginLeft: '5px' }} height={16} src={tip}
                                         alt="tip"/>
                                </TextTooltip>
                            </Title>
                        </div>
                        <FormLayout className={'radioList'}>
                            <FormItem>
                                <Radio name={'send'} value={1}>
                                    Самовывоз в ПВЗ в день заказа до 12:00
                                </Radio>
                            </FormItem>
                            <FormItem>
                                <Radio name={'send'} value={2}>
                                    Turbo доставка до ПВЗ за 1 час
                                </Radio>
                            </FormItem>
                            <FormItem>
                                <Radio name={'send'} value={3}>
                                    Turbo доставка до адреса за 1 час
                                </Radio>
                            </FormItem>
                            <FormItem>
                                <Radio name={'send'} value={4}>
                                    Курьерская доставка до ПВЗ за 1-2 дня
                                </Radio>
                            </FormItem>
                            <FormItem>
                                <Radio name={'send'} value={5}>
                                    Курьерская доставка до адреса за 1-2 дня
                                </Radio>
                            </FormItem>
                            <FormItem>
                                <Radio name={'send'} value={6}>
                                    Самовывоз из ПВЗ доставка 5-7 дней
                                </Radio>
                            </FormItem>
                            <FormItem>
                                <Radio name={'send'} value={7}>
                                    Самовывоз из Почта Росии 5-7 дней
                                </Radio>
                            </FormItem>
                            <FormItem>
                                <Radio name={'send'} value={8}>
                                    Курьерская, до адреса, Почта Росии 5-7 дней
                                </Radio>
                            </FormItem>
                            <FormItem>
                                <Radio name={'send'} value={9}>
                                    Самовывоз в ТК (ПЭК, СДЕК, ДЕЛОВЫЕ ЛИНИИ и т.д.)
                                </Radio>
                            </FormItem>
                            <FormItem>
                                <Radio name={'send'} value={10}>
                                    Курьерская до адреса ТК (ПЭК, СДЕК, ДЕЛОВЫЕ ЛИНИИ и т.д.)
                                </Radio>
                            </FormItem>
                        </FormLayout>
                    </Card>
                    {config.isMobile && (
                        sex === 2 ?
                            <SizesW/> :
                            <SizesM/>
                    )
                    }
                    {config.isMobile &&
                        <Card mode={'shadow'} style={{ width: '100%' }}>
                            <div className="header_card">
                                <Title weight="medium" level="2">Удаление личного кабинета</Title>
                            </div>
                            <Div style={{ paddingTop: 0, }}>
                                <p style={{ marginTop: 0, color: "#707070" }}>Как только Ваш Личный Кабинет будет
                                    удален,
                                    Вы автоматически выйдете из системы и
                                    больше не сможете войти в этот аккаунт.</p>
                                <Link style={{ color: '#1E7CF2' }}>Удалить личный кабинет</Link>
                            </Div>
                        </Card>
                    }
                </CardGrid> : <AddressChange/>}
                {config.isMobile && <div className={'notification'}>Уведомления: {!config.isMobile &&
                    <span className={'blue'}>&nbsp;не подключены</span>} <Button mode={'primary'}>Подключить</Button>
                </div>}
            </div>
        </div>
    );
}

const SizesM = () => {
    const [open, setOpen] = useState(false)
    const [openShoes, setOpenShoes] = useState(false)
    return (
        <Card className={'sizes'} mode={'shadow'}>
            <div className="header_card" onClick={() => setOpen(!open)}>
                <Title weight="medium" level="2"><Icon28ChevronDownOutline/> Размер одежды М</Title>
            </div>
            <FormLayout className={`formLayout ${open ? 'formLayout-close' : ''}`}>
                <FormItem>
                    <Checkbox>
                        <span>XS</span>
                        <span>Рост до 164</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>S</span>
                        <span>Рост от 165 до 173</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>M</span>
                        <span>Рост от 173 до 178</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>L</span>
                        <span>Рост от 179 до 184</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>XL</span>
                        <span>Рост от 185 до 189</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>2XL</span>
                        <span>Рост от 190 до 194</span>
                    </Checkbox>
                </FormItem>
            </FormLayout>
            <div className="header_card" onClick={() => setOpenShoes(!openShoes)}>
                <Title weight="medium" level="2"><Icon28ChevronDownOutline/>Размер обуви</Title>
            </div>
            <FormLayout className={`shoes formLayout ${openShoes ? 'formLayout-close' : ''}`}>
                <div>
                    <FormItem>
                        <Checkbox>
                            <span>39</span>
                            <span>25 см.</span>
                        </Checkbox>
                    </FormItem>
                    <FormItem>
                        <Checkbox>
                            <span>39,5</span>
                            <span>25,5 см.</span>
                        </Checkbox>
                    </FormItem>
                    <FormItem>
                        <Checkbox>
                            <span>40</span>
                            <span>26 см.</span>
                        </Checkbox>
                    </FormItem>
                    <FormItem>
                        <Checkbox>
                            <span>40,5</span>
                            <span>26,5 см.</span>
                        </Checkbox>
                    </FormItem>
                    <FormItem>
                        <Checkbox>
                            <span>41</span>
                            <span>27 см.</span>
                        </Checkbox>
                    </FormItem>
                    <FormItem>
                        <Checkbox>
                            <span>41,5</span>
                            <span>27,5 см.</span>
                        </Checkbox>
                    </FormItem>
                    <FormItem>
                        <Checkbox>
                            <span>42</span>
                            <span>28 см.</span>
                        </Checkbox>
                    </FormItem>
                </div>
                <div>
                    <FormItem>
                        <Checkbox>
                            <span>42,5</span>
                            <span>28,5 см.</span>
                        </Checkbox>
                    </FormItem>
                    <FormItem>
                        <Checkbox>
                            <span>43</span>
                            <span>29 см.</span>
                        </Checkbox>
                    </FormItem>
                    <FormItem>
                        <Checkbox>
                            <span>43,5</span>
                            <span>29,5 см.</span>
                        </Checkbox>
                    </FormItem> <FormItem>
                    <Checkbox>
                        <span>44</span>
                        <span>30 см.</span>
                    </Checkbox>
                </FormItem>
                    <FormItem>
                        <Checkbox>
                            <span>45</span>
                            <span>31 см.</span>
                        </Checkbox>
                    </FormItem>
                    <FormItem>
                        <Checkbox>
                            <span>46</span>
                            <span>32 см.</span>
                        </Checkbox>
                    </FormItem>
                </div>
            </FormLayout>
        </Card>
    )
}

const SizesW = () => {
    const [open, setOpen] = useState(false)
    const [openShoes, setOpenShoes] = useState(false)
    return (
        <Card className={'sizes sizes-woman'} mode={'shadow'}>
            <div className="header_card" onClick={() => setOpen(!open)}>
                <Title weight="medium" level="2"><Icon28ChevronDownOutline/>Размер одежды Ж</Title>
            </div>
            <div className={`row formLayout ${open ? 'formLayout-close' : ''}`}>
                <div></div>
                <div>Охват груди, см</div>
                <div>Охват талии, см</div>
                <div>Охват бедер, см</div>
            </div>
            <FormLayout className={`top formLayout ${open ? 'formLayout-close' : ''}`}>
                <FormItem>
                    <Checkbox>
                        <span>XXS/38</span>
                        <span>76</span>
                        <span>58</span>
                        <span>82</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>XS/40</span>
                        <span>80</span>
                        <span>62</span>
                        <span>86</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>S/42</span>
                        <span>84</span>
                        <span>66</span>
                        <span>92</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>M/44</span>
                        <span>88</span>
                        <span>70</span>
                        <span>96</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>M/46</span>
                        <span>92</span>
                        <span>74</span>
                        <span>100</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>L/48</span>
                        <span>96</span>
                        <span>78</span>
                        <span>104</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>L/50</span>
                        <span>100</span>
                        <span>82</span>
                        <span>108</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>XL/52</span>
                        <span>104</span>
                        <span>86</span>
                        <span>112</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>2XL/54</span>
                        <span>108</span>
                        <span>90</span>
                        <span>116</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>3XL/52</span>
                        <span>112</span>
                        <span>94</span>
                        <span>120</span>
                    </Checkbox>
                </FormItem>
            </FormLayout>
            <div className="header_card" onClick={() => setOpenShoes(!openShoes)}>
                <Title weight="medium" level="2"><Icon28ChevronDownOutline/>Размер обуви</Title>
            </div>
            <FormLayout className={`formLayout ${openShoes ? 'formLayout-close' : ''}`}>
                <FormItem>
                    <Checkbox>
                        <span>35</span>
                        <span>22-22,5 см.</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>36</span>
                        <span>22,5-23 см.</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>37</span>
                        <span>23-23,5 см.</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>38</span>
                        <span>24-24,5 см.</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>39</span>
                        <span>24,5-25 см.</span>
                    </Checkbox>
                </FormItem>
                <FormItem>
                    <Checkbox>
                        <span>40</span>
                        <span>25-25,5 см.</span>
                    </Checkbox>
                </FormItem>
            </FormLayout>
        </Card>
    )
}

export default PersonalInformation;