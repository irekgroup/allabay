import React, { FC, useContext } from 'react';
import './Notifacations.scss';
import Button from "../../../../components/Button/Button";
import { RootStoreContext } from "../../../../index";

const Notifacations:FC = () => {
    const {config} = useContext(RootStoreContext)
    return (
        <div className={'notifacations'}>
            <p>У Вас пока нет подключенных уведомлений</p> <p>Благодаря уведомлениям вы не пропустите ни одного сообщения</p>
            <div className={`${config.isMobile ? 'mob' : ''} container`}>
                {/* {!config.isMobile && <h1>На связи всегда, в любом удобном месте. Выбери свой месседжер,
                    храни историю в одном месте </h1>} */}
                <div className="socialList">
                    <div>
                        What`s App
                        <Button mode={'secondary'}>Покдлючить</Button>
                    </div>
                    <div>
                        Telegram
                        <Button mode={'secondary'}>Покдлючить</Button>
                    </div>
                    <div>
                        Вконтакте
                        <Button mode={'secondary'}>Покдлючить</Button>
                    </div>
                    <div>
                        Facebook
                        <Button mode={'secondary'}>Покдлючить</Button>
                    </div>
                </div>
                {config.isMobile && <div className={'notification'}>Уведомления: {!config.isMobile &&
                <span className={'blue'}>&nbsp;не подключены</span>} <Button mode={'secondary'}>Подключить</Button>
                </div>}
            </div>
        </div>
    );
};

export default Notifacations;