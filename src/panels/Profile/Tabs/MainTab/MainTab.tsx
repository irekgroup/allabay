import {
    Avatar,
    Card,
    CardGrid,
    Cell,
    Div,
    Link,
    Placeholder,
    Title,
    useAdaptivity,
    ViewWidth,
} from '@vkontakte/vkui';
import React, { FC, useContext } from 'react';
import { Icon24Favorite } from '@vkontakte/icons';
import './MainTab.css';
import { RootStoreContext } from "../../../../index";
import LetterAvatar from "../../../../components/LetterAvatar/LetterAvatar";
import Button from "../../../../components/Button/Button";
import { ProfileTabs } from "../../Profile";
import placeholder from '../../../../img/cart/cart-img-example.png';
import notification from '../../../../img/cardUser/notification.svg';

interface MainTabProps {
    setActiveModal: (tab: ProfileTabs) => void;
}

const MainTab: FC<MainTabProps> = ({ setActiveModal }) => {
    const { viewWidth }: any = useAdaptivity();
    const { user, chat, config } = useContext(RootStoreContext);
    return (
        <div className="main">
            <div className={`${config.isMobile ? 'mob' : ''} container`}>
                <CardGrid size={viewWidth > ViewWidth.MOBILE ? 'm' : 'l'}>
                    <Card mode="shadow">
                        <div className={`${config.isMobile ? 'grid' : ''} contentCard`}
                             style={config.isMobile ? {} : {marginTop: 10}}
                             onClick={() => config.isMobile && setActiveModal(ProfileTabs.INFO)}>
                            {config.isMobile && (user.userData.photo ?
                                (<Avatar src={user.userData.photo}/>) :
                                (<LetterAvatar height={50} width={50}/>))}
                            <div>
                                <Cell className="cell" before={!config.isMobile && (user.userData.photo ?
                                    <Avatar src={user.userData.photo}/> : <LetterAvatar/>)}>
                                    <span className={'profile-header-row'}>
                                        <Title style={!config.isMobile ? { marginLeft: 10 } : { }}
                                               weight="medium"
                                               level="3">{user.userData.firstName} {user.userData.lastName}</Title>
                                        <span className={'notificationWrapper'}>
                                            {(+chat.newMessagesCount > 0) &&
                                                <span className="notice">{chat.newMessagesCount}</span>}
                                            <img src={notification} alt="notification"/>
                                        </span>
                                    </span>
                                </Cell>
                                <Div className="info_cell">
                                    <span>Email: <br/></span>
                                    {!config.isMobile &&
                                    <>  Телефон: {user.userData.phone}<br/>
                                        Дата рорждения: {user.userData.birthdate}<br/>
                                    </>}
                                </Div>
                            </div>
                        </div>
                        {!config.isMobile && <div className="exit" onClick={() => user.logout()}>Выйти</div>}
                    </Card>

                    <Card mode="shadow">
                        <div className="contentCard">
                            <div className="header_card">
                                <Title weight="medium" level="2" onClick={() => config.isMobile && setActiveModal(ProfileTabs.ORDERS)}>{config.isMobile ?
                                    <span>Заказы и возврат</span> : 'Заказы и возврат'}</Title>
                            </div>
                            {/*empty*/ true ? <Placeholder>У Вас пока нет покупок. <br/>
                                    Воспользуйтесь поиском, чтобы найти что-то конкретное</Placeholder>
                                : /*<div className={'cellGrid'}>
                                    <img src={placeholder} alt=""/>
                                    <div>
                                        <div className="info_cell">
                                            <Title weight="medium" level="3">Капот</Title>
                                            <p>3268₽</p>
                                            <p>доставка: 24.07</p>
                                        </div>
                                    </div>
                                </div>*/
                            <Placeholder>10 товаров</Placeholder>}
                        </div>
                    </Card>
                    <Card mode="shadow">
                        <div className="contentCard">
                            <div className="header_card" onClick={() => config.isMobile && setActiveModal(ProfileTabs.CHECKS)}>
                                <Title weight="medium" level="2"><span>Электронные чеки </span></Title>
                            </div>
                            {/*empty*/ true ? <Placeholder>Вы еще не совершали никаких операций. <br/>
                                    Мы будем отображать здесь ваши покупки.</Placeholder>
                              : <div className={'cellGrid'}>
                                    <div className="info_cell info_cell-flex">
                                        <p><span>13.03.2021 в 23:20</span><span>2 713 ₽</span><span>Приход</span><Link>Посмотреть чек</Link></p>
                                        <p><span>13.03.2021 в 23:20</span><span>2 713 ₽</span><span>Приход</span><Link>Посмотреть чек</Link></p>
                                    </div>
                                </div>}
                        </div>
                    </Card>
                    <Card mode="shadow">
                        <div className="contentCard">
                            <div className="header_card" onClick={() => config.isMobile && setActiveModal(ProfileTabs.FAVOURITE)}>
                                <Title weight="medium" level="2"><span>Избранное </span></Title>
                            </div>
                            {/*empty*/  true ? <Placeholder>Здесь появятся товары, отмеченные как понравившиеся
                                    значком <Icon24Favorite fill={'#f8961d'}/> <br/>
                                    Вы также можете добавлять товары которых нет в наличии.
                                    Если они вновь поступят в продажу, эта информация
                                    отобразится в данном разделе</Placeholder> :
                                <div className={'cellGrid favCell'}>
                                    <div className="favList">
                                        <div className="favList_item" style={{background: `url(${placeholder})`}}></div>
                                        <div className="favList_item" style={{background: `url(${placeholder})`}}></div>
                                        <div className="favList_item" style={{background: `url(${placeholder})`}}></div>
                                        <div className="favList_item" style={{background: `url(${placeholder})`}}></div>
                                        <div className="favList_item" style={{background: `url(${placeholder})`}}></div>
                                        <div className="favList_item" style={{background: `url(${placeholder})`}}></div>
                                        <div className="favList_item more">+62</div>
                                    </div>
                                    <p>Доступно к заказу <b>46</b></p>
                                </div>}
                        </div>
                    </Card>
                </CardGrid>
                <div className={'notification'}>Уведомления: {!config.isMobile &&
                <span className={'blue'}>&nbsp;не подключены</span>} <Button mode={'secondary'}>Подключить</Button>
                </div>
                {config.isMobile &&
                <div className={'exitBtn'}>
                    <Button mode={'tetrairy'} onClick={() => user.logout()}>Выйти</Button>
                </div>}
            </div>
        </div>
    );
};

export default MainTab;
