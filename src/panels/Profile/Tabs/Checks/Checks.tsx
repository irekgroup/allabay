import React, { FC, useContext, useState } from 'react';
import './Checks.scss';
import { RootStoreContext } from "../../../../index";
import { Card, HorizontalScroll, Tabs, TabsItem } from "@vkontakte/vkui";
import Button from "../../../../components/Button/Button";
import { useRouter } from '@happysanta/router';
import { PAGE_FEED_DESKTOP } from '../../../../router';

enum ChecksTabs {
    ORDERS,
    OPERATIONS,
    CHECKS,
}

const Checks: FC = () => {
    const { config } = useContext(RootStoreContext);
    const [checksTab, setChecksTab] = useState<ChecksTabs>(0);
    const router = useRouter()
    return (
        <div className={'checks'}>
                <Tabs>
                    <HorizontalScroll
                        showArrows={true}
                        getScrollToLeft={i => i - 120}
                        getScrollToRight={i => i + 120}>
                            <TabsItem selected={checksTab === ChecksTabs.ORDERS} onClick={() => setChecksTab(ChecksTabs.ORDERS)}>История покупок</TabsItem>
                            <TabsItem selected={checksTab === ChecksTabs.OPERATIONS} onClick={() => setChecksTab(ChecksTabs.OPERATIONS)}>История операций</TabsItem>
                            <TabsItem selected={checksTab === ChecksTabs.CHECKS} onClick={() => setChecksTab(ChecksTabs.CHECKS)}>Электронные чеки</TabsItem>
                    </HorizontalScroll>
                </Tabs>
                {checksTab === ChecksTabs.ORDERS && 
                    <div className={'checks_tab'}>
                        <p>Вы еще не совершали никаких операций. </p> 
                        <p>Мы будем отображать здесь ваши покупки.</p>
                        <Button onClick={() => router.pushPage(PAGE_FEED_DESKTOP)} mode={'secondary'}>Перейти на главную</Button>
                    </div>} 
                    {checksTab === ChecksTabs.OPERATIONS && 
                    <div className={'checks_tab'}>
                        <p>Вы еще не совершали никаких операций. </p> 
                        <p>Мы будем отображать здесь ваши покупки.</p>
                        <Button onClick={() => router.pushPage(PAGE_FEED_DESKTOP)} mode={'secondary'}>Перейти на главную</Button>
                    </div>}
                    {checksTab === ChecksTabs.CHECKS && 
                    <div className={'checks_tab'}>
                        <p>Здесь будут отображаться Ваши электронные чеки.</p> 
                        <Button onClick={() => router.pushPage(PAGE_FEED_DESKTOP)} mode={'secondary'}>Перейти на главную</Button>
                    </div>}
           {false && <div className={`${config.isMobile ? 'mob' : ''} container`}>
                {!config.isMobile && <h1>Электронные чеки</h1>}
                {!config.isMobile && <div className="table">
                    <div className="row title">
                        <div>Дата создания чека</div>
                        <div>Сумма</div>
                        <div>Тип операции</div>
                        <div>Чек</div>
                        <div>Отправка</div>
                    </div>
                    <div className="row">
                        <div>13 марта 2021 в 23:20</div>
                        <div>2 713 ₽</div>
                        <div>Приход</div>
                        <div>Посмотреть чек</div>
                        <div>на Email</div>
                    </div>
                    <div className="row">
                        <div>13 марта 2021 в 23:20</div>
                        <div>2 713 ₽</div>
                        <div>Приход</div>
                        <div>Посмотреть чек</div>
                        <div>на Email</div>
                    </div>
                </div>}
                {config.isMobile && <div className={'checks-list'}>
                    <CheckCard/>
                    <CheckCard/>
                    <CheckCard/>
                    <CheckCard/>
                </div>}
                {config.isMobile && <div className={'notification'}>Уведомления: {!config.isMobile &&
                <span className={'blue'}>&nbsp;не подключены</span>} <Button mode={'secondary'}>Подключить</Button>
                </div>}
            </div>}
        </div>
    );
};

const CheckCard =() => {
    return(
        <Card className={'checkCard'} mode={'shadow'}>
            <h2>Электронные чеки</h2>
            <div className="col">
                <div className="row"><span>Дата создания</span><span>12.31.3211</span></div>
                <div className="row"><span>Сумма</span><span>2 713₽</span></div>
                <div className="row"><span>Тип операции</span><span>Приход</span></div>
                <div className="row"><span>Чек</span><span>Посмотреть чек</span></div>
                <div className="row"><span>Отправка</span><span>На Email</span></div>
            </div>
        </Card>
    )
}

export default Checks;