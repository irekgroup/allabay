import React, { FC, useState } from 'react';
import { Card, Link, Panel, Radio } from "@vkontakte/vkui";
import './MobileFeed.scss';
import MobileHeader from "../../components/MobileHeader/MobileHeader";
import CartItem from "../../components/CartItem/CartItem";
import { Icon28ArrowRightOutline, Icon28ChevronLeftOutline } from '@vkontakte/icons';
import map from '../../img/feed/map.svg';
import trend from '../../img/feed/trend.svg';
import Button from "../../components/Button/Button";
import { Icon28ChevronDownOutline } from '@vkontakte/icons';
import CardItem from "../../components/CardItem/CardItem";
import Filters from "../../components/Filters/Filters";

interface MobileFeedProps {
    id: string
}

const MobileFeed: FC<MobileFeedProps> = ({ id }) => {
    const [tab, setTab] = useState(0)
    return (
        <Panel id={id} className={"mobileFeed"}>
            {(tab === 0 || tab === 2) && <MobileHeader
                showSearchDefault={true}
                onClickFilter={() => setTab(1)}
                onChange={(e) => e.currentTarget.value.length ? setTab(2) : setTab(0)}/>}
            {tab === 0 ?
                <>
                    <Card className="card region">
                        <img src={map} alt="map"/>
                        <div className={'col'}>
                            <p>Хотите искать в Москве?</p>
                            <div className={'row'}>
                                <Link>Да</Link>
                                <Link>Нет, сменить регион</Link>
                            </div>
                        </div>
                    </Card>
                    <div className="card_list">
                        <CardItem product={{
                            title: 'Капот',
                            description: 'Описание',
                            newCost: '1234',
                            prevCost: '1000',
                        }}/>
                        <CardItem product={{
                            title: 'Капот',
                            description: 'Описание',
                            newCost: '1234',
                            prevCost: '1780',
                        }}/>
                        <CardItem product={{
                            title: 'Капот',
                            description: 'Описание',
                            newCost: '1234',
                            prevCost: '2000',
                        }}/>
                        <CardItem product={{
                            title: 'Капот',
                            description: 'Описание',
                            newCost: '1234',
                            prevCost: '1800',
                        }}/>
                    </div>
                    <Card className={'card more'}>
                        <Icon28ArrowRightOutline/>
                        <p>Другие карточки</p>
                    </Card>
                    <div className={'updateBtn'}>
                        <Button mode={"secondary"} width={"full"}>Нажмите, чтобы обновить</Button>
                    </div>
                </> : tab === 1
                    ? <Filters onClose={() => {}}/>
                    : <div className={'popular_tab'}>
                        <div className="popular">
                            <img src={trend} alt="top"/>
                            <p>ПОПУЛЯРНЫЕ ЗАПРОСЫ</p>
                        </div>
                    </div>}
        </Panel>
    );
};

interface SelectInputProps{
    title: string;
    values: string[];
}

export const SelectInput:FC<SelectInputProps> = ({title, values}) => {
    const [selected, setSelected] = useState('')
    return(
        <div className={'select_container'}>
            <select onChange={e => setSelected(e.currentTarget.value)}>
                {values.map(item => (
                    <option value={item} key={item}>{item}</option>
                ))}
            </select>
            <div className={'col'}>
                <p>{title}</p>
                {selected && <p>{selected}</p>}
            </div>
            <Icon28ChevronDownOutline fill={'#707070'}/>
        </div>
    )
}

export default MobileFeed;