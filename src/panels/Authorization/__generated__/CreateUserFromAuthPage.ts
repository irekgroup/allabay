/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateUserFromAuthPage
// ====================================================

export interface CreateUserFromAuthPage_updateInformationUser_user {
  __typename: "UserType";
  phone: string;
  birthday: any;
}

export interface CreateUserFromAuthPage_updateInformationUser {
  __typename: "CreateUser";
  user: CreateUserFromAuthPage_updateInformationUser_user | null;
}

export interface CreateUserFromAuthPage {
  updateInformationUser: CreateUserFromAuthPage_updateInformationUser | null;
}

export interface CreateUserFromAuthPageVariables {
  firstName: string;
  lastName: string;
  phone: string;
  birthday: any;
  gender: string;
  password: string;
}
