/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MeFromAuthPage
// ====================================================

export interface MeFromAuthPage_me {
  __typename: "UserType";
  id: string;
  phone: string;
  photo: string;
  birthday: any;
  firstName: string;
  lastName: string;
}

export interface MeFromAuthPage {
  me: MeFromAuthPage_me | null;
}

export interface MeFromAuthPageVariables {
  token: string;
}
