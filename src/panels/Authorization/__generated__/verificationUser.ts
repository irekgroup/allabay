/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: verificationUser
// ====================================================

export interface verificationUser_verificationUser {
  __typename: "VerificationUser";
  ok: boolean | null;
}

export interface verificationUser {
  verificationUser: verificationUser_verificationUser | null;
}

export interface verificationUserVariables {
  phone?: string | null;
}
