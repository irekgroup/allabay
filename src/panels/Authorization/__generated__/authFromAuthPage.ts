/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: authFromAuthPage
// ====================================================

export interface authFromAuthPage_tokenAuth {
  __typename: "ObtainJSONWebToken";
  token: string;
}

export interface authFromAuthPage {
  /**
   * Obtain JSON Web Token mutation
   */
  tokenAuth: authFromAuthPage_tokenAuth | null;
}

export interface authFromAuthPageVariables {
  phone: string;
  password: string;
}
