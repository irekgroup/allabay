import React, { FC, useContext, useEffect, useState } from 'react';
import { observer } from "mobx-react";
import { Div, Link, ModalDismissButton, Panel, Radio, Spinner } from "@vkontakte/vkui";
import Input, {inpStatus} from "../../components/Input/Input";
import Button from "../../components/Button/Button";
import { Icon24LockOutline, Icon24PhoneOutline } from "@vkontakte/icons";
import logo from "../../img/main/biglogo.png";
import './Authorization.scss'
import { gql, useApolloClient, useLazyQuery, useMutation } from "@apollo/client";
import { auth, authVariables } from "../../modals/UnauthorizedModal/__generated__/auth";
import { Me } from "../../modals/UnauthorizedModal/__generated__/Me";
import { CreateUser, CreateUserVariables } from "../../modals/UnauthorizedModal/__generated__/CreateUser";
import { PAGE_FEED_DESKTOP, router } from "../../router";
import { AUTH, CREATE_USER, ME, Tab } from "../../modals/UnauthorizedModal/UnauthorizedModal";
import arrowBack from "../../img/main/arrow-back.svg";
import { RootStoreContext, splitLink } from "../../index";

export interface formDataStatus {
    logInpTel?: { status: inpStatus; description?: string };
    logInpPass?: { status: inpStatus; description?: string };
    regInp?: { status: inpStatus; description?: string };
    codeInp?: { status: inpStatus; description?: string };
    profileName?: { status: inpStatus; description?: string };
    profileInpSurname?: { status: inpStatus; description?: string };
    profileInpPass?: { status: inpStatus; description?: string };
    profileInpDate?: { status: inpStatus; description?: string };
    profileInpSex?: { status: inpStatus; description?: string };
    profilePageAddress?: { status: inpStatus; description?: string };
    resetInp?: { status: inpStatus; description?: string };
    resetCodeInp?: { status: inpStatus; description?: string };
    changePassNew?: { status: inpStatus; description?: string };
    changePassCheck?: { status: inpStatus; description?: string };
}

interface AuthorizationProps {
    id: string
}

export interface FormData {
    phone: string;
    password: string;
    firstName: string;
    lastName: string;
    birthday: string;
    gender: string;
    pageAddress: string;
}

export const VERIFICATION_USER = gql`
    mutation verificationUser($phone: String) {
        verificationUser(input:{phone:$phone}){
            ok
        }
    }
`

const Authorization: FC<AuthorizationProps> = observer(({ id }) => {
    const [form, onChange] = useState<FormData>({
        phone: '',
        password: '',
        firstName: '',
        lastName: '',
        birthday: '',
        gender: '',
        pageAddress: '',
    });

    const [inputStatus, setInputStatus] = useState<formDataStatus>({ });
    const [showErrors, setShowErrors] = useState(false);
    const [tab, setActiveTab] = useState<Tab>(Tab.LOGIN);
    const [errMsgIdx, setErrMsgIdx] = useState(0);

    const errMsgs = ['Пароль должен содержать не менее пяти 5 символов состоящих из цифр или букв.',
        'Минимальное количество символов в пароле: 5. Пожалуйста, введите другой пароль.',
        'Выберите более надежный пароль. Используйте комбинацию букв и цифр, которые сможете запомнить.']

    const apollo = useApolloClient();
    const [auth, tokenData] = useLazyQuery<auth, authVariables>(AUTH, {
        fetchPolicy: 'network-only',
    });
    const [getUser, userData] = useLazyQuery<Me>(ME,{
        fetchPolicy: 'network-only',
    });
    const [createUser, createdUserData] = useMutation<CreateUser, CreateUserVariables>(CREATE_USER);
    const [verificationUser, verifiedUser] = useMutation(VERIFICATION_USER);

    const root = useContext(RootStoreContext);
    const { user, config } = root;

    let tokesLength = 0
    useEffect(() => {
        if (tokenData.data?.tokenAuth?.token) {
            const token = tokenData.data?.tokenAuth?.token;
            apollo.setLink(splitLink(token))
            getUser().catch(console.error);
            let tokens = [token]
            try{
                const prevToken = user.getTokens();
                if(Array.isArray(prevToken)){
                    tokens = [...prevToken.filter(t => t !== token), ...tokens]
                    tokesLength = tokens.length
                }
            } finally {
                user.setToken(tokens);
            }
        }
        if(!tokenData.loading && !tokenData.data?.tokenAuth?.token){
            setInputStatus((prev: formDataStatus) => ({
                ...prev,
                logInpPass: {
                    status: 'error',
                    description:
                        'Пароль, который вы указали, не соответствует ни одному аккаунту.',
                },
                logInpTel: {
                    status: 'error',
                    description:
                        'Пароль, который вы указали, не соответствует ни одному аккаунту.',
                },
            }));
        }
    }, [tokenData.loading]);

    useEffect(() => {
        if (!userData.loading && userData.data?.me) {
            user.addSavedUser({ i: user.authorizedUsers.length, userData: { ...userData.data?.me } })
            user.updateCurrUserIdx(user.authorizedUsers.length - 1)
            if(tokesLength-- <= 0){
                router.replacePage(PAGE_FEED_DESKTOP);
            }
        }
    }, [userData.data, userData.loading]);

    const setTab = (str:Tab) => {
        setShowErrors(false)
        setActiveTab(str)
    }

    return (
        <Panel id={id} className={'auth_panel'}>
            <Div className={'form form_auth'}>
                <ModalDismissButton className={'dismissButton'} onClick={() => router.popPage()}/>
                <img className={'auth_panel-logo'} src={logo} alt="allabay"/>
                {tab === Tab.LOGIN && (<>
                    <h1>Авторизация</h1>
                    <form onSubmit={e => e.preventDefault()} style={{width: '100%', height: '100%', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center',}}>
                    <Input
                        name={'logInpTel'}
                        status={inputStatus.logInpTel ? inputStatus.logInpTel : 'error'}
                        descr={
                            inputStatus.logInpTel?.description
                                ? inputStatus.logInpTel?.description
                                : 'Номер телефона не соответствует ни одному аккаунту. Повторите попытку или Создайте аккаунт'
                        }
                        showErrors={showErrors}
                        type="tel"
                        before={<Icon24PhoneOutline/>}
                        onChange={(e) => {
                            const { value, name } = e.currentTarget;
                            if (String(value).length === 0 || String(value)[1] === '_' || (String(value)[14] != '_' && String(value)[14] != undefined)) {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'right',
                                        description: 'Телефон введён корректно',
                                    },
                                }));
                            } else {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'error',
                                        description:
                                            'Номер телефона не соответствует ни одному аккаунту. Повторите попытку или Создайте аккаунт',
                                    },
                                }));
                            }
                            onChange((prev: FormData) => ({ ...prev, phone: '+7' + value }));
                        }}
                        placeholder={'Номер телефона'}
                    />
                    <Input
                        name={'logInpPass'}
                        before={<Icon24LockOutline/>}
                        placeholder={'Пароль'}
                        type="password"
                        status={inputStatus.logInpPass ? inputStatus.logInpPass : 'error'}
                        showErrors={showErrors}
                        descr={
                            inputStatus.logInpPass?.description
                                ? inputStatus.logInpPass?.description
                                : 'Пароль, который вы указали, не соответствует ни одному аккаунту.'
                        }
                        onChange={(e) => {
                            const { value, name } = e.currentTarget;
                            if (value.length === 0 || value.length >= 5) {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'right',
                                        description:
                                            'Пароль, который вы указали, не соответствует ни одному аккаунту.',
                                    },
                                }));
                            } else {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'error',
                                        description:
                                            'Пароль, который вы указали, не соответствует ни одному аккаунту.',
                                    },
                                }));
                            }

                            onChange((prev: FormData) => ({ ...prev, password: value }));
                        }}
                    />
                    <Button
                        style={{ marginTop: 8 }}
                        stretched
                        width="full"
                        mode="secondary"
                        onClick={() => {
                            let phone = form.phone.replace(/[^0-9]/g, '');
                            setShowErrors(true);
                            let phoneRight = inputStatus.logInpTel?.status == 'right' && String(form.phone)[14] !== '_'
                            let passRight = inputStatus.logInpPass?.status == 'right' && form.password.length >= 5
                            if(String(form.phone)[14] != '_' && String(form.phone)[14] != undefined && form.password.length >= 5){
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    logInpTel: {
                                        status: 'right',
                                        description: '',
                                    },
                                    logInpPass: {
                                        status: 'right',
                                        description: '',
                                    },
                                }));
                                phoneRight = passRight = true
                            }
                            if (phoneRight && passRight) {
                                auth({
                                    variables: {
                                        phone: phone.replace(/[^\d]/g, ''),
                                        password: form.password,
                                    },
                                });
                            }
                        }}>
                        {userData.loading || tokenData.loading ? <Spinner size="small" /> : 'Войти'}
                    </Button>
                    </form>
                    <Button
                        style={{ marginTop: 8 }}
                        stretched
                        width="full"
                        mode="tetrairy"
                        onClick={() => setTab(Tab.RESET)}
                    >Забыли пароль?</Button>
                    <Button
                        style={{ marginTop: 8 }}
                        stretched
                        width="full"
                        mode="commerce"
                        onClick={() => setTab(Tab.REG)}
                    >Создать аккаунт</Button>
                </>)}
                {tab === Tab.REG && (
                    <>
                        <h1>Регистрация</h1>
                        <div className={'reg_contact-tel'}>Контактный телефон</div>
                        <p className={'reg_description'}>Ваш номер телефона будет использоваться для входа</p>
                        <Input
                            before={<Icon24PhoneOutline />}
                            descr={
                                inputStatus.regInp?.description
                                    ? inputStatus.regInp?.description
                                    : 'Укажите действительный номер телефона'
                            }
                            onChange={(e) => {
                                const { value, name } = e.currentTarget;
                                if (String(value)[14] != '_' && String(value)[14] != undefined) {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'right',
                                            description: 'Телефон введён корректно',
                                        },
                                    }));
                                } else {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'error',
                                            description: 'Укажите действительный номер телефона',
                                        },
                                    }));
                                }
                                onChange((prev: FormData) => ({ ...prev, phone: '+7' + value }));
                            }}
                            placeholder="Телефон"
                            type="tel"
                            name="regInp"
                            status={inputStatus.regInp ? inputStatus.regInp : 'error'}
                            showErrors={showErrors}
                        />
                        <Button
                            style={{ marginTop: 8 }}
                            stretched
                            width="full"
                            mode="commerce"
                            onClick={() => {
                                setShowErrors(true);

                                if (inputStatus.regInp?.status == 'right') {
                                    verificationUser({
                                        variables: {
                                            phone: form.phone.replace(/[^\d]/g, '')
                                        }
                                    }).then(() => {
                                        setTab(Tab.CODE);
                                    }).catch((e:any) => {
                                        console.error(e.message)
                                        setInputStatus((prev: formDataStatus) => ({
                                            ...prev,
                                            ['regInp']:{
                                                status: 'error',
                                                description: e.message,
                                            }
                                        }))
                                    })
                                }
                            }}>
                            {verifiedUser.loading ? <Spinner size="small" /> : 'Получить код'}</Button>
                        <small onClick={() => setTab(Tab.LOGIN)}>Уже есть аккаунт? Войти!</small>
                        <div className="links-container links-politics">
                            <div style={{ textAlign: 'center', fontSize: '13px' }}>
                                Нажимая кнопку "Получить код", вы принимаете{' '}
                                <div>
                                    <Link className="link-orange">Условия использования сервиса Allabay</Link>, <Link className="link-orange">Политику в
                                    отношении организации обработки и обеспечения безопасности
                                    персональных данных</Link> и <Link className="link-orange">Политику в отношении файлов cookie</Link>.
                                </div>
                            </div>
                        </div>
                        <div onClick={() => setTab(Tab.LOGIN)} className="form_back_arrow">
                            <img src={arrowBack} alt="Вернуться" title="Вернуться" />
                        </div>
                    </>
                )}
                {tab === Tab.CODE && (
                    <>
                        <h1>Регистрация</h1>
                        <div
                            style={{
                                textAlign: 'center',
                                marginBottom: 8,
                                fontWeight: 400,
                                fontSize: 15,
                            }}>
                            <b
                                style={{
                                    fontWeight: 500,
                                }}>
                                Звоним вам...
                            </b>
                            <div style={{ padding: '12px 0' }}>
                                <Link>{form.phone}</Link>
                            </div>
                            Введите последние четыре цифры <br />
                            номера, с которого поступит звонок
                        </div>
                        <Input
                            before={<Icon24LockOutline />}
                            placeholder="Последние 4 цифры"
                            descr={
                                inputStatus.logInpTel?.description
                                    ? inputStatus.logInpTel?.description
                                    : 'Проверьте последние 4 цифры номера с которого поступил звонок, и повторите попытку'
                            }
                            type="last-numbers"
                            name="codeInp"
                            status={inputStatus.codeInp ? inputStatus.codeInp : 'error'}
                            showErrors={showErrors}
                            onChange={(e) => {
                                const { value, name } = e.currentTarget;

                                if (String(value)[4] != '_' && String(value)[4] != undefined) {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'right',
                                            description: 'Код введён корректно',
                                        },
                                    }));
                                } else {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'error',
                                            description:
                                                'Проверьте последние 4 цифры номера с которого поступил звонок, и повторите попытку',
                                        },
                                    }));
                                }
                            }}
                        />
                        <Button
                            style={{ marginTop: 8, marginBottom: 8 }}
                            stretched
                            width="full"
                            mode="commerce"
                            onClick={() => {
                                setShowErrors(true);

                                if (inputStatus.codeInp?.status == 'right') {
                                    setTab(Tab.PROFILE);
                                }
                            }}>
                            Подтвердить номер
                        </Button>
                        <div onClick={() => setTab(Tab.LOGIN)} className="form_back_arrow">
                            <img src={arrowBack} alt="Вернуться" title="Вернуться" />
                        </div>
                    </>
                )}
                {tab === Tab.PROFILE && (
                    <>
                        <h1>Последний штрих</h1>
                        <div
                            style={{
                                marginBottom: 8,
                                fontWeight: 400,
                                textAlign: 'center',
                                color: '#707070',
                                fontSize: 14,
                            }}>
                            Быстро и легко.
                        </div>
                        <Input
                            placeholder={'Адрес страницы'}
                            name={'profilePageAddress'}
                            descr={
                                inputStatus.profilePageAddress?.description
                                    ? inputStatus.profilePageAddress?.description
                                    : 'Какой будет адрес ваше страницы?'
                            }
                            status={inputStatus.profilePageAddress ? inputStatus.profilePageAddress.status : 'error'}
                            onChange={e => {
                                const { value, name } = e.currentTarget;
                                onChange((prev: FormData) => ({ ...prev, pageAddress: value }))
                                if (/^[A-Za-z]+$/.test(value)) {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'right',
                                            description: 'Корректный адрес страницы',
                                        },
                                    }));
                                } else {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'error',
                                            description: 'Какой будет адрес ваше страницы?',
                                        },
                                    }));
                                }
                            }}/>
                        <div style={{ display: 'flex', width: '100%' }}>
                            <div>
                                <Input
                                    placeholder="Имя"
                                    name="profileName"
                                    descr={
                                        inputStatus.profileName?.description
                                            ? inputStatus.profileName?.description
                                            : 'Как вас зовут?'
                                    }
                                    status={inputStatus.profileName ? inputStatus.profileName : 'error'}
                                    showErrors={showErrors}
                                    onChange={(e) => {
                                        const { value, name } = e.currentTarget;
                                        onChange((prev: FormData) => ({ ...prev, firstName: value }))
                                        if (/^[А-ЯЁа-яёA-Za-z]+$/.test(value)) {
                                            setInputStatus((prev: formDataStatus) => ({
                                                ...prev,
                                                [name]: {
                                                    status: 'right',
                                                    description: 'Имя введено корректно',
                                                },
                                            }));
                                        } else {
                                            setInputStatus((prev: formDataStatus) => ({
                                                ...prev,
                                                [name]: {
                                                    status: 'error',
                                                    description: 'Как вас зовут?',
                                                },
                                            }));
                                        }
                                    }}
                                    type="text"
                                />
                            </div>
                            <div style={{ marginLeft: 8 }}>
                                <Input
                                    placeholder="Фамилия"
                                    descr={
                                        inputStatus.profileInpSurname?.description
                                            ? inputStatus.profileInpSurname?.description
                                            : 'Какая у Вас фамилия?'
                                    }
                                    name="profileInpSurname"
                                    status={
                                        inputStatus.profileInpSurname
                                            ? inputStatus.profileInpSurname
                                            : 'error'
                                    }
                                    showErrors={showErrors}
                                    onChange={(e) => {
                                        const { value, name } = e.currentTarget;
                                        onChange((prev: FormData) => ({ ...prev, lastName: value }))
                                        if (/^[А-ЯЁа-яёA-Za-z]+$/.test(value)) {
                                            setInputStatus((prev: formDataStatus) => ({
                                                ...prev,
                                                [name]: {
                                                    status: 'right',
                                                    description: 'Фамилия введена корректно',
                                                },
                                            }));
                                        } else {
                                            setInputStatus((prev: formDataStatus) => ({
                                                ...prev,
                                                [name]: {
                                                    status: 'error',
                                                    description: 'Фамилия введена некорректно',
                                                },
                                            }));
                                        }
                                    }}
                                    type="text"
                                />
                            </div>
                        </div>
                        <Input
                            placeholder="Пароль"
                            name="profileInpPass"
                            descr={
                                inputStatus.profileInpPass?.description
                                    ? inputStatus.profileInpPass?.description
                                    : errMsgs[errMsgIdx]
                            }
                            status={inputStatus.profileInpPass ? inputStatus.profileInpPass : 'error'}
                            showErrors={showErrors}
                            onChange={(e) => {
                                const { value, name } = e.currentTarget;
                                onChange((prev: FormData) => ({ ...prev, password: value }))
                                if(value && value.length >= 5){
                                    // if (/[A-z]/g.test(value + '') || /[0-9]/g.test(value + '')) {
                                        setInputStatus((prev: formDataStatus) => ({
                                            ...prev,
                                            [name]: {
                                                status: 'right',
                                                description: 'Пароль введён корректно',
                                            },
                                        }));
                                    // } else {
                                    //     setInputStatus((prev: formDataStatus) => ({
                                    //         ...prev,
                                    //         [name]: {
                                    //             status: 'error',
                                    //             description: errMsgs[errMsgIdx],
                                    //         },
                                    //     }));
                                    // }
                                } else {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'error',
                                            description: errMsgs[errMsgIdx],
                                        },
                                    }));
                                }

                                onChange((prev: FormData) => ({ ...prev, password: value }));
                            }}
                            type="password"
                        />

                        <div
                            style={{
                                fontWeight: 500,
                                fontSize: 13,
                                padding: '4px 8px 2px',
                            }}>
                            Дата рождения
                        </div>
                        <Input
                            placeholder="Дата"
                            descr={
                                inputStatus.profileInpDate?.description
                                    ? inputStatus.profileInpDate?.description
                                    : 'Похоже, вы ввели неверную информацию. Убедитесь, что вы используете настоящую дату своего рождения'
                            }
                            name="profileInpDate"
                            status={inputStatus.profileInpDate ? inputStatus.profileInpDate : 'error'}
                            showErrors={showErrors}
                            onChange={(e) => {
                                const { name, value } = e.currentTarget;
                                onChange((prev: FormData) => ({ ...prev, birthday: value }))
                                if (value && value[0] != 0) {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'right',
                                            description: 'Дата введёна корректно',
                                        },
                                    }));
                                } else {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'error',
                                            description:
                                                'Похоже, вы ввели неверную информацию. Убедитесь, что вы используете настоящую дату своего рождения',
                                        },
                                    }));
                                }
                            }}
                            type="date"
                        />
                        <div className="sex-container">
                            <div
                                style={{
                                    fontWeight: 500,
                                    fontSize: 13,
                                    padding: '4px 8px 2px',
                                    whiteSpace: 'nowrap',
                                }}>
                                Ваш пол:
                            </div>
                            <div
                                className="radio-container"
                                style={{
                                    display: 'flex',
                                    width: '100%',
                                    padding: '0 8px',
                                    marginTop: 3,
                                    alignItems: 'center',
                                }}>
                                <div>
                                    <Radio
                                        onChange={(e) => {
                                            const { name } = e.currentTarget;
                                            onChange((prev: FormData) => ({ ...prev, gender: "male" }))
                                            setInputStatus((prev: formDataStatus) => ({
                                                ...prev,
                                                [name]: {
                                                    status: 'right',
                                                    description: 'Пол введён корректно',
                                                },
                                            }));
                                        }}
                                        className={
                                            showErrors
                                                ? inputStatus.profileInpSex?.status
                                                    ? inputStatus.profileInpSex?.status
                                                    : 'error'
                                                : ''
                                        }
                                        name="profileInpSex">
                                        Мужской
                                    </Radio>
                                </div>
                                <div style={{ marginLeft: 8 }}>
                                    <Radio
                                        onChange={(e) => {
                                            const { name, value } = e.currentTarget;
                                            onChange((prev: FormData) => ({ ...prev, gender: "female" }))
                                            setInputStatus((prev: formDataStatus) => ({
                                                ...prev,
                                                [name]: {
                                                    status: 'right',
                                                    description: 'Пол введён корректно',
                                                },
                                            }));
                                        }}
                                        className={
                                            showErrors
                                                ? inputStatus.profileInpSex?.status
                                                    ? inputStatus.profileInpSex?.status
                                                    : 'error'
                                                : ''
                                        }
                                        name="profileInpSex">
                                        Женский
                                    </Radio>
                                </div>
                            </div>
                        </div>
                        <div style={{ marginTop: 8, marginBottom: 8 }}>
                            <Button
                                width="full"
                                onClick={() => {
                                    setShowErrors(true);
                                    setErrMsgIdx(errMsgIdx + 1 >= errMsgs.length ? 0 : errMsgIdx + 1);
                                    if (
                                        inputStatus.profileName?.status == 'right' &&
                                        inputStatus.profileInpPass?.status == 'right' &&
                                        inputStatus.profileInpDate?.status == 'right' &&
                                        inputStatus.profileInpSex?.status == 'right' &&
                                        inputStatus.profilePageAddress?.status
                                    ) {
                                        createUser({
                                            variables: {
                                                // page: form.pageAddress,
                                                firstName: form.firstName,
                                                lastName: form.lastName,
                                                password: form.password,
                                                gender: form.gender,
                                                birthday: form.birthday,
                                                phone: form.phone.replace(/[^\d]/g, '')
                                            }
                                        }).then(() =>{
                                            setTab(Tab.LOGIN);
                                        })
                                        // alert('Пользователь зарегистрирован');
                                    } else {
                                        if(inputStatus.profileInpPass?.status != 'right'){
                                            setInputStatus((prev: formDataStatus) => ({
                                                ...prev,
                                                profileInpPass: {
                                                    status: 'error',
                                                    description: errMsgs[errMsgIdx],
                                                },
                                            }));
                                        }
                                    }
                                }}
                                stretched
                                mode="commerce">
                                {createdUserData.loading ? <Spinner size="small" /> : 'Регистрация'}
                            </Button>
                        </div>
                        <div onClick={() => setTab(Tab.LOGIN)} className="form_back_arrow">
                            <img src={arrowBack} alt="Вернуться" title="Вернуться" />
                        </div>
                    </>
                )}
                {tab === Tab.RESET && (
                    <>
                        <h1>Восстановить доступ</h1>
                        <div
                            style={{
                                textAlign: 'center',
                                marginBottom: 8,
                                fontWeight: 400,
                                marginTop: 16,
                            }}>
                            Восстановить досуп по номеру телефона
                        </div>

                        <Input
                            before={<Icon24PhoneOutline />}
                            descr={
                                inputStatus.resetInp?.description
                                    ? inputStatus.resetInp?.description
                                    : 'Укажите действительный номер телефона'
                            }
                            onChange={(e) => {
                                const { value, name } = e.currentTarget;

                                if (String(value)[14] != '_' && String(value)[14] != undefined) {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'right',
                                            description: 'Укажите действительный номер телефона',
                                        },
                                    }));
                                } else {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'error',
                                            description: 'Укажите действительный номер телефона',
                                        },
                                    }));
                                }
                                onChange((prev: FormData) => ({ ...prev, phone: '+7' + value }));
                            }}
                            status={inputStatus.resetInp ? inputStatus.resetInp : 'error'}
                            showErrors={showErrors}
                            placeholder="Номер телефона"
                            type="tel"
                            name="resetInp"
                        />

                        <Button
                            style={{ marginTop: 8 }}
                            stretched
                            width="full"
                            mode="commerce"
                            onClick={() => {
                                setShowErrors(true);

                                if (inputStatus.resetInp?.status == 'right') {
                                    verificationUser({
                                        variables: {
                                            phone: form.phone.replace(/[^\d]/g, '')
                                        }
                                    }).then(() => {
                                        setInputStatus((prev: formDataStatus) => ({
                                            ...prev,
                                            ['resetInp']:{
                                                status: 'error',
                                                description: 'Пользователь не зарегестрирован',
                                            }
                                        }))
                                    }).catch((e) => {
                                        setTab(Tab.RESET_CODE);
                                    })
                                }
                            }}>
                            {verifiedUser.loading ? <Spinner size="small" /> : 'Получить код'}
                        </Button>
                        <div className="links-container links-politics">
                            <p style={{ textAlign: 'center', fontSize: '13px' }}>
                                Нажимая кнопку "Получить код", вы принимаете{' '}
                                <a className="link-orange">условия использования сервиса Allabay</a>
                            </p>
                        </div>
                        <div onClick={() => setTab(Tab.LOGIN)} className="form_back_arrow">
                            <img src={arrowBack} alt="Вернуться" title="Вернуться" />
                        </div>
                    </>
                )}
                {tab === Tab.RESET_CODE && (
                    <>
                        <h1>Восстановить доступ</h1>
                        <div
                            style={{
                                textAlign: 'center',
                                marginBottom: 8,
                                fontWeight: 500,
                                fontSize: 15,
                            }}>
                            Звоним вам...
                        </div>
                        <div
                            style={{
                                textAlign: 'center',
                                marginBottom: 8,
                                fontWeight: 400,
                                fontSize: 14,
                            }}>
                            Введите последние четыре цифры номера,
                            <br /> с которого поступит звонок на номер
                            <div style={{ padding: '12px 0' }}>
                                <Link>{form.phone}</Link>
                            </div>
                        </div>
                        <Input
                            before={<Icon24LockOutline />}
                            placeholder="Последние 4 цифры"
                            descr={
                                inputStatus.logInpTel?.description
                                    ? inputStatus.logInpTel?.description
                                    : 'Проверьте последние 4 цифры номера с которого поступил звонок, и повторите попытку'
                            }
                            type="last-numbers"
                            name="resetCodeInp"
                            status={inputStatus.resetCodeInp ? inputStatus.resetCodeInp : 'error'}
                            showErrors={showErrors}
                            onChange={(e) => {
                                const { value, name } = e.currentTarget;

                                if (String(value)[4] != '_' && String(value)[4] != undefined) {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'right',
                                            description:
                                                'Проверьте последние 4 цифры номера с которого поступил звонок, и повторите попытку',
                                        },
                                    }));
                                } else {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'error',
                                            description:
                                                'Проверьте последние 4 цифры номера с которого поступил звонок, и повторите попытку',
                                        },
                                    }));
                                }
                            }}
                        />
                        <Button
                            style={{ marginTop: 8, marginBottom: 8 }}
                            stretched
                            width="full"
                            mode="commerce"
                            onClick={() => {
                                setShowErrors(true);

                                if (inputStatus.resetCodeInp?.status == 'right') {
                                    setTab(Tab.CHANGE_PASSWORD);
                                }
                            }}>
                            Восстановить досуп
                        </Button>
                        <div onClick={() => setTab(Tab.LOGIN)} className="form_back_arrow">
                            <img src={arrowBack} alt="Вернуться" title="Вернуться" />
                        </div>
                    </>
                )}
                {tab === Tab.CHANGE_PASSWORD && (
                    <>
                        <h1>Смена пароля</h1>
                        <Input
                            before={<Icon24LockOutline />}
                            descr={
                                inputStatus.changePassNew?.description
                                    ? inputStatus.changePassNew?.description
                                    : errMsgs[errMsgIdx]
                            }
                            placeholder="Новый пароль"
                            name="changePassNew"
                            type="password"
                            status={inputStatus.changePassNew ? inputStatus.changePassNew : 'error'}
                            showErrors={showErrors}
                            onChange={(e) => {
                                const { value, name } = e.currentTarget;

                                if(value && value.length >= 5){
                                    // if (/[A-z]/g.test(value + '') || /[0-9]/g.test(value + '')) {
                                        setInputStatus((prev: formDataStatus) => ({
                                            ...prev,
                                            [name]: {
                                                status: 'right',
                                                description:  value,
                                            },
                                        }));
                                    // } else {
                                    //     setInputStatus((prev: formDataStatus) => ({
                                    //         ...prev,
                                    //         [name]: {
                                    //             status: 'error',
                                    //             description: errMsgs[errMsgIdx],
                                    //         },
                                    //     }));
                                    // }
                                } else {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'error',
                                            description: errMsgs[errMsgIdx],
                                        },
                                    }));
                                }

                                onChange((prev: FormData) => ({ ...prev, password: value }));
                            }}
                        />
                        <Input
                            before={<Icon24LockOutline />}
                            placeholder="Повторите новый пароль"
                            descr={
                                inputStatus.changePassCheck?.description
                                    ? inputStatus.changePassCheck?.description
                                    : 'Пароли не совпадают, повторите попытку'
                            }
                            name="changePassCheck"
                            status={inputStatus.changePassCheck ? inputStatus.changePassCheck : 'error'}
                            showErrors={showErrors}
                            onChange={(e) => {
                                const { value, name } = e.currentTarget;

                                if (value == form.password) {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'right',
                                            description: value,
                                        },
                                    }));
                                } else {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'error',
                                            description: 'Пароли не совпадают, повторите попытку',
                                        },
                                    }));
                                }
                            }}
                            type="password"
                        />
                        <Button
                            style={{ marginTop: 8, marginBottom: 8 }}
                            stretched
                            width="full"
                            mode="commerce"
                            onClick={() => {
                                setShowErrors(true);
                                setErrMsgIdx(errMsgIdx + 1 >= errMsgs.length ? 0 : errMsgIdx + 1);
                                if (
                                    inputStatus.changePassCheck?.status == 'right' &&
                                    inputStatus.changePassNew?.status == 'right'
                                ) {
                                    if(inputStatus.changePassCheck.description == inputStatus.changePassNew.description) {
                                        // setTab(Tab.CHANGE_PASSWORD);
                                        alert('Пароль сменён');
                                    } else {
                                        setInputStatus((prev: formDataStatus) => ({
                                            ...prev,
                                            changePassCheck: {
                                                status: 'error',
                                                description: 'Пароли не совпадают, повторите попытку',
                                            },
                                        }));
                                    }
                                }
                            }}>
                            Сохранить
                        </Button>
                        <div onClick={() => setTab(Tab.LOGIN)} className="form_back_arrow">
                            <img src={arrowBack} alt="Вернуться" title="Вернуться" />
                        </div>
                    </>
                )}
            </Div>
            {!config.isMobile && <Div className={'link_list'}>
                <Link className="textBlock">Москва</Link>
                <Link className="textBlock">Политика конфедициальности</Link>
                <Link className="textBlock">Условия использования</Link>
                <Link className="textBlock">Всё о Allabay</Link>
                <Link className="textBlock">Введение</Link>
            </Div>}
        </Panel>
    );
});

export default Authorization;