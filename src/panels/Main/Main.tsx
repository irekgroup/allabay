import {
    Panel,
    FixedLayout,
    Tappable,
    Input,
    Separator,
    SimpleCell,
    IconButton,
    WriteBar,
    useAdaptivity,
    Link,
} from '@vkontakte/vkui';
import { v4 as uuidv4 } from 'uuid';

import searchIcon from '../../img/main/search.png';
import cameraIcon from '../../img/main/camera.png';
import newIcon from '../../img/main/new-icon.svg';
import closerIcon from '../../img/main/closer-icon.svg';

import Footer from '../../components/Footer/Footer';
import Header from '../../components/Header/Header';
import BigLogo from '../../img/main/biglogo.png';

import Chat from '../../components/Chat/Chat';
import React, { useEffect, useLayoutEffect, useRef } from 'react';
import './Main.scss';
import { observer } from 'mobx-react';
import { FC, useContext, useState } from 'react';
import { RootStoreContext } from '../../index';
import { Message } from '../../mobX/Chat.js';
import ButtonSearch from '../../components/ButtonSearch/ButtonSearch';
import Button from '../../components/Button/Button';
import { Icon28Camera, Icon28CameraOutline, Icon28ChevronDownOutline } from '@vkontakte/icons';
import {
    PAGE_CHAT_DESKTOP,
    PAGE_CHAT_MOBILE, PAGE_FEED_DESKTOP, PAGE_FEED_MOBILE,
    PAGE_PROFILE,
    PANEL_DESKTOP_CHAT,
    PANEL_MOBILE_CHAT,
} from '../../router';
import { useLocation, useRouter } from '@happysanta/router';
import { useMutation } from "@apollo/client";
import { createChat, createChatVariables } from "../../components/CreateChat/__generated__/createChat";
import { CREATE_CHAT } from "../../components/CreateChat/CreateChat";

interface MainPanelProps {
    id: string;
    children?: React.ReactNode;
}

const MainPanel: FC<MainPanelProps> = observer(({ id }) => {
    const rootStore = useContext(RootStoreContext);
    const { config, chat, user } = rootStore;
    const { isMobile, socket } = config;
    const adaptivity = useAdaptivity();

    const [showChat, setShowChat] = useState(false);
    const [showActionSearch, setShowActionSearch] = useState(false);
    const [expanded, setExpanded] = useState(false);
    const [showTip, setShowTip]: any = useState(false);
    const [showCloser, setShowCloser]: any = useState(false);
    const [showNewIcon, setShowNewIcon]: any = useState(false);
    let [countOfClicks, setCountOfClicks]: any = useState(0);

    const location = useLocation();

    useEffect(() => {
        window.addEventListener('keyup', onKeyUp, true);
        window.addEventListener('keydown', onKeyDown, true);
    }, []);

    const router = useRouter();

    const onKeyDown = (event: any) => {
        if ((event.code === 'Enter' || event.key === 'Enter') && !event.shiftKey) {
            if (!isMobile) {
                sendMessage();
                event.preventDefault();
            }
        }
    };

    const onKeyUp = (event: any) => {
        if ((event.code === 'Enter' || event.key === 'Enter') && !event.shiftKey) {
            if (!isMobile) {
            }
        }
    };

    const [createMessage, createdMessage] = useMutation<createChat, createChatVariables>(CREATE_CHAT);

    const sendMessage = () => {
        if (chat.message.replace(/(?:\r\n|\r|\n)/g, '').length > 0) {
            if (!socket.wss) {
                chat.updateDialog(null,null, null)
                chat.updateCurrentState('message')
                config.isMobile ? router.pushPage(PAGE_CHAT_MOBILE) : router.pushPage(PAGE_CHAT_DESKTOP)
            } else {
                let uuid = uuidv4();
                let message: Message = {
                    text: chat.message,
                    type: 'outgoing',
                    send: false,
                    read: false,
                    uuid: uuid,
                    date: '',
                } as Message;
                let date = new Date();
                let hours = date.getHours();
                let minutes = date.getMinutes();
                message.date = `${(hours < 10 ? '0' : '') + hours}:${
                    (minutes < 10 ? '0' : '') + minutes
                }`;
                let messages = chat.messages;
                messages.push(message);
                // chat.updateMessages(messages);

                if (socket.wss.readyState === 1) {
                    socket.wss.send(
                        JSON.stringify({
                            type: 'message',
                            text: chat.message,
                            send: false,
                            read: false,
                            uuid: uuid,
                        }),
                    );
                } else {
                    return config.socket.reconnect;
                }

                // let elem = document.getElementById("messages");
                // setTimeout(
                //   () => elem.scrollTo({ top: elem.scrollHeight, behavior: "smooth" }),
                //   50
                // // );
                // document.getElementById("input").value = "";
                setShowChat(true);
                chat.updateMessage('');
                // this.resizeTextarea();
            }
        }
    };

    return (
        <Panel id={id} style={{ backgroundColor: 'var(--background_content)' }}>
            {!showChat ? (
                <div
                    style={{
                        height: `${
                            config.isKeyboardActive
                                ? 'calc(var(--vh, 1vh) * 100)'
                                : 'calc(var(--vh, 1vh) * 100 - 56px)'
                        }`,
                    }}
                    className="flex">
                    <div id="logo">
                        <Link>
                            <img
                                src={BigLogo}
                                className="logo"
                                onClick={() => {
                                    router.pushPage(config.isMobile ? PAGE_FEED_MOBILE : PAGE_FEED_DESKTOP)
                                }}
                            />
                        </Link>
                    </div>

                    {!isMobile ? ( // Если desktop
                        <div
                            className={
                                expanded ? 'container__search expanded' : 'container__search'
                            }>
                            <div className="search">
                                <div className="search__logo">
                                    <img
                                        src={searchIcon}
                                        className="searchIconImg"
                                        alt="searchIcon"
                                    />
                                </div>
                                <input
                                    onChange={(e) => chat.updateMessage(e.target.value)}
                                    onFocus={() => {
                                        setExpanded((prev) => true);
                                    }}
                                    onBlur={() => {
                                        setExpanded((prev) => false);
                                    }}
                                    placeholder="Что вы ищете?"
                                    className="search__input"
                                />
                                <div className="search__logo">
                                    {/* <Search className="searchIcon"    showCloser   /> */}
                                    <IconButton title="Поиск по фото" onClick={() => sendMessage()}>
                                        <img
                                            src={cameraIcon}
                                            className="cameraIconImg"
                                            alt="cameraIcon"
                                        />
                                        <img
                                            src={newIcon}
                                            onMouseEnter={() => {
                                                setShowTip(true);
                                            }}
                                            onMouseLeave={() => {
                                                setShowTip(false);
                                            }}
                                            onClick={() => {
                                                setShowCloser(true);
                                            }}
                                            className={'newImg ' + (showNewIcon ? 'hide' : '')}
                                        />
                                        <img
                                            src={closerIcon}
                                            className={
                                                'closerTip ' +
                                                (showCloser ? 'active ' : '') +
                                                (showNewIcon ? 'hide' : '')
                                            }
                                            alt="closerIcon"
                                            onClick={() => {
                                                setShowNewIcon(true);
                                            }}
                                        />
                                        <div className={'showTip ' + (showTip ? 'active' : '')}>
                                            <p>Поиск по фото</p>
                                        </div>
                                    </IconButton>
                                </div>
                            </div>
                            {/* {this.state.showActionSearch && */}
                            <div className={expanded ? 'search_action expanded' : 'search_action'}>
                                <Separator />
                                <div className="container__search_action">
                                    <Button mode="outline">Как работает?</Button>
                                    <Button mode="outline">Возврат</Button>
                                    <Button mode="outline">Гараж</Button>
                                    <Button mode="outline">Каталог</Button>
                                    <Button mode="outline">Пункты вывоза</Button>
                                    <Button mode="outline">Ещё <Icon28ChevronDownOutline /></Button>
                                </div>
                            </div>
                            {/* } */}
                        </div>
                    ) : (
                        // Если mobile
                        <div
                            className={
                                expanded ? 'container__search expanded' : 'container__search'
                            }>
                            {/* {this.state.showActionSearch && */}
                            <div className={expanded ? 'search_action expanded' : 'search_action'}>
                                <Separator />
                                <div className="container__search_action container__search_action-mobile">
                                    <Button mode="outline">Гараж</Button>
                                    <Button mode="outline">Каталог</Button>
                                    <Button mode="outline">Пункты вывоза</Button>
                                    <Button mode="outline">Еще...</Button>
                                </div>
                            </div>
                            {/* } */}
                            <div className="search">
                                <div className="search__logo">
                                    <img
                                        src={searchIcon}
                                        className="searchIconImg"
                                        alt="searchIcon"
                                    />
                                </div>
                                <input
                                    onChange={(e) => chat.updateMessage(e.target.value)}
                                    onFocus={() => {
                                        setExpanded((prev) => true);
                                        // setIsKeyBoardActive(true);
                                        config.updateIsKeyboardActive(true);
                                    }}
                                    onBlur={() => {
                                        setExpanded((prev) => false);
                                        // setIsKeyBoardActive(false);
                                        config.updateIsKeyboardActive(false);
                                    }}
                                    placeholder="Что вы ищете?"
                                    className="search__input"
                                />
                                <div className="search__logo">
                                    {/* <Search className="searchIcon" /> */}
                                    <IconButton title="Поиск по фото" onClick={() => sendMessage()}>
                                        <img
                                            src={cameraIcon}
                                            className="cameraIconImg"
                                            alt="cameraIcon"
                                        />
                                        <img
                                            src={newIcon}
                                            onClick={() => {
                                                setCountOfClicks((countOfClicks += 1));
                                                if (countOfClicks == 1) {
                                                    setShowTip(true);
                                                    console.log('smy');
                                                } else {
                                                    setShowTip(false);
                                                    setShowCloser(true);
                                                }
                                            }}
                                            // onMouseLeave={() => {
                                            //     setShowTip(false);
                                            // }}
                                            // onClick={() => {
                                            //     setShowCloser(true);
                                            // }}
                                            className={'newImg ' + (showNewIcon ? 'hide' : '')}
                                        />
                                        <img
                                            src={closerIcon}
                                            className={
                                                'closerTip ' +
                                                (showCloser ? 'active ' : '') +
                                                (showNewIcon ? 'hide' : '')
                                            }
                                            alt="closerIcon"
                                            onClick={() => {
                                                setShowNewIcon(true);
                                            }}
                                        />
                                        <div className={'showTip ' + (showTip ? 'active' : '')}>
                                            <p>Поиск по фото</p>
                                        </div>
                                    </IconButton>
                                </div>
                            </div>
                        </div>
                    )}
                    <div
                        className={'searchButtons'}
                        style={{
                            marginTop:
                                adaptivity.viewWidth == 3 || adaptivity.viewWidth == 2 ? 6 : 26,
                            display: 'flex',
                            marginBottom: -22,
                            gap: 10,
                        }}>
                        {/* <ButtonSearch onClick={() => {}}>Искать</ButtonSearch> */}
                        {/* <ButtonSearch onClick={() => {}}>Мне повезет!</ButtonSearch> */}
                        <Button onClick={() => sendMessage()}>Искать</Button>
                        <Button>Мне повезет!</Button>
                    </div>
                </div>
            ) : (
                <div className="flex">
                    <div
                        style={{ height: `calc(var(--vh, 1vh) * 100 - 56px)` }}
                        className="container__chat flex">
                        <Chat />
                    </div>
                </div>
            )}
            {!config.isKeyboardActive ? (
                <Footer
                    messages={chat.messages}
                    showChat={showChat}
                    // toggleDesktopMenu={this.toggleDesktopMenu}
                    // showMessages={this.state.showMessages}
                />
            ) : (
                () => {}
            )}
        </Panel>
    );
});

export default MainPanel;
