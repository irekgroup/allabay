export const sendButtonIcon = ({ }) => {
    return(
        <svg
        xmlns="http://www.w3.org/2000/svg"
        width="43px"
        height="43px"
        version="1.1"
        shapeRendering="geometricPrecision"
        textRendering="geometricPrecision"
        imageRendering="optimizeQuality"
        fillRule="evenodd"
        clipRule="evenodd"
        viewBox="0 0 150 150"
      >
        <g id="Слой_x0020_1">
          <metadata id="CorelCorpID_0Corel-Layer" />
          <path
            fill="#1979F4"
            d="M8866.24 5414.29c-11.22,-25.31 10.57,-63.49 -0.79,-89.1 -11.21,-25.27 -55.57,-38.01 -66.66,-63.01 39.11,-5.37 148.03,-8.02 172.11,15.58 14.37,14.09 -17.42,52.26 -22.76,58.9 -22.53,28.04 -54.18,54.36 -82.36,76.59l0.46 1.04z"
          />
        </g>
        <g id="Слой_x0020_1_0">
          <metadata id="CorelCorpID_1Corel-Layer" />
          <path
            fill="#1979F4"
            d="M53.46 130.65c-7.41,-16.72 6.99,-41.94 -0.52,-58.86 -7.41,-16.7 -36.72,-25.12 -44.04,-41.63 25.84,-3.55 97.8,-5.3 113.7,10.29 9.5,9.31 -11.51,34.53 -15.03,38.91 -14.89,18.53 -35.8,35.92 -54.41,50.6l0.3 0.69z"
          />
        </g>
      </svg>
    );
}