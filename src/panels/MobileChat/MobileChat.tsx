import { Link, Panel } from '@vkontakte/vkui';
import { observer } from 'mobx-react';
import React, { FC, useContext, useEffect, useState } from 'react';
import { RootStoreContext } from '../..';
import Footer from '../../components/Footer/Footer';
import './MobileChat.scss';
import ChatListMain  from "../../components/ChatList/ChatListMain";
import CreateChat from "../../components/CreateChat/CreateChat";
import Chat from "../../components/Chat/Chat";
import {
    Icon28ChevronLeftOutline,
} from '@vkontakte/icons'
import { useRouter } from "@happysanta/router";
import MobileHeader from "../../components/MobileHeader/MobileHeader";
import { PAGE_CHAT_DESKTOP, PAGE_CHAT_MOBILE } from "../../router";
import {Message} from "../../mobX/Chat";
import {useGetChatLazyQuery} from "../../graphQL/Queries/GetChat/GetChat.generated";

interface MobileChatProps {
    id: string;
}

const MobileChat: FC<MobileChatProps> = observer(({ id }) => {
    const rootStore = useContext(RootStoreContext);
    const [msg, setMsg] = useState('')
    const [chatId, setChatId] = useState<null | string>(null);
    const [loadingMessages, setLoading] = useState(true)

    const { config, chat, user } = rootStore;

    const [getChat, getChatData] = useGetChatLazyQuery({fetchPolicy: "cache-and-network"});

    const router = useRouter()

    router.onEnterPage(PAGE_CHAT_MOBILE, (route) => {
        const {id} = route.getParams()
        setChatId(id)
        window.scrollTo(0,0)
    })

    useEffect(() => {
        !config.isMobile && router.pushPage(PAGE_CHAT_DESKTOP);
    }, [config.isMobile])

    useEffect(() => {
        setLoading(true)
        if (chatId) {
            getChat({
                variables: {
                    id: chatId,
                    limit: "50",
                }
            })
        }
    }, [chatId])

    useEffect(() => {
        if (chat.refetchChat) {
            if (chat.refetchChat === chatId) {
                getChatData.refetch({
                    id: chatId,
                    limit: "50",
                })
            }
            chat.setUpdateChat(null)
        }
    }, [chat.refetchChat])

    useEffect(() => {
        if(getChatData.loading) return;
        if(getChatData.error){
            console.error(getChatData.error)
            return;
        }
        if (getChatData.data && getChatData.data.chat && getChatData.data.chat[0]) {
            chat.updateMessages(getChatData.data.chat.map((msg: any) => {
                return {
                    uuid: msg.id,
                    date: msg.createAt,
                    read: msg.isRead,
                    type: msg.senderUser.id === user.userData.id ? 'outgoing' : 'incoming',
                    send: true,
                    text: msg.textMessage
                } as Message
            }))
            typeof chat.dialogIdx === 'number'
            && chat.updateLastMessage(chat.dialogIdx, {
                senderUser: '',
                textMessage: chat.messages[chat.messages.length - 1].text,
                createAt: chat.messages[chat.messages.length - 1].date
            })
        } else {
            chat.updateMessages([])
        }
        setLoading(false)
    }, [getChatData.loading])

    useEffect(() => {
        if(chat.message){
            setMsg(chat.message)
        }
    }, [chat.message])

    useEffect(() => {
        chat.updateCurrentState('start')
    }, [user.userData.id])

    return (
        <Panel id={id} className={'mobileChat'} style={{ backgroundColor: 'var(--background_content)' }}>
            <MobileHeader />
            <div style={{ marginTop: 60 }}>
                {chatId ? (
                    <div className={'chat-messager'}>
                        <Chat loading={loadingMessages}/>
                    </div>
                ) : chat.currentState === 'start' ? (
                    <ChatListMain isScroll={() => {}}/>
                ) : (
                    <>
                        <div className="chat-nav">
                            <Link className="backBtn" onClick={() => {
                                chat.updateCurrentState('start')
                            }}>
                                <Icon28ChevronLeftOutline
                                    width={40}
                                    height={40}
                                    onClick={() => chat.updateCurrentState('start')}/>
                            </Link>
                            <h1>Новый чат</h1>
                        </div>
                        <CreateChat/>
                    </>
                )}
            </div>
            {!config.isKeyboardActive ? <Footer/> : <></>}
        </Panel>
    );
});

export default MobileChat;
