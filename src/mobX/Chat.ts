import {makeAutoObservable} from "mobx";
import RootStore from "./RootStore";
import {ChatMessageStatusChat, ChatType, MessageQueryType, UserType} from "../graphql-types";

export interface Message extends MessageQueryType{
    text: string,
    type: string,
    send: boolean,
    read: boolean,
    uuid: string,
    date: string
}

interface ILastMessage {
    createAt: string,
    textMessage: string,
    senderUser: string,
    idForTooltip?: string,
}

export interface IChat extends ChatType {
    lastMessage: ILastMessage,
    isReadied?: boolean,
}

export enum helperModal {
    COMMANDS = 'COMMANDS',
    TEMPLATES = 'TEMPLATES',
}

export enum ChatFilters {
    ALL = 'ВСЕ',
    FC = 'ПЕРВИЧНЫЙ КОНТАКТ',
    NG = 'ПЕРЕГОВОРЫ',
    DC = 'ПРИНИМАЮТ РЕШЕНИЕ',
    AG = 'СОГЛАСОВАНИЕ',
    PD = 'ОПЛАЧЕНО',
    SU = 'УСПЕШНО',
    LS = 'ПОТЕРЯННЫЕ',
}


class Chat {
    rootStore: RootStore;
    chatList: IChat[] = [];
    filterBy: ChatFilters = ChatFilters.ALL;

    isCart: boolean = false;

    currentState: string = "start"
    message: string = "";
    messages: Message[] = [];
    refetchChat: (string | null) = null;

    dialogId: (string | null) = null
    dialogIdx: (number | null) = null
    title: (string | null) = null
    partner: string = ''
    newMessagesCount: number = 0
    selectedIds: Record<string, boolean> = {}

    helperModalType: helperModal = helperModal.COMMANDS;
    selectedChat: (number | null) = null

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
        makeAutoObservable(this);
    }

    setIsCart(b: boolean) {
        this.isCart = b
    }

    updateHelperModal = (type: helperModal) => {
        this.helperModalType = type
    }

    updateDialog = (id: string | null, title: string | null, dialogIdx: number | null, partner: string = '',) => {
        this.dialogId = id;
        this.title = title;
        this.partner = partner;
        this.dialogIdx = dialogIdx;
    }

    updateMessage = (message: string) => {
        this.message = message;
    }

    updateMessages = (messages: Message[]) => {
        this.messages = messages;
    }

    updateCurrentState = (currentState: string) => {
        this.currentState = currentState;
    }

    updateChatList = (chatList: IChat[]) => {
        this.chatList = chatList;
    }

    pushNewMessageServerless = (message: Message, chatId: string) => {
        const chat = this.chatList[this.dialogIdx || 0]
        if (chat && message && chat.id === chatId && this.dialogId === chatId) {
            this.messages.push({
                uuid: '00000',
                date: message.date,
                read: false,
                type: 'outgoing',
                send: false,
                text: message.text
            } as Message)
            return
        }
    }

    pushNewMessage = (message: (Message | null), sender: string, type?: string, idForTooltip?: string) => {
        let lastPinnedIdx: (number | null) = null

        for (let i = 0; i < this.chatList.length; i++) {
            const chat = this.chatList[i]
            if (chat.isPinned) {
                lastPinnedIdx = i
            }
            if (message?.chatMessages.id === chat.id) {
                 let tmp = {
                    ...chat,
                    lastMessages: {
                        ...message
                    },
                    isReadied: false,
                }
                if (this.dialogId === chat.id) {
                    if (type === 'outgoing' && this.messages[this.messages.length - 1].uuid === '00000') {
                        this.messages.pop()
                    }
                    this.messages.push({
                        uuid: message.id || '',
                        date: message.createAt || '',
                        read: message.isRead || false,
                        send: true,
                        text: message.textMessage || '',
                        type: type || 'outgoing',
                    } as Message)
                } else {
                    this.updateNewMessagesCount(this.newMessagesCount + 1)
                }
                if (chat.isPinned) {
                    lastPinnedIdx = null
                }
                this.chatList.splice(i, 1)
                this.chatList.splice(lastPinnedIdx ? lastPinnedIdx + 1 : 0, 0, tmp)
                if (this.dialogId === chat.id) {
                    this.updateDialog(tmp.id, tmp.nameChat, lastPinnedIdx ? lastPinnedIdx + 1 : 0, this.partner)
                }
                return
            }
        }
        let newChat : IChat = {
            __typename: "ChatType",
            nameChat: message?.chatMessages.nameChat ? message?.chatMessages.nameChat : '',
            user1: message?.chatMessages.user1 ? message?.chatMessages.user1 : {
                id: '',
                __typename: "UserType",
                lastName: '',
                firstName: '',
            } as UserType,
            user2: message?.chatMessages.user2 ? message?.chatMessages.user2 : {
                id: '',
                __typename: "UserType",
                lastName: '',
                firstName: '',
            } as UserType,
            id: message?.chatMessages.id || '',
            createAt: message?.createAt,
            lastMessages: {
                createAt: new Date().toString(),
                textMessage: message?.textMessage || '',
                senderUser: message?.senderUser.id || '',
            } as unknown as Message,
            isPinned: false,
            statusChat: ChatMessageStatusChat.Fc // FIX
        } as unknown as IChat;
        this.chatList.splice(lastPinnedIdx ? lastPinnedIdx + 1 : 0, 0, newChat);

        this.updateNewMessagesCount(this.newMessagesCount + 1)
    }

    updateNewMessagesCount(num: number) {
        this.newMessagesCount = num
    }

    readChat(chatId: string) {
        this.chatList = this.chatList.map((chat) => {
            if (chat.id === chatId) {
                return {...chat, isReadied: true}
            }
            this.messages = this.messages.map(msg => {
                return {...msg, read: true}
            })
            return chat
        })
        this.updateNewMessagesCount(this.newMessagesCount - 1)
    }

    // deleteMessages(listMessages: (chatSub_notifyMessage_listMessage | null)[]){
    //     if(this.dialogId === listMessages[0]?.chatMessages.id){
    //         const s = new Set(listMessages.map(e => e && e.id));
    //         this.messages = this.messages.filter(e => !s.has(e.uuid));
    //         if(!this.messages.length){
    //             this.chatList = this.chatList.filter(item => item.id !== this.dialogId)
    //             this.updateDialog(null,null,null)
    //         }
    //     }
    // }

    setUpdateChat(chat: null | string) {
        this.refetchChat = chat
    }

    setSelectedIds(data: Record<string, boolean>) {
        this.selectedIds = data;
    }

    pinChatLocal(idx?: number) {
        let index = typeof idx === "number" ? idx : this.dialogIdx
        if (typeof index === 'number') {
            this.chatList[index].isPinned = true
            let tmpItem = {...this.chatList[index]}
            this.chatList.splice(index, 1)
            this.chatList.splice(0, 0, tmpItem)
            if (this.selectedChat === null) {
                this.updateDialog(tmpItem.id, tmpItem.nameChat, 0, this.partner)
            }
        }
    }

    unpinLocal(idx?: number) {
        let index = typeof idx === "number" ? idx : this.dialogIdx
        let lastPinned = 0
        if (typeof index === 'number') {
            this.chatList[index].isPinned = false
            this.chatList.forEach((item, i) => {
                if (item.isPinned) {
                    lastPinned = i
                }
            })
            const tmp = {...this.chatList[index]}
            this.chatList.splice(index, 1)
            this.chatList.splice(lastPinned, 0, tmp)
        }

    }

    deleteChat(idx: number) {
        this.chatList = this.chatList.filter((_, i) => i !== idx)
    }

    setChatFilter(filter: ChatFilters = ChatFilters.ALL) {
        this.filterBy = filter
    }

    setSelectedChat(idx: number | null) {
        this.selectedChat = idx;
    }

    setStatus(key: string) {
        if (this.dialogIdx) {
            // @ts-ignore
            this.chatList[this.dialogIdx].statusChat = key
        }
    }

    updateLastMessage(chatIdx: number, lastMessage: ILastMessage) {
        if (this.chatList.length === 0) return;
        this.chatList[chatIdx].lastMessage = lastMessage
    }

}

export default Chat;
