import RootStore from "./RootStore";
import { makeAutoObservable } from "mobx";

export enum CreateCardTabs {
    FIRST_SCREEN = 'FIRST_SCREEN',
    SECOND_SCREEN = 'SECOND_SCREEN',
    PREVIEW = 'PREVIEW',
}

export interface IPhoto {
    title: string,
    url: string,
}

export interface IProductInfo {
    title?: string,
    brand?: string,
    photo?: IPhoto[],
    prevCost?: string,
    newCost?: string,
    description?: string,
    category?: string,
    subcategory?: string,
    type?: string,
    kind?: string,
    sendType?: string,
    timeFrom?: string,
    timeTo?: string,
    guaranteePeriod?: string,
    condition?: string,
    number?: string,
    seller?: {
        link?: string,
        city?: string,
        tel?: string,
        article?: string,
        cost?: string
    },
    sendInf?:{
        address?: string,
        tel?:string,
        cost?: string,
        operatorAddress?: string,
    },
}

export default class CreateCard {
    rootStore: RootStore;
    tab: CreateCardTabs = CreateCardTabs.FIRST_SCREEN;
    cardList: IProductInfo[] = [];
    cardIndex: number = 0;

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
        makeAutoObservable(this);
    }

    setTab(tab: CreateCardTabs) {
        this.tab = tab;
        this.saveToLocalStorage()
    }

    pushCard(card: IProductInfo) {
        this.cardList.push(card);
        this.saveToLocalStorage()
    }

    getCurrentCard() {
        return this.cardList[this.cardIndex];
    }

    offsetIndex(n: 1 | -1){
        const curr = this.cardIndex + n
        if(curr <= this.cardList.length && curr >= 0){
            this.cardIndex = curr
        }
        this.saveToLocalStorage()
    }

    setCard(card: IProductInfo){
        this.cardList[this.cardIndex] = card
        this.saveToLocalStorage()
    }

    clear(){
        this.cardList = []
        this.cardIndex = 0
    }

    removeCurrCard(){
        this.cardList.splice(this.cardIndex, 1,)
        this.saveToLocalStorage()
    }

    getFromLocalStorage(){
        this.cardList = JSON.parse(localStorage.getItem('cardList') || '[]')
    }

    saveToLocalStorage(){
        const cards = JSON.stringify(this.cardList)
        console.log('card save to local storage', cards)
         localStorage.setItem('cardList', cards)
    }

}
