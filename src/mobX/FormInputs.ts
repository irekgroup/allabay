import { makeAutoObservable } from "mobx"
import RootStore from "./RootStore";

export interface formData {
    logInpTel?: boolean,
    logInpPass?: boolean,
    regInp?: boolean,
    codeInp?: boolean,
    profileInpName?: boolean,
    profileInpSurName?: boolean,
    profileInpPass?: boolean,
    profileInpDate?: boolean,
    resetInp?: boolean,
    resetCodeInp?: boolean,
    changePassNew?: boolean,
    changePassCheck?: boolean
}

class FormInputs {

    rootStore: RootStore;

    formData: formData = {
        
    };

    templates: Template = [];
    template: Template = {};

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
        makeAutoObservable(this)
    }

    updateFormData(data: formData) {
        this.formData = {
            ...this.formData,
            ...data
        };
    }

    checkReg() {
        return this.formData.regInp
    }

    checkCode() {
        return this.formData.codeInp
    }

    checkProfile() {
        return this.formData.profileInpDate && this.formData.profileInpName && this.formData.profileInpPass && this.formData.profileInpSurName
    }

    checkReset() {
        return this.formData.resetInp
    }

    checkResetCode() {
        return this.formData.resetCodeInp
    }

    checkPassChange() {
        return this.formData.changePassNew && this.formData.changePassCheck
    }

}


export class Template {

}

export default FormInputs;