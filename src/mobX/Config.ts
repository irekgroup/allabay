import { makeAutoObservable, } from "mobx"
import React from "react";
import RootStore from "./RootStore";
import { PANEL_MAIN } from '@happysanta/router';

interface Socket {
    wss?: WebSocket,
    connect?: () => void,
    reconnect?: () => void
}

class Config {
    
    socketSubs: any[] = []
    scheme:string = "space_gray"
    socket: Socket = {}
    isMobile: boolean = false;
    activePanel: string = PANEL_MAIN;
    isKeyboardActive: boolean = false;
    dynamicModalHeader: React.ReactNode = "";
    mousePos:({x: number, y: number} | null) = null
    userCity:string | null = null
    notifications:{icon?:React.ReactNode, text:string}[] = []

    rootStore: RootStore

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
        makeAutoObservable(this)
    }

    updateSocket(socket: Socket) {
        this.socket = { ...this.socket, ...socket };
    }

    subscribeOnSocket(callback: any) {
        this.socketSubs = [...this.socketSubs, callback];
    }

    toggleScheme(scheme: string) {
        this.scheme = scheme;
    }

    updateIsMobile(isMobile: boolean) {
        this.isMobile = isMobile;
    }

    updateActivePanel(activePanel: string) {
        this.activePanel = activePanel;
    }

    updateIsKeyboardActive(isKeyboardActive: boolean) {
        this.isKeyboardActive = isKeyboardActive;
    }

    changeModelHeader(child: React.ReactNode) {
        this.dynamicModalHeader = child;
    }

    setMousePos(obj: {x: number, y: number}){
        this.mousePos = obj
    }

    pushNotification(text: string, icon?: React.ReactNode){
        this.notifications.push({icon, text});
    }

    clearNotifications(){
        this.notifications = []
    }

}

export default Config;
