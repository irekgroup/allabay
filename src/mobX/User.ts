import {makeAutoObservable} from "mobx"
import RootStore from "./RootStore";

export interface UserData {
    id?: string,
    firstName?: string,
    lastName?: string,
    phone?: string,
    gender?: string,
    birthdate?: string,
    photo?: string,
    manager?: boolean,
    isStaff?: boolean,
    isOperator?: boolean
}

export enum PendingStatus {
    LOADING = 'LOADING',
    READY = 'READY',
    REJECTED = 'REJECTED',
}

interface authorizedUser {
    i: number,
    userData: UserData
}

class User {
    rootStore: RootStore;
    userData: UserData = {};
    currUserIdx: number = 0;
    authorizedUsers: authorizedUser[] = []
    template: Template = {};
    status: PendingStatus = PendingStatus.READY
    tokens: string[] = [];

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
        this.updatesTokens();
        makeAutoObservable(this)
    }

    /**
     * Обновляет информацию о текущем пользователе.
     * @param data информация о пользователе.
     */
    updateUserData(data: UserData) {
        this.userData = {
            ...this.userData,
            ...data
        };
    }

    /**
     * Добавляет пользователя в писок авторизованных пользователей.
     * @param data авторизованный пользователь.
     */
    addSavedUser(data: authorizedUser) {
        if (this.userData.id !== data.userData.id) {
            this.authorizedUsers = this.authorizedUsers.filter(item => item.userData.id !== data.userData.id)
            this.authorizedUsers.push(data)
        }
    }

    /**
     * Удаляет пользователя из списка сохраненных пользователей.
     * @param id идентификатор пользователя.
     */
    removeFromSaved(id: number) {
        let usersCount = this.authorizedUsers.length;
        this.authorizedUsers.splice(id, 1)
        if (id !== usersCount) {
            // Если удаляется не последний в списке пользователь, идентификатор всех остальных пользователей смещается.
            for (let i = id; i < this.authorizedUsers.length; i++) {
                let user = this.authorizedUsers[i]
                user.i -= 1;
            }
        }
    }

    /**
     * Производит выход текущего пользователя.
     */
    logout() {
        if (this.authorizedUsers.length > 1) {
            this.tokens.splice(this.currUserIdx, 1);
            this.setToken(this.tokens);
            this.removeFromSaved(this.currUserIdx);
            this.updateCurrUserIdx(this.authorizedUsers[0].i)

        } else {
            this.userData = {}
            this.authorizedUsers = []
            this.currUserIdx = 0
            localStorage.removeItem('token')
            this.tokens = []
        }
    }

    /**
     * Обновляет в localStorage идентификатор текущего пользователя (idx).
     * @param id идентификатор.
     */
    updateCurrUserIdx(id: number) {
        this.currUserIdx = id;
        let userData = this.authorizedUsers.find(user => user.i === id)?.userData;
        if (userData) {
            this.userData = userData;
        } else {
            throw new Error(`User with id ${id} doesn't exist`)
        }
        localStorage.setItem('idx', '' + this.currUserIdx)
    }

    /**
     * Возвращает токен текущего пользователя.
     */
    getToken(): string | undefined {
        return this.tokens[this.currUserIdx];
    }

    /**
     * Обновляет статус очереди.
     * @param status статус.
     */
    updateStatus(status: PendingStatus) {
        this.status = status;
    }

    /**
     *
     * @param tokens список токенов
     */
    setToken(tokens: string[]) {
        localStorage.setItem('token', JSON.stringify(tokens));
        this.tokens = tokens;
    }

    /**
     * Обновляет список токенов из localStorage.
     */
    updatesTokens() {
        this.tokens = JSON.parse(localStorage.getItem('token') || "[]")
    }

    /**
     * Возвращает список токенов.
     */
    getTokens(): string[] {
        return this.tokens;
    }
}


export class Template {

}

export default User;
