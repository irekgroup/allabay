import Chat from "./Chat";
import Config from "./Config"
import User from "./User";
import FormInputs from "./FormInputs";
import CreateCard from "./CreateCard";

export default class RootStore {

    config: Config;
    user: User;
    chat: Chat;
    formInputs: FormInputs;
    createCard: CreateCard;

    constructor() {
        this.config = new Config(this)
        this.user = new User(this);
        this.chat = new Chat(this);
        this.formInputs = new FormInputs(this);
        this.createCard = new CreateCard(this);
        // this.todoStore = new TodoStore(this)
    }
}