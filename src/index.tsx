import 'core-js/features/map';
import 'core-js/features/set';
import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import bridge from '@vkontakte/vk-bridge';
import App from './App';
import { AdaptivityProvider } from '@vkontakte/vkui';
import { RouterContext } from '@happysanta/router';
import { router } from './router';
import { createContext } from 'react';
import RootStore from './mobX/RootStore';
import eruda from './eruda';

import {
    ApolloClient,
    ApolloLink,
    InMemoryCache,
    concat,
    ApolloProvider,
    HttpLink,
    split,
} from '@apollo/client';
import { WebSocketLink } from "@apollo/client/link/ws";
import { getMainDefinition } from "@apollo/client/utilities";
import { setContext } from "@apollo/client/link/context";

// Init VK  Mini App
bridge.send('VKWebAppInit');

let vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty('--vh', `${vh}px`);
window.addEventListener('resize', () => {
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
});

export const wsLink = (token?:string) => new WebSocketLink({
    uri: 'wss://allabay.com/subscriptions',
    options: {
        reconnect: true,
        connectionParams:{
            authorization: token
        },
    },
});

export const httpLink = new HttpLink({
    uri: 'https://allabay.com/graphql/',
});

// const authMiddleware = new ApolloLink((operation, forward) => {
//     console.log('operation')
//
//     // add the authorization to the headers
//     operation.setContext(({ headers = {} }) => ({
//         headers: {
//             ...headers,
//             // authorization: localStorage.getItem('token') || null,
//         },
//     }));
//     return forward(operation);
// });
export const authLink = (token?: string) => (
    setContext((_, { headers }) => {
    return {
        headers: {
            ...headers,
            authorization: token ? `JWT ${token}` : "",
        },
    }
}));


export const splitLink = (token?: string) => (
    split(
    ({ query }) => {
        const definition = getMainDefinition(query);
        return (
            definition.kind === 'OperationDefinition' &&
            definition.operation === 'subscription'
        );
    },
    wsLink(token),
    authLink(token).concat(httpLink),
));

const client = new ApolloClient({
    cache: new InMemoryCache(),
    link: splitLink() /*concat(httpLink, authMiddleware)*/,

});

const rootStore = new RootStore();
export const RootStoreContext = createContext<RootStore>(rootStore);

ReactDOM.render(
    <RouterContext.Provider value={router}>
        <ApolloProvider client={client}>
            <RootStoreContext.Provider value={rootStore}>
                <AdaptivityProvider>
                    <App />
                </AdaptivityProvider>
            </RootStoreContext.Provider>
        </ApolloProvider>
    </RouterContext.Provider>,
    document.getElementById('root'),
);

//if (process.env.NODE_ENV === "development") {
//  import("./eruda").then(({ default: eruda }) => {}); //runtime download
//}
