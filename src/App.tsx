import bridge from '@vkontakte/vk-bridge';
import {
    ANDROID,
    AppRoot,
    Avatar,
    ConfigProvider,
    Epic,
    Platform,
    Root,
    SplitCol,
    SplitLayout,
    Tabbar,
    TabbarItem,
    useAdaptivity,
    View,
    ViewWidth,
} from '@vkontakte/vkui';
import React, {FC, useContext, useEffect, useState} from 'react';

import {
    ApolloQueryResult,
    useApolloClient,
} from '@apollo/client';

import MainPanel from './panels/Main/Main';

import '@vkontakte/vkui/dist/vkui.css';
import './index.scss';
import './fonts.scss';
import './App.scss';
import Popout from './popouts/Popout';
import Profile from './panels/Profile/Profile';
import {observer} from 'mobx-react';

import {PANEL_MAIN, useLocation, useRouter, VIEW_MAIN} from '@happysanta/router';
import {RootStoreContext, splitLink} from './index';
import {
    MODAL_CARDUSER,
    MODAL_CREATE,
    MODAL_UNAUTHORIZED,
    PAGE_CART_MOBILE,
    PAGE_CHAT_MOBILE,
    PAGE_FEED_MOBILE,
    PAGE_LOGIN,
    PAGE_MAIN,
    PAGE_PROFILE,
    PANEL_AUTH,
    PANEL_CART_MOBILE,
    PANEL_DESKTOP_CHAT,
    PANEL_FEED_DESKTOP,
    PANEL_FEED_MOBILE,
    PANEL_MOBILE_CHAT,
    PANEL_PRODUCT,
    PANEL_PROFILE,
    VIEW_AUTH,
    VIEW_CART,
    VIEW_CHAT,
    VIEW_FEED,
    VIEW_PRODUCT,
    VIEW_PROFILE,
} from './router';

import ModalMain from './modals/ModalMain';

import newChatIcon from './img/main/create-new-white.svg';
import cartIcon from '../src/img/main/cart5.svg';
import loginIcon from '../src/img/main/footer-login.svg';
import messenegerIcon from '../src/img/main/messeneger-without-notification3.svg';
import footerNews from '../src/img/main/footer-news.svg';
import feedBold from '../src/img/main/feed_bold.svg';
import messengerBold from '../src/img/main/messanger_bold.svg';
import cartBold from '../src/img/main/cart_bold.svg';

import DesktopChat from './panels/DesktopChat/DesktopChat';
import MobileChat from './panels/MobileChat/MobileChat';
import Authorization from "./panels/Authorization/Authorization";
import LetterAvatar from "./components/LetterAvatar/LetterAvatar";
import MobileCart from "./panels/MobileCart/MobileCart";
import Loader from "./components/Loader/Loader";
import MobileFeed from "./panels/MobileFeed/MobileFeed";
import Product from "./panels/Product/Product";
// @ts-ignore
import notificationSound from "./sound/notification.mp3";
import {PendingStatus} from "./mobX/User";
import DesktopFeed from "./panels/DesktopFeed/DesktopFeed";
import Notifacations from "./components/Notifications/Notifications";
import {useFetchChatsLazyQuery} from "./graphQL/Queries/FetchChats/FetchChats.generated";
import {IChat, Message} from "./mobX/Chat";
import {useMeLazyQuery} from "./graphQL/Queries/Me/Me.generated";
import {ChatSubDocument, ChatSubSubscription} from "./graphQL/Subscriptions/ChatSubscription/ChatSubscription.generated";

const App: FC = observer(() => {
    const adaptivity = useAdaptivity();
    const isMobile = (adaptivity.viewWidth || 3) <= ViewWidth.SMALL_TABLET;

    const router = useRouter();
    const location = useLocation();

    const rootStore = useContext(RootStoreContext);
    const {config, user, chat} = rootStore;

    const apollo = useApolloClient();
    const [getUser] = useMeLazyQuery({fetchPolicy: "no-cache"});
    const [fetchChats, {data: fetchedChats, loading: fetchLoading, error: fetchError}] = useFetchChatsLazyQuery({fetchPolicy: "cache-and-network"});
    const [subscription, setSubscription] = useState<any>(null);

    const audio = new Audio(notificationSound)

    useEffect(() => {
        if (subscription) {
            subscription.unsubscribe()
        }
        apollo.setLink(splitLink(user.getToken()))
        if (user.userData.id) {
            const observer = apollo.subscribe<ChatSubSubscription>({
                query: ChatSubDocument,
            })
            const subscription = observer.subscribe(({data, errors}) => {
                console.log(data)
                if (errors) {
                    console.error(errors)
                    return;
                }
                if (data) {
                    if ((data.notifyMessage?.bools?.isRead || data.notifyMessage?.bools?.isDelete) && data.notifyMessage?.chat?.id) {
                        chat.setUpdateChat(data.notifyMessage?.chat?.id)
                    }
                    if (data?.notifyMessage?.bools?.isMessage) {
                        if (data.notifyMessage.message?.senderUser) {
                            if (data.notifyMessage.message.senderUser.id !== user.userData.id) {
                                audio.play()
                            }
                            chat.pushNewMessage(
                                data.notifyMessage.message as Message,
                                data.notifyMessage.message.senderUser.id,
                                data.notifyMessage.message.senderUser.id === user.userData.id ? 'outgoing' : 'incoming'
                            )
                        }
                    }

                }
            })
            setSubscription(subscription)
            return () => subscription.unsubscribe()
        }
    }, [user.userData.id])



    const [loading, setLoading] = useState(true)
    const [tokensIdx, setTokensIdx] = useState(0)
    const [tokens, setTokens] = useState<null | string[]>(null)

    useEffect(() => {
        if (user.userData.id && !loading) {
            chat.updateChatList([])
            chat.updateNewMessagesCount(0)
            fetchChats().catch(console.error)
        } else if (!loading && !user.authorizedUsers.length) {
            router.pushPage(PAGE_LOGIN)
        }
    }, [user.userData.id, loading])

    useEffect(() => {
        if (!loading && !user.userData.id) {
            router.pushPage(PAGE_LOGIN)
        }
    }, [config.activePanel])

    /**
     * Обнавляет список чатов.
     */
    useEffect(() => {
        if (fetchError){
            return console.error(`An error occurred while fetching a chat: ${fetchError?.message}`);
        }
        if (!fetchLoading && fetchedChats && fetchedChats.listChats){
            let currentUserChats = fetchedChats.listChats.filter(value => value.user1.id === user.userData.id);

            currentUserChats.sort((a, b) => {
                if (!a?.lastMessages || !b?.lastMessages) {
                    return 0
                }
                if (a?.lastMessages.createAt > b?.lastMessages.createAt) {
                    return -1
                }
                return 1
            })

            currentUserChats.forEach((userChat, idx) => {
                if (userChat.lastMessages?.isRead){
                    chat.updateNewMessagesCount(chat.newMessagesCount + 1);
                }
                if (userChat.isPinned) {
                    let roamingChat = {...userChat}
                    currentUserChats.splice(idx, 1)
                    currentUserChats.splice(0, 0, roamingChat)
                }
            })
            chat.updateChatList(currentUserChats as IChat[])
        }
    }, [fetchLoading])

    useEffect(() => {
        user.updateStatus(PendingStatus.LOADING)
        const token = user.getTokens();
        if (token) {
            try {
                setTokens(token)
            } catch (e) {
                setLoading(false)
            }
        } else {
            setLoading(false)
        }
    }, []);

    useEffect(() => {
        if (!loading) {
            const ele = document.getElementById('root-loader')
            if (ele) {
                ele.outerHTML = ''
            }
        }
    }, [loading])

    useEffect(() => {
        if (tokens && tokens.length > tokensIdx) {
            setLoading(true)
            user.updateStatus(PendingStatus.LOADING)
            apollo.setLink(splitLink(tokens[tokensIdx]))
            getUser()
                .then(userDataProceed)
                .catch((e: any) => {
                    console.error(e)
                    setTokensIdx(tokensIdx + 1)
                })
        } else if (Array.isArray(tokens)) {
            const localToken = localStorage.getItem('idx') || ''
            if (+localToken && +localToken < user.authorizedUsers.length) {
                user.updateCurrUserIdx(+localToken);
            }
            setLoading(false)
            user.updateStatus(PendingStatus.READY)
        }
    }, [tokensIdx, tokens])

    useEffect(() => {
        config.updateIsMobile(isMobile);
    }, [isMobile]);

    useEffect(() => {
        config.updateActivePanel(location.getPanelId());
    }, [location, config]);

    useEffect(() => {
        return location.getPageId() != '/'
            ? router.pushPage(location.getPageId())
            : () => {};
    }, []);

    const toggleScheme = () => {
        let scheme = config.scheme !== 'space_gray' ? 'space_gray' : 'bright_light';
        let isLight = ['bright_light', 'client_light'].includes(scheme);
        config.toggleScheme(isLight ? 'bright_light' : 'space_gray');
        // document.getElementById("app__body").setAttribute("scheme", scheme);
        bridge
            .send('VKWebAppSetViewSettings', {
                status_bar_style: isLight ? 'dark' : 'light',
                action_bar_color: isLight ? '#ffffff' : '#191919',
                navigation_bar_color: isLight ? '#ffffff' : '#191919',
            })
            .catch((e) => {
            });
    };

    const userDataProceed = (userData: ApolloQueryResult<any>) => {
        if (userData.error) {
            setTokensIdx(tokensIdx + 1)
            apollo.setLink(splitLink(user.getToken() || undefined))
            return
        }
        if (!userData.loading) {
            if (userData.data?.me) {
                user.addSavedUser({i: tokensIdx, userData: {...userData.data?.me}})
                if (!user.userData.id) {
                    user.updateUserData({...userData.data?.me});
                } else {
                    apollo.setLink(splitLink(user.getToken()))
                }
            }
            setTokensIdx(tokensIdx + 1)
        }
    }

    const popout = location.getPopupId() ? <Popout popout={location.getPopupId()}/> : null;
    const modal = <ModalMain/>;

    const props = {};

    const isAuthPage = window.location.host.split('.')[0] === "login";

    const root = [
        <View
            key="0"
            id={VIEW_MAIN}
            activePanel={location.getViewActivePanel(VIEW_MAIN) || PAGE_MAIN}>
            <MainPanel id={PANEL_MAIN} {...props} />
        </View>,
        <View
            key="1"
            id={VIEW_PROFILE}
            activePanel={location.getViewActivePanel(VIEW_PROFILE) || PAGE_PROFILE}>
            <Profile id={PANEL_PROFILE} {...props} />
        </View>,
        <View key="2" id={VIEW_CHAT} activePanel={config.activePanel}>
            <DesktopChat id={PANEL_DESKTOP_CHAT} {...props} />
            <MobileChat id={PANEL_MOBILE_CHAT} {...props} />
        </View>,
        <View key="3" id={VIEW_AUTH} activePanel={isAuthPage ? PANEL_AUTH : config.activePanel}>
            <Authorization id={PANEL_AUTH}/>
        </View>,
        <View key="4" id={VIEW_CART} activePanel={config.activePanel}>
            <MobileCart id={PANEL_CART_MOBILE}/>
        </View>,
        <View key="5" id={VIEW_FEED} activePanel={config.activePanel}>
            <DesktopFeed id={PANEL_FEED_DESKTOP}/>
            <MobileFeed id={PANEL_FEED_MOBILE}/>
        </View>,
        <View key="6" id={VIEW_PRODUCT} activePanel={config.activePanel}>
            <Product id={PANEL_PRODUCT}/>
        </View>,
        // <View key={"7"} id={VIEW_FEED} activePanel={config.activePanel}>
        // </View>,
    ];

    return (
        <ConfigProvider platform={isMobile ? ANDROID : Platform.ANDROID}>
            <AppRoot>
                <Notifacations/>
                <SplitLayout style={{justifyContent: 'center'}} popout={popout} modal={modal}>
                    {loading ? <Loader firstScreen={true}/> :
                        !isMobile ? (
                            <SplitCol
                                maxWidth={(config.activePanel !== PANEL_PRODUCT && config.activePanel !== PANEL_FEED_DESKTOP) ? "1000px" : "1400px"}>
                                <Root className="root" activeView={location.getViewId()}>
                                    {root.map((view) => view)}
                                </Root>
                            </SplitCol>
                        ) : (
                            <Epic
                                className={config.isKeyboardActive ? 'epic-hidden' : ''}
                                tabbar={
                                    isMobile && <>
                                        <Tabbar className="bottom_navigation" shadow={false} style={{marginBottom: 40}}>
                                            <TabbarItem onClick={() => router.pushPage(PAGE_FEED_MOBILE)}>
                                                <img
                                                    style={{
                                                        height: 30,
                                                        width: 30,
                                                        marginTop: '-4px',
                                                    }}
                                                    src={location.getPageId() !== PAGE_FEED_MOBILE ? footerNews : feedBold}
                                                    alt="news"
                                                    className={'epicIcon'}/>
                                                <p style={{marginTop: '0'}}>Лента</p>
                                            </TabbarItem>
                                            <TabbarItem onClick={() => router.pushPage(PAGE_CHAT_MOBILE)}>
                                                <div className="epicIcon withNotice">
                                                    <img
                                                        className="epicIcon"
                                                        src={location.getPageId() !== PAGE_CHAT_MOBILE ? messenegerIcon : messengerBold}
                                                        alt="messeneger"/>
                                                    {(+chat.newMessagesCount > 0) &&
                                                        <div className="notice">{chat.newMessagesCount}</div>}
                                                </div>
                                                <p>Входящие</p>
                                            </TabbarItem>
                                            <TabbarItem onClick={() => router.pushModal(MODAL_CREATE)}>
                                                <img src={newChatIcon} alt="create" className={'epicIcon createIcon'}/>
                                            </TabbarItem>
                                            <TabbarItem onClick={() => router.pushPage(PAGE_CART_MOBILE)}>
                                                <div className="epicIcon cardItemCount"
                                                     style={{
                                                         height: 27,
                                                         width: 'auto',
                                                     }}>
                                                    <img className="epicIcon"
                                                         style={{
                                                             height: '22px',
                                                             width: 'auto',
                                                         }}
                                                         src={location.getPageId() !== PAGE_CART_MOBILE ? cartIcon : cartBold}
                                                         alt="cart"/>
                                                    <div className="notice"
                                                         style={location.getPageId() !== PAGE_CART_MOBILE ? {} : {left: 15}}>0
                                                    </div>
                                                </div>
                                                <p>Корзина</p>
                                            </TabbarItem>
                                            {!user.userData.id ? (
                                                <TabbarItem
                                                    style={{paddingBottom: 0}}
                                                    onClick={() => router.pushModal(MODAL_UNAUTHORIZED)}>
                                                    <img className="epicIcon" src={loginIcon} alt="login"/>
                                                    <p>Войти</p>
                                                </TabbarItem>) : (
                                                <TabbarItem
                                                    style={{paddingBottom: 5}}
                                                    onClick={() => router.pushModal(MODAL_CARDUSER)}>
                                                    {user.userData.photo
                                                        ? <Avatar size={35} style={{cursor: 'pointer'}} /*src={}*//>
                                                        : <LetterAvatar width={35} height={35}/>}
                                                </TabbarItem>
                                            )}
                                        </Tabbar>
                                        <Tabbar shadow={false} className="bottom_navigation_about">
                                            <TabbarItem className={'current'}>
                                                <p>Москва</p>
                                            </TabbarItem>
                                            <TabbarItem>
                                                <p>Все о allabay</p>
                                            </TabbarItem>
                                            <TabbarItem>
                                                <p>Условия</p>
                                            </TabbarItem>
                                            <TabbarItem>
                                                <p>Введение</p>
                                            </TabbarItem>
                                        </Tabbar>
                                    </>
                                }
                                activeStory={location.getViewId()}>
                                {config.updateActivePanel(location.getPanelId())}
                                {root.map((view) => view)}
                            </Epic>
                        )}
                </SplitLayout>
            </AppRoot>
        </ConfigProvider>
    );
});

// export default compose(
//   withAdaptivity(App, {
//     viewHeight: true,
//     viewWidth: true
//   }),
//   observe
// )(App);

export default App;
