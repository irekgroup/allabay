export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Date: any;
  DateTime: any;
  GenericScalar: any;
  JSONString: any;
};

export type BoolType = {
  __typename?: 'BoolType';
  isChat?: Maybe<Scalars['Boolean']>;
  isDelete?: Maybe<Scalars['Boolean']>;
  isMessage?: Maybe<Scalars['Boolean']>;
  isOperator?: Maybe<Scalars['Boolean']>;
  isRead?: Maybe<Scalars['Boolean']>;
  isSelect?: Maybe<Scalars['Boolean']>;
  isStatus?: Maybe<Scalars['Boolean']>;
};

export type CardType = {
  __typename?: 'CardType';
  category?: Maybe<CategoryType>;
  createAt: Scalars['DateTime'];
  deliveryTimeFrom: Scalars['String'];
  deliveryTimeUntil: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  externalId: Scalars['Int'];
  favouriteCount: Scalars['Int'];
  guarantee?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  isDelete: Scalars['Boolean'];
  kind?: Maybe<KindType>;
  messageSet: Array<MessageQueryType>;
  name: Scalars['String'];
  oldPrice: Scalars['Float'];
  photo?: Maybe<Scalars['String']>;
  price: Scalars['Float'];
  receiveType: Scalars['String'];
  subcategory?: Maybe<SubcategoryType>;
  subtype?: Maybe<SubtypeType>;
  type?: Maybe<TypeType>;
  updateAt: Scalars['DateTime'];
  vendor: Scalars['String'];
};

export type CategoryType = {
  __typename?: 'CategoryType';
  cardSet: Array<CardType>;
  id: Scalars['ID'];
  name: Scalars['String'];
  subcategorySet: Array<SubcategoryType>;
};

export type ChatChangeInput = {
  idChat: Scalars['String'];
  isAvatar?: InputMaybe<Scalars['Boolean']>;
  status?: InputMaybe<Scalars['String']>;
};

export type ChatCreateInput = {
  messageAllabay?: InputMaybe<Scalars['Boolean']>;
  nameChat?: InputMaybe<Scalars['String']>;
  userRecipient?: InputMaybe<Scalars['Boolean']>;
};

export type ChatDelete = {
  __typename?: 'ChatDelete';
  ok?: Maybe<Scalars['Boolean']>;
};

/** An enumeration. */
export enum ChatMessageStatusChat {
  /** Согласование */
  Ag = 'AG',
  /** Принимают решение */
  Dc = 'DC',
  /** Первый контакт */
  Fc = 'FC',
  /** Потеряно */
  Ls = 'LS',
  /** Переговоры */
  Ng = 'NG',
  /** Оплачено */
  Pd = 'PD',
  /** Успешно */
  Su = 'SU'
}

export type ChatPinnedInput = {
  idChat?: InputMaybe<Scalars['String']>;
};

export type ChatType = {
  __typename?: 'ChatType';
  administrator?: Maybe<UserType>;
  botAllabay?: Maybe<UserType>;
  chatOnMainPage: Scalars['Boolean'];
  chatmessages: Array<MessageQueryType>;
  createAt: Scalars['DateTime'];
  id: Scalars['ID'];
  imageChat: Scalars['String'];
  isAllabay: Scalars['Boolean'];
  isDelete: Scalars['Boolean'];
  isPinned: Scalars['Boolean'];
  isWork: Scalars['Boolean'];
  lastMessages?: Maybe<MessageQueryType>;
  nameChat: Scalars['String'];
  operator?: Maybe<UserType>;
  statusChat: ChatMessageStatusChat;
  user1: UserType;
  user2?: Maybe<UserType>;
};

export type CheckVerifyCode = {
  __typename?: 'CheckVerifyCode';
  ok?: Maybe<Scalars['Boolean']>;
};

export type CheckVerifyCodeInput = {
  phone?: InputMaybe<Scalars['String']>;
  verifyCode?: InputMaybe<Scalars['String']>;
};

export type CreateChat = {
  __typename?: 'CreateChat';
  chat?: Maybe<ChatType>;
  ok?: Maybe<Scalars['Boolean']>;
};

export type CreateMessage = {
  __typename?: 'CreateMessage';
  message?: Maybe<MessageQueryType>;
  ok?: Maybe<Scalars['Boolean']>;
};

export type CreateUser = {
  __typename?: 'CreateUser';
  ok?: Maybe<Scalars['Boolean']>;
  user?: Maybe<UserType>;
};

export type DeleteMessages = {
  __typename?: 'DeleteMessages';
  ok?: Maybe<Scalars['Boolean']>;
};

export type KindType = {
  __typename?: 'KindType';
  belongsTo?: Maybe<SubcategoryType>;
  cardSet: Array<CardType>;
  id: Scalars['ID'];
  isLastStep: Scalars['Boolean'];
  name: Scalars['String'];
  typeSet: Array<TypeType>;
};

export type MessageCreateInput = {
  cardMessage?: InputMaybe<Scalars['String']>;
  chatMessage?: InputMaybe<Scalars['String']>;
  imageMessage?: InputMaybe<Scalars['String']>;
  textMessage?: InputMaybe<Scalars['String']>;
};

export type MessageDeleteInput = {
  idMessages?: InputMaybe<Scalars['JSONString']>;
};

export type MessageQueryType = {
  __typename?: 'MessageQueryType';
  cardMessage?: Maybe<CardType>;
  chatMessages: ChatType;
  createAt: Scalars['DateTime'];
  id: Scalars['ID'];
  idForTooltip: Scalars['Int'];
  imageMassage: Scalars['String'];
  isDelete: Scalars['Boolean'];
  isRead: Scalars['Boolean'];
  senderUser: UserType;
  textMessage: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  checkVerifyCode?: Maybe<CheckVerifyCode>;
  createChat?: Maybe<CreateChat>;
  createMessage?: Maybe<CreateMessage>;
  deleteChat?: Maybe<ChatDelete>;
  deleteMessages?: Maybe<DeleteMessages>;
  pinChat?: Maybe<PinnedChat>;
  pinOnMain?: Maybe<PinnedChatOnMainPage>;
  readChat?: Maybe<ReadChat>;
  resetPassword?: Maybe<ResetPasswordUser>;
  selectChat?: Maybe<SelectChat>;
  sendTooltip?: Maybe<SendTooltip>;
  unPinChat?: Maybe<UnPinnedChat>;
  unPinOnMain?: Maybe<UnPinnedChatOnMainPage>;
  updateChat?: Maybe<UpdateChat>;
  updateInformationUser?: Maybe<CreateUser>;
  updateOperator?: Maybe<UpdateOperator>;
  verificationResetPassword?: Maybe<VerificationResetPasswordUser>;
  verificationUser?: Maybe<VerificationUser>;
};


export type MutationCheckVerifyCodeArgs = {
  input?: InputMaybe<CheckVerifyCodeInput>;
};


export type MutationCreateChatArgs = {
  input?: InputMaybe<ChatCreateInput>;
};


export type MutationCreateMessageArgs = {
  input?: InputMaybe<MessageCreateInput>;
};


export type MutationDeleteChatArgs = {
  input?: InputMaybe<ChatChangeInput>;
};


export type MutationDeleteMessagesArgs = {
  input?: InputMaybe<MessageDeleteInput>;
};


export type MutationPinChatArgs = {
  input?: InputMaybe<ChatPinnedInput>;
};


export type MutationPinOnMainArgs = {
  input?: InputMaybe<ChatPinnedInput>;
};


export type MutationReadChatArgs = {
  input?: InputMaybe<ChatChangeInput>;
};


export type MutationResetPasswordArgs = {
  input?: InputMaybe<ResetPasswordInput>;
};


export type MutationSelectChatArgs = {
  input?: InputMaybe<ChatChangeInput>;
};


export type MutationSendTooltipArgs = {
  input?: InputMaybe<SendTooltipInput>;
};


export type MutationUnPinChatArgs = {
  input?: InputMaybe<ChatPinnedInput>;
};


export type MutationUnPinOnMainArgs = {
  input?: InputMaybe<ChatPinnedInput>;
};


export type MutationUpdateChatArgs = {
  input?: InputMaybe<ChatChangeInput>;
};


export type MutationUpdateInformationUserArgs = {
  input?: InputMaybe<UserInput>;
};


export type MutationUpdateOperatorArgs = {
  input?: InputMaybe<UpdateOperatorsInput>;
};


export type MutationVerificationResetPasswordArgs = {
  input?: InputMaybe<VerificationInput>;
};


export type MutationVerificationUserArgs = {
  input?: InputMaybe<VerificationInput>;
};

export type NotifyType = {
  __typename?: 'NotifyType';
  bools?: Maybe<BoolType>;
  chat?: Maybe<ChatType>;
  message?: Maybe<MessageQueryType>;
};

/** Obtain JSON Web Token mutation */
export type ObtainJsonWebToken = {
  __typename?: 'ObtainJSONWebToken';
  payload: Scalars['GenericScalar'];
  refreshExpiresIn: Scalars['Int'];
  token: Scalars['String'];
};

export type OperatorsType = {
  __typename?: 'OperatorsType';
  administrator: UserType;
  createAt: Scalars['DateTime'];
  id: Scalars['ID'];
  isDelete: Scalars['Boolean'];
  operator: UserType;
  updateAt: Scalars['DateTime'];
};

export type PinnedChat = {
  __typename?: 'PinnedChat';
  ok?: Maybe<Scalars['Boolean']>;
};

export type PinnedChatOnMainPage = {
  __typename?: 'PinnedChatOnMainPage';
  ok?: Maybe<Scalars['Boolean']>;
};

export type Query = {
  __typename?: 'Query';
  card?: Maybe<CardType>;
  categories?: Maybe<Array<Maybe<CategoryType>>>;
  chat?: Maybe<Array<MessageQueryType>>;
  kinds?: Maybe<Array<Maybe<KindType>>>;
  listChats?: Maybe<Array<ChatType>>;
  listPinnedChat?: Maybe<Array<ChatType>>;
  listPinnedChatOnMain?: Maybe<Array<ChatType>>;
  listTooltip?: Maybe<Array<TooltipType>>;
  listUnselectedChat?: Maybe<Array<ChatType>>;
  me?: Maybe<UserType>;
  operators?: Maybe<Array<Maybe<OperatorsType>>>;
  refreshToken?: Maybe<Refresh>;
  subcategories?: Maybe<Array<Maybe<SubcategoryType>>>;
  subtypes?: Maybe<Array<Maybe<SubtypeType>>>;
  /** Obtain JSON Web Token mutation */
  tokenAuth?: Maybe<ObtainJsonWebToken>;
  types?: Maybe<Array<Maybe<TypeType>>>;
  user?: Maybe<UserType>;
  users?: Maybe<Array<Maybe<UserType>>>;
  verifyToken?: Maybe<Verify>;
};


export type QueryCardArgs = {
  externalId?: InputMaybe<Scalars['String']>;
};


export type QueryChatArgs = {
  idChat?: InputMaybe<Scalars['String']>;
  limit?: InputMaybe<Scalars['String']>;
  start?: InputMaybe<Scalars['String']>;
};


export type QueryKindsArgs = {
  parent?: InputMaybe<Scalars['String']>;
};


export type QueryListTooltipArgs = {
  idMessage?: InputMaybe<Scalars['Int']>;
};


export type QueryRefreshTokenArgs = {
  token?: InputMaybe<Scalars['String']>;
};


export type QuerySubcategoriesArgs = {
  parent?: InputMaybe<Scalars['String']>;
};


export type QuerySubtypesArgs = {
  parent?: InputMaybe<Scalars['String']>;
};


export type QueryTokenAuthArgs = {
  password: Scalars['String'];
  phone: Scalars['String'];
};


export type QueryTypesArgs = {
  parent?: InputMaybe<Scalars['String']>;
};


export type QueryUserArgs = {
  password?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
};


export type QueryUsersArgs = {
  listId?: InputMaybe<Scalars['JSONString']>;
};


export type QueryVerifyTokenArgs = {
  token?: InputMaybe<Scalars['String']>;
};

export type ReadChat = {
  __typename?: 'ReadChat';
  ok?: Maybe<Scalars['Boolean']>;
};

export type Refresh = {
  __typename?: 'Refresh';
  payload: Scalars['GenericScalar'];
  refreshExpiresIn: Scalars['Int'];
  token: Scalars['String'];
};

export type ResetPasswordInput = {
  password?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
};

export type ResetPasswordUser = {
  __typename?: 'ResetPasswordUser';
  ok?: Maybe<Scalars['Boolean']>;
  user?: Maybe<UserType>;
};

export type SelectChat = {
  __typename?: 'SelectChat';
  ok?: Maybe<Scalars['Boolean']>;
};

export type SendTooltip = {
  __typename?: 'SendTooltip';
  ok?: Maybe<Scalars['Boolean']>;
  tooltips?: Maybe<Array<Maybe<TooltipType>>>;
};

export type SendTooltipInput = {
  idChat?: InputMaybe<Scalars['String']>;
  idTooltip?: InputMaybe<Scalars['String']>;
};

export type SubcategoryType = {
  __typename?: 'SubcategoryType';
  belongsTo?: Maybe<CategoryType>;
  cardSet: Array<CardType>;
  id: Scalars['ID'];
  kindSet: Array<KindType>;
  name: Scalars['String'];
};

export type Subscription = {
  __typename?: 'Subscription';
  notifyMessage?: Maybe<NotifyType>;
};

export type SubtypeType = {
  __typename?: 'SubtypeType';
  belongsTo?: Maybe<TypeType>;
  cardSet: Array<CardType>;
  id: Scalars['ID'];
  isLastStep: Scalars['Boolean'];
  name: Scalars['String'];
};

export type TooltipType = {
  __typename?: 'TooltipType';
  id: Scalars['ID'];
  idTooltip: Scalars['Int'];
  isDelete: Scalars['Boolean'];
  textTooltip: Scalars['String'];
};

export type TypeType = {
  __typename?: 'TypeType';
  belongsTo?: Maybe<KindType>;
  cardSet: Array<CardType>;
  id: Scalars['ID'];
  isLastStep: Scalars['Boolean'];
  name: Scalars['String'];
  subtypeSet: Array<SubtypeType>;
};

export type UnPinnedChat = {
  __typename?: 'UnPinnedChat';
  ok?: Maybe<Scalars['Boolean']>;
};

export type UnPinnedChatOnMainPage = {
  __typename?: 'UnPinnedChatOnMainPage';
  ok?: Maybe<Scalars['Boolean']>;
};

export type UpdateChat = {
  __typename?: 'UpdateChat';
  ok?: Maybe<Scalars['Boolean']>;
};

export type UpdateOperator = {
  __typename?: 'UpdateOperator';
  ok?: Maybe<Scalars['Boolean']>;
};

export type UpdateOperatorsInput = {
  id?: InputMaybe<Scalars['String']>;
  isPromotion: Scalars['Boolean'];
  phone?: InputMaybe<Scalars['String']>;
};

export type UserInput = {
  birthday?: InputMaybe<Scalars['Date']>;
  firstName?: InputMaybe<Scalars['String']>;
  gender?: InputMaybe<Scalars['String']>;
  lastName?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
};

export type UserType = {
  __typename?: 'UserType';
  birthday?: Maybe<Scalars['Date']>;
  counterConnectQueue: Scalars['Int'];
  createAt: Scalars['DateTime'];
  dateJoined: Scalars['DateTime'];
  email: Scalars['String'];
  firstName: Scalars['String'];
  gender: Scalars['String'];
  id: Scalars['ID'];
  isActive: Scalars['Boolean'];
  isAdmin: Scalars['Boolean'];
  isOnline: Scalars['Boolean'];
  isOperator: Scalars['Boolean'];
  /** Designates whether the user can log into this admin site. */
  isStaff: Scalars['Boolean'];
  /** Designates that this user has all permissions without explicitly assigning them. */
  isSuperuser: Scalars['Boolean'];
  lastLogin?: Maybe<Scalars['DateTime']>;
  lastName: Scalars['String'];
  lastTimeConnect: Scalars['DateTime'];
  password: Scalars['String'];
  phone: Scalars['String'];
  photo: Scalars['String'];
  sender: Array<MessageQueryType>;
  updateAt: Scalars['DateTime'];
  verificationCode: Scalars['String'];
  verificationCodeAt?: Maybe<Scalars['DateTime']>;
};

export type VerificationInput = {
  phone?: InputMaybe<Scalars['String']>;
};

export type VerificationResetPasswordUser = {
  __typename?: 'VerificationResetPasswordUser';
  message?: Maybe<Scalars['String']>;
  ok?: Maybe<Scalars['Boolean']>;
};

export type VerificationUser = {
  __typename?: 'VerificationUser';
  message?: Maybe<Scalars['String']>;
  ok?: Maybe<Scalars['Boolean']>;
};

export type Verify = {
  __typename?: 'Verify';
  payload: Scalars['GenericScalar'];
};
