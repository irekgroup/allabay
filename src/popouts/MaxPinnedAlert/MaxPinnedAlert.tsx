import React, { FC } from 'react';
import './MaxPinnedAlert.scss';
import { Link } from "@vkontakte/vkui";
import { useRouter } from "@happysanta/router";

const MaxPinnedAlert:FC = () => {
    const router = useRouter()
    return (
        <div className={'maxPinnedAlert'}>
            <p>Можно закрепить не более 5 чатов.</p>
            <Link onClick={() => {
                router.popPage()
            }}>ОК</Link>
        </div>
    );
};

export default MaxPinnedAlert;