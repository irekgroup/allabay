/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ChatPinnedInput } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL mutation operation: unpinChat
// ====================================================

export interface unpinChat_unPinChat {
  __typename: "UnPinnedChat";
  ok: boolean | null;
}

export interface unpinChat {
  unPinChat: unpinChat_unPinChat | null;
}

export interface unpinChatVariables {
  input: ChatPinnedInput;
}
