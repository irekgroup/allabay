/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ChatPinnedInput } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL mutation operation: pinChat
// ====================================================

export interface pinChat_pinChat {
  __typename: "PinnedChat";
  ok: boolean | null;
}

export interface pinChat {
  pinChat: pinChat_pinChat | null;
}

export interface pinChatVariables {
  input: ChatPinnedInput;
}
