/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MessageDeleteInput } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL mutation operation: deleteMessages
// ====================================================

export interface deleteMessages_deleteMessages {
  __typename: "DeleteMessages";
  ok: boolean | null;
}

export interface deleteMessages {
  deleteMessages: deleteMessages_deleteMessages | null;
}

export interface deleteMessagesVariables {
  input: MessageDeleteInput;
}
