import React, { FC, useContext } from 'react';
import { Link } from "@vkontakte/vkui";
import pin from "../../img/chat/pin.svg";
import unpin from "../../img/chat/unpin.svg";
import userSvg from "../../img/main/footer-login-blue.svg";
import create from "../../img/chat/create-new-chat.svg";
import mute from "../../img/chat/mute.svg";
import del from "../../img/chat/delete.svg";
import bill from '../../img/chatOptions/bill.svg';
import changeTitle from '../../img/chatOptions/changeTitle.svg';
import home from '../../img/chatOptions/home.svg';
import navigator from '../../img/chatOptions/navigator.svg';
import person from '../../img/chatOptions/preson.svg';
import woman from '../../img/chatOptions/woman.svg';
import userFilled from '../../img/chatOptions/userFilled.svg';
import rating from '../../img/chatOptions/rating.svg';
import './ChatOptions.scss';
import { useRouter } from "@happysanta/router";
import { PAGE_PROFILE, POPOUT_DELETE_ALERT, POPOUT_MAX_PINNED_ALERT } from "../../router";
import { RootStoreContext } from "../../index";
import { gql, useMutation } from "@apollo/client";
import { pinChat, pinChatVariables } from "./__generated__/pinChat";
import { unpinChat, unpinChatVariables } from "./__generated__/unpinChat";
import { observer } from "mobx-react";
import { ChatFilters } from "../../mobX/Chat";

export const DELETE_MESSAGES = gql`
    mutation deleteMessages($input: MessageDeleteInput!){
        deleteMessages(input: $input) {
            ok
        }
    }
`

export const PIN_CHAT = gql`
    mutation pinChat($input: ChatPinnedInput!){
        pinChat(input: $input){
            ok
        }
    }
`

export const UNPIN_CHAT = gql`
    mutation unpinChat($input: ChatPinnedInput!){
        unPinChat(input: $input){
            ok
        }
    }
`

const ModalChatOptions:FC = observer(() => {
    const router = useRouter()
    const [pinChat] = useMutation<pinChat, pinChatVariables>(PIN_CHAT, {fetchPolicy: 'network-only'})
    const [unpinChat] = useMutation<unpinChat, unpinChatVariables>(UNPIN_CHAT, {fetchPolicy: 'network-only'})
    const { config, chat, user } = useContext(RootStoreContext)
    return (
        <div className={`ChatOptions ${config.isMobile && 'mobile'}`}>
            {user.userData.isStaff &&
                <>
                    <Link>
                        <img src={changeTitle} alt="title"/>
                        <p>{chat.title}</p>
                    </Link>
                    <Link>
                        <img src={navigator} alt="city"/>
                        <p>Москва</p>
                    </Link>
                </>
            }
            <Link onClick={() => {
                if (chat.dialogId && typeof chat.dialogIdx === "number") {
                    !chat.chatList[chat.dialogIdx].isPinned
                        ? pinChat({
                            variables: {
                                input: {
                                    idChat: chat.dialogId
                                },
                            },
                        })
                            .then(() => chat.pinChatLocal())
                            .catch(() => router.pushPopup(POPOUT_MAX_PINNED_ALERT))
                        : unpinChat({
                            variables: {
                                input: {
                                    idChat: chat.dialogId
                                }
                            }
                        })
                            .then(() => chat.unpinLocal())
                            .catch(console.error)
                }
            }}>
                <img alt="pin" src={(chat.dialogIdx !== null && chat.chatList[chat.dialogIdx].isPinned) ? unpin : pin}/>
                <p>{(chat.dialogIdx !== null && chat.chatList[chat.dialogIdx].isPinned) ? 'Открепить' : 'Закрепить'}</p>
            </Link>
            <Link onClick={() => router.pushPage(PAGE_PROFILE)}>
                <img src={userSvg} alt="user"/>
                <p>Открыть мой аккаунт</p>
            </Link>
            <Link>
                <img src={create} alt="add"/>
                <p>Добавить на гл. экран</p>
            </Link>
            <Link>
                <img src={mute} alt="mute"/>
                <p>Отключить уведомления</p>
            </Link>
            <Link onClick={() => {
                router.pushPopup(POPOUT_DELETE_ALERT)
            }}>
                <img src={del} alt="delete"/>
                <p>Очистить историю</p>
            </Link>
            {user.userData.isStaff &&
                <>
                    <Link>
                        <img src={rating} alt="rating"/>
                        <p style={{textTransform: 'capitalize'}}>{chat.dialogIdx && ChatFilters[chat.chatList[chat.dialogIdx].statusChat].toLowerCase()}</p>
                    </Link>
                    <Link>
                        <img src={(user.userData.gender === 'male' || !user.userData.gender) ? person : woman} alt="man"/>
                        <p>{user.userData.firstName} {user.userData.lastName}</p>
                    </Link>
                    <Link>
                        <img src={bill} alt="bill"/>
                        <p>0 р</p>
                    </Link>
                    <Link>
                        <img src={userFilled} alt="user"/>
                        <p>Профиль клиента</p>
                    </Link>
                    <Link>
                        <img src={home} alt="home"/>
                        <p>Страница клиента</p>
                    </Link>
                </>
            }
        </div>
    );
});

export default ModalChatOptions;