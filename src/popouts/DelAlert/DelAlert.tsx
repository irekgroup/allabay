import React, { FC, useContext } from 'react';
import { Alert } from "@vkontakte/vkui";
import { RootStoreContext } from "../../index";
import { gql, useMutation } from "@apollo/client";
import { deleteMessages, deleteMessagesVariables } from "../ChatOptions/__generated__/deleteMessages";
import { DELETE_MESSAGES } from "../ChatOptions/ChatOptions";
import { useRouter } from "@happysanta/router";
import './DelAlerts.scss';
import { deleteChat, deleteChatVariables } from "./__generated__/deleteChat";

export const DELETE_CHAT = gql`
    mutation deleteChat($input: ChatChangeInput!){
        deleteChat(input: $input){
            ok
        }
    }
`

const DelAlert:FC = () => {
    const { chat } = useContext(RootStoreContext);
    const [deleteMessages] = useMutation<deleteMessages, deleteMessagesVariables>(DELETE_MESSAGES)
    const [deleteChat] = useMutation<deleteChat, deleteChatVariables>(DELETE_CHAT)
    const router = useRouter()

    return (
        <Alert
            className={'delAlert'}
            actions={[
                {
                    title: "Да, удалить",
                    autoclose: true,
                    mode: "destructive",
                    action: () => {
                        if(Object.keys(chat.selectedIds).length > 0){
                            deleteMessages({
                                variables: {
                                    input: {
                                        idMessages: JSON.stringify({
                                            data: Object.keys(chat.selectedIds).filter((id) => chat.selectedIds[id]),
                                        }),
                                    },
                                },
                            })
                                .then(() => {
                                    chat.setSelectedIds({})
                                })
                                .catch(console.error)
                        } else {
                            if(chat.selectedChat !== null){
                                deleteChat({
                                    variables: {
                                        input: {
                                            idChat: chat.chatList[chat.selectedChat].id,
                                        },
                                    },
                                })
                                    .then(() => chat.deleteChat(chat.selectedChat || 0))
                                    .catch(console.error)
                                    .finally(() => chat.setSelectedChat(null))
                            } else {
                                deleteMessages({
                                    variables: {
                                        input: {
                                            idMessages: JSON.stringify({
                                                data: chat.messages.map(i => {
                                                    return +i.uuid
                                                }),
                                            }),
                                        },
                                    }
                                }).catch(console.error)
                            }
                        }
                    },
                },
                {
                    title: "Отменить",
                    autoclose: true,
                    mode: "cancel",
                },
            ]}
            actionsLayout="horizontal"
            onClose={() => router.popPage()}
            text={`Вы действительно хотите удалить сообщения?`}
        />
    );
};

export default DelAlert;