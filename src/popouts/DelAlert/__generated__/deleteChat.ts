/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ChatChangeInput } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL mutation operation: deleteChat
// ====================================================

export interface deleteChat_deleteChat {
  __typename: "ChatDelete";
  ok: boolean | null;
}

export interface deleteChat {
  deleteChat: deleteChat_deleteChat | null;
}

export interface deleteChatVariables {
  input: ChatChangeInput;
}
