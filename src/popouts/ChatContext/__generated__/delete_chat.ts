/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ChatChangeInput } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL mutation operation: delete_chat
// ====================================================

export interface delete_chat_deleteChat {
  __typename: "ChatDelete";
  ok: boolean | null;
}

export interface delete_chat {
  deleteChat: delete_chat_deleteChat | null;
}

export interface delete_chatVariables {
  input: ChatChangeInput;
}
