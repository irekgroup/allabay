import React, { FC, useContext, useEffect, } from 'react';
import { POPOUT_DELETE_ALERT, POPOUT_MAX_PINNED_ALERT } from "../../router";
import del from "../../img/chat/delete.svg";
import { Link } from "@vkontakte/vkui";
import {  useRouter } from "@happysanta/router";
import { RootStoreContext } from "../../index";
import { useMutation } from "@apollo/client";
import { pinChat, pinChatVariables } from "../ChatOptions/__generated__/pinChat";
import { PIN_CHAT, UNPIN_CHAT } from "../ChatOptions/ChatOptions";
import './ChatContext.scss';
import { unpinChat, unpinChatVariables } from "../ChatOptions/__generated__/unpinChat";
import { observer } from "mobx-react";

const ChatContext:FC = observer( () => {
    const router = useRouter()
    const {chat, config} = useContext(RootStoreContext)
    const [pinChat] = useMutation<pinChat, pinChatVariables>(PIN_CHAT)
    const [unpinChat] = useMutation<unpinChat, unpinChatVariables>(UNPIN_CHAT)
    const pos = config.mousePos

    useEffect(() => {
        if(chat.selectedChat === null){
            router.popPage()
        }
    }, [chat.selectedChat])

    return (
        <div className={'chatContext'} style={pos ? {position: 'fixed', left: `calc(${pos.x}px - 150px)`, top: `calc(${pos.y}px - 50px)`} : {}}>
            <Link onClick={() => {
                if (chat.selectedChat !== null) {
                    const id = chat.chatList[chat.selectedChat].id
                    !chat.chatList[chat.selectedChat].isPinned
                        ? pinChat({
                            variables: {
                                input: {
                                    idChat: id
                                }
                            }
                        })
                            .then(() => {
                                chat.pinChatLocal(chat.selectedChat || 0)
                                router.popPage()
                                chat.setSelectedChat(null)
                            })
                            .catch(() => router.pushPopup(POPOUT_MAX_PINNED_ALERT))
                        : unpinChat({
                            variables: {
                                input: {
                                    idChat: id
                                }
                            }
                        })
                            .then(() => {
                                chat.unpinLocal(chat.selectedChat || 0)
                                router.popPage()
                                chat.setSelectedChat(null)
                            })
                            .catch(console.error)
                }
            }}>
                {(chat.selectedChat !== null && chat.chatList[chat.selectedChat].isPinned)
                    ? <svg width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fillRule="evenodd" clipRule="evenodd"
                              d="M8.11102 1.55868C8.11102 1.91618 8.28073 1.92161 8.41173 2.11701C8.32235 2.25089 7.97532 2.56424 7.83708 2.70247L5.49508 5.04431C5.44659 5.09316 5.40497 5.14201 5.35721 5.18579C5.28158 5.25599 5.23924 5.23355 5.10318 5.23247C4.74855 5.22957 3.56995 5.19701 3.37526 5.26648C2.83536 5.45862 2.67903 6.08135 3.13136 6.53799L4.39392 7.78888C4.30164 7.92674 1.922 10.2476 1.62745 10.5657C1.44398 10.7636 0.636292 12 1.19248 12C1.33035 12 1.76278 11.8165 1.87315 11.7565C2.05879 11.6562 2.27157 11.5155 2.42247 11.3823C2.70328 11.1341 3.968 9.83727 4.27052 9.53477C4.41093 9.39401 5.12778 8.69674 5.18894 8.60556C5.36951 8.72641 6.44028 9.90891 6.67766 10.0385C7.07318 10.2545 7.5617 10.0877 7.7278 9.64007C7.84975 9.31007 7.70464 7.79612 7.79366 7.66477C7.79366 7.66441 10.7827 4.65497 10.8829 4.58803C11.0783 4.71901 11.0838 4.88872 11.4413 4.88872C11.7756 4.88872 12 4.63507 12 4.26562C12 4.05395 11.8075 3.86905 11.6939 3.75543L9.24438 1.30612C9.0649 1.12628 8.94548 1 8.66974 1C8.36215 1 8.11102 1.25329 8.11102 1.55868Z"
                              fill="#707070" stroke="#707070" strokeWidth="0.5" strokeMiterlimit="22.9256"/>
                        <line x1="2.70631" y1="1.03363" x2="12.0336" y2="11.2937" stroke="#707070"
                              strokeLinecap="round"/>
                    </svg>
                    : <svg width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fillRule="evenodd" clipRule="evenodd"
                              d="M8.11102 1.55868C8.11102 1.91618 8.28073 1.92161 8.41173 2.11701C8.32235 2.25089 7.97532 2.56424 7.83708 2.70247L5.49508 5.04431C5.44659 5.09316 5.40497 5.14201 5.35721 5.18579C5.28158 5.25599 5.23924 5.23355 5.10318 5.23247C4.74855 5.22957 3.56995 5.19701 3.37526 5.26648C2.83536 5.45862 2.67903 6.08135 3.13136 6.53799L4.39392 7.78888C4.30164 7.92674 1.922 10.2476 1.62745 10.5657C1.44398 10.7636 0.636292 12 1.19248 12C1.33035 12 1.76278 11.8165 1.87315 11.7565C2.05879 11.6562 2.27157 11.5155 2.42247 11.3823C2.70328 11.1341 3.968 9.83727 4.27052 9.53477C4.41093 9.39401 5.12778 8.69674 5.18894 8.60556C5.36951 8.72641 6.44028 9.90891 6.67766 10.0385C7.07318 10.2545 7.5617 10.0877 7.7278 9.64007C7.84975 9.31007 7.70464 7.79612 7.79366 7.66477C7.79366 7.66441 10.7827 4.65497 10.8829 4.58803C11.0783 4.71901 11.0838 4.88872 11.4413 4.88872C11.7756 4.88872 12 4.63507 12 4.26562C12 4.05395 11.8075 3.86905 11.6939 3.75543L9.24438 1.30612C9.0649 1.12628 8.94548 1 8.66974 1C8.36215 1 8.11102 1.25329 8.11102 1.55868Z"
                              fill="#707070" stroke="#707070" strokeWidth="0.5" strokeMiterlimit="22.9256"/>
                    </svg>}
                <p>{(chat.selectedChat !== null && chat.chatList[chat.selectedChat].isPinned) ? 'Открепить' : 'Закрепить'}</p>
            </Link>
            <Link onClick={() => {
                router.pushPopup(POPOUT_DELETE_ALERT)
            }}>
                <img src={del} alt="delete"/>
                <p>Удалить чат</p>
            </Link>
        </div>
    );
});

export default ChatContext;