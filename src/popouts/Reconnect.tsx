import React from "react";
import { Alert } from "@vkontakte/vkui";
import { useRouter } from "@happysanta/router";
import { FC } from "react";
import { observer } from "mobx-react";
import { useContext } from "react";
import { RootStoreContext } from "..";

export const Reconnect: FC<any> = observer(({ reconnect }) => {
    const rootStore = useContext(RootStoreContext);
    const router = useRouter();

    const { config } = rootStore;

    return(
        <Alert
            actions={[
              {
                title: "Переподключиться",
                action: () => config.socket.reconnect,
                mode: "cancel",
              },
            ]}
            onClose={() => router.popPage()}
            header="Соединение потеряно"
            text="Повторите попытку позже"
          >
        </Alert>
    );
});
