import React, { FC, useContext } from "react";
import { Alert, PopoutWrapper, ScreenSpinner } from "@vkontakte/vkui";
import {
    POPOUT_CARDUSER, POPOUT_CHAT_CONTEXT_MENU,
    POPOUT_CHAT_OPTIONS, POPOUT_CHAT_TYPES,
    POPOUT_CREATE_CARD_ALERT,
    POPOUT_DELETE_ALERT, POPOUT_MAX_PINNED_ALERT, POPOUT_PRODUCT_ADDED,
    POPOUT_RECONNECT,
    POPOUT_THANKS
} from "../router";
import { Reconnect } from "./Reconnect";
import CardUser from "../components/CardUser/CardUser";
import { useRouter } from "@happysanta/router";
import ChatOptions from "./ChatOptions/ChatOptions";
import { RootStoreContext } from "../index";
import Thanks from "./Thanks/Thanks";
import DelAlert from "./DelAlert/DelAlert";
import CreateCardAlert from "./CreateCardAlert/CreateCardAlert";
import ChatTypes from "./ChatTypes/ChatTypes";
import ChatContext from "./ChatContext/ChatContext";
import MaxPinnedAlert from "./MaxPinnedAlert/MaxPinnedAlert";
import ProductToCart from "./ProductToCart/ProductToCart";

interface PopoutProps {
    popout: any,
    children?: React.ReactNode
}

const Popout: FC<PopoutProps> = ({ popout }) => {
    const router = useRouter()
    const { config, chat } = useContext(RootStoreContext)
    switch (popout) {
        case "screen_spinner":
            return <ScreenSpinner/>

        case POPOUT_RECONNECT:
            return <Reconnect/>;

        case POPOUT_CARDUSER:
            return (
                <PopoutWrapper onClick={() => router.popPage()}>
                    <CardUser/>
                </PopoutWrapper>);

        case POPOUT_CHAT_OPTIONS:
            return (
                <PopoutWrapper alignX={config.isMobile ? "right" : "center"} alignY={"top"}
                               onClick={() => router.popPage()}>
                    <ChatOptions/>
                </PopoutWrapper>
            )

        case POPOUT_THANKS:
            return (
                <PopoutWrapper alignX={'center'} alignY={'center'} onClick={() => router.popPage()}>
                    <Thanks/>
                </PopoutWrapper>
            );

        case POPOUT_DELETE_ALERT:
            return (
                <PopoutWrapper>
                    <DelAlert/>
                </PopoutWrapper>
            );

        case POPOUT_CREATE_CARD_ALERT:
            return (
                <PopoutWrapper>
                    <CreateCardAlert/>
                </PopoutWrapper>
            )

        case POPOUT_CHAT_TYPES:
            return (
                <PopoutWrapper onClick={() => router.popPage()}>
                    <ChatTypes/>
                </PopoutWrapper>
            )

        case POPOUT_CHAT_CONTEXT_MENU:
            return (
                <PopoutWrapper onClick={() => {
                    chat.setSelectedChat(null)
                    router.popPage()
                }}>
                    <ChatContext/>
                </PopoutWrapper>
            )

        case POPOUT_MAX_PINNED_ALERT:
            return (
                <PopoutWrapper onClick={() => {
                    router.popPage()
                }}>
                    <MaxPinnedAlert/>
                </PopoutWrapper>
            )

        case POPOUT_PRODUCT_ADDED:
            return (
                <PopoutWrapper onClick={() => router.popPage()}>
                    <ProductToCart/>
                </PopoutWrapper>
            )

        default:
            return null;
    }
}

export default Popout;
