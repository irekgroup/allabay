import React, { useContext } from 'react';
import cartExample from '../../img/cart/cart-img-example.png';
import './ProductToCart.scss';
import Button from "../../components/Button/Button";
import { LastSearchedItem } from "../../panels/Product/Product";
import { Icon16Add, Icon16Minus, Icon24CancelOutline } from "@vkontakte/icons";
import { useRouter } from "@happysanta/router";
import { RootStoreContext } from "../../index";
import { CardScroll } from "@vkontakte/vkui";

const ProductToCart = () => {
    const router = useRouter()
    const { config } = useContext(RootStoreContext)
    return (
        <div className={'productToCart'}>
            <Icon24CancelOutline onClick={() => router.popPage()} className={'close'}/>
            <h2>Товар успешно добавлен в корзину</h2>
            <div className="item row">
                <img src={cartExample}/>
                <div className="col">
                    <p className={'title'}>Капот Volkswagen Рестальинг</p>
                    <div className="row">{config.isMobile
                        ? <div className="count">
                            <Icon16Add onClick={() => {
                            }}/>
                            <p>{0}</p>
                            <Icon16Minus onClick={() => {
                            }}/>
                        </div>
                        : <p>Доставка: Почта России до адреса доставки бесплатно за 5 дней </p>}
                        {config.isMobile && <div className={'cost'}>2570 ₽</div>}
                    </div>
                </div>
                {!config.isMobile && <>
                    <div className="count">
                        <Icon16Add fill={'#000'} onClick={() => {
                        }}/>
                        <p>{0}</p>
                        <Icon16Minus fill={'#000'} onClick={() => {
                        }}/>
                    </div>
                    <div className={'cost'}>2570 ₽</div>
                </>}
            </div>
            <div className="row buttons">
                <p>В корзине 2 товара на сумму 4 749 ₽</p>
                <div className={'row'}>
                    <Button mode={'outline'}>ПРОДОЛЖИТЬ ПОКУПКИ</Button>
                    <Button mode={'primary'}>ПЕРЕЙТИ В КОРЗИНУ</Button>
                </div>
            </div>
            {!config.isMobile && <div className="row itemsHeader">
                <p className="bold">С этим товаром покупают</p>
                <p>Присмотритесь - могут пригодится и вам</p>
            </div>}
            {config.isMobile
                ? <div className={'scrollWrapper'}>
                    {/*<LastSearchedItem/>*/}
                    {/*<LastSearchedItem/>*/}
                    {/*<LastSearchedItem/>*/}
                    {/*<LastSearchedItem/>*/}
                    {/*<LastSearchedItem/>*/}
                </div>
                : <>
                    <div className="row sb">
                        <LastSearchedItem/>
                        <LastSearchedItem/>
                        <LastSearchedItem/>
                    </div>
                    <div className="row sb">
                        <LastSearchedItem/>
                        <LastSearchedItem/>
                        <LastSearchedItem/>
                    </div>
                </>}
        </div>
    );
};

export default ProductToCart;