import React, { FC, useEffect } from 'react';
import thanks from "../../img/feed/thanks.svg";
import './Thanks.scss';
import { useRouter } from "@happysanta/router";

const Thanks:FC = () => {
    const router = useRouter()
    const t = setTimeout(() => {
        router.popPage()
    }, 2000)
    useEffect(() => {
        return () => {clearTimeout(t)}
    }, [])
    return (
        <div className={'thanks'}>
            <p>Сообщение отправлено</p>
            <img src={thanks} alt=""/>
            <p>Спасибо за отзыв</p>
        </div>
    );
};

export default Thanks;