import React, { FC, useContext } from 'react';
import { Link } from "@vkontakte/vkui";
import './ChatTypes.scss';
import { RootStoreContext } from "../../index";
import { ChatFilters} from "../../mobX/Chat";
import { useLocation, useRouter } from "@happysanta/router";
import { observer } from "mobx-react";
import { useMutation } from "@apollo/client";
import { updateChat, updateChatVariables } from "../../components/Chat/__generated__/updateChat";
import { UPDATE_CHAT } from "../../components/Chat/Chat";

const ChatTypes: FC = observer(() => {
    const router = useRouter()
    const location = useLocation()
    const { chat } = useContext(RootStoreContext)
    const [updateChat] = useMutation<updateChat, updateChatVariables>(UPDATE_CHAT)

    return (
        <div className={'chatTypesPopup'}>
            {Object.values(ChatFilters).map((text, i) =>
                <Link
                    key={i}
                    onClick={() => {
                        if(!location.getParams().id){
                            chat.setChatFilter(text)
                            router.popPage()
                        } else {
                            const key = Object.keys(ChatFilters)[Object.values(ChatFilters).indexOf(text)]
                            updateChat({
                                variables:{
                                    input:{
                                        idChat: chat.dialogId || "" ,
                                        status: key
                                    }
                                }
                            })
                                .then(() => {chat.setStatus(key)})
                                .catch(console.error)
                                .finally(() => router.popPage())
                        }
                    }}>
                    {text[0] + text.toLowerCase().slice(1)}
                    {((!location.getParams().id && chat.filterBy === text) /*|| chat.chatType*/) && <div className="isnRead"/>}
                </Link>)}
        </div>
    );
});

export default ChatTypes;