import React, { useContext } from 'react';
import { useRouter } from "@happysanta/router";
import { Alert } from "@vkontakte/vkui";
import './CreateCardAlert.scss';
import { RootStoreContext } from "../../index";
import { CreateCardTabs } from "../../mobX/CreateCard";

const CreateCardAlert = () => {
    const router = useRouter()
    const {createCard} = useContext(RootStoreContext)
    return (
        <Alert
            className={'createCardAlert delAlert'}
            actions={[
                {
                    title: "Да, отправить",
                    autoclose: true,
                    mode: "default",
                    action: () => {
                        // createCard.setTab(CreateCardTabs.PREVIEW)
                    },
                },
                {
                    title: "Проверить данные",
                    autoclose: true,
                    mode: "destructive",
                },
            ]}
            actionsLayout="horizontal"
            onClose={() => router.popPage()}
            header="Вы дейсвительно хотите отправить карточку?"
            text={`Все данные заполнены верно?`}
        />
    );
};

export default CreateCardAlert;