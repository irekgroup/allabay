import { Button, FormItem, FormLayout, FormStatus, Input, ScreenSpinner, Textarea } from "@vkontakte/vkui";
import React, { useState } from "react";
import "./ModalCreateTemplate.css";
const placeholders = {
    first_name: "Имя",
    last_name: "Фамилия",
}

const ModalCreateTemplate = ({ setActiveModal, setGlobalState, globState, sendWithSocket, setPopout }) => {
    let template = globState.user.template ? globState.user.template : { description: "", name: ""};
    const [form, onChangeForm ] = useState(template)

    function onChange(e) {
        const { name, value } = e.target;
        onChangeForm({
            ...form, 
            [name]: value
        })
    }


    function save() {
        if(!form.id) 
            sendWithSocket({ type :"template", create:{ ...form }});
        else 
            sendWithSocket({ type :"template", modify:{ ...form }});
        setPopout(<ScreenSpinner />);
    }

    return(
        <div>
            <FormLayout>
                <FormItem top="Название">
                    <Input value={form.name} onChange={onChange} name="name" />
                </FormItem>

                <FormItem top="Текст">
                    <Textarea value={form.description} name="description" onChange={onChange} />
                </FormItem>

          
                <div className="container__placeholders">
                    {Object.keys(placeholders).map((placeholder, i) => (
                        <Button mode="tertiary" size="m"onClick={() => {
                            onChange({ target: {name: "description", value: form.description + `{${placeholder}}`}})
                        }} key={i}>{placeholders[placeholder]}</Button>
                    ))}
                </div>

                {globState.user.template_error && <FormItem>
                    <FormStatus mode="error">
                        {globState.user.template_error}
                    </FormStatus>
                </FormItem>}    
                
                <FormItem>
                    <div className="buttons">
                        <Button stretched mode="secondary" onClick={() => setActiveModal("menu_hints_manager") } size="m">Отмена</Button>
                        <Button stretched size="m" style={{ marginLeft: 8 }} onClick={save}>Сохранить</Button>
                    </div>
                </FormItem>
                
            </FormLayout>
        </div>
    );
}

export default ModalCreateTemplate;
