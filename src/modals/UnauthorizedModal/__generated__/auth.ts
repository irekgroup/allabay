/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: auth
// ====================================================

export interface auth_tokenAuth {
  __typename: "ObtainJSONWebToken";
  token: string;
}

export interface auth {
  /**
   * Obtain JSON Web Token mutation
   */
  tokenAuth: auth_tokenAuth | null;
}

export interface authVariables {
  phone: string;
  password: string;
}
