/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateUser
// ====================================================

export interface CreateUser_updateInformationUser_user {
  __typename: "UserType";
  phone: string;
  birthday: any;
}

export interface CreateUser_updateInformationUser {
  __typename: "CreateUser";
  user: CreateUser_updateInformationUser_user | null;
}

export interface CreateUser {
  updateInformationUser: CreateUser_updateInformationUser | null;
}

export interface CreateUserVariables {
  firstName: string;
  lastName: string;
  phone: string;
  birthday: any;
  gender: string;
  password: string;
}
