/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Me
// ====================================================

export interface Me_me {
  __typename: "UserType";
  id: string;
  phone: string;
  photo: string;
  birthday: any;
  firstName: string;
  lastName: string;
  /**
   * Designates whether the user can log into this admin site.
   */
  isStaff: boolean;
  gender: string;
  isOperator: boolean;
}

export interface Me {
  me: Me_me | null;
}
