import {
    Icon24LockOutline,
    Icon24PhoneOutline,
} from '@vkontakte/icons';
import { Link, Radio, Spinner } from '@vkontakte/vkui';
import { observer } from 'mobx-react';
import React, { FC, useContext, useEffect, useLayoutEffect } from 'react';
import { useState } from 'react';
import Button from '../../components/Button/Button';
import Input from '../../components/Input/Input';
import arrowBack from '../../img/main/arrow-back.svg';
import './UnauthorizedModal.scss';
import { RootStoreContext, splitLink } from '../..';
import { gql, useApolloClient, useLazyQuery, useMutation } from '@apollo/client';
import { useRouter } from '@happysanta/router';
import { auth, authVariables } from './__generated__/auth';
import { Me } from './__generated__/Me';
import { CreateUser, CreateUserVariables } from './__generated__/CreateUser';
import { FormData, formDataStatus, VERIFICATION_USER } from "../../panels/Authorization/Authorization";

interface UnauthorizedModalProps {
    // children: ReactNode;
    // connect: () => void;
}

const UnauthorizedModal: FC<UnauthorizedModalProps> = observer(({}) => {
    const [form, onChange] = useState<FormData>({
        phone: '',
        password: '',
        firstName: '',
        lastName: '',
        birthday: '',
        gender: '',
        pageAddress: '',
    });

    const [inputStatus, setInputStatus] = useState<formDataStatus>({});

    const apollo = useApolloClient();
    const [auth, tokenData] = useLazyQuery<auth, authVariables>(AUTH, {
        fetchPolicy: 'network-only',
    });
    const [getUser, userData] = useLazyQuery<Me>(ME,{
        fetchPolicy: 'network-only',
    });
    const [verificationUser, verifiedUser] = useMutation(VERIFICATION_USER);
    const [createUser, createdUserData] = useMutation<CreateUser, CreateUserVariables>(CREATE_USER);
    const router = useRouter();

    const [tab, setActiveTab] = useState<Tab>(Tab.LOGIN);
    const root = useContext(RootStoreContext);
    const { config, user, formInputs } = root;

    const [showErrors, setShowErrors] = useState(false);

    const [errMsgIdx, setErrMsgIdx] = useState(0);

    const errMsgs = ['Пароль должен содержать не менее пяти 5 символов состоящих из цифр или букв.',
        'Минимальное количество символов в пароле: 5. Пожалуйста, введите другой пароль.',
        'Выберите более надежный пароль. Используйте комбинацию букв и цифр, которые сможете запомнить.']

    useLayoutEffect(() => {
        config.changeModelHeader('Авторизация');
    }, []);

    let tokensLength = 0
    useEffect(() => {
        if (tokenData.data?.tokenAuth?.token) {
            const token = tokenData.data?.tokenAuth?.token;
            apollo.setLink(splitLink(token))
            getUser().catch(console.error);
            let tokens = [token]
            try{
                const prevToken = user.getTokens();
                if(Array.isArray(prevToken)){
                    tokens = [...prevToken.filter(t => t !== token),...tokens,]
                    tokensLength = tokens.length
                }
            } finally {
                user.setToken(tokens);
            }
        }
        if(!tokenData.loading && !tokenData.data?.tokenAuth?.token){
            setInputStatus((prev: formDataStatus) => ({
                ...prev,
                logInpPass: {
                    status: 'error',
                    description:
                        'Пароль, который вы указали, не соответствует ни одному аккаунту.',
                },
                logInpTel: {
                    status: 'error',
                    description:
                        'Пароль, который вы указали, не соответствует ни одному аккаунту.',
                },
            }));
        }
    }, [tokenData.loading]);

    useEffect(() => {
        if (!userData.loading && userData.data?.me) {
            user.addSavedUser({ i: user.authorizedUsers.length, userData: { ...userData.data?.me } })
            user.updateCurrUserIdx(user.authorizedUsers.length - 1)
            if(tokensLength-- <= 0){
                router.popPage();
            }
        }

    }, [userData.data, userData.loading]);

    const setTab = (activeTab: Tab) => {
        setShowErrors(false);
        setActiveTab(activeTab);
        config.changeModelHeader(getHeader(activeTab));
    };

    const getHeader = (tab: Tab): string => {
        switch (tab) {
            case Tab.LOGIN:
                return 'Авторизация';
            case Tab.REG:
                return 'Регистрация';
            case Tab.CODE:
                return 'Регистрация';
            case Tab.PROFILE:
                return 'Последний штрих';
            case Tab.RESET:
                return 'Восстановить доступ';
            case Tab.RESET_CODE:
                return 'Восстановить доступ';
            case Tab.CHANGE_PASSWORD:
                return 'Смена пароля';

            default:
                return 'Авторизация';
        }
    };

    const { isMobile } = config;

    return (
        <div className="modal_container">
            {tab === Tab.LOGIN && (
                <div className={'form form_login' + (!isMobile ? ' paddingTop' : '')}>
                    {/* {userData.called && !userData.loading && !userData.data && (
                        <Div>
                            <FormStatus mode="error" header="Ошибка входа">
                                {userData.error?.message}
                            </FormStatus>
                        </Div>
                    )} */}
                    <Input
                        before={<Icon24PhoneOutline />}
                        name="logInpTel"
                        status={inputStatus.logInpTel ? inputStatus.logInpTel : 'error'}
                        descr={
                            inputStatus.logInpTel?.description
                                ? inputStatus.logInpTel?.description
                                : 'Номер телефона не соответствует ни одному аккаунту. Повторите попытку или Создайте аккаунт'
                        }
                        showErrors={showErrors}
                        onChange={(e) => {
                            const { value, name } = e.currentTarget;

                            if (String(value)[14] != '_' && String(value)[14] != undefined) {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'right',
                                        description: 'Телефон введён корректно',
                                    },
                                }));
                            } else {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'error',
                                        description:
                                            'Номер телефона не соответствует ни одному аккаунту. Повторите попытку или Создайте аккаунт',
                                    },
                                }));
                            }

                            onChange((prev: FormData) => ({ ...prev, phone: '+7' + value }));
                        }}
                        placeholder="Телефон"
                        type="tel"
                    />
                    <Input
                        before={<Icon24LockOutline />}
                        status={inputStatus.logInpPass ? inputStatus.logInpPass : 'error'}
                        showErrors={showErrors}
                        descr={
                            inputStatus.logInpPass?.description
                                ? inputStatus.logInpPass?.description
                                : 'Пароль, который вы указали, не соответствует ни одному аккаунту.'
                        }
                        onChange={(e) => {
                            const { value, name } = e.currentTarget;

                            if (value.length >= 5) {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'right',
                                        description:
                                            'Пароль, который вы указали, не соответствует ни одному аккаунту.',
                                    },
                                }));
                            } else {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'error',
                                        description:
                                            'Пароль, который вы указали, не соответствует ни одному аккаунту.',
                                    },
                                }));
                            }

                            onChange((prev: FormData) => ({ ...prev, password: value }));
                        }}
                        placeholder="Пароль"
                        type="password"
                        name="logInpPass"
                    />
                    <Button
                        error={userData.called && !userData.loading && !userData.data}
                        style={{ marginTop: 8 }}
                        stretched
                        width="full"
                        mode="secondary"
                        onClick={() => {
                            let phone = form.phone.replace(/[^0-9]/g, '');
                            setShowErrors(true);
                            let phoneRight = inputStatus.logInpTel?.status == 'right'
                            let passRight = inputStatus.logInpPass?.status == 'right'
                            if(String(form.phone)[14] != '_' && String(form.phone)[14] != undefined){
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    logInpTel: {
                                        status: 'right',
                                        description: '',
                                    },
                                    logInpPass: {
                                        status: 'right',
                                        description: '',
                                    },
                                }));
                                phoneRight = passRight = true
                            }
                            if (phoneRight && passRight) {
                                auth({
                                    variables: {
                                        phone: phone.replace(/[^\d]/g, ''),
                                        password: form.password,
                                    },
                                });
                            }
                        }}>
                        {userData.loading || tokenData.loading ? <Spinner size="small" /> : 'Войти'}
                    </Button>
                    <div
                        style={{
                            display: 'flex',
                            marginBottom: 8,
                            flexDirection: 'column',
                            alignItems: 'center',
                            width: '100%',
                        }}>
                        <Button
                            width="full"
                            mode="tetrairy"
                            stretched
                            onClick={() => setTab(Tab.RESET)}>
                            Забыли пароль?
                        </Button>
                        <Button
                            width="full"
                            mode="commerce"
                            stretched
                            onClick={() => setTab(Tab.REG)}>
                            Создать аккаунт
                        </Button>
                    </div>
                    <div className="links-container">
                    </div>
                </div>
            )}

            {tab === Tab.REG && (
                <div className={'form form_reg' + (!isMobile ? ' paddingTop' : '')}>
                    <div
                        style={{
                            textAlign: 'center',
                            marginBottom: 2,
                            fontWeight: 500,
                            marginTop: 4,
                        }}>
                        Контактный телефон
                    </div>
                    <div
                        style={{
                            textAlign: 'center',
                            marginBottom: 8,
                            fontWeight: 400,
                            marginTop: 2,
                            fontSize: 15,
                        }}>
                        Ваш номер телефона будет
                        <br /> использоваться для входа
                    </div>

                    <Input
                        before={<Icon24PhoneOutline />}
                        descr={
                            inputStatus.regInp?.description
                                ? inputStatus.regInp?.description
                                : 'Укажите действительный номер телефона'
                        }
                        onChange={(e) => {
                            const { value, name } = e.currentTarget;
                            if (String(value)[14] != '_' && String(value)[14] != undefined) {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'right',
                                        description: 'Телефон введён корректно',
                                    },
                                }));
                            } else {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'error',
                                        description: 'Укажите действительный номер телефона',
                                    },
                                }));
                            }
                            onChange((prev: FormData) => ({ ...prev, phone: '+7' + value }));
                        }}
                        placeholder="Телефон"
                        type="tel"
                        name="regInp"
                        status={inputStatus.regInp ? inputStatus.regInp : 'error'}
                        showErrors={showErrors}
                    />

                    <Button
                        style={{ marginTop: 8, marginBottom: 8 }}
                        stretched
                        width="full"
                        mode="commerce"
                        onClick={() => {
                            setShowErrors(true);

                            if (inputStatus.regInp?.status == 'right') {
                                verificationUser({
                                    variables: {
                                        phone: form.phone.replace(/[^\d]/g, '')
                                    }
                                }).then(() => {
                                    setTab(Tab.CODE);
                                }).catch((e:any) => {
                                    console.error(e.message)
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        ['regInp']:{
                                            status: 'error',
                                            description: e.message,
                                        }
                                    }))
                                })
                            }
                        }}>
                        {verifiedUser.loading ? <Spinner size="small" /> : 'Получить код'}
                    </Button>
                    <div className="links-container links-politics">
                        <div style={{ textAlign: 'center', fontSize: '13px' }}>
                            Нажимая кнопку "Получить код", вы принимаете{' '}
                            <div>
                                <Link className="link-orange">Условия использования сервиса Allabay</Link>, <Link className="link-orange">Политику в
                                отношении организации обработки и обеспечения безопасности
                                персональных данных</Link> и <Link className="link-orange">Политику в отношении файлов cookie</Link>.
                            </div>
                        </div>
                    </div>
                    <div className="links-container">
                    </div>
                    <div onClick={() => setTab(Tab.LOGIN)} className="form_back_arrow">
                        <img src={arrowBack} alt="Вернуться" title="Вернуться" />
                    </div>
                </div>
            )}

            {tab === Tab.CODE && (
                <div className={'form form_code' + (!isMobile ? ' paddingTop' : '')}>
                    <div
                        style={{
                            textAlign: 'center',
                            marginBottom: 8,
                            fontWeight: 400,
                            fontSize: 15,
                        }}>
                        <b
                            style={{
                                fontWeight: 500,
                            }}>
                            Звоним вам...
                        </b>
                        <div style={{ padding: '12px 0' }}>
                            <Link>{form.phone}</Link>
                        </div>
                        Введите последние четыре цифры <br />
                        номера, с которого поступит звонок
                    </div>
                    <Input
                        before={<Icon24LockOutline />}
                        placeholder="Последние 4 цифры"
                        descr={
                            inputStatus.logInpTel?.description
                                ? inputStatus.logInpTel?.description
                                : 'Проверьте последние 4 цифры номера с которого поступил звонок, и повторите попытку'
                        }
                        type="last-numbers"
                        name="codeInp"
                        status={inputStatus.codeInp ? inputStatus.codeInp : 'error'}
                        showErrors={showErrors}
                        onChange={(e) => {
                            const { value, name } = e.currentTarget;

                            if (String(value)[4] != '_' && String(value)[4] != undefined) {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'right',
                                        description: 'Код введён корректно',
                                    },
                                }));
                            } else {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'error',
                                        description:
                                            'Проверьте последние 4 цифры номера с которого поступил звонок, и повторите попытку',
                                    },
                                }));
                            }
                        }}
                    />
                    <Button
                        style={{ marginTop: 8, marginBottom: 8 }}
                        stretched
                        width="full"
                        mode="commerce"
                        onClick={() => {
                            setShowErrors(true);

                            if (inputStatus.codeInp?.status == 'right') {
                                setTab(Tab.PROFILE);
                            }
                        }}>
                        Подтвердить номер
                    </Button>
                    <div className="links-container">
                    </div>
                    <div onClick={() => setTab(Tab.LOGIN)} className="form_back_arrow">
                        <img src={arrowBack} alt="Вернуться" title="Вернуться" />
                    </div>
                </div>
            )}

            {tab === Tab.PROFILE && (
                <div className={'form form_profile' + (!isMobile ? ' paddingTop' : '')}>
                    <div
                        style={{
                            marginBottom: 8,
                            fontWeight: 400,
                            textAlign: 'center',
                            color: '#707070',
                            fontSize: 14,
                        }}>
                        Быстро и легко.
                    </div>
                    <Input
                        placeholder={'Адрес страницы'}
                        name={'profilePageAddress'}
                        descr={
                            inputStatus.profilePageAddress?.description
                                ? inputStatus.profilePageAddress?.description
                                : 'Какой будет адрес ваше страницы?'
                        }
                        status={inputStatus.profilePageAddress ? inputStatus.profilePageAddress.status : 'error'}
                        onChange={e => {
                            const { value, name } = e.currentTarget;
                            onChange((prev: FormData) => ({ ...prev, pageAddress: value }))
                            if (/^[A-Za-z]+$/.test(value)) {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'right',
                                        description: 'Корректный адрес страницы',
                                    },
                                }));
                            } else {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'error',
                                        description: 'Какой будет адрес ваше страницы?',
                                    },
                                }));
                            }
                        }}/>
                    <div style={{ display: 'flex', width: '100%' }}>
                        <div>
                            <Input
                                placeholder="Имя"
                                name="profileName"
                                descr={
                                    inputStatus.logInpTel?.description
                                        ? inputStatus.logInpTel?.description
                                        : 'Как вас зовут?'
                                }
                                status={inputStatus.profileName ? inputStatus.profileName : 'error'}
                                showErrors={showErrors}
                                onChange={(e) => {
                                    const { value, name } = e.currentTarget;
                                    onChange((prev: FormData) => ({ ...prev, firstName: value }))
                                    if (/^[А-ЯЁа-яёA-Za-z]+$/.test(value)) {
                                        setInputStatus((prev: formDataStatus) => ({
                                            ...prev,
                                            [name]: {
                                                status: 'right',
                                                description: 'Имя введено корректно',
                                            },
                                        }));
                                    } else {
                                        setInputStatus((prev: formDataStatus) => ({
                                            ...prev,
                                            [name]: {
                                                status: 'error',
                                                description: 'Как вас зовут?',
                                            },
                                        }));
                                    }
                                }}
                                type="text"
                            />
                        </div>
                        <div style={{ marginLeft: 8 }}>
                            <Input
                                placeholder="Фамилия"
                                descr={
                                    inputStatus.logInpTel?.description
                                        ? inputStatus.logInpTel?.description
                                        : 'Какая у Вас фамилия?'
                                }
                                name="profileInpSurname"
                                status={
                                    inputStatus.profileInpSurname
                                        ? inputStatus.profileInpSurname
                                        : 'helo'
                                }
                                showErrors={showErrors}
                                onChange={(e) => {
                                    const { value, name } = e.currentTarget;
                                    onChange((prev: FormData) => ({ ...prev, lastName: value }))
                                    if (/^[А-ЯЁа-яёA-Za-z]+$/.test(value)) {
                                        setInputStatus((prev: formDataStatus) => ({
                                            ...prev,
                                            [name]: {
                                                status: 'right',
                                                description: 'Фамилия введена корректно',
                                            },
                                        }));
                                    } else {
                                        setInputStatus((prev: formDataStatus) => ({
                                            ...prev,
                                            [name]: {
                                                status: 'error',
                                                description: 'Фамилия введена некорректно',
                                            },
                                        }));
                                    }
                                }}
                                type="text"
                            />
                        </div>
                    </div>
                    <Input
                        placeholder="Пароль"
                        name="profileInpPass"
                        descr={
                            inputStatus.profileInpPass?.description
                                ? inputStatus.profileInpPass?.description
                                : errMsgs[errMsgIdx]
                        }
                        status={inputStatus.profileInpPass ? inputStatus.profileInpPass : 'error'}
                        showErrors={showErrors}
                        onChange={(e) => {
                            const { value, name } = e.currentTarget;
                            onChange((prev: FormData) => ({ ...prev, password: value }))
                            if(value && value.length >= 5){
                                // if (/[A-z]/g.test(value + '') || /[0-9]/g.test(value + '')) {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'right',
                                            description: 'Пароль введён корректно',
                                        },
                                    }));
                                // } else {
                                //     setInputStatus((prev: formDataStatus) => ({
                                //         ...prev,
                                //         [name]: {
                                //             status: 'error',
                                //             description: errMsgs[errMsgIdx],
                                //         },
                                //     }));
                                // }
                            } else {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'error',
                                        description: errMsgs[errMsgIdx],
                                    },
                                }));
                            }

                            onChange((prev: FormData) => ({ ...prev, password: value }));
                        }}
                        type="password"
                    />

                    <div
                        style={{
                            fontWeight: 500,
                            fontSize: 13,
                            padding: '4px 8px 2px',
                        }}>
                        Дата рождения
                    </div>
                    <Input
                        placeholder="Дата"
                        descr={
                            inputStatus.profileInpDate?.description
                                ? inputStatus.profileInpDate?.description
                                : 'Похоже, вы ввели неверную информацию. Убедитесь, что вы используете настоящую дату своего рождения'
                        }
                        name="profileInpDate"
                        status={inputStatus.profileInpDate ? inputStatus.profileInpDate : 'error'}
                        showErrors={showErrors}
                        onChange={(e) => {
                            const { name, value } = e.currentTarget;
                            onChange((prev: FormData) => ({ ...prev, birthday: value }))
                            if (value && value[0] != 0) {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'right',
                                        description: 'Дата введёна корректно',
                                    },
                                }));
                            } else {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'error',
                                        description:
                                            'Похоже, вы ввели неверную информацию. Убедитесь, что вы используете настоящую дату своего рождения',
                                    },
                                }));
                            }
                        }}
                        type="date"
                    />
                    <div className="sex-container">
                        <div
                            style={{
                                fontWeight: 500,
                                fontSize: 13,
                                padding: '4px 8px 2px',
                                whiteSpace: 'nowrap',
                            }}>
                            Ваш пол:
                        </div>
                        <div
                            className="radio-container"
                            style={{
                                display: 'flex',
                                width: '100%',
                                padding: '0 8px',
                                marginTop: 3,
                                alignItems: 'center',
                            }}>
                            <div>
                                <Radio
                                    onChange={(e) => {
                                        const { name } = e.currentTarget;
                                        onChange((prev: FormData) => ({ ...prev, gender: "male" }))
                                        setInputStatus((prev: formDataStatus) => ({
                                            ...prev,
                                            [name]: {
                                                status: 'right',
                                                description: 'Пол введён корректно',
                                            },
                                        }));
                                    }}
                                    className={
                                        showErrors
                                            ? inputStatus.profileInpSex?.status
                                                ? inputStatus.profileInpSex?.status
                                                : 'error'
                                            : ''
                                    }
                                    name="profileInpSex">
                                    Мужской
                                </Radio>
                            </div>
                            <div style={{ marginLeft: 8 }}>
                                <Radio
                                    onChange={(e) => {
                                        const { name, value } = e.currentTarget;
                                        onChange((prev: FormData) => ({ ...prev, gender: "female" }))
                                        setInputStatus((prev: formDataStatus) => ({
                                            ...prev,
                                            [name]: {
                                                status: 'right',
                                                description: 'Пол введён корректно',
                                            },
                                        }));
                                    }}
                                    className={
                                        showErrors
                                            ? inputStatus.profileInpSex?.status
                                                ? inputStatus.profileInpSex?.status
                                                : 'error'
                                            : ''
                                    }
                                    name="profileInpSex">
                                    Женский
                                </Radio>
                            </div>
                        </div>
                    </div>
                    <div style={{ marginTop: 8, marginBottom: 8 }}>
                        <Button
                            width="full"
                            onClick={() => {
                                setShowErrors(true);
                                setErrMsgIdx(errMsgIdx + 1 >= errMsgs.length ? 0 : errMsgIdx + 1);
                                if (
                                    inputStatus.profileName?.status == 'right' &&
                                    inputStatus.profileInpPass?.status == 'right' &&
                                    inputStatus.profileInpDate?.status == 'right' &&
                                    inputStatus.profileInpSex?.status == 'right'
                                ) {
                                    createUser({
                                        variables: {
                                            firstName: form.firstName,
                                            lastName: form.lastName,
                                            password: form.password,
                                            gender: form.gender,
                                            birthday: form.birthday,
                                            phone: form.phone.replace(/[^\d]/g, '')
                                        }
                                    }).then(() => {
                                        setTab(Tab.PROFILE);
                                    })
                                    // alert('Пользователь зарегистрирован');
                                } else {
                                    if(inputStatus.profileInpPass?.status != 'right'){
                                        setInputStatus((prev: formDataStatus) => ({
                                            ...prev,
                                            profileInpPass: {
                                                status: 'error',
                                                description: errMsgs[errMsgIdx],
                                            },
                                        }));
                                    }
                                }
                            }}
                            stretched
                            mode="commerce">
                            {createdUserData.loading ? <Spinner size="small" /> : 'Регистрация'}
                        </Button>
                    </div>
                    <div className="links-container">
                    </div>
                </div>
            )}

            {tab === Tab.RESET && (
                <div className={'form form_reset' + (!isMobile ? ' paddingTop' : '')}>
                    <div
                        style={{
                            textAlign: 'center',
                            marginBottom: 8,
                            fontWeight: 400,
                            marginTop: 16,
                        }}>
                        Восстановить досуп по номеру телефона
                    </div>

                    <Input
                        before={<Icon24PhoneOutline />}
                        descr={
                            inputStatus.resetInp?.description
                                ? inputStatus.resetInp?.description
                                : 'Укажите действительный номер телефона'
                        }
                        onChange={(e) => {
                            const { value, name } = e.currentTarget;

                            if (String(value)[14] != '_' && String(value)[14] != undefined) {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'right',
                                        description: 'Укажите действительный номер телефона',
                                    },
                                }));
                            } else {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'error',
                                        description: 'Укажите действительный номер телефона',
                                    },
                                }));
                            }
                            onChange((prev: FormData) => ({ ...prev, phone: '+7' + value }));
                        }}
                        status={inputStatus.resetInp ? inputStatus.resetInp : 'error'}
                        showErrors={showErrors}
                        placeholder="Номер телефона"
                        type="tel"
                        name="resetInp"
                    />

                    <Button
                        style={{ marginTop: 8 }}
                        stretched
                        width="full"
                        mode="commerce"
                        onClick={() => {
                            setShowErrors(true);

                            if (inputStatus.resetInp?.status == 'right') {
                                verificationUser({
                                    variables: {
                                        phone: form.phone.replace(/[^\d]/g, '')
                                    }
                                }).then(() => {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        ['resetInp']:{
                                            status: 'error',
                                            description: 'Пользователь не зарегестрирован',
                                        }
                                    }))
                                }).catch((e) => {
                                    setTab(Tab.RESET_CODE);
                                })
                            }
                        }}>
                        {verifiedUser.loading ? <Spinner size="small" /> : 'Получить код'}
                    </Button>
                    <div className="links-container links-politics">
                        <p style={{ textAlign: 'center', fontSize: '13px' }}>
                            Нажимая кнопку "Получить код", вы принимаете{' '}
                            <a className="link-orange">условия использования сервиса Allabay</a>
                        </p>
                    </div>
                    <div className="links-container">
                    </div>
                    <div onClick={() => setTab(Tab.LOGIN)} className="form_back_arrow">
                        <img src={arrowBack} alt="Вернуться" title="Вернуться" />
                    </div>
                </div>
            )}

            {tab === Tab.RESET_CODE && (
                <div className={'form form_reset-code' + (!isMobile ? ' paddingTop' : '')}>
                    <div
                        style={{
                            textAlign: 'center',
                            marginBottom: 8,
                            fontWeight: 500,
                            fontSize: 15,
                        }}>
                        Звоним вам...
                    </div>
                    <div
                        style={{
                            textAlign: 'center',
                            marginBottom: 8,
                            fontWeight: 400,
                            fontSize: 14,
                        }}>
                        Введите последние четыре цифры номера,
                        <br /> с которого поступит звонок на номер
                        <div style={{ padding: '12px 0' }}>
                            <Link>{form.phone}</Link>
                        </div>
                    </div>
                    <Input
                        before={<Icon24LockOutline />}
                        placeholder="Последние 4 цифры"
                        descr={
                            inputStatus.logInpTel?.description
                                ? inputStatus.logInpTel?.description
                                : 'Проверьте последние 4 цифры номера с которого поступил звонок, и повторите попытку'
                        }
                        type="last-numbers"
                        name="resetCodeInp"
                        status={inputStatus.resetCodeInp ? inputStatus.resetCodeInp : 'error'}
                        showErrors={showErrors}
                        onChange={(e) => {
                            const { value, name } = e.currentTarget;

                            console.log(inputStatus.resetCodeInp);

                            if (String(value)[4] != '_' && String(value)[4] != undefined) {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'right',
                                        description:
                                            'Проверьте последние 4 цифры номера с которого поступил звонок, и повторите попытку',
                                    },
                                }));
                            } else {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'error',
                                        description:
                                            'Проверьте последние 4 цифры номера с которого поступил звонок, и повторите попытку',
                                    },
                                }));
                            }
                        }}
                    />
                    <Button
                        style={{ marginTop: 8, marginBottom: 8 }}
                        stretched
                        width="full"
                        mode="commerce"
                        onClick={() => {
                            setShowErrors(true);

                            if (inputStatus.resetCodeInp?.status == 'right') {
                                setTab(Tab.CHANGE_PASSWORD);
                            }
                        }}>
                        Восстановить досуп
                    </Button>
                    <div className="links-container">
                    </div>
                    <div onClick={() => setTab(Tab.LOGIN)} className="form_back_arrow">
                        <img src={arrowBack} alt="Вернуться" title="Вернуться" />
                    </div>
                </div>
            )}

            {tab === Tab.CHANGE_PASSWORD && (
                <div className={'form form_change-password' + (!isMobile ? ' paddingTop' : '')}>
                    <Input
                        before={<Icon24LockOutline />}
                        descr={
                            inputStatus.changePassNew?.description
                                ? inputStatus.changePassNew?.description
                                : 'Минимальное количество символов в пароле: 5. Пожалуйста, введите другой пароль'
                        }
                        placeholder="Новый пароль"
                        name="changePassNew"
                        type="password"
                        status={inputStatus.changePassNew ? inputStatus.changePassNew : 'error'}
                        showErrors={showErrors}
                        onChange={(e) => {
                            const { value, name } = e.currentTarget;

                            if(value && value.length >= 5){
                                // if (/[A-z]/g.test(value + '') || /[0-9]/g.test(value + '')) {
                                    setInputStatus((prev: formDataStatus) => ({
                                        ...prev,
                                        [name]: {
                                            status: 'right',
                                            description: 'Пароль введён корректно',
                                        },
                                    }));
                                // } else {
                                //     setInputStatus((prev: formDataStatus) => ({
                                //         ...prev,
                                //         [name]: {
                                //             status: 'error',
                                //             description: errMsgs[errMsgIdx],
                                //         },
                                //     }));
                                // }
                            } else {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'error',
                                        description: errMsgs[errMsgIdx],
                                    },
                                }));
                            }

                            onChange((prev: FormData) => ({ ...prev, password: value }));
                        }}
                    />
                    <Input
                        before={<Icon24LockOutline />}
                        placeholder="Повторите новый пароль"
                        descr={
                            inputStatus.changePassCheck?.description
                                ? inputStatus.changePassCheck?.description
                                : 'Пароли не совпадают, повторите попытку'
                        }
                        name="changePassCheck"
                        status={inputStatus.changePassCheck ? inputStatus.changePassCheck : 'error'}
                        showErrors={showErrors}
                        onChange={(e) => {
                            const { value, name } = e.currentTarget;

                            if (value == form.password) {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'right',
                                        description: 'Пароль введён корректно',
                                    },
                                }));
                            } else {
                                setInputStatus((prev: formDataStatus) => ({
                                    ...prev,
                                    [name]: {
                                        status: 'error',
                                        description: 'Пароли не совпадают, повторите попытку',
                                    },
                                }));
                            }
                        }}
                        type="password"
                    />
                    <Button
                        style={{ marginTop: 8, marginBottom: 8 }}
                        stretched
                        width="full"
                        mode="commerce"
                        onClick={() => {
                            setShowErrors(true);
                            setErrMsgIdx(errMsgIdx + 1 >= errMsgs.length ? 0 : errMsgIdx + 1);
                            if (
                                inputStatus.changePassCheck?.status == 'right' &&
                                inputStatus.changePassNew?.status == 'right'
                            ) {
                                // setTab(Tab.CHANGE_PASSWORD);
                                alert('Пароль сменён');
                            }
                        }}>
                        Сохранить
                    </Button>
                    <div className="links-container">
                    </div>
                    <div onClick={() => setTab(Tab.LOGIN)} className="form_back_arrow">
                        <img src={arrowBack} alt="Вернуться" title="Вернуться" />
                    </div>
                </div>
            )}
        </div>
    );
});

export enum Tab {
    LOGIN,
    REG,
    CODE,
    PROFILE,
    RESET,
    RESET_CODE,
    CHANGE_PASSWORD,
}

export const AUTH = gql`
    query auth($phone: String!, $password: String!) {
        tokenAuth(phone: $phone, password: $password) {
            token
        }
    }
`;

export const ME = gql`
    query Me {
        me {
            id
            phone
            photo
            birthday
            firstName
            lastName
            isStaff
            gender
            isOperator
        }
    }
`;

export const CREATE_USER = gql`
    mutation CreateUser($firstName: String!, $lastName: String!, $phone: String!, $birthday: Date!, $gender: String!, $password: String!) {
        updateInformationUser(input:{
            firstName: $firstName,
            lastName: $lastName,
            phone: $phone,
            birthday: $birthday,
            gender: $gender ,
            password: $password
        }) {
            user {
                phone, 
                birthday
            }
        }
    }
`;

export default UnauthorizedModal;
