import { FormLayoutGroup, FormItem, FormLayout, Button, Input, Checkbox } from "@vkontakte/vkui";
import React, { useState } from "react";

const ProfileAddress = ({ address }) => {
    const [form, onChange] = useState(address ? address : {});

    
    function onChangeForm(e) {
        const { name, value } = e.target;
        onChange({ [name]: value });
    }


    return(
        <div>
            <FormLayout>
                <FormItem top="Ваша страна">
                    <Input />
                </FormItem>

                <FormItem top="Город">
                    <Input />
                </FormItem>

                <FormItem top="Улица дом номер квартиры ">
                    <Input />
                </FormItem>


                <FormItem top="Индекс">
                    <Input />
                </FormItem>

                <FormItem>
                    <Checkbox>
                        Использовать адресс по умолчанию
                    </Checkbox>
                </FormItem>


                <FormItem>
                    <Button size="m" stretched>Сохранить</Button>
                </FormItem>
            </FormLayout>
        </div>
    );
}

export default ProfileAddress;