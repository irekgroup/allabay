import React, { FC, useContext, useEffect, useState } from 'react';
import './ModalFastResponse.scss';
import cool from '../../img/feed/cool.png';
import cry from '../../img/feed/cry.png';
import heart from '../../img/feed/heart.png';
import notCool from '../../img/feed/notcool.png';
import wrongCost from '../../img/feed/wrongCost.png';
import phh from '../../img/feed/phh.png';
import send from '../../img/feed/send.svg';
import { Radio, RadioGroup, Select, withModalRootContext } from "@vkontakte/vkui";
import { RootStoreContext } from "../../index";
import { Icon28ChevronBack, Icon24Search } from "@vkontakte/icons";
import Input from "../../components/Input/Input";
import { useRouter } from "@happysanta/router";
import { PAGE_FEED_MOBILE, POPOUT_THANKS } from "../../router";

enum Tabs {
    MAIN = 'MAIN',
    COOL = 'COOL',
    NOTCOOL = 'NOTCOOL',
    WRONGCOST = 'WRONGCOST',
    PHH = 'PHH',
    CRY = 'CRY',
    HEART = 'HEART',
}

interface ModalFastResponseProps {
    updateModalHeight: () => void;
}

const ModalFastResponse: FC<ModalFastResponseProps> = ({ updateModalHeight }) => {
    const [tab, setTab] = useState<Tabs>(Tabs.MAIN)
    const { config } = useContext(RootStoreContext)
    const router = useRouter()
    useEffect(() => {
        config.changeModelHeader('Быстрая реакция')
    }, [])
    const setNewTab = (t: Tabs) => {
        setTab(t)
        if (t === Tabs.MAIN) {
            config.changeModelHeader('Быстрая реакция')
        } else {
            config.changeModelHeader('')
        }
        updateModalHeight()
    }
    const thanks = () => {
        router.pushPage(PAGE_FEED_MOBILE)
        router.pushPopup(POPOUT_THANKS)
    }

    return (
        <div className={'modalFastResponse'}>
            {tab !== Tabs.MAIN
            && <Icon28ChevronBack
                style={{ position: 'absolute', left: '20px', top: '0', cursor: 'pointer', zIndex: 2 }}
                onClick={() => setNewTab(Tabs.MAIN)}
            />}
            {
                tab === Tabs.MAIN
                && <div className={'p_list'}>
                    <p onClick={() => setNewTab(Tabs.COOL)}><img src={cool} alt="👍" width={20}/>Отлично! Это то, что я
                        ищу
                    </p>
                    <p onClick={() => setNewTab(Tabs.NOTCOOL)}><img src={notCool} alt="👎" width={20}/>Это не то, что я
                        ищу
                    </p>
                    <p onClick={() => setNewTab(Tabs.WRONGCOST)}><img src={wrongCost} alt="😮" width={20}/>Не подходит цена
                    </p>
                    <p onClick={() => setNewTab(Tabs.PHH)}><img src={phh} alt="😤" width={20}/>Не подходит срок доставки
                    </p>
                    <p onClick={() => setNewTab(Tabs.CRY)}><img src={cry} alt="😭" width={20}/>Есть сомнения</p>
                    <p onClick={() => setNewTab(Tabs.HEART)}><img src={heart} alt="♥" width={20}/>Спасибо, зайду позже
                    </p>
                </div>
            }
            {tab === Tabs.COOL
            && <div className={'tab cool'}>
                <img src={cool} alt="👍" height={40}/>
                <h1>Спасибо! Ваш ответ поможет нам стать лучше</h1>
                <p>Оцените вероятность того, что вы станете рекомендовать нас друзьям или коллегам?
                    (9-10 стану рекомендовать, 7-8 возможно буду рекомендовать, 1-6 не стану рекомендовать)</p>
                <RadioGroup mode={'horizontal'}>
                    {
                        new Array(10).fill(0).map((_, i) => (
                            <Radio key={i} name={'num'} value={i} onClick={thanks}>{i+1}</Radio>
                        ))
                    }
                </RadioGroup>
            </div>}
            {tab === Tabs.NOTCOOL
            && <div className={'tab notcool'}>
                <img src={notCool} alt="👎" height={40}/>
                <h1>Спасибо! Ваш ответ поможет нам стать лучше</h1>
                <p>Мы внимательно анализируем все реакции. По итогам анализа устраняем проблемы и улучшаем наши
                    услуги.</p>
                <p>Почему это не то, что Вы ищите?</p>
                <div className={'inputWithSendBtn'}>
                    <Input before={<Icon24Search fill={'#707070'}/>}
                           placeholder={'Это не то, что я ищу, потому что...'}/>
                    <img src={send} alt="send" onClick={thanks}/>
                </div>
            </div>}
            {tab === Tabs.WRONGCOST
            && <div className={'tab notcool wrongCost'}>
                <img src={wrongCost} alt="😮" height={40}/>
                <h1>Спасибо! Ваш ответ поможет нам стать лучше</h1>
                <p>Мы внимательно анализируем все реакции.
                    По итогам анализа мы можем предложить лучшую цену.</p>
                <p>На какую цену вы рассчитываете?</p>
                <div className={'inputWithSendBtn'}>
                    <Input before={<Icon24Search fill={'#707070'}/>} placeholder={'Введите сумму'}/>
                    <img src={send} alt="send" onClick={thanks}/>
                </div>
            </div>}
            {tab === Tabs.PHH
            && <div className={'tab notcool phh'}>
                <img src={phh} alt="😤" height={40}/>
                <h1>Спасибо! Ваш ответ поможет нам стать лучше</h1>
                <p>В некоторых случаях в зависимости от метоположения пользователя мы можем
                    предложить более короткие сроки доставки.</p>
                <p> Какой срок доставки для вас будет оптимальным?</p>
                <div className={'inputWithSendBtn'}>
                    <Input before={<Icon24Search fill={'#707070'}/>} placeholder={'Введите срок, например, 5-7 дней'}/>
                    <img src={send} alt="send" onClick={thanks}/>
                </div>
            </div>}
            {tab === Tabs.CRY
            && <div className={'tab notcool cry'}>
                <img src={cry} alt="😭" height={40}/>
                <h1>Спасибо! Ваш ответ поможет нам стать лучше</h1>
                <p style={{ fontWeight: 400 }}>Вы вправе воспользоваться функцией возврата товара, срок возврата товара
                    надлежащего качества
                    составляет 21 день с момента получения товара, если иное не было оговорено при продаже товара.
                    <span style={{ textDecoration: 'underline' }}>Подробнее о правилах и способах возврата</span>
                </p>
                <div className={'inputWithSendBtn'}>
                    <Icon24Search fill={'#707070'}/>
                    <img src={send} alt="send" onClick={thanks}/>
                    <Select
                        placeholder={'Выберите причину'}
                        options={[
                            { value: 1, label: 'Есть сомненя по качеству' },
                            { value: 2, label: 'Есть сомнения что товар не придет' },
                            { value: 3, label: 'Есть сомнения что придет не то, что на фото' },
                        ]}/>
                </div>
            </div>}
            {tab === Tabs.HEART
            && <div className={'tab'}>
                <img src={heart} alt="♥" height={40}/>
                <h1>Спасибо! Ваш ответ поможет нам стать лучше</h1>
                <p>Оцените вероятность того, что вы станете рекомендовать нас друзьям или коллегам?
                    (9-10 стану рекомендовать, 7-8 возможно буду рекомендовать, 1-6 не стану рекомендовать)</p>
                <RadioGroup mode={'horizontal'}>
                    {
                        new Array(10).fill(0).map((_, i) => (
                            <Radio key={i} name={'num'} value={i} onClick={thanks}>{i+1}</Radio>
                        ))
                    }
                </RadioGroup>
            </div>}
        </div>
    );
};

export default withModalRootContext(ModalFastResponse);