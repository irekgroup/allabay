import { FormLayout, ModalPage, FormItem, Input, ModalPageHeader, Header, CardGrid, Card, Div, DatePicker, Radio, Textarea, ScreenSpinner, Button, PanelHeaderButton, SimpleCell, IconButton } from "@vkontakte/vkui"
import { Icon12Cancel, Icon28AddOutline, Icon28CancelOutline, Icon28ChevronLeftOutline, Icon28ChevronRightOutline, Icon28DeleteOutline, Icon28Send, Icon28ViewOutline } from "@vkontakte/icons"
import "./ModalCard.css";



const ModalCardCreate = ({ onClose, id, globState, setPopout, setGlobalState, sendWithSocket, setActiveModal }) => {
    const activeCard = globState.admin.activeCard;
    const card = globState.admin.cards[activeCard];
    const card_error = globState.admin.cards_errors[activeCard] ?  globState.admin.cards_errors[activeCard]: {};

    function onChangeCard(e) {
        const { name, value } = e.target;
        let { cards } = globState.admin;
        cards[globState.admin.activeCard][name] = value;
        setGlobalState({ admin: { ...globState.admin, cards }});
    }
    
    function addCard() {
        let { cards } = globState.admin;
        cards.push({});
        setActiveCard(cards.length-1)
    }

    function setActiveCard(i) {
        setGlobalState({ admin: { ...globState.admin, activeCard: i }});
        setActiveModal(null);
        setPopout(<ScreenSpinner />)
        setTimeout(() => {
            setPopout(null)
            setActiveModal("create_card")
        }, 500)  
    }

    function addImg() {
        if(!card.href) {
            let input = document.getElementById("input_file");
            input.click();
        } else {
            let imgs = card.imgs;
            if(!imgs)
                imgs = [];
            imgs.push({ type: "href", href: card.href });
            onChangeCard({ target: { name: "imgs", value: imgs }});
            onChangeCard({ target: { name: "href", value: "" }});
        }
    }

    function deleteCard() {
        let { cards } = globState.admin;
        cards.splice(activeCard, 1);
        setGlobalState({ admin: { ...globState.admin, cards }});
        setActiveCard((activeCard == 0 ? 0 : activeCard-1));
    }

    function uploadImg(e) {
        let files = e.target.files;
        for(let i = 0; i < files.length; i++) {
            let reader = new FileReader();
            reader.readAsDataURL(files[i]);
            reader.onload = function() {
                let imgs = card.imgs;
                if(!imgs)
                    imgs = [];
                imgs.push({ type: "blob", blob: reader.result, name: files[i].name });
                onChangeCard({ target: { name: "imgs", value: imgs }});
            }
        }
    }

    function deleteImg(i) {
        let imgs = card.imgs;
        imgs.splice(i, 1);
        onChangeCard({ target: { name: "imgs", value: imgs }});
    }

    function sendCards() {
        setPopout(<ScreenSpinner />)
        sendWithSocket({ type: "cards", cards: globState.admin.cards });
    }

    return(
    <div>
        <input multiple accept="image/*" id="input_file" onChange={uploadImg} type="file" style={{ display: "none" }} />
        <FormLayout>
            {(globState.admin.cards.length > 1) ? 
                <SimpleCell 
                    after={<IconButton disabled={activeCard == globState.admin.cards.length-1} onClick={() => setActiveCard(activeCard+1)}>
                            <Icon28ChevronRightOutline fill="var(--accent)" />
                        </IconButton>} 
                    before={
                        <IconButton disabled={activeCard == 0} onClick={() => setActiveCard(activeCard-1)}>
                            <Icon28ChevronLeftOutline fill="var(--accent)" />
                        </IconButton>} 
                    disabled 
                    className="card_nav"
                >
                    Карточка {activeCard+1}
                </SimpleCell>: null}
            <FormItem top="Ссылка на поставщика" bottom={card_error.link_provider && card_error.link_provider} status={card_error.link_provider ? "error" : "default"}>
                <Input value={card.link_provider} onChange={onChangeCard} name="link_provider" />
            </FormItem>
            <FormItem top="Город" bottom={card_error.city && card_error.city} status={card_error.city ? "error" : "default"}>
                <Input value={card.city} onChange={onChangeCard} name="city" />
            </FormItem>
            <FormItem top="Номер телефона" bottom={card_error.number && card_error.number} status={card_error.number ? "error" : "default"}>
                <Input value={card.number} onChange={onChangeCard} name="number" />
            </FormItem>

            <FormItem top="Артикул" bottom={card_error.vendor_code && card_error.vendor_code} status={card_error.vendor_code ? "error" : "default"}>
                <Input value={card.vendor_code} onChange={onChangeCard} name="vendor_code"/>
            </FormItem>

            <Header>Сведения о товаре</Header>

            <FormItem top="Наименование" bottom={card_error.name && card_error.name} status={card_error.name ? "error" : "default"}>
                <Input value={card.name} onChange={onChangeCard} name="name" />
            </FormItem>

            <FormItem top="Бренд" bottom={card_error.brand && card_error.brand} status={card_error.brand ? "error" : "default"}>
                <Input value={card.brand} onChange={onChangeCard} name="brand" />
            </FormItem>

            <Header>Загрузка фото </Header>
            <FormItem>
                <Input value={card.href} name="href" onChange={onChangeCard} />
            </FormItem>
            <Div>
                <CardGrid>
                    {card.imgs && card.imgs.map((img, i) => (
                        <Card key={i}>
                            <button className="deleteButton" onClick={() => deleteImg(i)}><Icon12Cancel fill="#fff" /></button>
                            <Div>
                                <img src={img.type === "href" ? img.href : img.blob} className="imgCard" />
                            </Div>
                        </Card>
                    ))}
                </CardGrid>
                <Button before={<Icon28AddOutline fill="#fff"/>} align="center" onClick={addImg}>
                    Добавить
                </Button>
            </Div>

            <FormItem top="Цена" bottom={card_error.cost && card_error.cost} status={card_error.cost ? "error" : "default"}>
                <Input value={card.cost} onChange={onChangeCard} name="cost"/>
            </FormItem>

            <FormItem top="Сведения о доставке" bottom={card_error.date && card_error.date} status={card_error.date ? "error" : "default"}>
                <Input 
                    value={card.date}
                    name="date"
                    onChange={onChangeCard}
                    type="date"
                />
            </FormItem>

            <FormItem top="Гарантийный срок" bottom={card_error.guarantee_period && card_error.guarantee_period} status={card_error.guarantee_period ? "error" : "default"}>
                <Input type="date" value={card.guarantee_period} onChange={onChangeCard} name="guarantee_period"/>
            </FormItem>

            <FormItem top="Вид товара" bottom={card_error.view && card_error.view} status={card_error.view ? "error" : "default"}>
                <Input value={card.view} onChange={onChangeCard} name="view"/>
            </FormItem>

            <FormItem top="Тип товара" bottom={card_error.type && card_error.type} status={card_error.type ? "error" : "default"}>
                <Input value={card.type} onChange={onChangeCard} name="type" />
            </FormItem>

            <FormItem top="Состояние товара" bottom={card_error.state && card_error.state} status={card_error.state ? "error" : "default"}>
                <Radio checked={card.state === "б/у"} value="б/у" onChange={onChangeCard} name="state">б/у</Radio>
                <Radio checked={card.state === "новое"} onChange={onChangeCard} value="новое" name="state">новое</Radio>
            </FormItem>

            <FormItem top="Описание" bottom={card_error.description && card_error.description} status={card_error.description ? "error" : "default"}>
                <Textarea value={card.description} onChange={onChangeCard} name="description"/>
            </FormItem>
        </FormLayout>
        <Div className="cointenerButtons">
            <Button before={<Icon28ViewOutline />} size="l" onClick={() => setActiveModal("preview")} stretched>Предосмотр</Button>
            {globState.admin.cards.length > 1 && <Button size="l" style={{ marginTop: 10 }} before={<Icon28DeleteOutline />} stretched onClick={deleteCard}>Удалить карточку</Button>}
            <Button before={<Icon28AddOutline />} size="l" style={{ marginTop: 10 }} stretched onClick={addCard}>Добавить еще карточку</Button>
            <Button before={<Icon28Send />} size="l" style={{ marginTop: 10 }} onClick={sendCards} stretched>Отправить</Button>
        </Div>
    </div>
    )
}

export default ModalCardCreate;