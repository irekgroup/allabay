import React, { FC, useEffect, useState } from 'react';
import { Link, Radio, RadioGroup, withModalRootContext } from "@vkontakte/vkui";
import { useRouter } from "@happysanta/router";
import { PAGE_PRODUCT, POPOUT_THANKS } from "../../router";
import './ModalReveiwReport.scss';

interface ModalReviewProps {
    updateModalHeight: () => void;
}

const ModalReviewReport:FC<ModalReviewProps> = ({updateModalHeight}) => {
    const [tab, setTab] = useState(0)

    const router = useRouter()

    const thanks = () => {
        router.pushPage(PAGE_PRODUCT)
        router.pushPopup(POPOUT_THANKS)
    }

    useEffect(() => {
        updateModalHeight()
    }, [tab])

    return (
        <div className={'modalReviewReport'}>
            {tab === 0 &&
                <div className={'tab1'}>
                    <p onClick={() => setTab(1)}>Пожаловаться на отзыв</p>
                    <Link onClick={() => router.popPage()}>ЗАКРЫТЬ</Link>
                </div>
            }
            {tab === 1 &&
                <div className={'tab2'}>
                    <RadioGroup>
                        <Radio onChange={() => setTab(2)}>Отзыв содержит нецензурную, бранную лексику</Radio>
                        <Radio onChange={() => setTab(2)}>Заказной, фиктивный</Radio>
                        <Radio onChange={() => setTab(2)}>Спам</Radio>
                        <Radio onChange={() => setTab(2)}>Некорректное фото, видео</Radio>
                    </RadioGroup>
                </div>}
            {tab === 2 &&
                <div className={'tab3'}>
                    <p>Вы дейсвительно хотите отправить сообщение?</p>
                    <div className="links">
                        <Link onClick={thanks}>Да, отправить</Link>
                        <Link onClick={() => router.popPage()}>Отменить</Link>
                    </div>
                </div>}
        </div>
    );
};

export default withModalRootContext(ModalReviewReport);