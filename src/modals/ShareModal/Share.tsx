import React, { FC, useContext, useEffect, useState, useRef } from 'react';
import './Share.scss'
import { RootStoreContext } from "../../index";
import { observer } from "mobx-react";
import {
    Icon24LogoVkColor,
    Icon24LogoTwitter,
    Icon24LogoFacebook,
    Icon24Copy,
    Icon24MoreHorizontal
} from '@vkontakte/icons';
import tgLogo from '../../img/cardUser/tg.svg'
import messengerLogo from '../../img/cardUser/messenger.svg'
import Button from "../../components/Button/Button";
import { useRouter } from "@happysanta/router";
import { MODAL_CARDUSER } from "../../router";

const Share: FC = observer(() => {
    const rootStore = useContext(RootStoreContext);
    const { config } = rootStore;

    const router = useRouter()

    const [size, setSize] = useState(70)

    const desc = encodeURI('Привет, я использую allabay для поиска и передвижения. Присоединяйся!');
    const url = encodeURI('https://allabay.com');
    const img = encodeURI('https://allabay.com/favicon.ico')

    useEffect(() => {
        config.isMobile ? setSize(50) : setSize(70)
    }, [config.isMobile])

    useEffect(() => {
        const head = document.querySelector('head')
        if (head) {
            head.innerHTML += `      
      <meta property="og:type" content="website">
      <meta property="og:site_name" content="Allabay Мир Общей Победы">
      <meta property="og:title" content="Allabay Мир Общей Победы">
      <meta property="og:description" content="${desc}">
      <meta property="og:url" content="https://allabay.com">
      <meta property="og:locale" content="ru_RU">
      <meta property="og:image" content="https://allabay.com/favicon.ico">
      <meta property="og:image:width" content="968">
      <meta property="og:image:height" content="504">
      <meta name="twitter:card" content="summary_large_image">
      <meta name="twitter:title" content="Allabay Мир Общей Победы">
      <meta name="twitter:description" content="${desc}">
      <meta name="twitter:image:src" content="https://allabay.com/favicon.ico">
      <meta name="twitter:url" content="https://allabay.com">
      <meta name="twitter:domain" content="allabay.com">
      <meta name="twitter:site" content="@">
      <meta name="twitter:creator" content="@...">`
        }
    }, [])

    return (
        <div className={'sharePopup modal_container'}>
            <div className={config.isMobile ? 'rows marginTop' : 'rows'}>
                <div className="row">
                    <a href={`https://www.facebook.com/sharer/sharer.php?u=${url}&picture=${img}`} target="_blank"
                       className="item">
                        <Icon24LogoFacebook height={size} width={size} fill={'#365899'}/>
                        <p>Facebook</p>
                    </a>
                    <a href={`https://vk.com/share.php?url=${url}&title=${encodeURI('Allabay Мир Общей Победы')}&description=${desc}&noparse=true&image=${img}`}
                       target="_blank" className="item">
                        <Icon24LogoVkColor height={size} width={size}/>
                        <p>В Контакте</p>
                    </a>
                    <a href={`https://twitter.com/intent/tweet?url=${url}&text=${desc}`} target="_blank"
                       className="item">
                        <Icon24LogoTwitter height={size} width={size} fill={'#1d9bf0'}/>
                        <p>Twitter</p>
                    </a>
                </div>
                <div className="row">
                    <a href={`https://t.me/share/url?url=${encodeURI(' ')}&text=${desc + ' ' + url}`} target="_blank"
                       className="item">
                        <img className={config.isMobile ? 'smaller' : ''} src={tgLogo}/>
                        <p>Telegram</p>
                    </a>
                    <a href={`http://www.facebook.com/dialog/send?app_id=123456789&amp;link=${url}&amp;redirect_uri=${url}`}
                       className="item" target={'_blank'}>
                        <img className={config.isMobile ? 'smaller' : ''} src={messengerLogo}/>
                        <p>Messenger</p>
                    </a>
                    <div className="item" onClick={() => {
                        navigator.clipboard.writeText('Привет, я использую Allabay для поиска и передвижения. Присоединяйся! Держи ссылку: allabay.com')
                    }}>
                        <Icon24Copy width={size} height={size}/>
                        <p>Скопировать</p>
                    </div>
                </div>
                {config.isMobile &&
                <div className="row">
                    <div className="item" onClick={(ev) => {
                        if (navigator.share) {
                            navigator.share({
                                title: 'Allabay Мир Общей Победы',
                                text: 'Привет, я использую allabay для поиска и передвижения. Присоединяйся! Держи ссылку: allabay.com',
                                url: 'https://allabay.com',
                            }).catch(e => console.error('Navigator.share error'))
                        }
                    }
                    }>
                        <Icon24MoreHorizontal width={size} height={size}/>
                        <p>Еще</p>
                    </div>
                </div>}
            </div>
            {config.isMobile && <Button onClick={() => router.replaceModal(MODAL_CARDUSER)} mode={'secondary'}
                                        width={'full'}>Отмена</Button>}
        </div>
    );
});

export default Share;