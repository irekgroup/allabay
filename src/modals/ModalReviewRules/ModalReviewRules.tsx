import React, { FC, useEffect, useState } from 'react';
import { Link, withModalRootContext } from "@vkontakte/vkui";
import { useRouter } from "@happysanta/router";
import './ModalReviewRules.scss';

interface ModalReviewProps {
    updateModalHeight: () => void;
}

const ModalReviewRules:FC<ModalReviewProps> = ({updateModalHeight}) => {
    const [tab, setTab] = useState(0)
    const router = useRouter()
    useEffect(() => {
        updateModalHeight()
    }, [tab])
    return (
        <div className={'modalReviewRules'}>
            {tab === 0 &&
                <div className={'tab1'}>
                    <p onClick={() => setTab(1)}>Правила оформления отзывов</p>
                    <Link onClick={() => router.popPage()}>ЗАКРЫТЬ</Link>
                </div>}
            {tab === 1 &&
                <div className={'tab2'}>
                    <p className={'bold'} style={{fontWeight: 500, marginTop: 0}}>Правила оформления отзывов и вопросов</p>

                    <p>В отзывах и вопросах должна содержаться информация только о товаре.
                        Отзывы можно оставить только на заказанный и полученный покупателем товар.</p>
                    <p> К одному товару покупатель может оставить не более одного отзыва.
                        К отзывам можно прикрепить до 5 фотографий. Товар на фото должен быть хорошо виден.</p>
                    <p className={'bold'}> К публикации не допускаются следующие отзывы и вопросы:</p>
                    <ul className={'bold'}>
                        <li>Указывающие на покупку данного товара в других магазинах</li>
                        <li>Содержащие любые контактные данные (номера телефонов, адреса, email, ссылки на сторонние
                            сайты)
                        </li>
                        <li>С ненормативной лексикой, оскорбляющие достоинство других покупателей или магазина</li>
                        <li>С обилием символов в верхнем регистре (заглавных букв)</li>
                    </ul>

                    <p>Вопросы публикуются лишь после того, как на них получен ответ.</p>
                    <p>Мы оставляем за собой право редактировать или не публиковать отзыв и вопрос, который не
                        соответствует установленным правилам!</p>
                </div>}
        </div>
    );
};

export default withModalRootContext(ModalReviewRules);