import React, {useContext, useEffect, useState} from 'react';
import {
    Button,
    Div,
    ModalCard,
    ModalPage,
    ModalPageHeader,
    ModalRoot,
    SimpleCell,
    Placeholder,
    List,
    RichCell,
    ScreenSpinner,
    IconButton,
    useAdaptivity,
    ViewWidth,
} from '@vkontakte/vkui';
import UnauthorizedModal from './UnauthorizedModal/UnauthorizedModal';
// import ModalCardCreate from './ModalCardCreate/ModalCardCreate';

import HorizontalProductCards from '../components/HorizontalProductCards/HorizontalProductCards';
import {
    Icon28Document,
    Icon28NameTagOutline,
    Icon28AddOutline,
    Icon28DoneOutline,
} from '@vkontakte/icons';
// import ModalTemplateCreate from './ModalTemplateCreate/ModalTemplateCreate';

import './ModalMain.css';
// import ProfileAddress from './ProfileAddress/ProfileAddress.js';
import {useLocation, useRouter} from '@happysanta/router';
import {RootStoreContext} from '..';
import {observer} from 'mobx-react';
import {
    MODAL_CARDUSER,
    MODAL_CHAT_COMMANDS,
    MODAL_CREATE, MODAL_CREATE_CARD, MODAL_CREATE_CARD_CONFIRM,
    MODAL_FAST_RESPONSE,
    MODAL_PERSONAL_INFORMATION,
    MODAL_PRODUCT_CARD,
    MODAL_PROFILE_ADD_CAR,
    MODAL_PROFILE_ADDRESS,
    MODAL_PROFILE_ADDRESS_SELF, MODAL_REVIEW_REPORT, MODAL_REVIEW_RULES,
    MODAL_SHARE,
    MODAL_UNAUTHORIZED
} from '../router';
import Share from "./ShareModal/Share";
import CardUser from "../components/CardUser/CardUser";
import ModalCreate from "./ModalCreate/ModalCreate";
import ModalCommands from "./ModalCommands/ModalCommands";
import {helperModal} from "../mobX/Chat";
import ModalFastResponse from "./ModalFastResponse/ModalFastResponse";
import ModalProductCard from "./ModalProductCard/ModalProductCard";
import ModalProfileAddress from "./Profile/ModalProfileAddress/ModalProfileAddress";
import ModalProfileAddCar from "./Profile/ModalProfileAddCar/ModalProfileAddCar";
import ModalPersonalInformation from "./Profile/ModalPersonalInformation/ModalPersonalInformation";
import CreateCard from "../components/CreateCard/CreateCard";
import ModalCardCreateConfirm from "./ModalCardCreateConfirm/ModalCardCreateConfirm";
import ModalReviewReport from "./ModalReviewReport/ModalReviewReport";
import ModalReviewRules from "./ModalReviewRules/ModalReviewRules";

const Bang = ({isMobile, header}) => {
    return (
        <div className="container_header" align="center">
            {isMobile && <div className="modal_close"/>}
            {header}
        </div>
    );
};

const ModalMain = observer(() => {
    const router = useRouter();
    const location = useLocation();

    const rootStore = useContext(RootStoreContext);
    const {config, chat} = rootStore;

    const onClose = () => {
        console.log('ddd');
        router.pushModal(null);
    };

    // function sendCards() {
    //     console.log(globState.admin.cards)
    //     sendWithSocket({ type: "cards", cards: globState.admin.cards });
    // }

    // function deleteTemplate(template, i) {
    //     sendWithSocket({ type: "template", delete: { ...template }});
    //     setPopout(<ScreenSpinner />);

    // }

    // function edit(template) {
    //     setGlobalState({
    //         admin: {
    //             ...globState.admin,
    //             template
    //         }
    //     })
    //     setActiveModal("template_edit")
    // }

    const adaptivity = useAdaptivity();

    return (
        <ModalRoot activeModal={location.getModalId()}>
            {/* <ModalPage id="create_card" onClose={onClose} settlingHeight={100}  dynamicContentHeight header={
                <ModalPageHeader>
                    <Bang isMobile={isMobile} header="Карточка" />
                </ModalPageHeader>}>
                <ModalCardCreate   
                  globState={globState}
                  setPopout={setPopout}
                  setGlobalState={setGlobalState}
                  setActiveModal={setActiveModal}
                  sendWithSocket={sendWithSocket}
                />
            </ModalPage>

            <ModalCard id="menu_attach" onClose={onClose}>
                <SimpleCell before={<Icon28Document />}>Файл</SimpleCell>
                {globState.user.manager == "True" && <SimpleCell before={<Icon28NameTagOutline />} onClick={() => setActiveModal("create_card")}>Карточка</SimpleCell>}
            </ModalCard> */}

            {/* <ModalPage id="menu_hints_user" dynamicContentHeight onClose={onClose} header={<ModalPageHeader><Bang isMobile={isMobile} header="Команды" /></ModalPageHeader>}>
                <SimpleCell description="Добавьте VIN код ">VIN код</SimpleCell>

                <SimpleCell description="Добавьте адрес">Адрес</SimpleCell>
                <SimpleCell description="Вызов оператора">Помощь</SimpleCell>
                <SimpleCell description="Изучите справку, если вам нужна помощь">Справка</SimpleCell>

                {globState.user.templates && globState.user.templates.length === 0 ? <Placeholder action={<Button onClick={() => setActiveModal("template_edit")}>Добавить</Button>}>У вас пока нет шаблонов</Placeholder>:
                    <List>
                        {globState.user.templates && globState.user.templates.map((template, i) => (
                            <RichCell 
                                disabled
                                key={template.id} 
                                bottom={
                                    <Div style={{ display: "flex", paddingLeft: 0 }}><Button onClick={() => edit(template)} mode="tertiary">Редактировать</Button><Button style={{ marginLeft: 8 }} mode="tertiary" onClick={() => deleteTemplate(template, i)}>Удалить</Button></Div>
                                } 
                                multiline caption={template.description}
                                after={<IconButton 
                                            onClick={() =>{
                                                setGlobalState({ message: globState.message + template.description })
                                                onClose()
                                        }} >
                                            <Icon28DoneOutline />
                                    </IconButton>
                                }
                            >
                                {template.name}
                            </RichCell>
                            
                        ))}
                        <SimpleCell after={<Icon28AddOutline />} onClick={() => setActiveModal("template_edit")}>Добавить шаблон</SimpleCell>
                    </List>
                }
            </ModalPage> */}

            {/* <ModalPage id="menu_hints_manager" dynamicContentHeight onClose={onClose} header={<ModalPageHeader><Bang isMobile={isMobile} header="Шаблоны" /></ModalPageHeader>}>
                {globState.admin.templates && globState.admin.templates.length === 0 ? <Placeholder action={<Button onClick={() => setActiveModal("template_edit")}>Добавить</Button>}>У вас пока нет шаблонов</Placeholder>:
                    <List>
                        {globState.admin.templates && globState.admin.templates.map((template, i) => (
                            <RichCell 
                                disabled
                                key={template.id} 
                                bottom={
                                    <Div style={{ display: "flex", paddingLeft: 0 }}><Button onClick={() => edit(template)} mode="tertiary">Редактировать</Button><Button style={{ marginLeft: 8 }} mode="tertiary" onClick={() => deleteTemplate(template, i)}>Удалить</Button></Div>
                                } 
                                multiline caption={template.description}
                                after={<IconButton 
                                            onClick={() =>{
                                                setGlobalState({ message: globState.message + template.description })
                                                onClose()
                                        }} >
                                            <Icon28DoneOutline />
                                    </IconButton>
                                }
                            >
                                {template.name}
                            </RichCell>
                            
                        ))}
                        <SimpleCell after={<Icon28AddOutline />} onClick={() => setActiveModal("template_edit")}>Добавить шаблон</SimpleCell>
                    </List>
                }
            </ModalPage> */}

            {/* <ModalPage id="template_edit" header={<ModalPageHeader><Bang isMobile={isMobile} header="Добавить шаблон" /></ModalPageHeader>} dynamicContentHeight onClose={onClose}>
                <ModalTemplateCreate 
                    globState={globState}
                    setPopout={setPopout}
                    setGlobalState={setGlobalState}
                    setActiveModal={setActiveModal}
                    sendWithSocket={sendWithSocket}
                />
            </ModalPage>

            <ModalPage id="preview" dynamicContentHeight onClose={onClose} header={<ModalPageHeader><Bang isMobile={isMobile} header="Превью" /></ModalPageHeader>}>
                <Div>
                    <HorizontalProductCards cards={globState.admin.cards} />
                </Div>
                <Div>
                    <Button size="l" stretched="true"  onClick={() => setActiveModal("create_card")}>Редактировать</Button>
                </Div>
            </ModalPage> */}

            <ModalPage
                id={MODAL_UNAUTHORIZED}
                onClose={() => router.popPage()}
                header={
                    <ModalPageHeader>
                        <Bang isMobile={config.isMobile} header={config.dynamicModalHeader}/>
                    </ModalPageHeader>
                }>
                <UnauthorizedModal/>
            </ModalPage>

            <ModalPage
                id={MODAL_SHARE}
                onClose={() => router.popPage()}
                header={
                    <ModalPageHeader separator={false}
                                     style={{
                                         height: config.isMobile ? '90px' : '60px',
                                         textAlign: 'center',
                                         marginBottom: '10px'
                                     }}
                                     aria-multiline={true}>
                        <p style={{
                            whiteSpace: 'break-spaces',
                            paddingTop: config.isMobile ? '80px' : '50px',
                            fontSize: !config.isMobile ? '1rem' : 'auto'
                        }}>Привет, я использую Allabay для поиска и передвижения.<br/>
                            Присоединяйся!<br/>
                            Держи ссылку: allabay.com</p>
                        <Bang isMobile={config.isMobile} header={''}/>
                    </ModalPageHeader>
                }>
                <Share/>
            </ModalPage>

            <ModalPage id={MODAL_CARDUSER}
                       settlingHeight={adaptivity.viewHeight <= 2 ? 100 : 75}
                       onClose={() => router.popPage()}
                       style={!config.isMobile ? {
                           position: 'absolute',
                           top: 'calc(-50vh + 290px)',
                           left: 'calc(50vw - 235px)'
                       } : {}}
                       header={
                           <ModalPageHeader hidden={config.isMobile ? false : true} style={{height: '0'}}>
                               <Bang isMobile={config.isMobile}
                                     header={''}/>
                           </ModalPageHeader>
                       }>
                <CardUser/>
            </ModalPage>

            <ModalPage
                id={MODAL_CREATE}
                dynamicContentHeight={true}
                onClose={() => router.popPage()}
                header={
                    <ModalPageHeader hidden={config.isMobile ? false : true}>
                        <Bang isMobile={config.isMobile}
                              header={''}/>
                    </ModalPageHeader>
                }
            >
                <ModalCreate/>
            </ModalPage>

            <ModalPage
                id={MODAL_CHAT_COMMANDS}
                onClose={() => router.popPage()}
                header={
                    <ModalPageHeader>
                        <Bang isMobile={config.isMobile}
                              header={chat.helperModalType === helperModal.TEMPLATES ? 'Шаблоны' : 'Комманды'}/>
                    </ModalPageHeader>
                }
            >
                <ModalCommands/>
            </ModalPage>

            <ModalPage
                id={MODAL_FAST_RESPONSE}
                settlingHeight={adaptivity.viewHeight <= 2 ? 100 : 75}
                onClose={() => router.popPage()}
                dynamicContentHeight
                header={
                    <ModalPageHeader separator={false} style={!config.dynamicModalHeader ? {height: '25px'} : {}}>
                        <Bang isMobile={config.isMobile} header={config.dynamicModalHeader}/>
                    </ModalPageHeader>
                }>
                <ModalFastResponse/>
            </ModalPage>

            <ModalPage
                id={MODAL_PRODUCT_CARD}
                onClose={() => router.popPage()}
                header={
                    <ModalPageHeader>
                        <Bang isMobile={config.isMobile} header={'Карточка'}/>
                    </ModalPageHeader>
                }>
                <ModalProductCard/>
            </ModalPage>

            <ModalPage
                id={MODAL_PROFILE_ADDRESS}
                onClose={() => router.popPage()}
                settlingHeight={100}
                style={{overflow: 'auto'}}
                header={
                    <ModalPageHeader style={{height: '25px'}}>
                        <Bang isMobile={config.isMobile} header={''}/>
                    </ModalPageHeader>
                }>
                <ModalProfileAddress/>
            </ModalPage>

            <ModalPage
                id={MODAL_PROFILE_ADDRESS_SELF}
                onClose={() => router.popPage()}
                settlingHeight={100}
                style={{overflow: 'auto'}}
                header={
                    <ModalPageHeader style={{height: '25px'}}>
                        <Bang isMobile={config.isMobile} header={''}/>
                    </ModalPageHeader>
                }>
                <ModalProfileAddress self={true}/>
            </ModalPage>

            <ModalPage
                id={MODAL_PROFILE_ADD_CAR}
                onClose={() => router.popPage()}
                settlingHeight={adaptivity.viewHeight <= 2 ? 100 : 75}
                style={{overflow: 'auto'}}
                header={
                    <ModalPageHeader style={{height: '25px'}}>
                        <Bang isMobile={config.isMobile} header={''}/>
                    </ModalPageHeader>
                }>
                <ModalProfileAddCar/>
            </ModalPage>

            <ModalPage
                id={MODAL_PERSONAL_INFORMATION}
                onClose={() => router.popPage()}
                style={{overflow: 'auto'}}
                settlingHeight={adaptivity.viewHeight <= 2 ? 100 : 75}
                header={
                    <ModalPageHeader style={{height: '25px'}}>
                        <Bang isMobile={config.isMobile} header={''}/>
                    </ModalPageHeader>
                }>
                <ModalPersonalInformation/>
            </ModalPage>

            <ModalPage
                id={MODAL_CREATE_CARD}
                onClose={() => router.popPage()}
                settlingHeight={100}
                header={
                    <ModalPageHeader style={{height: '25px'}}>
                        <Bang isMobile={config.isMobile} header={''}/>
                    </ModalPageHeader>
                }>
                <CreateCard/>
            </ModalPage>

            <ModalPage
                id={MODAL_CREATE_CARD_CONFIRM}
                onClose={() => router.popPage()}
                header={
                    <ModalPageHeader style={{height: '25px'}}>
                        <Bang isMobile={config.isMobile} header={''}/>
                    </ModalPageHeader>
                }>
                <ModalCardCreateConfirm/>
            </ModalPage>

            <ModalPage
                id={MODAL_REVIEW_REPORT}
                dynamicContentHeight
                onClose={() => router.popPage()}
                header={
                    <ModalPageHeader style={{height: 25}}>
                        <Bang isMobile={config.isMobile} header={''}/>
                    </ModalPageHeader>
                }
            >
                <ModalReviewReport/>
            </ModalPage>

            <ModalPage
                id={MODAL_REVIEW_RULES}
                dynamicContentHeight
                onClose={() => router.popPage()}
                header={
                    <ModalPageHeader style={{height: 25}}>
                        <Bang isMobile={config.isMobile} header={''}/>
                    </ModalPageHeader>
                }
            >
                <ModalReviewRules/>
            </ModalPage>


            {/* <ModalPage id="profile-address" dynamicContentHeight onClose={onClose} header={


                <ModalPageHeader>
                    <Bang isMobile={isMobile} header={"Адрес"} />
                </ModalPageHeader>}
            >
                <ProfileAddress />
                </ModalPage>*/}
        </ModalRoot>
    );
});
export default ModalMain;
