import React, { FC, useContext, useEffect, useState } from 'react';
import { observer } from "mobx-react";
import { Gallery, withModalRootContext } from "@vkontakte/vkui";
import pluse from '../../img/createModal/pluse.svg';
import fire from '../../img/createModal/fire.svg';
import man from '../../img/createModal/man.svg';
import card from '../../img/createModal/card.svg';
import box from '../../img/createModal/box.svg';
import car from '../../img/createModal/car.svg';
import './ModalCreate.scss';
import { MODAL_UNAUTHORIZED, PAGE_CHAT_DESKTOP, PAGE_CHAT_MOBILE, PAGE_LOGIN } from "../../router";
import { useRouter } from "@happysanta/router";
import { RootStoreContext } from "../../index";

interface ModalCreateProps {
    updateModalHeight: () => void
}

const ModalCreate: FC<ModalCreateProps> = observer(({ updateModalHeight }) => {
    const [tab, setTab] = useState(0)
    const [type, setType] = useState(1)
    const router = useRouter()
    const { chat, config, user } = useContext(RootStoreContext)
    useEffect(() => {
        updateModalHeight()
    }, [tab])

    const authCheck = (fn: () => void) => {
        if (user.userData.id) {
            fn()
        } else {
            config.isMobile ? router.pushModal(MODAL_UNAUTHORIZED) : router.pushPage(PAGE_LOGIN)
        }
    }

    return (
        <Gallery className={'modal_create'}
                 style={!config.isMobile ? { paddingTop: '45px', height: '100%' } : { height: '100%' }}
                 slideWidth="100%" align="center" slideIndex={tab} onChange={setTab}>
            <div className={'gap firstTab'}>
                <div className={'modal_create-item'} onClick={() => {
                    setTab(1)
                    setType(1)
                }}>
                    <img src={pluse} alt="pluse"/>
                    <p>Я ищу...</p>
                </div>
                <div className={'modal_create-item'} onClick={() => {
                    setTab(1)
                    setType(2)
                }}>
                    <img src={fire} alt="fire"/>
                    <p>Я предлагаю...</p>
                </div>
            </div>
            {
                tab === 1 &&
                <div className={'gap secondTab'}>
                    <div className={'modal_create-item'}>
                        {!config.isMobile && (type === 1
                            ? <div className="title">Найти поездку по самой низкой цене</div>
                            : <div className="title">Опубликуйте маршрут легко и просто</div>)}
                        <img src={car} alt="pluse"/>
                        {!config.isMobile && (type === 1
                            ? <div className="desk">Забронируйте маршрут легко и просто</div>
                            : <div className="desk">Берите попутчиков и экономьте на бензине</div>)}
                        {config.isMobile && <p>Маршурт</p>}
                    </div>
                    <div className={'modal_create-item'} onClick={() => {
                        authCheck(() => {
                            chat.updateCurrentState("create")
                            if (!config.isMobile) {
                                router.pushPage(PAGE_CHAT_DESKTOP)
                            } else {
                                router.pushPage(PAGE_CHAT_MOBILE)
                            }
                        })

                    }}>
                        {!config.isMobile && (type === 1
                            ? <div className="title">Узнайте предложения из 19 категорий, стоимость, характеристики, сроки доставки в ваш город </div>
                            : <div className="title">Станьте часть сообщества Общей Победы делайте предложения, создайте уникальную Страницу</div>)}
                        <img src={card} alt="fire"/>
                        {!config.isMobile && (type === 1
                            ? <div className="desk">Быстрая доставка, быстрая реакция, задавайте вопросы, делитесь с друзями и многое другое</div>
                            : <div className="desk">Задавайте тренды, публикуйте новинки, отвечайте на вопросы, улыбайтесь.</div>)}
                        {config.isMobile && <p>Карточка</p>}
                    </div>
                    <div className={'modal_create-item'}>
                        {!config.isMobile && (type === 1
                            ? <div className="title">Экономьте с помощью долевых покупок покупайте только часть от оптовой партии</div>
                            : <div className="title">Создайте свою уникальную Страницу </div>)}
                        <img src={box} alt="pluse"/>
                        {!config.isMobile && (type === 1
                            ? <div className="desk">Выгодно преобретайте,проверяйте рейтинги организаторов, отзывы и реакции. участников что бы принять правильное решение</div>
                            : <div className="desk">Работайте по своим правилам, удобные инструменты </div>)}
                        {config.isMobile && <p>Лот</p>}
                    </div>
                    <div className={'modal_create-item'}>
                        {!config.isMobile && (type === 1
                            ? <div className="title">Найдите курьера, что бы отправить посылку или доставить груз</div>
                            : <div className="title">Добровольная доставка попутных грузов или от адреса до адреса, на авто, ж/д, авиа маршрутах, с выкупом или без </div>)}
                        <img src={man} alt="fire"/>
                        {!config.isMobile && (type === 1
                            ? <div className="desk">Добровольная доставка попутных грузов или от адреса до адреса, на авто, ж/д, авиа маршрутах, с выкупом или без </div>
                            : <div className="desk">Найдите заказ, что бы съекономить на передвижении</div>)}
                        {config.isMobile && <p>Доставка</p>}
                    </div>
                </div>
            }
        </Gallery>
    );
});

export default withModalRootContext(ModalCreate);