import React, { FC, useEffect, useRef } from 'react';
import './ModalProfileAddCar.scss';
import { Button, FormItem, FormLayout, FormLayoutGroup, Input, Link, Title } from "@vkontakte/vkui";
import Switcher from "../../../components/Switcher/Switcher";

const ModalProfileAddCar: FC = () => {
    const modal = useRef<HTMLDivElement>(null)
    useEffect(() => {
        function onMouseDown (e: TouchEvent) {
            e.stopPropagation()
        }
        if(modal.current){
            modal.current.addEventListener("touchend", onMouseDown, {capture: true})
            return () => {
                modal.current && modal.current.removeEventListener('touchend', onMouseDown)
            }
        }
    }, [])
    return (
        <div className={'modalProfileAddCar'} ref={modal}>
            <div className="header_card">
                <Title weight="medium" level="2">Добавить ТС</Title>
            </div>
            <FormLayout>
                <FormItem>
                    <Input placeholder={'VIN'}/>
                    <p className={'vin-desc'}>Найдите модель по VIN-номеру
                        VIN автомобиля является самым надежным
                        идентификатором</p>
                </FormItem>
            </FormLayout>
            <div className="header_card">
                <Title weight="medium" level="2">ХЕНДЭ ТУКСОН, 2019</Title>
            </div>
            <FormLayout>
                <FormItem>
                    <Input placeholder={'БЕЛЫЙ'}/>
                </FormItem>
                <FormLayoutGroup mode={'horizontal'}>
                    <FormItem>
                        <Input placeholder={'см³ 1995.0'}/>
                    </FormItem>
                    <FormItem>
                        <Input placeholder={'л.с.: 184.91'}/>
                    </FormItem>
                </FormLayoutGroup>
                <FormLayoutGroup mode={'horizontal'}>
                    <FormItem>
                        <Input placeholder={'Дата замены масла'}/>
                    </FormItem>
                    <FormItem>
                        <Input placeholder={'Пробег'}/>
                    </FormItem>
                </FormLayoutGroup>
                <FormItem>
                    <Link style={{textAlign: 'left'}}>+Добавить дату замены</Link>
                </FormItem>
            </FormLayout>
            <div className="row">
                <p>Задать как ТС по умолчанию</p>
                <Switcher />
            </div>
            <Button className={'saveBtn'} mode={'outline'}>Сохранить</Button>
        </div>
    );
};

export default ModalProfileAddCar;