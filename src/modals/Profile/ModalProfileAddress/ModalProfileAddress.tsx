import React, { FC, useEffect, useRef } from 'react';
import './ModalProfileAddress.scss';
import AddressChange from "../../../components/AddressChange/AddressChange";

interface ModalProfileAddressProps{
    self?: boolean,
}

const ModalProfileAddress:FC<ModalProfileAddressProps> = ({ self }) => {
    const modal = useRef<HTMLDivElement>(null)
    useEffect(() => {
        function onMouseDown (e: TouchEvent) {
            e.stopPropagation()
        }
        if(modal.current){
            modal.current.addEventListener("touchend", onMouseDown, {capture: true})
            return () => {
                modal.current && modal.current.removeEventListener('touchend', onMouseDown)
            }
        }
    }, [])
    return (
        <div className={'modalProfileAddress'}>
            <div className="wrap" ref={modal}>
                <AddressChange self={self}/>
            </div>
        </div>
    );
};

export default ModalProfileAddress;