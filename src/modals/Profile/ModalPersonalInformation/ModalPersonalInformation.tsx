import React, { FC, useEffect, useRef, useState } from 'react';
import { Button, FormItem, FormLayout, FormLayoutGroup, Input, Radio, RadioGroup, Title } from "@vkontakte/vkui";
import Switcher from "../../../components/Switcher/Switcher";
import './ModalPersonalInformation.scss';

const ModalPersonalInformation: FC = () => {
    const [sex, setSex] = useState(1)
    const modal = useRef<HTMLDivElement>(null)
    useEffect(() => {
        function onMouseDown (e: TouchEvent) {
            e.stopPropagation()
        }
        if(modal.current){
            modal.current.addEventListener("touchend", onMouseDown, {capture: true})
            return () => {
                modal.current && modal.current.removeEventListener('touchend', onMouseDown)
            }
        }
    }, [])
    return (
        <div className="modalPersonalInformation" ref={modal}>
            <div className="header_card">
                <Title weight="medium" level="2">Персональные данные</Title>
            </div>
            <FormLayout className="contentCardForm">
                <FormItem>
                    <Input placeholder="ФИО"/>
                </FormItem>

                <FormItem>
                    <Input placeholder="Номер телефона"/>
                </FormItem>

                <FormItem>
                    <Input placeholder="Почта"/>
                </FormItem>

                <FormLayoutGroup mode={"vertical"}>
                    <FormItem>
                        <Input type="date" placeholder={'Дата рождения'}/>
                    </FormItem>

                    <FormItem>
                        <RadioGroup mode={"horizontal"}>
                            Пол
                            <Radio name={'sex'} value={'men'} onChange={() => setSex(1)}>Мужской</Radio>
                            <Radio style={{ marginTop: 5 }} name={'sex'}
                                   value={'woman'} onChange={() => setSex(2)}>Женский</Radio>
                        </RadioGroup>
                    </FormItem>
                </FormLayoutGroup>

            </FormLayout>
            <div className="row">
                <p>Задать по умолчанию</p>
                <Switcher/>
            </div>
            <Button className={'saveBtn'} mode={'outline'}>Сохранить</Button>
        </div>
    );
};

export default ModalPersonalInformation;