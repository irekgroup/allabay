import React, { useContext } from 'react';
import { Link } from "@vkontakte/vkui";
import { useRouter } from "@happysanta/router";
import './ModalCardCreateConfirm.scss';
import { RootStoreContext } from "../../index";

const ModalCardCreateConfirm = () => {
    const router = useRouter()
    const {createCard} = useContext(RootStoreContext)
    return (
        <div className={'modalCardCreateConfirm'}>
            <h2 style={{ color: '#707070', fontSize: 18 }}>Вы дейсвительно хотите отправить
                карточку?</h2>
            <h3 style={{ color: '#707070', fontSize: 14 }}>Все данные заполнены верно?</h3>
            <div className="row">
                <Link style={{ color: '#1E7CF2' }} onClick={() => {
                    createCard.clear()
                }}>Да, отправить</Link>
                <Link style={{ color: '#E21B1B' }} onClick={() => router.popPage()}>Проверить данные</Link>
            </div>
        </div>
    );
};

export default ModalCardCreateConfirm;