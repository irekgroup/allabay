import React, { FC, useContext } from 'react';
import dots from '../../img/chat/dots.svg';
import './ModalCommands.scss';
import { RootStoreContext } from "../../index";
import { helperModal } from "../../mobX/Chat";
import { Icon36Add } from '@vkontakte/icons';
import { useRouter } from "@happysanta/router";

const ModalCommands: FC = () => {
    const { chat } = useContext(RootStoreContext)
    const router = useRouter()
    const data = chat.helperModalType === helperModal.COMMANDS ? [
        ['/startsearchroute', 'Поиск попутных маршрутов'],
        ['/startsearchcard', 'Поиск стоимости, наличие, сроков доставки и гарантии в Москву'],
        ['/startsearchlot', 'Поиск товаров для совместной покупки'],
        ['/startsearchpackage', 'Поиск исполнителя для перемещения посылки'],
        ['/closed', 'Завершить сеанс']
    ] : [
        ['VIN', 'Добавьте VIN код и покупайте в 1 клик'],
        ['Адрес', 'Добавьте адрес и покупайте в один клик\n'],
        ['Помощь', 'Вызов оператора'],
        ['Справка', 'Изучите справку, если вам нужна помощь'],
    ]

    return (
        <div className={'modalCommands'}>
            {data.map((item, i) => (
                <div className="command" key={i} onClick={() => {
                    chat.updateMessage(item[0])
                    router.popPage()
                }}>
                    <div>
                        <p>{item[0]}</p>
                        <p className={'desc'}>{item[1]}</p>
                    </div>
                    <img src={dots} alt="help"/>
                </div>
            ))}
            {chat.helperModalType === helperModal.TEMPLATES && (
                <div className={'template_add'}>
                    <p>У вас пока нет шаблонов</p>
                    <div className="plus"><Icon36Add fill={'#fff'}/></div>
                </div>
            )}
        </div>
    );
};

export default ModalCommands;