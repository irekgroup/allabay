import React, { FC } from 'react';
import './ModalProductCard.scss';
import { useRouter } from "@happysanta/router";
import { MODAL_FAST_RESPONSE } from "../../router";
import lightning from '../../img/feed/lightning.svg';
import cart from '../../img/feed/cartOutline.svg';
import back from '../../img/feed/back.svg';
import front from '../../img/feed/front.svg';
import { Icon24FavoriteOutline } from "@vkontakte/icons";

const ModalProductCard: FC = () => {
    const router = useRouter()
    return (
        <div className={'modalProductCard'}>
            <div className={'item'} onClick={() => router.replaceModal(MODAL_FAST_RESPONSE)}><img src={lightning} alt=""/>Быстрая реакция</div>
            <div className={'item'}><img src={cart} alt=""/>Добавить в корзину</div>
            <div className={'item'}><img src={back} alt=""/>Ответить</div>
            <div className={'item'}><img src={front} alt=""/>Поделиться</div>
            <div className={'item'}><Icon24FavoriteOutline/>Добавить в избранное</div>
        </div>
    );
};

export default ModalProductCard;