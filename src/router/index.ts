import { Page, Router } from "@happysanta/router";

//pages
export const PAGE_MAIN = "/";
export const PAGE_PROFILE = "/profile";
export const PAGE_CHAT_MOBILE = "/mob/chat/:id([0-9]+)?";
export const PAGE_CHAT_DESKTOP = "/chat/:id([0-9]+)?";
export const PAGE_LOGIN = "/login";
export const PAGE_CART_MOBILE = '/mob/cart';
export const PAGE_FEED_MOBILE = '/tape';
export const PAGE_FEED_DESKTOP = '/tape';
export const PAGE_PRODUCT = '/product/:id([0-9]+)?';

//views
export const VIEW_MAIN = "view_main";
export const VIEW_PROFILE = "view_profile";
export const VIEW_CHAT = "view_chat";
export const VIEW_AUTH = "view_auth";
export const VIEW_CART = "view_cart";
export const VIEW_FEED = "view_feed";
export const VIEW_PRODUCT = "view_product";

//panels
export const PANEL_MAIN = "panel_main";
export const PANEL_PROFILE = "panel_profile";
export const PANEL_DESKTOP_CHAT = "panel_chat-desktop";
export const PANEL_MOBILE_CHAT = "panel_chat-mobile";
export const PANEL_MOBILE_CHATS = "panel_chats-mobile";
export const PANEL_AUTH = "panel_auth";
export const PANEL_CART_MOBILE = 'panel_cart-mobile';
export const PANEL_FEED_MOBILE = "panel_feed-mobile";
export const PANEL_FEED_DESKTOP = "panel_feed-desktop";
export const PANEL_PRODUCT = "panel_product";

//modals 
export const MODAL_UNAUTHORIZED = "unauthorized";
export const MODAL_SHARE = "share"
export const MODAL_CARDUSER = "card-user-modal";
export const MODAL_CREATE = "modal-create";
export const MODAL_CHAT_COMMANDS = "chat-commands";
export const MODAL_FAST_RESPONSE = "fast-response";
export const MODAL_PRODUCT_CARD = "product-card";
export const MODAL_PROFILE_ADDRESS = "profile-address";
export const MODAL_PROFILE_ADDRESS_SELF = "profile-address_self";
export const MODAL_PROFILE_ADD_CAR = "profile-add_car";
export const MODAL_PERSONAL_INFORMATION = "profile-personal_info";
export const MODAL_CREATE_CARD = 'create-card';
export const MODAL_CREATE_CARD_CONFIRM = 'create-card-confirm';
export const MODAL_REVIEW_REPORT = 'review_report';
export const MODAL_REVIEW_RULES = 'review_rules';

//popouts
export const POPOUT_RECONNECT = "reconnect"; 
export const POPOUT_CARDUSER = "card-user";
export const POPOUT_CHAT_OPTIONS = "chat-options";
export const POPOUT_THANKS = 'thanks';
export const POPOUT_DELETE_ALERT = 'delete-alert';
export const POPOUT_CREATE_CARD_ALERT = 'create-alert';
export const POPOUT_CHAT_TYPES = 'chat-types';
export const POPOUT_CHAT_CONTEXT_MENU = 'chat-context';
export const POPOUT_MAX_PINNED_ALERT = 'max-pinned';
export const POPOUT_PRODUCT_ADDED = 'product-to-cart'

const routes = {
    PAGE_MAIN: new Page(PANEL_MAIN, VIEW_MAIN),
    [ PAGE_PROFILE ]: new Page(PANEL_PROFILE, VIEW_PROFILE),
    [ PAGE_CHAT_DESKTOP ]: new Page(PANEL_DESKTOP_CHAT, VIEW_CHAT),
    [ PAGE_CHAT_MOBILE ]: new Page(PANEL_MOBILE_CHAT, VIEW_CHAT),
    [ PAGE_LOGIN ]: new Page(PANEL_AUTH, VIEW_AUTH),
    [ PAGE_CART_MOBILE ]: new Page(PANEL_CART_MOBILE, VIEW_CART),
    [ PAGE_FEED_MOBILE ]: new Page(PANEL_FEED_MOBILE, VIEW_FEED),
    [ PAGE_FEED_DESKTOP ]: new Page(PANEL_FEED_DESKTOP, VIEW_FEED),
    [ PAGE_PRODUCT ]: new Page(PANEL_PRODUCT, VIEW_PRODUCT),
}

export const router = new Router(routes);
router.start();
