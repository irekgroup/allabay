/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

/**
 * An enumeration.
 */
export enum ChatMessageStatusChat {
  AG = "AG",
  DC = "DC",
  FC = "FC",
  LS = "LS",
  NG = "NG",
  PD = "PD",
  SU = "SU",
}

export interface ChatChangeInput {
  idChat: string;
  status?: string | null;
  isAvatar?: boolean | null;
}

export interface ChatCreateInput {
  messageAllabay?: boolean | null;
  userRecipient?: boolean | null;
  nameChat?: string | null;
}

export interface ChatPinnedInput {
  idChat?: string | null;
}

export interface MessageCreateInput {
  chatMessage?: string | null;
  textMessage?: string | null;
  imageMessage?: string | null;
  cardMessage?: string | null;
}

export interface MessageDeleteInput {
  idMessages?: any | null;
}

export interface SendTooltipInput {
  idChat?: string | null;
  idTooltip?: string | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
