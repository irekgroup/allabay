FROM node:17 as build

WORKDIR /app

COPY package.json /app

RUN npm install

COPY . /app

RUN npm run build

FROM nginx as server

COPY --from=build /app /app

COPY /settingsserver/default.conf /etc/nginx/conf.d/default.conf

COPY /settingsserver/proxy_params /etc/nginx/
