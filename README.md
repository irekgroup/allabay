[<img width="134" src="https://vk.com/images/apps/mini_apps/vk_mini_apps_logo.svg">](https://vk.com/services)

# Create VK Mini App [![npm][npm]][npm-url] [![deps][deps]][deps-url]

## How to install

### Create VK Mini App with gh-pages deploy

`npx @vkontakte/create-vk-mini-app <app-directory-name>`

### Create VK Mini App with Zeit deploy

Firstly, you have to create Zeit account and connect it with your GitHub profile — https://zeit.co/

`npx @vkontakte/create-vk-mini-app <app-directory-name> --zeit`

### Create VK Mini App with Surge deploy

Firstly, you have to create Surge account and Surge-domain — https://surge.sh/

`npx @vkontakte/create-vk-mini-app <app-directory-name> --surge <surge-domain>`

## How to start work with app

Go to created folder and run:
`yarn start` || `npm start` — this will start dev server with hot reload on `localhost:10888`.

`yarn run build` || `npm run build` — this will build production bundle, with tree-shaking, uglify and all this modern fancy stuff

[npm]: https://img.shields.io/npm/v/@vkontakte/create-vk-mini-app.svg
[npm-url]: https://npmjs.com/package/@vkontakte/create-vk-mini-app
[deps]: https://img.shields.io/david/vkcom/create-vk-mini-app.svg
[deps-url]: https://david-dm.org/vkcom/create-vk-mini-app

## GraphQL generate types 

Для генерации запросов и TypeScript типов используется GraphQL Code Generator. 
[Подробнее.](https://www.graphql-code-generator.com/docs/guides/react#apollo-and-urql "Ссылка на гайд.").


Запуск выполняется командой `npm run codegen`.

В файле codegen.yml находится настройка. [Подробнее.](https://www.graphql-code-generator.com/docs/config-reference/codegen-config "Ссылка на инструкцию.").

### Как использовать.

1) Создаете query, mutation, subscription в отдельном файле (.graphql) в папке src/graphql.
2) Запускаете скрипт.
3) Рядом с файлом появится сгенерированный файл **"you_name".generated.tsx** содержащий в себе хуки для использования, а также типы для TypeScript.